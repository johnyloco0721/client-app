import 'package:equatable/equatable.dart';

class RequestOtpStates extends Equatable {
  final String? mobileValue;
  final bool? enabled;
  final bool? isLoading;
  final bool? isError;

  RequestOtpStates({
    this.mobileValue,
    this.enabled = false,
    this.isLoading = false,
    this.isError = false,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class NextButtonEnabled extends RequestOtpStates {
  final bool enabled;

  NextButtonEnabled({this.enabled = false});

  @override
  List<Object> get props => [enabled];
}

class NextButtonClickState extends RequestOtpStates {
  final bool isLoading, isMobileFieldError;
  final String mobileFieldError;

  NextButtonClickState(
      {this.isLoading = false, this.isMobileFieldError = false, this.mobileFieldError = ""});

  @override
  List<Object?> get props => [isLoading, isMobileFieldError, mobileFieldError];
}

class OtpLoginStates extends Equatable {
  final String? otpValue;
  final String? ownerID;
  final bool? enabled;
  final bool? isLoading;
  final bool? isError;

  OtpLoginStates({
    this.otpValue,
    this.ownerID,
    this.enabled = false,
    this.isLoading = false,
    this.isError = false,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class OtpFieldEnabled extends OtpLoginStates {
  final bool enabled;

  OtpFieldEnabled({this.enabled = false});

  @override
  List<Object> get props => [enabled];
}

class ResendButtonClickState extends OtpLoginStates {
  final bool isLoading, isOtpFieldError;
  final String otpFieldError;

  ResendButtonClickState(
      {this.isLoading = false, this.isOtpFieldError = false, this.otpFieldError = ""});

  @override
  List<Object> get props => [isLoading, isOtpFieldError, otpFieldError];
}

class SendEmailButtonEnabled extends OtpLoginStates {
  final bool enabled;

  SendEmailButtonEnabled({this.enabled = false});

  @override
  List<Object> get props => [enabled];
}

class SendEmailButtonClickState extends OtpLoginStates {
  final bool isEmailLoading;

  SendEmailButtonClickState(
      {this.isEmailLoading = false});

  @override
  List<Object?> get props => [isLoading];
}

class OtpEmailLoginStates extends Equatable {
  final String? otpEmailValue;
  final bool? enabled;
  final bool? isLoading;
  final bool? isError;

  OtpEmailLoginStates({
    this.otpEmailValue,
    this.enabled = false,
    this.isLoading = false,
    this.isError = false,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class OtpEmailFieldEnabled extends OtpEmailLoginStates {
  final bool enabled;

  OtpEmailFieldEnabled({this.enabled = false});

  @override
  List<Object> get props => [enabled];
}

class EmailResendButtonClickState extends OtpEmailLoginStates {
  final bool isEmailLoading, isOtpEmailFieldError;
  final String otpEmailFieldError;

  EmailResendButtonClickState(
      {this.isEmailLoading = false, this.isOtpEmailFieldError = false, this.otpEmailFieldError = ""});

  @override
  List<Object> get props => [isEmailLoading, isOtpEmailFieldError, otpEmailFieldError];
}
