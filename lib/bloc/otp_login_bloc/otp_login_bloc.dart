import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/otp_login_bloc/otp_login_events.dart';
import 'package:pmaclient/bloc/otp_login_bloc/otp_login_states.dart';

class RequestOtpBloc extends Bloc<RequestOtpEvents, RequestOtpStates> {
  RequestOtpBloc() : super(RequestOtpStates()) {
    on<MobileFieldChange>(mobileFieldChange);
    on<NextButtonClickEvents>(nextButtonClick);
  }

  mobileFieldChange(MobileFieldChange event, Emitter<RequestOtpStates> emit) {
    bool enabled = event.enabled;
    if (event.mobileValue.isEmpty) {
      enabled = false;
    } else {
      enabled = true;
    }
    emit(NextButtonEnabled(enabled: enabled));
  }

  nextButtonClick(NextButtonClickEvents event, Emitter<RequestOtpStates> emit) {
    emit(NextButtonClickState(
      isLoading: event.isLoading,
      isMobileFieldError: event.isMobileFieldError,
      mobileFieldError: event.mobileFieldErrorMessage,
    ));
  }
}

class OtpLoginBloc extends Bloc<OtpLoginEvents, OtpLoginStates> {
  OtpLoginBloc(): super(OtpLoginStates()) {
    on<OtpFieldChanged>(otpFieldChange);
    on<ResendButtonClickEvents>(resendButtonClick);
    on<EmailFieldChange>(emailFieldChange);
    on<SendEmailButtonClickEvents>(sendEmailButtonClick);
  }

  otpFieldChange(OtpFieldChanged event, Emitter<OtpLoginStates> emit) {
    bool enabled = event.enabled;
    emit(OtpFieldEnabled(enabled: enabled));
  }

  resendButtonClick(ResendButtonClickEvents event, Emitter<OtpLoginStates> emit) {
    emit(ResendButtonClickState(
      isLoading: event.isLoading,
      isOtpFieldError: event.isOtpFieldError,
      otpFieldError: event.otpFieldErrorMessage,
    ));
  }

  emailFieldChange(EmailFieldChange event, Emitter<OtpLoginStates> emit) {
    bool enabled = event.enabled;
    if (event.ownerID.isEmpty) {
      enabled = false;
    } else {
      enabled = true;
    }
    emit(SendEmailButtonEnabled(enabled: enabled));
  }

  sendEmailButtonClick(SendEmailButtonClickEvents event, Emitter<OtpLoginStates> emit) {
    emit(SendEmailButtonClickState(
      isEmailLoading: event.isEmailLoading,
    ));
  }
}

class OtpEmailLoginBloc extends Bloc<OtpEmailLoginEvents, OtpEmailLoginStates> {
  OtpEmailLoginBloc(): super(OtpEmailLoginStates()) {
    on<OtpEmailFieldChanged>(otpEmailFieldChange);
    on<EmailResendButtonClickEvents>(resendEmailButtonClick);
  }

  otpEmailFieldChange(OtpEmailFieldChanged event, Emitter<OtpEmailLoginStates> emit) {
    bool enabled = event.enabled;
    emit(OtpEmailFieldEnabled(enabled: enabled));
  }

  resendEmailButtonClick(EmailResendButtonClickEvents event, Emitter<OtpEmailLoginStates> emit) {
    emit(EmailResendButtonClickState(
      isEmailLoading: event.isEmailLoading,
      isOtpEmailFieldError: event.isOtpEmailFieldError,
      otpEmailFieldError: event.otpEmailFieldErrorMessage,
    ));
  }
}