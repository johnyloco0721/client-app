import 'package:equatable/equatable.dart';

class RequestOtpEvents extends Equatable {
  RequestOtpEvents();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class MobileFieldChange extends RequestOtpEvents {
  final bool enabled;
  final String mobileValue;

  MobileFieldChange(this.enabled, this.mobileValue);

  @override
  // TODO: implement props
  List<Object?> get props => [enabled, mobileValue];
}

class NextButtonClickEvents extends RequestOtpEvents {
  final bool isLoading;
  final bool isMobileFieldError;
  final String mobileFieldErrorMessage;

  NextButtonClickEvents(this.isLoading, this.isMobileFieldError, this.mobileFieldErrorMessage);

  @override
  // TODO: implement props
  List<Object?> get props => [isLoading, isMobileFieldError, mobileFieldErrorMessage];
}

class OtpLoginEvents extends Equatable {
  OtpLoginEvents();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class OtpFieldChanged extends OtpLoginEvents {
  final bool enabled;

  OtpFieldChanged(this.enabled);

  @override
  // TODO: implement props
  List<Object?> get props => [enabled];
}

class ResendButtonClickEvents extends OtpLoginEvents {
  final bool isLoading;
  final bool isOtpFieldError;
  final String otpFieldErrorMessage;

  ResendButtonClickEvents(this.isLoading, this.isOtpFieldError, this.otpFieldErrorMessage);

  @override
  // TODO: implement props
  List<Object?> get props => [isLoading, isOtpFieldError, otpFieldErrorMessage];
}

class EmailFieldChange extends OtpLoginEvents {
  final bool enabled;
  final String ownerID;

  EmailFieldChange(this.enabled, this.ownerID);

  @override
  // TODO: implement props
  List<Object?> get props => [enabled, ownerID];
}

class SendEmailButtonClickEvents extends OtpLoginEvents {
  final bool isEmailLoading;

  SendEmailButtonClickEvents(this.isEmailLoading);

  @override
  // TODO: implement props
  List<Object?> get props => [isEmailLoading];
}

class OtpEmailLoginEvents extends Equatable {
  OtpEmailLoginEvents();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class OtpEmailFieldChanged extends OtpEmailLoginEvents {
  final bool enabled;

  OtpEmailFieldChanged(this.enabled);

  @override
  // TODO: implement props
  List<Object?> get props => [enabled];
}

class EmailResendButtonClickEvents extends OtpEmailLoginEvents {
  final bool isEmailLoading;
  final bool isOtpEmailFieldError;
  final String otpEmailFieldErrorMessage;

  EmailResendButtonClickEvents(this.isEmailLoading, this.isOtpEmailFieldError, this.otpEmailFieldErrorMessage);

  @override
  // TODO: implement props
  List<Object?> get props => [isEmailLoading, isOtpEmailFieldError, otpEmailFieldErrorMessage];
}