import 'package:equatable/equatable.dart';

class DownloadEvent extends Equatable {
  DownloadEvent();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class DownloadProgressEvent extends DownloadEvent {
  final bool isLoad;
  final bool isComplete;

  DownloadProgressEvent({this.isLoad = false, this.isComplete = false});

  @override
  // TODO: implement props
  List<Object?> get props => [isLoad, isComplete];
}
