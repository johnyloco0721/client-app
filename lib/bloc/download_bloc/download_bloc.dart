import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/download_bloc/download_events.dart';
import 'package:pmaclient/bloc/download_bloc/download_states.dart';

class DownloadBloc extends Bloc<DownloadEvent, DownloadStates> {
  DownloadBloc() : super(DownloadStates()) {
    on<DownloadProgressEvent>(downloadProgress);
  }

  downloadProgress(DownloadProgressEvent event, Emitter<DownloadStates> emit) {
    emit(DownloadCompleteStates(
      isLoading: event.isLoad,
      isCompleted: event.isComplete,
    ));
  }
}
