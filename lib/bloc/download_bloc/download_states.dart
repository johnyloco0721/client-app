import 'package:equatable/equatable.dart';

class DownloadStates extends Equatable {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class LoadingStates extends DownloadStates {}

class DownloadCompleteStates extends DownloadStates {
  final bool isLoading, isCompleted;

  DownloadCompleteStates({this.isLoading = false, this.isCompleted = false});

  @override
  List<Object?> get props => [isLoading, isCompleted];
}
