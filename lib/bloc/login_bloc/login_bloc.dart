import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/login_bloc/login_events.dart';
import 'package:pmaclient/bloc/login_bloc/login_states.dart';

class LoginBloc extends Bloc<LoginEvents, LoginStates> {
  LoginBloc() : super(LoginStates()) {
    on<TextFieldChange>(textFieldChange);
    on<LoginButtonClickEvents>(loginButtonClick);
  }

  textFieldChange(TextFieldChange event, Emitter<LoginStates> emit) {
    bool enabled = event.enabled;
    if (event.userValue.isEmpty || event.passwordValue.isEmpty) {
      enabled = false;
    } else {
      enabled = true;
    }
    emit(LoginEnabled(enabled: enabled));
  }

  loginButtonClick(LoginButtonClickEvents event, Emitter<LoginStates> emit) {
    emit(LoginButtonClickState(
      isLoading: event.isLoading,
      isTextFieldError: event.isTextFieldError,
    ));
  }
}
