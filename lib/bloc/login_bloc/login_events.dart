import 'package:equatable/equatable.dart';

class LoginEvents extends Equatable {
  LoginEvents();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class TextFieldChange extends LoginEvents {
  final bool enabled;
  final String userValue;
  final String passwordValue;

  TextFieldChange(this.enabled, this.passwordValue, this.userValue);

  @override
  // TODO: implement props
  List<Object?> get props => [enabled, passwordValue, userValue];
}

class LoginButtonClickEvents extends LoginEvents {
  final bool isLoading;
  final bool isTextFieldError;

  LoginButtonClickEvents(this.isLoading, this.isTextFieldError);

  @override
  // TODO: implement props
  List<Object?> get props => [isLoading, isTextFieldError];
}
