import 'package:equatable/equatable.dart';

class LoginStates extends Equatable {
  final String? userValue;
  final String? passwordValue;
  final bool? enabled;
  final bool? isLoading;
  final bool? isError;

  LoginStates({
    this.userValue,
    this.passwordValue,
    this.enabled = false,
    this.isLoading = false,
    this.isError = false,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class LoginEnabled extends LoginStates {
  final bool enabled;

  LoginEnabled({this.enabled = false});

  @override
  List<Object> get props => [enabled];
}

class LoginButtonClickState extends LoginStates {
  final bool isLoading, isTextFieldError;

  LoginButtonClickState(
      {this.isLoading = false, this.isTextFieldError = false});

  @override
  List<Object> get props => [isLoading, isTextFieldError];
}
