import 'dart:io';

import 'package:equatable/equatable.dart';

class ApiEvent extends Equatable {
  ApiEvent();

  @override
  List<Object?> get props => [];
}

class ConfigApiEvent extends ApiEvent {
  ConfigApiEvent();

  @override
  List<Object?> get props => [];
}

class ProjectsApiEvent extends ApiEvent {
  final int pageNo;
  final bool isLoadMore;

  ProjectsApiEvent({required this.pageNo, this.isLoadMore = false});

  @override
  List<Object?> get props => [pageNo, isLoadMore];
}

class NotificationsApiEvent extends ApiEvent {
  final int pageNo;
  final bool isLoadMore;

  NotificationsApiEvent({required this.pageNo, this.isLoadMore = false});

  @override
  List<Object?> get props => [pageNo, isLoadMore];
}

class ProfilePhotoApiEvent extends ApiEvent {
  final File image;

  ProfilePhotoApiEvent({required this.image});

  @override
  List<Object?> get props => [image];
}

class ProjectDetailsApiEvent extends ApiEvent {
  final int projectId;

  ProjectDetailsApiEvent({required this.projectId});

  @override
  List<Object?> get props => [projectId];
}

class DownloadLoaderEvent extends ApiEvent {
  final bool isLoad;

  DownloadLoaderEvent({required this.isLoad});

  @override
  List<Object?> get props => [isLoad];
}

class UpdateNotificationsApiEvent extends ApiEvent {
  final int notificationsId, index;

  UpdateNotificationsApiEvent({required this.notificationsId, this.index = -1});

  @override
  List<Object?> get props => [notificationsId, index];
}

class LoginApiEvent extends ApiEvent {
  final String userName;
  final String password;
  final String token;

  LoginApiEvent({
    required this.password,
    required this.token,
    required this.userName,
  });

  @override
  List<Object?> get props => [userName, token, password];
}

class MaintenanceByPassApiEvent extends ApiEvent {
  final String password;

  MaintenanceByPassApiEvent({
    required this.password,
  });

  @override
  List<Object?> get props => [password];
}

class RequestOtpApiEvent extends ApiEvent {
  final String contactNumber;

  RequestOtpApiEvent({
    required this.contactNumber,
  });

  @override
  List<Object?> get props => [contactNumber];
}

class RequestOtpEmailApiEvent extends ApiEvent {
  final String ownerID;

  RequestOtpEmailApiEvent({
    required this.ownerID,
  });

  @override
  List<Object?> get props => [ownerID];
}

class OtpLoginApiEvent extends ApiEvent {
  final String ownerID;
  final String otpCode;
  final String token;
  final String tokenGenerateBy;

  OtpLoginApiEvent({
    required this.ownerID,
    required this.otpCode,
    required this.token,
    required this.tokenGenerateBy,
  });

  @override
  List<Object?> get props => [ownerID, otpCode, token, tokenGenerateBy];
}

class ForgotPasswordApiEvent extends ApiEvent {
  final String userName;

  ForgotPasswordApiEvent({required this.userName});

  @override
  List<Object?> get props => [userName];
}

class SendReviewEvent extends ApiEvent {
  final String body;

  SendReviewEvent({
    required this.body,
  });

  @override
  List<Object?> get props => [body];
}

class UpdatePasswordAPiApiEvent extends ApiEvent {
  final String currentPassword;
  final String password;
  final String confirmPassword;

  UpdatePasswordAPiApiEvent({
    required this.password,
    required this.confirmPassword,
    this.currentPassword = "",
  });

  @override
  List<Object?> get props => [password, confirmPassword, currentPassword];
}

class ProfileApiEvent extends ApiEvent {
  ProfileApiEvent();

  @override
  List<Object?> get props => [];
}

class LogoutApiEvent extends ApiEvent {}

class TermsApiEvent extends ApiEvent {}

class ReadAllNotificationsApiEvent extends ApiEvent {}
