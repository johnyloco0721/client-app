import 'package:equatable/equatable.dart';
import 'package:pmaclient/constants/app_constants.dart';
import 'package:pmaclient/models/config_model.dart';
import 'package:pmaclient/models/feedback_model.dart';
import 'package:pmaclient/models/forgot_password_model.dart';
import 'package:pmaclient/models/maintenance_model.dart';
import 'package:pmaclient/models/notification_api_model.dart';
import 'package:pmaclient/models/otp_model.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:pmaclient/models/projects_model.dart';
import 'package:pmaclient/models/user_model.dart';

class ApiStates extends Equatable {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class ApiLoadingState extends ApiStates {}

class RequestOtpApiLoadingState extends ApiStates {}

class RequestOtpEmailApiLoadingState extends ApiStates {}

class OtpLoginApiLoadingState extends ApiStates {}

class ReviewApiLoadingState extends ApiStates {}

class ProjectApiLoadingState extends ApiStates {}

class NotificationLoadingState extends ApiStates {}

class EmptyState extends ApiStates {}

class LogoutApiLoadingState extends ApiStates {
  final bool isEnabled;

  LogoutApiLoadingState({this.isEnabled = false});

  @override
  List<Object?> get props => [isEnabled];
}

class SingleLoaderState extends ApiStates {
  final bool isEnabled;

  SingleLoaderState({this.isEnabled = false});

  @override
  List<Object?> get props => [isEnabled];
}

class LoadMoreLoadingState extends ApiStates {
  final bool isEnabled;

  LoadMoreLoadingState({this.isEnabled = false});

  @override
  List<Object?> get props => [isEnabled];
}

class DownloadLoaderStates extends ApiStates {
  final bool isLoad;

  DownloadLoaderStates({this.isLoad = false});

  @override
  List<Object?> get props => [isLoad];
}

class ConfigApiDataLoadState extends ApiStates {
  final ConfigModel? configModel;

  ConfigApiDataLoadState({this.configModel});

  @override
  List<Object?> get props => [configModel];
}

class ApiErrorState extends ApiStates {
  final String? errorMessage;
  final bool isLoadMore;

  ApiErrorState({this.errorMessage, this.isLoadMore = false});

  @override
  List<Object?> get props => [errorMessage];
}

class RequestOtpApiErrorState extends ApiStates {
  final String? errorMessage;
  final bool isLoadMore;

  RequestOtpApiErrorState({this.errorMessage, this.isLoadMore = false});

  @override
  List<Object?> get props => [errorMessage];
}

class RequestOtpEmailApiErrorState extends ApiStates {
  final String? errorMessage;
  final bool isLoadMore;

  RequestOtpEmailApiErrorState({this.errorMessage, this.isLoadMore = false});

  @override
  List<Object?> get props => [errorMessage];
}

class OtpLoginApiErrorState extends ApiStates {
  final String? errorMessage;
  final bool isLoadMore;

  OtpLoginApiErrorState({this.errorMessage, this.isLoadMore = false});

  @override
  List<Object?> get props => [errorMessage];
}

class ProfileApiErrorState extends ApiStates {
  final String? errorMessage;
  final bool isLoadMore;

  ProfileApiErrorState({this.errorMessage, this.isLoadMore = false});

  @override
  List<Object?> get props => [errorMessage];
}

class ReviewApiErrorState extends ApiStates {
  final String? errorMessage;
  final bool isLoadMore;

  ReviewApiErrorState({this.errorMessage, this.isLoadMore = false});

  @override
  List<Object?> get props => [errorMessage];
}

class LoginApiDataLoadState extends ApiStates {
  final UserModel? model;

  LoginApiDataLoadState({this.model});

  @override
  List<Object?> get props => [model];
}

class MaintenanceByPassApiDataLoadState extends ApiStates {
  final MaintenanceModel? model;

  MaintenanceByPassApiDataLoadState({this.model});

  @override
  List<Object?> get props => [model];
}

class MaintenanceByPassApiErrorState extends ApiStates {
  final String? errorMessage;
  final bool isLoadMore;

  MaintenanceByPassApiErrorState({this.errorMessage, this.isLoadMore = false});

  @override
  List<Object?> get props => [errorMessage];
}

class RequestOtpApiDataLoadState extends ApiStates {
  final OtpModel? model;

  RequestOtpApiDataLoadState({this.model});

  @override
  List<Object?> get props => [model];
}

class RequestOtpEmailApiDataLoadState extends ApiStates {
  final EmailOtpModel? model;

  RequestOtpEmailApiDataLoadState({this.model});

  @override
  List<Object?> get props => [model];
}

class OtpLoginApiDataLoadState extends ApiStates {
  final UserModel? model;

  OtpLoginApiDataLoadState({this.model});

  @override
  List<Object?> get props => [model];
}

class ForgotPasswordApiDataLoadState extends ApiStates {
  final ForgotPasswordModel? model;

  ForgotPasswordApiDataLoadState({this.model});

  @override
  List<Object?> get props => [model];
}

class UpdatePasswordAPiLoadData extends ApiStates {}

class SendReviewApiLoadState extends ApiStates {
  final FeedBackResponseData? model;

  SendReviewApiLoadState({this.model});

  @override
  List<Object?> get props => [model];
}

class ProfileApiDataLoadState extends ApiStates {
  final UserModel? model;

  ProfileApiDataLoadState({this.model});

  @override
  List<Object?> get props => [model];
}

class ProfilePhotoApiDataLoadState extends ApiStates {
  final String? photoUrl;

  ProfilePhotoApiDataLoadState({this.photoUrl});

  @override
  List<Object?> get props => [photoUrl];
}

class LogoutApiDataLoadState extends ApiStates {}

class ProjectsApiDataLoadState extends ApiStates {
  final ProjectModel? model;
  final bool isLoadMore;

  ProjectsApiDataLoadState({this.model, this.isLoadMore = false});

  @override
  List<Object?> get props => [model, isLoadMore];
}

class ProjectDetailsApiDataLoadState extends ApiStates {
  final ProjectDetailsModel? model;

  ProjectDetailsApiDataLoadState({this.model});

  @override
  List<Object?> get props => [model];
}

class NotificationsApiDataLoadState extends ApiStates {
  final NotificationApiModel? model;
  final bool isLoadMore;

  NotificationsApiDataLoadState({this.model, this.isLoadMore = false});

  @override
  List<Object?> get props => [model, isLoadMore];
}

class UpdateNotificationsApiDataLoadState extends ApiStates {
  final int? isRead, index, projectID;

  UpdateNotificationsApiDataLoadState({
    this.isRead,
    this.index = -1,
    this.projectID,
  });

  @override
  List<Object?> get props => [isRead, index, projectID];
}

class ReadAllNotificationsApiDataLoadState extends ApiStates {
  final String? message;

  ReadAllNotificationsApiDataLoadState({this.message});

  @override
  List<Object?> get props => [message];
}

class TermsApiDataLoadState extends ApiStates {
  final String url;

  TermsApiDataLoadState({this.url = termsOfServicesUrl});

  @override
  List<Object?> get props => [url];
}
