import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/api_services/api_repository.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/models/config_model.dart';
import 'package:pmaclient/models/feedback_model.dart';
import 'package:pmaclient/models/forgot_password_model.dart';
import 'package:pmaclient/models/notification_api_model.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:pmaclient/models/projects_model.dart';
import 'package:pmaclient/models/user_model.dart';

class ApiBloc extends Bloc<ApiEvent, ApiStates> {
  ApiBloc() : super(ApiStates()) {
    on<ApiEvent>(onEventsChange);
  }

  onEventsChange(ApiEvent event, Emitter<ApiStates> emit) {
    if (event is ConfigApiEvent) return configApi(event, emit);
    if (event is LoginApiEvent) return loginApi(event, emit);
    if (event is MaintenanceByPassApiEvent) return maintenanceApi(event, emit);
    if (event is RequestOtpApiEvent) return requestOtpApi(event, emit);
    if (event is RequestOtpEmailApiEvent) return requestOtpEmailApi(event, emit);
    if (event is OtpLoginApiEvent) return otpLoginApi(event, emit);
    if (event is ForgotPasswordApiEvent) return forgotPasswordApi(event, emit);
    if (event is UpdatePasswordAPiApiEvent)
      return updatePasswordApi(event, emit);
    if (event is ProfileApiEvent) return profileApi(event, emit);
    if (event is LogoutApiEvent) return logoutApi(event, emit);
    if (event is ProjectsApiEvent) return projectsApi(event, emit);
    if (event is NotificationsApiEvent) return notificationApi(event, emit);
    if (event is ReadAllNotificationsApiEvent)
      return readAllNotificationApi(event, emit);
    if (event is UpdateNotificationsApiEvent)
      return updateNotificationApi(event, emit);
    if (event is TermsApiEvent) return termsApi(event, emit);
    if (event is ProjectDetailsApiEvent) return projectDetailsApi(event, emit);
    if (event is ProfilePhotoApiEvent) return profilePhotoApi(event, emit);
    if (event is SendReviewEvent) return sendReviewApi(event, emit);
    if (event is DownloadLoaderEvent) return downloader(event, emit);
  }

  configApi(ConfigApiEvent event, Emitter<ApiStates> emit) async {
    try {
      ConfigModel model = await ApiRepository.getConfigApiData();
      emit(ConfigApiDataLoadState(configModel: model));
    } catch (e) {
      emit(ApiErrorState(errorMessage: e.toString()));
    }
  }

  loginApi(LoginApiEvent event, Emitter<ApiStates> emit) async {
    emit(ApiLoadingState());
    try {
      UserModel model = await ApiRepository.postLoginData(
        password: event.password,
        userName: event.userName,
        token: event.token,
      );
      emit(LoginApiDataLoadState(model: model));
    } catch (e) {
      emit(ApiErrorState(errorMessage: e.toString()));
    }
  }

  maintenanceApi(MaintenanceByPassApiEvent event, Emitter<ApiStates> emit) async {
    emit(ApiLoadingState());
    try {
      var model = await ApiRepository.postMaintenanceByPass(
        password: event.password,
      );
      emit(MaintenanceByPassApiDataLoadState(model: model));
    } catch (e) {
      emit(MaintenanceByPassApiErrorState(errorMessage: e.toString()));
    }
  }

  requestOtpApi(RequestOtpApiEvent event, Emitter<ApiStates> emit) async {
    emit(RequestOtpApiLoadingState());
    try {
      var model = await ApiRepository.postRequestOtpData(
        contactNumber: event.contactNumber,
      );
      emit(RequestOtpApiDataLoadState(model: model));
    } catch (e) {
      emit(RequestOtpApiErrorState(errorMessage: e.toString()));
    }
  }

  requestOtpEmailApi(RequestOtpEmailApiEvent event, Emitter<ApiStates> emit) async {
    emit(RequestOtpEmailApiLoadingState());
    try {
      var model = await ApiRepository.postRequestOtpEmailData(
        ownerID: event.ownerID,
      );
      emit(RequestOtpEmailApiDataLoadState(model: model));
    } catch (e) {
      emit(RequestOtpEmailApiErrorState(errorMessage: e.toString()));
    }
  }

  otpLoginApi(OtpLoginApiEvent event, Emitter<ApiStates> emit) async {
    emit(OtpLoginApiLoadingState());
    try {
      UserModel model = await ApiRepository.postOtpLoginData(
        ownerID: event.ownerID,
        otpCode: event.otpCode,
        token: event.token,
        tokenGenerateBy: event.tokenGenerateBy,
      );
      emit(OtpLoginApiDataLoadState(model: model));
    } catch (e) {
      emit(OtpLoginApiErrorState(errorMessage: e.toString()));
    }
  }

  forgotPasswordApi(
      ForgotPasswordApiEvent event, Emitter<ApiStates> emit) async {
    emit(ApiLoadingState());
    try {
      ForgotPasswordModel model = await ApiRepository.postResetPasswordRequest(
        userName: event.userName,
      );
      emit(ForgotPasswordApiDataLoadState(model: model));
    } catch (e) {
      emit(ApiErrorState(errorMessage: e.toString()));
    }
  }

  updatePasswordApi(
      UpdatePasswordAPiApiEvent event, Emitter<ApiStates> emit) async {
    emit(ApiLoadingState());
    try {
      await ApiRepository.sendPasswordUpdate(
        currentPassword: event.currentPassword,
        password: event.password,
        confirmPassword: event.confirmPassword,
      );
      emit(UpdatePasswordAPiLoadData());
    } catch (e) {
      if (e.toString() == 'Token mismatched.') {
        emit(LogoutApiLoadingState(isEnabled: false));
        emit(LogoutApiDataLoadState());
      }
      emit(ApiErrorState(errorMessage: e.toString()));
    }
  }

  profileApi(ProfileApiEvent event, Emitter<ApiStates> emit) async {
    emit(ApiLoadingState());
    try {
      UserModel model = await ApiRepository.getProfileData();
      emit(ProfileApiDataLoadState(model: model));
    } catch (e) {
      if (e.toString() == 'Token mismatched.') {
        emit(LogoutApiLoadingState(isEnabled: false));
        emit(LogoutApiDataLoadState());
      }
      emit(ProfileApiErrorState(errorMessage: e.toString()));
    }
  }

  logoutApi(LogoutApiEvent event, Emitter<ApiStates> emit) async {
    emit(LogoutApiLoadingState(isEnabled: true));
    try {
      await ApiRepository.logoutUser();
      emit(LogoutApiLoadingState(isEnabled: false));
      emit(LogoutApiDataLoadState());
    } catch (e) {
      emit(LogoutApiLoadingState(isEnabled: false));
      emit(ApiErrorState(errorMessage: e.toString()));
    }
  }

  projectsApi(ProjectsApiEvent event, Emitter<ApiStates> emit) async {
    emit(event.isLoadMore
        ? LoadMoreLoadingState(isEnabled: true)
        : ProjectApiLoadingState());
    try {
      ProjectModel model = await ApiRepository.getProjects(event.pageNo);
      emit(
          ProjectsApiDataLoadState(model: model, isLoadMore: event.isLoadMore));
      emit(LoadMoreLoadingState(isEnabled: false));
    } catch (e) {
      if (e.toString() == 'Token mismatched.') {
        emit(LogoutApiLoadingState(isEnabled: false));
        emit(LogoutApiDataLoadState());
      }
      emit(ApiErrorState(
        errorMessage: e.toString(),
        isLoadMore: event.isLoadMore,
      ));
      emit(LoadMoreLoadingState(isEnabled: false));
    }
  }

  notificationApi(NotificationsApiEvent event, Emitter<ApiStates> emit) async {
    emit(event.isLoadMore
        ? LoadMoreLoadingState(isEnabled: true)
        : NotificationLoadingState());
    try {
      NotificationApiModel model =
          await ApiRepository.getNotifications(event.pageNo);
      emit(NotificationsApiDataLoadState(
          model: model, isLoadMore: event.isLoadMore));
      emit(LoadMoreLoadingState(isEnabled: false));
    } catch (e) {
      if (e.toString() == 'Token mismatched.') {
        emit(LogoutApiLoadingState(isEnabled: false));
        emit(LogoutApiDataLoadState());
      }
      emit(ApiErrorState(
        errorMessage: e.toString(),
        isLoadMore: event.isLoadMore,
      ));
      emit(LoadMoreLoadingState(isEnabled: false));
    }
  }

  readAllNotificationApi(
      ReadAllNotificationsApiEvent event, Emitter<ApiStates> emit) async {
    emit(SingleLoaderState());
    try {
      String message = await ApiRepository.readAllNotification();
      emit(ReadAllNotificationsApiDataLoadState(message: message));
    } catch (e) {
      if (e.toString() == 'Token mismatched.') {
        emit(LogoutApiLoadingState(isEnabled: false));
        emit(LogoutApiDataLoadState());
      }
      emit(ApiErrorState(errorMessage: e.toString()));
    }
  }

  updateNotificationApi(
      UpdateNotificationsApiEvent event, Emitter<ApiStates> emit) async {
    emit(SingleLoaderState());
    try {
      NotificationDataList model =
          await ApiRepository.updateNotificationStatus(event.notificationsId);
      emit(UpdateNotificationsApiDataLoadState(
          isRead: model.isRead,
          index: event.index,
          projectID: model.projectID));
    } catch (e) {
      if (e.toString() == 'Token mismatched.') {
        emit(LogoutApiLoadingState(isEnabled: false));
        emit(LogoutApiDataLoadState());
      }
      emit(ApiErrorState(errorMessage: e.toString()));
    }
  }

  termsApi(TermsApiEvent event, Emitter<ApiStates> emit) async {
    emit(ApiLoadingState());
    try {
      String url = await ApiRepository.getTermsAndPrivacy();
      emit(TermsApiDataLoadState(url: url));
    } catch (e) {
      if (e.toString() == 'Token mismatched.') {
        emit(LogoutApiLoadingState(isEnabled: false));
        emit(LogoutApiDataLoadState());
      }
      emit(ApiErrorState(errorMessage: e.toString()));
    }
  }

  projectDetailsApi(
      ProjectDetailsApiEvent event, Emitter<ApiStates> emit) async {
    emit(ApiLoadingState());
    try {
      ProjectDetailsModel model = await ApiRepository.getProjectDetails(
        event.projectId,
      );
      emit(ProjectDetailsApiDataLoadState(model: model));
    } catch (e) {
      if (e.toString() == 'Token mismatched.') {
        emit(LogoutApiLoadingState(isEnabled: false));
        emit(LogoutApiDataLoadState());
      }
      emit(ApiErrorState(errorMessage: e.toString()));
    }
  }

  profilePhotoApi(ProfilePhotoApiEvent event, Emitter<ApiStates> emit) async {
    emit(LogoutApiLoadingState(isEnabled: true));
    try {
      String image = await ApiRepository.updateProfilePhoto(event.image);
      emit(ProfilePhotoApiDataLoadState(photoUrl: image));
      emit(LogoutApiLoadingState(isEnabled: false));
    } catch (e) {
      emit(ApiErrorState(errorMessage: e.toString()));
      emit(LogoutApiLoadingState(isEnabled: false));
    }
  }

  sendReviewApi(SendReviewEvent event, Emitter<ApiStates> emit) async {
    emit(ReviewApiLoadingState());
    try {
      FeedBackResponseData model =
          await ApiRepository.sendReviewPost(event.body);
      emit(SendReviewApiLoadState(model: model));
    } catch (e) {
      if (e.toString() == 'Token mismatched.') {
        emit(LogoutApiLoadingState(isEnabled: false));
        emit(LogoutApiDataLoadState());
      }
      emit(ReviewApiErrorState(errorMessage: e.toString()));
    }
  }

  downloader(DownloadLoaderEvent event, Emitter<ApiStates> emit) {}
}
