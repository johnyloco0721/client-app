import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_events.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_states.dart';

class ProfileBloc extends Bloc<ProfileEvents, ProfileStates> {
  ProfileBloc() : super(ProfileStates(image: "")) {
    on<ChangeProfileImageEvent>(changeProfileImage);
    on<UserDataChangeEvent>(userDataChange);
  }

  changeProfileImage(
      ChangeProfileImageEvent event, Emitter<ProfileStates> emit) {
    emit(ProfileImageSetState(image: event.image));
  }

  userDataChange(UserDataChangeEvent event, Emitter<ProfileStates> emit) {
    emit(UserDataState(model: event.model));
  }
}
