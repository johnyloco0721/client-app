import 'package:equatable/equatable.dart';
import 'package:pmaclient/models/user_model.dart';

class ProfileStates extends Equatable {
  final String? image;
  final UserModel? model;

  ProfileStates({this.image, this.model});

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class ProfileImageSetState extends ProfileStates {
  final String image;

  ProfileImageSetState({required this.image}) : super(image: image);

  @override
  // TODO: implement props
  List<Object?> get props => [image];
}

class UserDataState extends ProfileStates {
  final UserModel? model;

  UserDataState({this.model});

  @override
  // TODO: implement props
  List<Object?> get props => [model];
}
