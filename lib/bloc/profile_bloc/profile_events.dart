import 'package:equatable/equatable.dart';
import 'package:pmaclient/models/user_model.dart';

class ProfileEvents extends Equatable {
  ProfileEvents();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class ChangeProfileImageEvent extends ProfileEvents {
  final String image;

  ChangeProfileImageEvent(this.image);

  @override
  List<Object?> get props => [image];
}

class UserDataChangeEvent extends ProfileEvents {
  final UserModel model;

  UserDataChangeEvent(this.model);

  @override
  List<Object?> get props => [model];
}
