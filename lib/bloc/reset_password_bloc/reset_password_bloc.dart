import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/reset_password_bloc/reset_password_events.dart';
import 'package:pmaclient/bloc/reset_password_bloc/reset_password_states.dart';

class ResetPasswordBloc extends Bloc<ResetPasswordEvents, ResetPasswordStates> {
  ResetPasswordBloc() : super(ResetPasswordStates()) {
    on<UserValueChangeEvent>(userValueChangeEvent);
    on<ConfirmPasswordValidateEvent>(confirmPasswordValidateEvent);
    on<ResetButtonClickEvents>(resetButtonClickEvent);
  }

  userValueChangeEvent(
      UserValueChangeEvent event, Emitter<ResetPasswordStates> emit) {
    bool enabled = false;
    if (event.value.isEmpty) {
      enabled = false;
    } else {
      enabled = true;
    }
    emit(ResetPasswordEnabledState(enabled: enabled));
  }

  confirmPasswordValidateEvent(
      ConfirmPasswordValidateEvent event, Emitter<ResetPasswordStates> emit) {
    emit(ResetPasswordConfirmState(isConfirm: event.isConfirm));
  }

  resetButtonClickEvent(
      ResetButtonClickEvents event, Emitter<ResetPasswordStates> emit) {
    emit(ResetButtonClickState(isLoading: event.isLoading));
  }
}
