import 'package:equatable/equatable.dart';

class ResetPasswordStates extends Equatable {
  final bool? enabled, isConfirm, isLoading;

  ResetPasswordStates(
      {this.enabled = false, this.isConfirm = false, this.isLoading = false});

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class ResetPasswordEnabledState extends ResetPasswordStates {
  final bool enabled;

  ResetPasswordEnabledState({this.enabled = false});

  @override
  List<Object> get props => [enabled];
}

class ResetPasswordConfirmState extends ResetPasswordStates {
  final bool isConfirm;

  ResetPasswordConfirmState({this.isConfirm = false});

  @override
  List<Object> get props => [isConfirm];
}

class ResetButtonClickState extends ResetPasswordStates {
  final bool isLoading;

  ResetButtonClickState({this.isLoading = false});

  @override
  List<Object> get props => [isLoading];
}
