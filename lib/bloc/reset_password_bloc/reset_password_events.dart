import 'package:equatable/equatable.dart';

class ResetPasswordEvents extends Equatable {
  ResetPasswordEvents();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class UserValueChangeEvent extends ResetPasswordEvents {
  final String value;

  UserValueChangeEvent(this.value);

  @override
  // TODO: implement props
  List<Object?> get props => [value];
}

class ConfirmPasswordValidateEvent extends ResetPasswordEvents {
  final bool isConfirm;

  ConfirmPasswordValidateEvent(this.isConfirm);

  @override
  // TODO: implement props
  List<Object?> get props => [isConfirm];
}

class ResetButtonClickEvents extends ResetPasswordEvents {
  final bool isLoading;

  ResetButtonClickEvents(this.isLoading);

  @override
  // TODO: implement props
  List<Object?> get props => [isLoading];
}
