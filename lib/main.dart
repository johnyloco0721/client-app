import 'dart:convert';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_bloc.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/route_management/property_details_arguments.dart';
import 'package:pmaclient/route_management/route_generator.dart';
import 'package:pmaclient/screens/splash_screen.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/shared_preference_utils/pref_keys.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:statusbarz/statusbarz.dart';
import 'package:huawei_push/huawei_push.dart' as HuaWei;
import 'package:huawei_hmsavailability/huawei_hmsavailability.dart';

GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

_getTokenGenerateBy() async {
  if (Platform.isAndroid) {
    HmsApiAvailability hmsApiAvailability = HmsApiAvailability();
    try {
      int resultCode = await hmsApiAvailability.isHMSAvailableWithApkVersion(28);
      if (resultCode == 0) {
        // HuaWei Devices
        AppDataModel.setTokenGenerateByList("Huawei");
      } else {
        // Android Devices
        AppDataModel.setTokenGenerateByList("Android");
      }
    } catch (e) {
      AppDataModel.setTokenGenerateByList("Android");
      print(e);
    }
  } else {
    // IOS Devices
    AppDataModel.setTokenGenerateByList("IOS");
  }
}

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await _getTokenGenerateBy();

  if (AppDataModel.getTokenGenerateByList() == 'Huawei') {
    await HuaWei.Push.registerBackgroundMessageHandler(
      backgroundMessageCallback,
    );
  } else {
    await Firebase.initializeApp();
    FirebaseMessaging.onBackgroundMessage(_messageHandler);
  }

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) async {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness:
              Platform.isAndroid ? Brightness.dark : Brightness.dark,
          systemNavigationBarIconBrightness: Brightness.dark,
          statusBarBrightness: Platform.isAndroid
              ? Brightness.dark
              : Brightness.dark //or set color with: Color(0xFF0000FF)
          ),
    );
    await SpManager.getInstance();
    runApp(KeyboardDismisser(
      gestures: [
        GestureType.onTap,
        GestureType.onPanUpdateDownDirection,
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<ProfileBloc>(
            create: (BuildContext context) => ProfileBloc(),
          ),
          BlocProvider<ApiBloc>(
            create: (BuildContext context) => ApiBloc(),
          ),
        ],
        child: StatusbarzCapturer(
          child: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle(
              statusBarColor: Colors.transparent,
              systemNavigationBarColor: Colors.transparent,
            ),
            sized: false,
            child: MaterialApp(
              navigatorObservers: [Statusbarz.instance.observer],
              navigatorKey: navigatorKey,
              theme: ThemeData.from(
                colorScheme: const ColorScheme.light(),
              ),
              home: MyApp(),
              title: "The Makeover Guys",
              debugShowCheckedModeBanner: false,
              localizationsDelegates: [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
              initialRoute: "/",
              onGenerateRoute: RouteGenerator.generateRoute,
            ),
          ),
        ),
      ),
    ));
  });
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  late FirebaseMessaging messaging;

  String _token = '';

  void _onTokenEvent(String event) {
    _token = event;
    print("hmsToken : - $_token");
    AppDataModel.setDeviceToken(_token.toString());
  }

  void _onTokenError(Object error) {
    PlatformException e = error as PlatformException;
    print('TokenErrorEvent: ' + e.message!);
  }

  void _onNotificationOpenedApp(dynamic initialNotification, {bool isTerminated = false}) {
    if (initialNotification != null) {
      handleNotificationClick(initialNotification, isTerminated, isHuawei: true);
      print('onNotificationOpenedApp: ' + initialNotification.toString());
    }
  }

  void _onMessageReceiveError(Object error) {
    print('onMessageReceiveError: ' + error.toString());
  }

  void _onMessageReceived(HuaWei.RemoteMessage remoteMessage) {
    print('_onMessageReceived');
    String? data = remoteMessage.data;
    // print(data);
    if (data != null) {
      HuaWei.Push.localNotification(
        <String, dynamic>{
          HuaWei.HMSLocalNotificationAttr.TITLE: jsonDecode(data)['title'],
          HuaWei.HMSLocalNotificationAttr.MESSAGE: jsonDecode(data)['body'],
          HuaWei.HMSLocalNotificationAttr.SMALL_ICON:
              '@drawable/notification_icon',
          HuaWei.HMSLocalNotificationAttr.DATA:
              Map<String, dynamic>.from(jsonDecode(data)),
        },
      );

      print('onMessageReceived Data: $data');
    } else {
      print('onMessageReceived: No data is present.');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // Add observer to detect app state: inactive, paused, resumed
    WidgetsBinding.instance.addObserver(this);

    if (AppDataModel.getTokenGenerateByList() == 'Huawei') {
      HuaWei.Push.getTokenStream.listen(
        _onTokenEvent,
        onError: _onTokenError,
      );
      HuaWei.Push.getToken('');
      initPlatformState();
    } else {
      getMessageNotifications();
      messaging = FirebaseMessaging.instance;
      messaging.getToken().then((value) {
        print("fcmToken : - $value");
        AppDataModel.setDeviceToken(value.toString());
      });
    }
  }

  // huawei push kit listener
  Future<void> initPlatformState() async {
    if (Platform.isAndroid) {
      await Permission.notification.request();
    }
    //huawei
    if (!mounted) return;
    // If you want auto init enabled, after getting user agreement call this method.
    await HuaWei.Push.setAutoInitEnabled(true);

    HuaWei.Push.getTokenStream.listen(
      _onTokenEvent,
      onError: _onTokenError,
    );

    HuaWei.Push.onNotificationOpenedApp.listen(
      _onNotificationOpenedApp,
    );

    final dynamic initialNotification =
        await HuaWei.Push.getInitialNotification();
    _onNotificationOpenedApp(initialNotification, isTerminated: true);

    HuaWei.Push.onMessageReceivedStream.listen(
      _onMessageReceived,
      onError: _onMessageReceiveError,
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    // Clear app badger when app inactive or resumed
    if (state == AppLifecycleState.resumed || state == AppLifecycleState.inactive) {
      clearAppBadge();
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Firebase.initializeApp(),
      builder: (context, snapShot) {
        if (snapShot.hasError) {}

        if (snapShot.connectionState == ConnectionState.done) {
          return SplashScreen();
        }
        return CircularProgressIndicator();
      },
    );
  }

  getMessageNotifications() async {
    if (Platform.isAndroid) {
      await Permission.notification.request();
    }
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();
    if (initialMessage != null) {
      handleNotificationClick(initialMessage, true);
    }

    if (Platform.isIOS) {
      NotificationSettings settings = await FirebaseMessaging.instance
          .requestPermission(
              alert: true,
              announcement: false,
              badge: true,
              carPlay: false,
              criticalAlert: false,
              sound: true,
              provisional: false);
      print("user granted permission : ${settings.authorizationStatus}");
    }
    final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        FlutterLocalNotificationsPlugin();
    AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('mipmap/launcher_icon');

    /// Note: permissions aren't requested here just to demonstrate that can be
    /// done later
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
            requestAlertPermission: false,
            requestBadgePermission: false,
            requestSoundPermission: false,
            onDidReceiveLocalNotification: (
              int id,
              String? title,
              String? body,
              String? payload,
            ) async {
              print(title);
            });
    const MacOSInitializationSettings initializationSettingsMacOS =
        MacOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
    );

    var initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
      macOS: initializationSettingsMacOS,
    );
    await flutterLocalNotificationsPlugin.initialize(initializationSettings);
    const AndroidNotificationChannel channel = AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      description: 'This channel is used for important notifications.',
      importance: Importance.max,
    );
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      print("message received");
      print(event.notification!.body);
      RemoteNotification? notification = event.notification;
      AndroidNotification? android = event.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
          notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              playSound: true,
              channelDescription: channel.description,
              icon: android.smallIcon,
            ),
          ),
        );
      }
    });
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('Message clicked!');
      handleNotificationClick(message, false);
    });
  }

  handleNotificationClick(message, isTerminated, {isHuawei = false}) {
    var data = isHuawei ? jsonDecode(message['extras']['notification']['data']) : message.data;

    if (data["type"] != null) {
      String? projectId =
          data["projectId"] != null ? data["projectId"] : "0";
      String? projectStatus = data["projectStatus"] != null
          ? data["projectStatus"]
          : "";
      String? type = data["type"];
      String redirectPage =
          (projectStatus == "completed" || projectStatus == "rented" || projectStatus == "handovered") && (data['isHandoverTaskCompleted'] == '1')
              ? propertyDetailsCompleteScreen
              : propertyDetailsScreen;
      PropertyDetailsArguments arguments = PropertyDetailsArguments(
          projectID: int.parse(projectId ?? "0"), type: type);
      if (isTerminated) {
        goAndClearAllPageNamed(navigatorKey.currentContext ?? context, homeScreen);
      } 
      goToNextNamedRoute(
        navigatorKey.currentContext ?? context,
        redirectPage,
        args: arguments,
      );
    }
  }

  clearAppBadge() async {
    try {
      bool res = await FlutterAppBadger.isAppBadgeSupported();
      if (res) {
        FlutterAppBadger.removeBadge();
      }
    } on PlatformException {
      print('App Badge not supported');
    }
  }
}

Future<void> _messageHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('background message ${message.notification?.body}');
}

@pragma('vm:entry-point')
void backgroundMessageCallback(HuaWei.RemoteMessage remoteMessage) async {
  print('backgroundMessageCallback');
  String? data = remoteMessage.data;
  if (data != null) {
    debugPrint(
      'Background message is received, sending local notification.',
    );
    print(data);
    HuaWei.Push.localNotification(
      <String, dynamic>{
        HuaWei.HMSLocalNotificationAttr.TITLE: jsonDecode(data)['title'],
        HuaWei.HMSLocalNotificationAttr.MESSAGE: jsonDecode(data)['body'],
        HuaWei.HMSLocalNotificationAttr.SMALL_ICON:
            '@drawable/notification_icon',
        HuaWei.HMSLocalNotificationAttr.DATA: Map<String, dynamic>.from(jsonDecode(data)),
      },
    );
  } else {
    debugPrint(
      'Background message is received. There is no data in the message.',
    );
  }
}

void getInitialNotification() async {
  final dynamic initialNotification =
      await HuaWei.Push.getInitialNotification();
  print('getInitialNotification: ' + initialNotification.toString());
}