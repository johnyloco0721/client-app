import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_fonts.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';

Widget welcomeToMakeoverGuysImage(BuildContext context, {color}) => Container(
      width: SizeUtils.width(context, 75),
      child: AspectRatio(
        aspectRatio: 916 / 216,
        child: Image.asset(
          ic_welcome_to_makeover,
          fit: BoxFit.fill,
          color: color,
        ),
      ),
    );

Widget headerLayoutWithCross(BuildContext context, title, {color, backClick}) =>
    Container(
      height: 60,
      width: SizeUtils.width(context, 100),
      color: color ?? black,
      padding: EdgeInsets.only(left: 20, right: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              child: Text(
                title,
                style: boldFontsStyle(
                  fontSize: bigLargeFont,
                  color: white,
                  fontFamily: platForm,
                  letterSpacing: 1.0,
                ),
              ),
            ),
          ),
          InkWell(
            customBorder: CircleBorder(),
            onTap: backClick ?? () => Navigator.pop(context),
            child: Container(
              padding: EdgeInsets.all(10.0),
              child: Image.asset(ic_close, height: 20, width: 20)
            ),
          )
        ],
      ),
    );

Widget headerLayoutWithBackArrow(BuildContext context, title,
        {color, backClick, isCenter = true}) =>
    Container(
      height: 60,
      width: SizeUtils.width(context, 100),
      color: black,
      padding: EdgeInsets.only(left: 10, right: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            customBorder: CircleBorder(),
            onTap: () => Navigator.pop(context),
            child: Container(
              padding: EdgeInsets.only(
                left: 10,
                top: 10,
                right: 10,
                bottom: 10,
              ),
              child: Image.asset(
                ic_left_arrow,
                height: 20,
                width: 20,
                color: white,
              ),
            ),
          ),
          Expanded(
            child: Container(
              alignment: isCenter ? Alignment.center : Alignment.centerLeft,
              margin: EdgeInsets.only(left: 15, right: 15),
              child: Text(
                title,
                style: boldFontsStyle(
                  fontSize: bigLargeFont,
                  color: white,
                  fontFamily: platForm,
                  letterSpacing: 1.0,
                ),
              ),
            ),
          ),
          Opacity(
            opacity: 0,
            child: Container(
              padding: EdgeInsets.only(
                left: 10,
                top: 10,
                right: 10,
                bottom: 10,
              ),
              child: Image.asset(
                ic_left_arrow,
                height: 20,
                width: 20,
                color: white,
              ),
            ),
          ),
        ],
      ),
    );
