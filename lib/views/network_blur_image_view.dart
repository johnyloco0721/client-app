import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:octo_image/octo_image.dart';
import 'package:pmaclient/constants/app_constants.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/utils/size_utils.dart';

class NetworkBlurHashImageView extends StatelessWidget {
  final String? imageUrl, blurHash;
  final double? width, height;
  final BoxFit? fit;
  final String? dummyErrorUrl;

  NetworkBlurHashImageView({
    this.imageUrl,
    this.height,
    this.width,
    this.blurHash = 'LEHV6nWB2yk8pyo0adR*.7kCMdnj',
    this.dummyErrorUrl,
    this.fit = BoxFit.cover,
  });

  @override
  Widget build(BuildContext context) {
    return OctoImage(
      width: width ?? SizeUtils.width(context, 100),
      height: height ?? SizeUtils.height(context, 100),
      image: CachedNetworkImageProvider(imageUrl ?? ""),
      placeholderBuilder: OctoPlaceholder.blurHash(blurHash ?? demoBlurHash),
      errorBuilder: (context, error, stackTrace) {
        return Image.asset(
          no_image_placeholder,
          fit: BoxFit.cover,
          width: width ?? SizeUtils.width(context, 100),
          height: height ?? SizeUtils.height(context, 100),
        );
      },
      fit: fit,
    );
  }
}
