import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/utils/size_utils.dart';

class HorizontalLine extends StatelessWidget {
  final Color color;
  final double? lineLength;
  final double height;
  final EdgeInsetsGeometry? margin;

  const HorizontalLine({
    Key? key,
    this.lineLength,
    this.color = lightGrey,
    this.height = 0.5,
    this.margin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: lineLength ?? SizeUtils.width(context, 100),
      color: color,
      margin: margin,
    );
  }
}
