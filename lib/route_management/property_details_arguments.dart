class PropertyDetailsArguments {
  int? projectID;
  String? type;
  int? affectedModuleID;

  PropertyDetailsArguments({this.type, this.projectID, this.affectedModuleID});
}
