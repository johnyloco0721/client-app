import 'package:flutter/material.dart';
import 'package:pmaclient/animation/page_routes.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/route_management/media_screen_args.dart';
import 'package:pmaclient/route_management/property_details_arguments.dart';
import 'package:pmaclient/screens/app_support_screen.dart';
import 'package:pmaclient/screens/change_password_screen.dart';
import 'package:pmaclient/screens/cozy_homes_web_screen.dart';
import 'package:pmaclient/screens/email_send_screen.dart';
import 'package:pmaclient/screens/enquiry_screen.dart';
import 'package:pmaclient/screens/forgot_password_screen.dart';
import 'package:pmaclient/screens/gallery_screen.dart';
import 'package:pmaclient/screens/home_screen.dart';
import 'package:pmaclient/screens/login_otp_screen.dart';
import 'package:pmaclient/screens/login_screen.dart';
import 'package:pmaclient/screens/maintenance_screen.dart';
import 'package:pmaclient/screens/media_screen.dart';
import 'package:pmaclient/screens/notification_screen.dart';
import 'package:pmaclient/screens/profile_screen.dart';
import 'package:pmaclient/screens/property_details_complete_screen.dart';
import 'package:pmaclient/screens/property_details_screen.dart';
import 'package:pmaclient/screens/rate_resolve_issue_complete_screen.dart';
import 'package:pmaclient/screens/rate_resolve_issue_screen.dart';
import 'package:pmaclient/screens/rate_us_screen.dart';
import 'package:pmaclient/screens/rating_questions_screen.dart';
import 'package:pmaclient/screens/splash_screen.dart';
import 'package:pmaclient/screens/terms_and_services_screen.dart';
import 'package:pmaclient/screens/tms_screen.dart';
import 'package:pmaclient/screens/update_app_screen.dart';
import 'package:pmaclient/screens/welcome_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case splashScreen:
        return MaterialPageRoute(
            builder: (context) => SplashScreen(), settings: settings);
      case welcomeScreen:
        return MaterialPageRoute(
            builder: (context) => WelcomeScreen(), settings: settings);
      case loginScreen:
        return MaterialPageRoute(
            builder: (context) => LoginScreen(), settings: settings);
      case loginOtpScreen:
        return MaterialPageRoute(
            builder: (context) => LoginOtpScreen(), settings: settings);
      case forgotPasswordScreen:
        return MaterialPageRoute(
            builder: (context) => ForgotPasswordScreen(), settings: settings);
      case emailSendScreen:
        return MaterialPageRoute(
            builder: (context) => EmailSendScreen(), settings: settings);
      case homeScreen:
        return MaterialPageRoute(
            builder: (context) => HomeScreen(), settings: settings);
      case notificationScreen:
        return MaterialPageRoute(
            builder: (context) => NotificationScreen(), settings: settings);
      case profileScreen:
        return MaterialPageRoute(
            builder: (context) => ProfileScreen(), settings: settings);
      case termsScreen:
        return MaterialPageRoute(
            builder: (context) => TermsAndServicesScreen(), settings: settings);
      case appSupportScreen:
        return MaterialPageRoute(
            builder: (context) => AppSupportScreen(), settings: settings);
      case propertyDetailsScreen:
        return SlideRoute(
            page: PropertyDetailsScreen(
              propertyArguments: settings.arguments != null
                  ? settings.arguments as PropertyDetailsArguments
                  : PropertyDetailsArguments(),
            ),
            settings: settings);
      case propertyDetailsCompleteScreen:
        return SlideRoute(
            page: PropertyDetailsCompleteScreen(
              propertyArguments: settings.arguments != null
                  ? settings.arguments as PropertyDetailsArguments
                  : PropertyDetailsArguments(),
            ),
            settings: settings);
      case tmsScreen:
        return MaterialPageRoute(
            builder: (context) => TmsScreen(), settings: settings);
      case codyHomesWebScreen:
        return MaterialPageRoute(
            builder: (context) => CozyHomesWebScreen(), settings: settings);
      case enquiryScreen:
        return MaterialPageRoute(
            builder: (context) => EnquiryScreen(), settings: settings);
      case maintenanceScreen:
        return MaterialPageRoute(
            builder: (context) => MaintenanceScreen(), settings: settings);
      case updateScreen:
        return MaterialPageRoute(
            builder: (context) => UpdateAppScreen(), settings: settings);
      case mediaScreen:
        return MaterialPageRoute(
            builder: (context) => MediaScreen(), settings: settings);
      case galleryScreen:
        return MaterialPageRoute(
            builder: (context) => GalleryScreen(
                  mediaScreenArgs: settings.arguments != null
                      ? settings.arguments as MediaScreenArgs
                      : MediaScreenArgs(),
                ),
            settings: settings);
      case changePasswordScreen:
        return MaterialPageRoute(
            builder: (context) => ChangePasswordScreen(), settings: settings);
      case rateUsScreen:
        return MaterialPageRoute(
            builder: (context) => RateUsScreen(), settings: settings);
      case ratingQuestionsScreen:
        return MaterialPageRoute(
            builder: (context) => RatingQuestionsScreen(), settings: settings);
      case rateResolveIssueScreen:
        return MaterialPageRoute(
            builder: (context) => RateResolveIssueScreen(), settings: settings);
      case rateResolveIssueCompleteScreen:
        return MaterialPageRoute(
            builder: (context) => RateResolveIssueCompleteScreen(),
            settings: settings);
      default:
        return MaterialPageRoute(
          builder: (BuildContext context) => Scaffold(
            body: Center(
              child: Text('The Page ${settings.name} does not exists.'),
            ),
          ),
        );
    }
  }
}
