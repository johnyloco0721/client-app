class MediaScreenArgs {
  int? index;
  List? mediaList;
  bool isGallery;

  MediaScreenArgs({this.mediaList, this.index, this.isGallery = true});
}
