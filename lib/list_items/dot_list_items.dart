import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';

class DotListIndicator extends StatelessWidget {
  final bool isCurrent;
  final bool icLastIndex;

  const DotListIndicator({
    Key? key,
    this.isCurrent = false,
    this.icLastIndex = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 5),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          AnimatedContainer(
            height: isCurrent ? 10 : 10,
            width: isCurrent ? 30 : 10,
            duration: Duration(milliseconds: 500),
            decoration: BoxDecoration(
              color: isCurrent ? yellow : white,
              borderRadius: BorderRadius.circular(isCurrent ? 10 : 100),
              border: isCurrent
                  ? Border.fromBorderSide(BorderSide.none)
                  : Border.all(color: black, width: 1),
            ),
          ),
        ],
      ),
    );
  }
}
