import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/views/network_blur_image_view.dart';

class GalleryImageIndicatorListItem extends StatelessWidget {
  final String? imageUrl;
  final String? blurHashImage;
  final bool isCurrent;
  final int? index;
  final PageController? pageController;
  final bool isVideo;

  const GalleryImageIndicatorListItem(
      {Key? key,
      this.imageUrl,
      this.blurHashImage,
      this.isCurrent = false,
      this.index,
      this.pageController,
      this.isVideo = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: index == 0 ? 20 : 0, right: 8),
      padding: EdgeInsets.only(bottom: 1),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
      ),
      child: CupertinoButton(
        padding: EdgeInsets.zero,
        borderRadius: BorderRadius.circular(12),
        onPressed: () => pageController?.animateToPage(index ?? 0, duration: Duration(milliseconds: 200), curve: Curves.linear),
        child: Card(
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsets.zero,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          elevation: isCurrent ? 2 : 0,
          color: Colors.transparent,
          borderOnForeground: false,
          child: AnimatedContainer(
            height: isCurrent ? 65 : 60,
            width: isCurrent ? 65 : 60,
            duration: Duration(milliseconds: 500),
            child: Stack(
              children: [
                CachedNetworkImage(
                  width: 65,
                  height: 65,
                  imageUrl: imageUrl ?? '',
                  fit: BoxFit.cover,
                  maxWidthDiskCache: 130,
                  maxHeightDiskCache: 130,
                ),
                Align(
                  alignment: Alignment.center,
                  child: isVideo
                      ? Image.asset(ic_play, height: 25, width: 25)
                      : Container(),
                ),
                Positioned.fill(
                  child: AnimatedContainer(
                    color: !isCurrent
                        ? black.withOpacity(0.5)
                        : Colors.transparent,
                    duration: Duration(milliseconds: 500),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
