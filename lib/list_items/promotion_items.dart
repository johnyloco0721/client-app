import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/views/network_blur_image_view.dart';
import 'package:pmaclient/widgets/round_icon_button.dart';

class PromotionItems extends StatelessWidget {
  final String? imagePath;

  const PromotionItems({
    Key? key,
    this.imagePath,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.zero,
      elevation: 2,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12),
      ),
      child: Stack(
        children: [
          Positioned.fill(
            child: NetworkBlurHashImageView(
              imageUrl: imagePath ?? "",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 220,
            padding: EdgeInsets.all(15),
            alignment: Alignment.bottomLeft,
            child: roundIconButton(icon: ic_arrow_next, iconColor: black),
          ),
        ],
      ),
    );
  }
}
