import 'dart:io';

import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_constants.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/network_blur_image_view.dart';
import 'package:pmaclient/widgets/round_icon_button.dart';
import 'package:url_launcher/url_launcher.dart';

class InquiryListItem extends StatelessWidget {
  final int? teamMemberID;
  final String? teamMemberName;
  final String? companyName;
  final String? designationName;
  final String? whatsappLink;
  final String? teamMemberAvatar;

  const InquiryListItem({
    Key? key,
    this.companyName,
    this.designationName,
    this.teamMemberAvatar,
    this.teamMemberID,
    this.teamMemberName,
    this.whatsappLink,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeUtils.width(context, 100),
      margin: EdgeInsets.only(bottom: 25),
      child: Stack(
        children: [
          Container(),
          Container(
            width: SizeUtils.width(context, 100),
            margin: EdgeInsets.only(left: 50),
            decoration: BoxDecoration(
              color: white,
              borderRadius: BorderRadius.circular(12),
              boxShadow: [
                BoxShadow(
                  color: shadowColor.withOpacity(0.1),
                  spreadRadius: 1,
                  blurRadius: 12,
                  offset: Offset(0, 0.19),
                ),
              ],
            ),
            child: Container(
              margin: EdgeInsets.only(left: 45),
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 5),
                        Text(
                          teamMemberName ?? "Bryan Wong",
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: boldFontsStyle(fontSize: bigFonts),
                        ),
                        SizedBox(height: 8),
                        Flexible(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Image.asset(ic_career, height: 20, width: 20),
                              SizedBox(width: 10),
                              Flexible(
                                child: Container(
                                  child: Text(
                                    companyName ?? "",
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: regularFontsStyle(color: grey, height: 1.3),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 5),
                        Flexible(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Image.asset(
                                ic_user,
                                height: 20,
                                width: 20,
                                color: darkYellow,
                              ),
                              SizedBox(width: 10),
                              Flexible(
                                child: Text(
                                  designationName ?? "",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 6,
                                  softWrap: false,
                                  style: regularFontsStyle(color: grey, height: 1.3),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 5),
                  roundIconButton(
                    color: green,
                    height: 35.0,
                    width: 35.0,
                    icon: ic_whatsapp,
                    onPressed: () async {
                      if (await canLaunch(whatsappLink ?? "")) {
                        if (Platform.isIOS) {
                          await launch(whatsappLink ?? "",
                              forceSafariVC: false);
                        } else {
                          await launch(whatsappLink ?? "");
                        }
                      } else {
                        showToast(S().whatsapp_not_installed);
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
          Positioned.fill(
            top: 0,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Container(
                height: 90,
                width: 90,
                decoration: BoxDecoration(color: grey, shape: BoxShape.circle),
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child: Card(
                  margin: EdgeInsets.zero,
                  elevation: 1,
                  shape: CircleBorder(),
                  child: NetworkBlurHashImageView(
                    fit: BoxFit.cover,
                    imageUrl: teamMemberAvatar,
                    dummyErrorUrl: dummyUserPic,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
