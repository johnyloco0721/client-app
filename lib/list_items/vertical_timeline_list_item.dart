import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/widgets/expandable_html_widget.dart';

class VerticalTimeLineListItem extends StatelessWidget {
  final String? status;
  final String? nextStatus;
  final String? name;
  final String? startDate;
  final String? endDate;
  final int? index;
  final int? lastIndex;
  final String? weekLabel;

  const VerticalTimeLineListItem({
    Key? key,
    this.status = "active",
    this.nextStatus = "active",
    this.index,
    this.lastIndex,
    this.name,
    this.endDate,
    this.startDate,
    this.weekLabel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(right: 10, left: 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                width: 40,
                alignment: Alignment.center,
                child: status == "active"
                    ? Container(
                        height: 8,
                        width: 8,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: black, width: 1),
                        ),
                      )
                    : status == "complete"
                        ? Container(
                            child: Image.asset(
                              ic_round_tick,
                              height: 18,
                              width: 18,
                            ),
                          )
                        : status == "star"
                          ? Container(
                              child: Image.asset(
                                ic_star,
                                height: 18,
                                width: 18,
                              ),
                            ) : Container(
                              height: 25,
                              child: Image.asset(
                                ic_tmog_head,
                                height: 25,
                                width: 25,
                              ),
                            ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 5),
                  child: ExpandableHtmlWidget(
                    htmlContent: (name ?? ""),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              index == lastIndex
                  ? Container(width: 40)
                  : Container(
                      width: 40,
                      alignment: Alignment.center,
                      child: status == "complete" && (nextStatus != "star" && nextStatus != "active")
                          ? Container(
                              width: 1.5,
                              margin:
                                  EdgeInsets.only(left: 0, top: 5, bottom: 5),
                              color: black,
                              height: 40,
                            )
                          : Container(
                              height: 40,
                              margin: EdgeInsets.only(
                                  left: 5, right: 5, bottom: 5, top: 5),
                              child: DottedLine(
                                dashLength: 1.5,
                                dashGapLength: 3,
                                direction: Axis.vertical,
                                lineThickness: 1.5,
                                dashRadius: 16,
                              ),
                            ),
                    ),
              Flexible(
                child: Container(
                  margin: EdgeInsets.only(top: 10, left: 5),
                  child: 
                  ExpandableHtmlWidget(
                    htmlContent: (weekLabel ?? ""),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
