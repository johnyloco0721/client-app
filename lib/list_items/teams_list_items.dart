import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_constants.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/views/network_blur_image_view.dart';

class TeamsListItem extends StatelessWidget {
  final String? memberName;
  final String? designationName;
  final String? memberInfo;
  final String? teamAvatar;
  final String? biography;
  final bool isLast;

  const TeamsListItem({
    Key? key,
    this.designationName,
    this.memberInfo,
    this.memberName,
    this.teamAvatar,
    this.isLast = false,
    this.biography,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: isLast ? 25 : 0),
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 50),
            child: Card(
              margin: EdgeInsets.zero,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16), // if you need this
                side: BorderSide(
                  color: shadowColor.withOpacity(0.1),
                  width: 1,
                ),
              ),
              shadowColor: shadowColor,
              elevation: 2,
              child: Container(
                padding: EdgeInsets.only(
                  top: 60,
                  left: 15,
                  right: 15,
                  bottom: 15,
                ),
                child: Column(
                  children: [
                    memberName != null && memberName!.isNotEmpty
                        ? Text(
                            memberName ?? "",
                            textAlign: TextAlign.center,
                            style: boldFontsStyle(fontSize: bigFonts),
                          )
                        : Container(),
                    SizedBox(height: 5),
                    designationName != null && designationName!.isNotEmpty
                        ? Text(
                            designationName ?? "",
                            textAlign: TextAlign.center,
                            style: regularFontsStyle(color: grey),
                          )
                        : Container(),
                    SizedBox(height: 10),
                    biography != null && biography!.isNotEmpty
                        ? Text(
                            biography ?? "",
                            textAlign: TextAlign.center,
                            style: regularFontsStyle(height: 1.3),
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
          ),
          Positioned.fill(
            top: 0,
            child: Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: 100,
                width: 100,
                margin: EdgeInsets.only(top: 3),
                decoration: BoxDecoration(
                  color: grey,
                  shape: BoxShape.circle,
                ),
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child: Card(
                  margin: EdgeInsets.zero,
                  shadowColor: shadowColor,
                  elevation: 5,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  color: Colors.pink,
                  shape: CircleBorder(
                    side: BorderSide(
                      color: shadowColor.withOpacity(0.1),
                      width: 1,
                    ),
                  ),
                  child: NetworkBlurHashImageView(
                    fit: BoxFit.cover,
                    imageUrl: teamAvatar ?? "",
                    dummyErrorUrl: dummyUserPic,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
