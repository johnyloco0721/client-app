import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/notification_api_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/route_management/property_details_arguments.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';

class NotificationsListItems extends StatelessWidget {
  final String? headerText;
  final int? index;
  final List<NotificationDataList>? notificationDataList;
  final int length;
  final VoidCallback? notificationClick;
  final ApiBloc? apiBloc;
  final ApiStates? apiStates;
  final bool isVisible;

  const NotificationsListItems({
    Key? key,
    this.headerText,
    this.index,
    this.notificationDataList,
    this.length = 0,
    this.notificationClick,
    this.apiBloc,
    this.apiStates,
    this.isVisible = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: index == 0 ? 25 : 0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          headerText != null && headerText!.isNotEmpty
              ? Container(
                  margin: EdgeInsets.only(
                      left: 25, right: 25, top: index != 0 ? 25 : 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Container(
                          child: Text(
                            headerText ?? "",
                            style: boldFontsStyle(fontSize: largeFont),
                          ),
                        ),
                      ),
                      index == 0 && isVisible
                          ? CupertinoButton(
                              padding: EdgeInsets.zero,
                              onPressed: () => apiBloc?.add(
                                ReadAllNotificationsApiEvent(),
                              ),
                              child: Container(
                                padding: EdgeInsets.only(
                                    top: 8, bottom: 8, left: 12, right: 12),
                                decoration: BoxDecoration(
                                  border: Border.all(color: black, width: 1.0),
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      ic_close,
                                      height: 15,
                                      width: 15,
                                      color: black,
                                    ),
                                    SizedBox(width: 10),
                                    Container(
                                      margin: EdgeInsets.only(top: 2),
                                      child: Text(
                                        S().read_all,
                                        style: regularFontsStyle(
                                            fontSize: bigFonts),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                )
              : Container(),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: notificationDataList?.length,
            itemBuilder: (context, sectionIndex) {
              NotificationDataList model = notificationDataList![sectionIndex];
              bool isToday = checkTodayDate(model.createdDateTime);
              String time;
              if (isToday) {
                time = Jiffy(model.createdDateTime, "yyyy-MM-dd HH:mm:ss")
                    .fromNow();
              } else {
                time = formatDate(date: model.createdDateTime);
              }
              return BlocListener(
                bloc: apiBloc,
                listener: (BuildContext context, state) {
                  if (state is UpdateNotificationsApiDataLoadState) {
                    if (state.index == sectionIndex) {
                      model.isRead = state.isRead;
                      // goToDetailsPage(context, model);
                    }
                  } else if (state is ReadAllNotificationsApiDataLoadState) {
                    model.isRead = 1;
                  }
                },
                child: ElevatedButton(
                  onPressed: () {
                    if (model.isRead == 0) {
                      apiBloc?.add(
                        UpdateNotificationsApiEvent(
                          notificationsId: model.notificationID ?? 0,
                          index: sectionIndex,
                        ),
                      );
                    }
                    goToDetailsPage(context, model);
                  },
                  style: ElevatedButton.styleFrom(
                    primary: white,
                    elevation: 0,
                    shadowColor: Colors.transparent,
                    onPrimary: shimmerGrey,
                    padding: EdgeInsets.zero,
                  ),
                  child: Container(
                    padding: EdgeInsets.only(bottom: 20),
                    margin: EdgeInsets.only(left: 25, right: 25),
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: lightGrey, width: 0.5),
                      ),
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 20),
                        model.isRead == 0
                            ? Container(
                                alignment: Alignment.topRight,
                                child: Container(
                                  height: 10,
                                  width: 10,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle, color: red),
                                ),
                              )
                            : Container(),
                        HtmlWidget(
                          model.description ?? "",
                          textStyle: boldFontsStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: mediumFont,
                            height: 1.3,
                            color: model.isRead == 0 ? black : grey,
                          ),
                        ),
                        SizedBox(height: 15),
                        Text(
                          time,
                          style: regularFontsStyle(color: grey),
                        )
                      ],
                    ),
                  ),
                ),
              );
            },
          )
        ],
      ),
    );
  }

  goToDetailsPage(context, model) {
    goToNextNamedRoute(
      context,
      (model.projectStatus == "completed" || model.projectStatus == "rented" || model.projectStatus == "handovered") && (model.isHandoverTaskCompleted == true)
          ? propertyDetailsCompleteScreen
          : propertyDetailsScreen,
      args: PropertyDetailsArguments(
        projectID: model.projectID, type: model.moduleName, affectedModuleID: int.tryParse(model.affectedModuleID)
      ),
    );
  }
}
