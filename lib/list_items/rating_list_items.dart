import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/constants/app_array_list.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/widgets/bordered_textfield.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

class RatingListItems extends StatelessWidget {
  final int index;
  final bool isQuestion;
  final bool isSuggestion;
  final String? question;
  final ValueChanged<dynamic>? onChanged;
  final double sliderValue;
  final TextEditingController? controller;
  final ValueChanged<String>? onTextFieldChange;
  final String? hintText;
  final VoidCallback? onPressed;

  const RatingListItems({
    Key? key,
    this.index = 0,
    this.isQuestion = false,
    this.isSuggestion = false,
    this.question,
    this.onChanged,
    this.sliderValue = 0.0,
    this.controller,
    this.onTextFieldChange,
    this.hintText,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String sliderLabelStart = S().very_unhappy;
    String sliderLabelEnd = S().i_love_it;
    if (index == 1) {
      sliderLabelStart = S().not_at_all;
      sliderLabelEnd = S().highly_satisfied;
    } else if (index == 2) {
      sliderLabelStart = S().never;
      sliderLabelEnd = S().very_likely;
    }
    return Container(
      margin: EdgeInsets.only(top: 25),
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              margin: EdgeInsets.only(left: 25, right: 25),
              alignment: Alignment.center,
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: !isQuestion
                      ? [
                          TextSpan(
                            text: " (${S().required})",
                            style: italicFontsStyle(
                                color: darkYellow, fontSize: bigFonts),
                          )
                        ]
                      : [],
                  text: question ??
                      "Are you happy with the design outcome of your makeover?",
                  style: boldFontsStyle(fontSize: bigFonts, height: 1.2),
                ),
              ),
            ),
            !isQuestion
                ? Container(
                    margin: EdgeInsets.only(top: 25, left: 25, right: 25),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        BorderedTextField(
                          controller: controller,
                          onChanged: onTextFieldChange,
                          hintText: S().state_your_suggestion_here,
                          enableBorderColor: lightGrey,
                          maxLines: 5,
                          textInputAction: TextInputAction.newline,
                          obscureText: false,
                          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                          isFullBorder: true,
                          fontSize: bigFonts,
                          hintFontSize: bigFonts,
                        ),
                        // isSuggestion
                        //     ? Container(
                        //         margin: EdgeInsets.only(top: 15),
                        //         alignment: Alignment.centerRight,
                        //         child: RoundedButton(
                        //           buttonText: S().done,
                        //           onPressed: onPressed,
                        //           width: 100,
                        //         ),
                        //       )
                        //     : Container(),
                      ],
                    ),
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SfSliderTheme(
                        data: SfSliderThemeData(
                            thumbColor: Colors.transparent,
                            thumbRadius: 45,
                            activeTrackColor: lightGrey,
                            inactiveTrackColor: lightGrey,
                            activeTrackHeight: 4,
                            activeDividerStrokeWidth:
                                SizeUtils.width(context, 100),
                            inactiveDividerStrokeWidth:
                                SizeUtils.width(context, 100),
                            inactiveTrackHeight: 4,
                            overlayColor: Colors.transparent,
                            overlayRadius: 0,
                            thumbStrokeColor: Colors.transparent),
                        child: Container(
                          padding: EdgeInsets.zero,
                          child: SfSlider(
                            interval: 1,
                            showTicks: false,
                            min: 0,
                            max: 9.0,
                            thumbIcon: Transform.translate(
                              offset: Offset(0, -27),
                              child: Lottie.asset(
                                  ratingLottieList[sliderValue.toInt()]),
                            ),
                            onChanged: onChanged,
                            value: sliderValue,
                          ),
                        ),
                      ),
                      Transform.translate(
                        offset: Offset(0, -18),
                        child: Container(
                          margin: EdgeInsets.only(left: 25, right: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                sliderLabelStart,
                                style: regularFontsStyle(color: grey),
                              ),
                              Text(
                                sliderLabelEnd,
                                style: regularFontsStyle(color: grey),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
          ],
        ),
      ),
    );
  }
}
