import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pinch_zoom/pinch_zoom.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/widgets/video_player_widget.dart';

class GalleryPageViewItem extends StatelessWidget {
  final String? imageUrl;
  final String? blurHashImage;
  final String? fileType;
  final Function? onZoomStart, onZoomEnd;

  const GalleryPageViewItem({
    Key? key,
    this.blurHashImage,
    this.imageUrl,
    this.fileType,
    this.onZoomEnd,
    this.onZoomStart,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: darkGrey,
      child: fileType == "video"
          ? VideoPlayerWidget(url: imageUrl)
          : PinchZoom(
              child: CachedNetworkImage(
                imageUrl: imageUrl ?? "",
                maxWidthDiskCache: 250,
                maxHeightDiskCache: 250,
              ),
              resetDuration: const Duration(milliseconds: 100),
              onZoomEnd: onZoomEnd,
              maxScale: 3.5,
              onZoomStart: onZoomStart,
            ),
    );
  }
}
