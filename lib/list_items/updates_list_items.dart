import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_fonts.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/route_management/media_screen_args.dart';
import 'package:pmaclient/utils/button_utils.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/network_blur_image_view.dart';
import 'package:pmaclient/widgets/expandable_html_widget.dart';

class UpdatesListItems extends StatelessWidget {
  final String? imagePath;
  final String? dropboxLink;
  final String? title;
  final String? date;
  final String? time;
  final String? fileType;
  final bool isViewAll;
  final bool isEdited;
  final List<MediaList>? mediaList;
  final List<Document>? documentList;
  final int itemCount = 3;
  final int hasUpdate;
  final bool moreDocs;
  final VoidCallback? moreClick;

  const UpdatesListItems({
    Key? key,
    this.imagePath,
    this.dropboxLink,
    this.title,
    this.date,
    this.time,
    this.fileType,
    this.isViewAll = false,
    this.isEdited = false,
    this.mediaList,
    this.documentList,
    this.moreDocs = true,
    this.moreClick,
    this.hasUpdate = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ApiBloc apiBloc = BlocProvider.of<ApiBloc>(context);
    int cIndex = 0;
    return Container(
      width: SizeUtils.width(context, 100),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          imagePath?.isEmpty ?? true
              ? isViewAll
                  ? Container()
                  : Container(
                      width: SizeUtils.width(context, 100),
                      padding: EdgeInsets.all(20),
                      color: darkGrey,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          hasUpdate == 1
                              ? Container(
                                  height: 10,
                                  width: 10,
                                  margin: EdgeInsets.only(right: 10),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: red,
                                  ),
                                )
                              : Container(),
                          Expanded(
                            child: ExpandableHtmlWidget(
                              htmlContent: title ?? "",
                              contentColor: white,
                              fontSize: mediumFont,
                              readMoreColor: white,
                            )
                          ),
                        ],
                      ),
                    )
              : Container(
                  child: IntrinsicWidth(
                    child: Row(
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: () => goToGalleryScreen(context, 0),
                            child: Stack(
                              children: [
                                CachedNetworkImage(
                                  imageUrl: imagePath ?? "",
                                  fit: BoxFit.cover,
                                  height: 250,
                                  width: SizeUtils.width(context, 100),
                                  maxWidthDiskCache: 500,
                                  maxHeightDiskCache: 500,
                                ),
                                videoPlayButton(context, fileType),
                              ],
                            ),
                          ),
                        ),
                        mediaList != null && mediaList!.length > 1
                            ? SizedBox(width: 4)
                            : Container(),
                        moreImageWidgetView(context, mediaList ?? []),
                      ],
                    ),
                  ),
                ),
          Container(
            color: isViewAll ? white : darkGrey,
            width: SizeUtils.width(context, 100),
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (!(imagePath?.isEmpty ?? true) || isViewAll)
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      hasUpdate == 1
                          ? Container(
                              height: 10,
                              width: 10,
                              margin: EdgeInsets.only(right: 10),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: red,
                              ),
                            )
                          : Container(),
                      Expanded(
                        child: ExpandableHtmlWidget(
                          htmlContent: title ?? "",
                          contentColor: isViewAll ? black : white,
                          fontSize: mediumFont,
                          readMoreColor: isViewAll ? black : white,
                        ),
                      ),
                    ],
                  ),
                documentList != null && documentList!.isNotEmpty
                    ? Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: isViewAll ? 15 : 10),
                          documentListBuilder(context,
                              isViewAll ? black : white, cIndex, apiBloc),
                          documentList!.length > 3 && moreDocs
                              ? InkWell(
                                  onTap: moreClick,
                                  child: Container(
                                    padding: EdgeInsets.all(3),
                                    child: Text(
                                      "${documentList!.length - 3} More...",
                                      style: boldFontsStyle(
                                        color: isViewAll ? black : white,
                                      ).copyWith(
                                        decoration: TextDecoration.underline,
                                      ),
                                    ),
                                  ),
                                )
                              : Container()
                        ],
                      )
                    : Container(), 
                Container(
                  margin: EdgeInsets.only(top: !(imagePath?.isEmpty ?? true && !isViewAll) ? 15 : 0, bottom: 15),
                  color: isViewAll ? grey : white,
                  height: 1,
                  width: 40,
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      date ?? "",
                      style: regularFontsStyle(color: isViewAll ? grey : white),
                    ),
                    Container(
                      height: 4,
                      width: 4,
                      margin: EdgeInsets.only(left: 5, right: 5),
                      decoration: BoxDecoration(
                        color: isViewAll ? grey : white,
                        shape: BoxShape.circle,
                      ),
                    ),
                    Text(
                      (time ?? "") + (isEdited ? (" " + S().edited) : ""),
                      style: regularFontsStyle(color: isViewAll ? grey : white),
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget moreImageWidgetView(context, List<MediaList> mediaList) {
    return mediaList.length > 1
        ? Expanded(
            child: Container(
              height: 250,
              width: SizeUtils.width(context, 100),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () => goToGalleryScreen(context, 1),
                      child: Stack(
                        children: [
                          CachedNetworkImage(
                            imageUrl: mediaList[1].url ?? "",
                            fit: BoxFit.cover,
                            height: 250,
                            width: SizeUtils.width(context, 100),
                            maxWidthDiskCache: 500,
                            maxHeightDiskCache: 500,
                          ),
                          videoPlayButton(context, mediaList[1].fileType ?? ""),
                        ],
                      ),
                    ),
                  ),
                  mediaList.length > 2 ? SizedBox(height: 4) : Container(),
                  mediaList.length > 2
                      ? Expanded(
                          child: InkWell(
                            onTap: () => goToGalleryScreen(context, 2),
                            child: Stack(
                              children: [
                                Positioned.fill(
                                  child: Stack(
                                    children: [
                                      CachedNetworkImage(
                                        imageUrl: mediaList[2].url ?? "",
                                        fit: BoxFit.cover,
                                        height: 250,
                                        width: SizeUtils.width(context, 100),
                                        maxWidthDiskCache: 300,
                                        maxHeightDiskCache: 300,
                                      ),
                                      videoPlayButton(
                                          context, mediaList[2].fileType ?? ""),
                                    ],
                                  ),
                                ),
                                mediaList.length > 3
                                    ? Container(
                                        color: black.withOpacity(0.5),
                                        alignment: Alignment.center,
                                        child: Text(
                                          "+${mediaList.length - 2}",
                                          style: boldFontsStyle(
                                            fontSize: font40,
                                            color: white,
                                            fontFamily: platForm,
                                          ),
                                        ),
                                      )
                                    : Container()
                              ],
                            ),
                          ),
                        )
                      : Container()
                ],
              ),
            ),
          )
        : Container();
  }

  Widget videoPlayButton(context, fileType) => fileType == "video"
      ? Container(
          width: SizeUtils.width(context, 100),
          height: 250,
          alignment: Alignment.center,
          color: black.withOpacity(0.5),
          child: Image.asset(ic_play, height: 50, width: 50),
        )
      : Container(
          height: 250,
        );

  Widget documentListBuilder(
          context, Color color, int cIndex, ApiBloc apiBloc) =>
      BlocBuilder<ApiBloc, ApiStates>(
        bloc: apiBloc,
        builder: (context, state) {
          return ListView.builder(
            controller: ScrollController(),
            padding: EdgeInsets.zero,
            itemCount: documentList!.length < 3 || !moreDocs
                ? documentList!.length
                : itemCount,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemBuilder: (context, index) {
              Document document = documentList![index];
              if (state is DownloadLoaderStates) {
                document.isLoad = state.isLoad;
              }
              return Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    decoration: BoxDecoration(
                      border: Border.all(color: color, width: 2),
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: ElevatedButton(
                      onPressed: () {
                        cIndex = index;
                        openDocumentInApp(apiBloc, document.url ?? "");
                      },
                      style: defaultButtonStyle().copyWith(
                          tapTargetSize: MaterialTapTargetSize.shrinkWrap),
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            document.isLoad != null &&
                                    document.isLoad! &&
                                    index == cIndex
                                ? Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: Lottie.asset(
                                      login_black_loading,
                                      height: 20,
                                    ),
                                  )
                                : Image.asset(
                                    ic_document,
                                    color: isViewAll ? darkYellow : yellow,
                                    height: 18,
                                    width: 18,
                                  ),
                            SizedBox(width: 15),
                            Container(
                                width: MediaQuery.of(context).size.width * 0.54,
                                child: Text(
                                  "${document.mediaName}.${document.fileExtension ?? ""}",
                                  style: boldFontsStyle(
                                      color: color, height: 1.29),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 2,
                                )),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              );
            },
          );
        },
      );

  goToGalleryScreen(context, index) => goToNextNamedRoute(
        context,
        galleryScreen,
        args: MediaScreenArgs(
          mediaList: mediaList ?? [],
          index: index ?? 0,
          isGallery: false,
        ),
      );

  goToMediaScreen(context, index) => goToNextNamedRoute(
        context,
        mediaScreen,
        args: mediaList != null ? mediaList![index] : MediaList(),
      );
}
