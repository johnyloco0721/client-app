import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_images.dart';

class HorizontalTimeLineListItem extends StatelessWidget {
  final String? status;
  final String? nextStatus;
  final int? index;
  final int? lastIndex;

  const HorizontalTimeLineListItem({
    Key? key,
    this.status = "active",
    this.nextStatus = "active",
    this.index,
    this.lastIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          status == "active"
              ? Container(
                  height: 8,
                  width: 8,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: black, width: 1),
                  ),
                )
              : status == "complete"
                  ? Container(
                      child: Image.asset(ic_round_tick, height: 18, width: 18),
                    )
                  : Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(
                          color: yellow.withOpacity(0.3),
                          width: 6,
                        ),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: yellow,
                            width: 6,
                          ),
                        ),
                        child: Image.asset(ic_tmog_head, height: 25, width: 25),
                      ),
                    ),
          index == lastIndex
              ? Container()
              : status == "complete" && (nextStatus != "star" && nextStatus != "active")
                  ? Container(
                      height: 1.5,
                      margin: EdgeInsets.only(left: 3, right: 3),
                      color: black,
                      width: 20,
                    )
                  : Container(
                      width: 20,
                      margin: EdgeInsets.only(left: 3, right: 0),
                      child: DottedLine(
                        dashLength: 1.5,
                        dashGapLength: 3,
                        lineThickness: 1.5,
                        dashRadius: 16,
                      ),
                    )
        ],
      ),
    );
  }
}
