import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';

class WelcomeListItems extends StatelessWidget {
  final String? lottiePath;
  final String? title;
  final String? description;

  const WelcomeListItems({
    Key? key,
    this.title,
    this.lottiePath,
    this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: Container(
              height: SizeUtils.height(context, 60),
              alignment: Alignment.center,
              child: Lottie.asset(
                lottiePath ?? "",
                height: SizeUtils.height(context, 45),
                width: SizeUtils.width(context, 70),
              ),
            ),
          ),
          Container(
            width: SizeUtils.width(context, 100),
            height: SizeUtils.height(context, 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(
                      top: SizeUtils.height(context, 7), left: 30, right: 30),
                  alignment: Alignment.center,
                  child: Text(
                    title ?? "",
                    textAlign: TextAlign.center,
                    style: boldFontsStyle(fontSize: bigFonts, height: 1.5),
                  ),
                ),
                Flexible(
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 25,
                      left: 30,
                      right: 30,
                    ),
                    alignment: Alignment.topCenter,
                    child: Text(
                      description ?? "",
                      textAlign: TextAlign.center,
                      style: regularFontsStyle(
                          color: grey, fontSize: bigFonts, height: 1.5),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
