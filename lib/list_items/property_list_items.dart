import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/views/network_blur_image_view.dart';

class PropertyListItems extends StatelessWidget {
  final String? imagePath;
  final String? propertyName;
  final String? projectUnit;
  final String? propertyType;
  final int? projectSize;
  final Color dotColor;
  final Color dotBorderColor;
  final String status;
  final GestureTapCallback? onTap;
  final int hasUpdate;

  const PropertyListItems({
    Key? key,
    this.imagePath,
    this.dotColor = darkYellow,
    this.dotBorderColor = darkYellow,
    this.status = "Makeover \nIn progress",
    this.propertyName,
    this.projectUnit,
    this.propertyType,
    this.projectSize,
    this.onTap,
    this.hasUpdate = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        margin: EdgeInsets.zero,
        elevation: 2,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
          side: BorderSide(
            color: shadowColor.withOpacity(0.1),
            width: 1,
          ),
        ),
        child: Stack(
          children: [
            Positioned.fill(
              child: NetworkBlurHashImageView(
                imageUrl: imagePath ?? "",
                fit: BoxFit.cover,
              ),
            ),
            Positioned.fill(
              child: Container(
                color: black.withOpacity(0.5),
              ),
            ),
            Container(
              padding: EdgeInsets.all(13),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Card(
                        margin: EdgeInsets.zero,
                        elevation: 1,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        child: Container(
                          margin: EdgeInsets.only(
                              top: 10, bottom: 10, right: 10, left: 10),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                height: 10,
                                width: 10,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: dotColor,
                                  border: Border.all(color: dotBorderColor)
                                ),
                              ),
                              SizedBox(width: 10),
                              Flexible(
                                child: Text(
                                  status,
                                  textAlign: TextAlign.start,
                                  style:
                                      regularFontsStyle(color: grey, height: 1.2),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      hasUpdate == 1
                          ? Container(
                        height: 10,
                        width: 10,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: red,
                        ),
                      )
                          : Container()

                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 35),
                    child: Text(
                      propertyName ?? "",
                      style: boldFontsStyle(
                        fontSize: bigFonts,
                        color: white,
                        height: 1.3,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 25),
                    child: Text(
                      projectUnit ?? "",
                      style: regularFontsStyle(
                        fontSize: defaultFont,
                        color: white,
                      ),
                    ),
                  ),
                  if (projectSize != null && projectSize != 0)
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(ic_sq_ft, height: 17, width: 17),
                        SizedBox(width: 10),
                        Flexible(
                          child: Text(
                            "${projectSize ?? 0}sqft",
                            style: regularFontsStyle(
                              fontSize: defaultFont,
                              color: white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(ic_room_type, height: 17, width: 17),
                        SizedBox(width: 10),
                        Flexible(
                          child: Text(
                            propertyType ?? "",
                            style: regularFontsStyle(
                              fontSize: defaultFont,
                              color: white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
