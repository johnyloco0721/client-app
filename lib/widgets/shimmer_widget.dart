import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:shimmer_animation/shimmer_animation.dart';

class ShimmerWidget extends StatelessWidget {
  final String? imagePath;
  final BoxFit fit;
  final double aspectRatio;
  final bool isWidget;
  final Widget? widget;

  const ShimmerWidget({
    Key? key,
    this.imagePath,
    this.fit = BoxFit.fill,
    this.aspectRatio = 1080 / 1920,
    this.isWidget = false,
    this.widget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer(
      color: shimmerGrey,
      colorOpacity: 0.5,
      interval: Duration(milliseconds: 100),
      child: isWidget
          ? widget ?? Container()
          : Container(
              width: SizeUtils.width(context, 100),
              child: AspectRatio(
                aspectRatio: aspectRatio,
                child: Image.asset(imagePath ?? "", fit: fit),
              ),
            ),
    );
  }
}
