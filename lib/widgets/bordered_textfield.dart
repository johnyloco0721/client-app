import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/utils/text_utils.dart';

class BorderedTextField extends StatelessWidget {
  final AutovalidateMode? autoValidateMode;
  final TextEditingController? controller;
  final FormFieldValidator<String>? validator;
  final bool? obscureText;
  final ValueChanged<String>? onChanged;
  final bool? enabled;
  final TextInputType? textInputType;
  final List<TextInputFormatter>? inputFormatters;
  final VoidCallback? onEditingComplete;
  final TextInputAction? textInputAction;
  final double? fontSize;
  final double? hintFontSize;
  final Color? fontColor;
  final Color? hintFontColor;
  final bool? alignLabelWithHint;
  final double? enableBorderWidth;
  final double? focusBorderWidth;
  final bool? filled;
  final Color? enableBorderColor;
  final Color? focusBorderColor;
  final Widget? suffixIcon;
  final double? leftPadding;
  final double? topPadding;
  final double? rightPadding;
  final double? bottomPadding;
  final String? hintText;
  final String? errorText;
  final bool isError;
  final bool isFullBorder;
  final int? maxLines;
  final EdgeInsetsGeometry? contentPadding;

  const BorderedTextField({
    Key? key,
    this.autoValidateMode,
    this.controller,
    this.validator,
    this.obscureText,
    this.onChanged,
    this.enabled,
    this.textInputType,
    this.inputFormatters,
    this.onEditingComplete,
    this.textInputAction,
    this.fontColor,
    this.fontSize,
    this.alignLabelWithHint,
    this.enableBorderWidth,
    this.enableBorderColor,
    this.focusBorderWidth,
    this.focusBorderColor,
    this.filled,
    this.suffixIcon,
    this.bottomPadding,
    this.leftPadding,
    this.rightPadding,
    this.topPadding,
    this.hintText,
    this.hintFontSize,
    this.hintFontColor,
    this.errorText,
    this.isError = false,
    this.maxLines,
    this.isFullBorder = false,
    this.contentPadding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autovalidateMode: autoValidateMode ?? AutovalidateMode.onUserInteraction,
      controller: controller,
      validator: validator,
      obscureText: obscureText ?? false,
      onChanged: onChanged,
      enabled: enabled ?? true,
      keyboardType: textInputType,
      inputFormatters: inputFormatters,
      onEditingComplete: onEditingComplete,
      textInputAction: textInputAction ?? TextInputAction.go,
      maxLines: obscureText ?? true ? 1 : maxLines,
      style: boldFontsStyle(
        fontSize: fontSize ?? defaultFont,
        color: fontColor ?? black,
      ),
      textAlignVertical: TextAlignVertical.bottom,
      decoration: InputDecoration(
        hintText: hintText,
        fillColor: white,
        errorText: errorText,
        errorMaxLines: isError ? 1 : 4,
        isDense: true,
        hintStyle: italicFontsStyle(
          fontSize: hintFontSize ?? defaultFont,
          color: hintFontColor ?? grey,
        ),
        errorStyle: isError
            ? TextStyle(height: 0)
            : regularFontsStyle(
                fontSize: fontSize ?? defaultFont,
                color: fontColor ?? red,
              ),
        alignLabelWithHint: alignLabelWithHint ?? false,
        enabledBorder: isFullBorder
            ? OutlineInputBorder(
                borderRadius: BorderRadius.circular(12),
                borderSide: BorderSide(
                  width: enableBorderWidth ?? borderWidth,
                  color: enableBorderColor ?? black,
                ),
              )
            : UnderlineInputBorder(
                borderSide: BorderSide(
                  width: enableBorderWidth ?? borderWidth,
                  color: enableBorderColor ?? black,
                ),
              ),
        focusedBorder: isFullBorder
            ? OutlineInputBorder(
                borderRadius: BorderRadius.circular(12),
                borderSide: BorderSide(
                  width: focusBorderWidth ?? 2.0,
                  color: focusBorderColor ?? yellow,
                ),
              )
            : UnderlineInputBorder(
                borderSide: BorderSide(
                  width: focusBorderWidth ?? 2.0,
                  color: focusBorderColor ?? yellow,
                ),
              ),
        disabledBorder: isFullBorder
            ? OutlineInputBorder(
                borderRadius: BorderRadius.circular(12),
                borderSide: BorderSide(width: 1, color: grey),
              )
            : UnderlineInputBorder(
                borderSide: BorderSide(width: 1, color: grey),
              ),
        filled: filled ?? false,
        suffixIcon: suffixIcon,
        contentPadding: contentPadding ??
            EdgeInsets.fromLTRB(
              leftPadding ?? 15.0,
              topPadding ?? 0.0,
              rightPadding ?? 15.0,
              bottomPadding ?? 10.0,
            ),
      ),
    );
  }
}
