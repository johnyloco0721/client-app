import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_images.dart';

Widget roundIconButton(
        {color,
        icon,
        iconWidth,
        iconHeight,
        iconColor,
        width,
        height,
        onPressed,
        elevation,
        padding}) =>
    CupertinoButton(
      onPressed: onPressed,
      padding: EdgeInsets.zero,
      child: Container(
        height: height ?? 40.0,
        width: width ?? 40.0,
        child: Card(
          color: color ?? yellow,
          margin: EdgeInsets.zero,
          shape: CircleBorder(),
          elevation: elevation ?? 5,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shadowColor: grey.withOpacity(0.5),
          child: Container(
            alignment: Alignment.center,
            padding: padding ?? EdgeInsets.all(10),
            child: Image.asset(
              icon ?? ic_down,
              width: iconWidth ?? 20.0,
              height: iconHeight ?? 20.0,
              color: iconColor ?? white,
            ),
          ),
        ),
      ),
    );
