import 'package:flutter/material.dart';
import 'package:pmaclient/utils/util_helper.dart';

class HighLightTextView extends StatelessWidget {
  final String? highLightText;
  final String? mainText;
  final TextStyle? highlightTextStyle;
  final TextStyle? textStyle;

  const HighLightTextView({
    Key? key,
    this.highLightText,
    this.mainText,
    this.highlightTextStyle,
    this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: highlightText(
          mainString: mainText,
          highLightText: highLightText,
          highLightTextStyle: highlightTextStyle,
          textStyle: textStyle,
        ),
      ),
    );
  }
}
