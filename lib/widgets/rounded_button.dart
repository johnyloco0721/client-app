import 'package:flutter/cupertino.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/utils/text_utils.dart';

class RoundedButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final Color buttonColor;
  final Color borderColor;
  final double? height, width;
  final bool enabled;
  final double? borderWidth;
  final double? fontSize;
  final Color? fontColor;
  final String? buttonText;
  final bool isLoading;
  final String? progressPath;
  final EdgeInsetsGeometry? padding;

  const RoundedButton({
    Key? key,
    this.onPressed,
    this.buttonColor = yellow,
    this.height,
    this.width,
    this.enabled = true,
    this.borderColor = white,
    this.borderWidth,
    this.buttonText,
    this.fontColor,
    this.fontSize,
    this.isLoading = false,
    this.progressPath,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      padding: EdgeInsets.all(0.0),
      child: Container(
        margin: EdgeInsets.zero,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: enabled ? buttonColor : buttonColor.withOpacity(0.3),
          border: Border.all(
            color: enabled ? borderColor : borderColor.withOpacity(0.3),
            width: borderWidth ?? 0.0,
          ),
        ),
        height: height ?? buttonHeight,
        width: width,
        padding: padding,
        child: isLoading
            ? Lottie.asset(progressPath ?? login_black_loading)
            : Text(
                buttonText ?? "",
                maxLines: 1,
                style: boldFontsStyle(
                  height: 1.2,
                  fontSize: fontSize ?? mediumFont,
                  color: enabled
                      ? fontColor ?? black
                      : fontColor?.withOpacity(0.3),
                ),
              ),
      ),
      onPressed: enabled ? onPressed : null,
      borderRadius: BorderRadius.circular(30),
    );
  }
}
