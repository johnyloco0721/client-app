import 'dart:ui' as ui;
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/widgets/html_widget/html_stylist.dart';

class ExpandableHtmlWidget extends StatelessWidget {
  final String htmlContent;
  final Color contentColor;
  final double fontSize;
  final Color readMoreColor;
  final int maxLines;

  const ExpandableHtmlWidget({
    Key? key,
    this.htmlContent = '',
    this.contentColor = black,
    this.fontSize = mediumFont,
    this.readMoreColor = black,
    this.maxLines = 3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return ExpandableNotifier(
          child: Expandable(
            collapsed: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: htmlToTextSpan(context, this.htmlContent),
                  maxLines: this.maxLines,
                  overflow: TextOverflow.ellipsis
                ),
                if (isTextOverflow(context, this.htmlContent, minWidth: constraints.minWidth, maxWidth: constraints.maxWidth))
                  ExpandableButton(
                    child: Container(
                      margin: EdgeInsets.only(bottom: 12.0),
                      child: Text(
                        'Show More',
                        style: boldFontsStyle(fontSize: this.fontSize, color: this.readMoreColor),
                      ),
                    ),
                  ),
              ],
            ),
            expanded: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RichText(
                  text: htmlToTextSpan(context, this.htmlContent),
                ),
                ExpandableButton(
                  child: Container(
                    margin: EdgeInsets.only(bottom: 12.0),
                    child: Text(
                      'Show Less',
                      style: boldFontsStyle(fontSize: this.fontSize, color: this.readMoreColor),
                    ),
                  ),
                ),
              ],
            ),
          )
        );
      }
    );
  }

  bool isTextOverflow(BuildContext context, String content, {double minWidth = 0, double maxWidth = double.infinity}) {
    final TextPainter textPainter = TextPainter(
      text: htmlToTextSpan(context, content),
      maxLines: this.maxLines,
      textDirection: ui.TextDirection.ltr,
    )..layout(minWidth: minWidth, maxWidth: maxWidth);

    return textPainter.didExceedMaxLines;
  }

  TextSpan htmlToTextSpan(BuildContext context, String content) {
    String result = content.replaceAll(' xss=removed', '');

    return HTML.toTextSpan(
      context,
      result,
      defaultTextStyle: regularFontsStyle(
        fontSize: this.fontSize,
        color: this.contentColor,
      ),
      overrideStyle: {
        'a': regularFontsStyle(
          color: darkYellow, 
          fontSize: this.fontSize,
        ),
      },
      linksCallback: (link) {
        launchURL(link);
      }
    );
  }
}