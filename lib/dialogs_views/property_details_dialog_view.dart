import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/property_tab_bloc/property_tab_bar_cubit.dart';
import 'package:pmaclient/constants/app_array_list.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';

class PropertyDetailsDialogView extends StatelessWidget {
  const PropertyDetailsDialogView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PageController pageController = PageController();
    return BlocProvider(
      create: (_) => TabCubit(),
      child: BlocBuilder<TabCubit, int>(builder: (context, index) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(left: 25, right: 25),
              child: Row(
                children: [
                  Expanded(
                    child: MaterialButton(
                      padding: EdgeInsets.zero,
                      onPressed: () {
                        context.read<TabCubit>().changeIndex(0);
                        pageController.animateToPage(0,
                            duration: Duration(milliseconds: 300),
                            curve: Curves.easeIn);
                      },
                      child: Container(
                        margin: EdgeInsets.only(top: 15),
                        width: SizeUtils.width(context, 100),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              child: Text(
                                S().project,
                                style: boldFontsStyle(
                                  fontSize: mediumFont,
                                  color: index == 0 ? black : grey,
                                  fontWeight: index == 0
                                      ? FontWeight.w700
                                      : FontWeight.w400,
                                ),
                              ),
                            ),
                            SizedBox(height: 5),
                            Opacity(
                              opacity: index == 0 ? 1 : 0,
                              child: Container(
                                height: 2,
                                width: SizeUtils.width(context, 100),
                                color: darkYellow,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: MaterialButton(
                      onPressed: () {
                        context.read<TabCubit>().changeIndex(1);
                        pageController.animateToPage(1,
                            duration: Duration(milliseconds: 300),
                            curve: Curves.easeIn);
                      },
                      padding: EdgeInsets.zero,
                      child: Container(
                        margin: EdgeInsets.only(top: 15),
                        width: SizeUtils.width(context, 100),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              child: Text(
                                S().team,
                                style: boldFontsStyle(
                                  fontSize: mediumFont,
                                  color: index == 1 ? black : grey,
                                  fontWeight: index == 1
                                      ? FontWeight.w700
                                      : FontWeight.w400,
                                ),
                              ),
                            ),
                            SizedBox(height: 5),
                            Opacity(
                              opacity: index == 1 ? 1 : 0,
                              child: Container(
                                height: 2,
                                width: SizeUtils.width(context, 100),
                                color: darkYellow,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                child: PageView(
                  scrollDirection: Axis.horizontal,
                  children: tabList,
                  controller: pageController,
                  onPageChanged: (int value) =>
                      context.read<TabCubit>().changeIndex(value),
                ),
              ),
            )
          ],
        );
      }),
    );
  }
}
