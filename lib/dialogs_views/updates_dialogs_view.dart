import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/list_items/updates_list_items.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

class UpdatesDialogView extends StatefulWidget {
  final int? autoScrollTo;
  const UpdatesDialogView({Key? key, this.autoScrollTo}) : super(key: key);

  @override
  State<UpdatesDialogView> createState() => _UpdatesDialogViewState();
}

class _UpdatesDialogViewState extends State<UpdatesDialogView> {
  List<Update> updatesList = [];
  bool moreDocs = true;
  AutoScrollController autoScrollController = AutoScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    updatesList = AppDataModel.getUpdatesList();
    
    if (widget.autoScrollTo != null) {
      int scrollToindex = updatesList.indexWhere((e) => e.milestoneUpdateID == widget.autoScrollTo);
      autoScrollController.scrollToIndex(
        scrollToindex,
        duration: Duration(milliseconds: 500),
        preferPosition: AutoScrollPosition.begin,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      controller: autoScrollController,
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemCount: updatesList.length,
      itemBuilder: (context, index) {
        Update model = updatesList[index];
        String date = formatDate(date: model.createdDateTime);
        String time = formatDate(date: model.createdDateTime, format: 'hh:mm a');
        String? imageUrl;
        String? fileType;
        if (model.mediaList!.length > 0) {
          imageUrl = model.mediaList![0].fileType == "video"
              ? model.mediaList![0].thumbnail
              : model.mediaList![0].url;
          fileType = model.mediaList![0].fileType;
        }
        return AutoScrollTag(
          key: ValueKey(index),
          controller: autoScrollController,
          index: index,
          child: Container(
            clipBehavior: Clip.antiAliasWithSaveLayer,
            margin: EdgeInsets.only(
                left: 25,
                right: 25,
                top: 25,
                bottom: index == updatesList.length - 1 ? 25 : 0),
            decoration: BoxDecoration(
              color: white,
              borderRadius: BorderRadius.circular(16),
              boxShadow: [
                BoxShadow(
                  color: shadowColor.withOpacity(0.1),
                  spreadRadius: 5,
                  blurRadius: 16,
                  offset: Offset(2, 2),
                ),
              ],
            ),
            child: UpdatesListItems(
              imagePath: imageUrl ?? "",
              date: date,
              title: model.description,
              time: time,
              isViewAll: true,
              documentList: model.documentList ?? [],
              fileType: fileType ?? "",
              moreDocs: moreDocs,
              hasUpdate: model.hasUpdate ?? 0,
              mediaList: model.mediaList ?? [],
              moreClick: () {
                setState(() => moreDocs = false);
              },
              isEdited: model.createdDateTime != model.updatedDateTime,
            ),
          ),
        );
      },
    );
  }
}
