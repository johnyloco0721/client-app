import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/route_management/media_screen_args.dart';
import 'package:pmaclient/utils/button_utils.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/network_blur_image_view.dart';

class DocumentsDialogView extends StatefulWidget {
  const DocumentsDialogView({
    Key? key,
  }) : super(key: key);

  @override
  State<DocumentsDialogView> createState() => _DocumentsDialogViewState();
}

class _DocumentsDialogViewState extends State<DocumentsDialogView> {
  int mediaTabCurrentIndex = 0;

  void setMediaGallery(int index) {
    setState(() {
      mediaTabCurrentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    ApiBloc apiBloc = BlocProvider.of<ApiBloc>(context);
    
    return BlocBuilder<ApiBloc, ApiStates>(
      bloc: apiBloc,
      builder: (context, state) {
        return DefaultTabController(
          key: PageStorageKey<String>('MediaGalleryTabController'),
          initialIndex: mediaTabCurrentIndex,
          length: 2,
          child: NotificationListener(
            onNotification: (ScrollNotification scrollNotification) {
              if (scrollNotification is ScrollEndNotification) {
                if (scrollNotification.metrics.pixels == 0) {
                  setMediaGallery(0);
                } else {
                  setMediaGallery(1);
                }
              }
              return false;
            },
            child: Column(
              children: <Widget>[
                // Tab bar
                Container(
                  padding: EdgeInsets.all(24),
                  constraints: BoxConstraints(maxHeight: 150.0),
                  child: Material(
                    color: Colors.transparent,
                    child: TabBar(
                      key: PageStorageKey<String>('MediaGalleryTabBar'),
                      labelColor: black,
                      labelStyle: boldFontsStyle(fontSize: 17.0, height: 1.35),
                      unselectedLabelColor: grey,
                      unselectedLabelStyle: regularFontsStyle(fontSize: 17.0, color: grey),
                      // Line below active tab indicator
                      indicatorColor: darkYellow,
                      labelPadding: EdgeInsets.only(top: 0.0, bottom: 5.0),
                      tabs: [
                        Text('Medias'),
                        Text('Docs'),
                      ],
                    ),
                  ),
                ),
                // Tabs content view
                Expanded(
                  child: TabBarView(
                    key: PageStorageKey<String>('MediaGalleryTabView'),
                    children: [
                      _buildMediaTabView(context, state, apiBloc),
                      _buildDocTabView(context, state, apiBloc)
                    ],
                  ),
                )
              ],
            ),
          )
        );
      },
    );
  }

  Widget _buildMediaTabView(BuildContext context, ApiStates state, ApiBloc apiBloc) {
    List<Gallery>? galleryList = AppDataModel.getMediaTabList();

    return galleryList.length > 0
            ? GridView.builder(
                padding: EdgeInsets.only(left: 24, right: 24, bottom: 24),
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisSpacing: 16,
                  crossAxisCount: 3,
                  childAspectRatio: 1,
                  mainAxisSpacing: 16
                ),
                shrinkWrap: true,
                itemCount: galleryList.length,
                itemBuilder: (context, index) {
                  Gallery gallery = galleryList[index];

                  return GestureDetector(
                    onTap: () async {
                      // await openDocumentInApp(apiBloc, gallery.media ?? "");
                      goToNextNamedRoute(
                        context,
                        galleryScreen,
                        args: MediaScreenArgs(
                          mediaList: galleryList.map((e) {
                            return MediaList(
                              mediaID: e.mediaID,
                              url: e.media,
                              fileType: e.fileType,
                              fileExtension: e.mediaType,
                              mediaWithWatermark: e.mediaWithWatermark,
                              thumbnail: e.thumbnail
                            );
                          }).toList(),
                          index: index,
                          isGallery: false,
                        ),
                      );
                    },
                    child: Container(
                      child: AspectRatio(
                        aspectRatio: 1,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: Stack(
                            children: [
                              CachedNetworkImage(
                                imageUrl: (gallery.fileType == "video" ? gallery.thumbnail : gallery.media) ?? "",
                                fit: BoxFit.cover,
                                width: 250,
                                height: 250,
                                alignment: Alignment.center,
                                maxWidthDiskCache: 300,
                                maxHeightDiskCache: 300,
                              ),
                              if (gallery.fileType == "video")
                                Container(
                                  width: SizeUtils.width(context, 100),
                                  height: 250,
                                  alignment: Alignment.center,
                                  color: black.withOpacity(0.5),
                                  child: Image.asset(ic_play, height: 50, width: 50),
                                )
                            ],
                          )
                        ),
                      ),
                    ),
                  );
                },
              )
            : Container(
                alignment: Alignment.center,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Lottie.asset(
                      no_document_lottie,
                      height: 250,
                      width: 250,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20, right: 20, top: 10),
                      child: Text(
                        S().theres_no_media_yet,
                        textAlign: TextAlign.center,
                        style: regularFontsStyle(fontSize: mediumFont),
                      ),
                    ),
                    SizedBox(height: 30)
                  ],
                ),
              );
  }

  Widget _buildDocTabView(BuildContext context, ApiStates state, ApiBloc apiBloc) {
    List<Document>? documentList = AppDataModel.getDocumentList();
    int cIndex = 0;

    return documentList.length > 0
            ? ListView.builder(
                shrinkWrap: true,
                itemCount: documentList.length,
                itemBuilder: (context, index) {
                  Document document = documentList[index];
                  if (state is DownloadLoaderStates) {
                    document.isLoad = state.isLoad;
                  }
                  // bool isLoad = false;
                  return ElevatedButton(
                    onPressed: () async {
                      cIndex = index;
                      await openDocumentInApp(apiBloc, document.media ?? "");
                    },
                    style: defaultButtonStyle(),
                    child: Container(
                      margin: EdgeInsets.only(left: 6, right: 6),
                      padding: EdgeInsets.only(top: 20, bottom: 20),
                      decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(color: lightGrey, width: 1)),
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Image.asset(
                                ic_document,
                                height: 22,
                                width: 22,
                              ),
                              SizedBox(width: 20),
                              Expanded(
                                child: Text(
                                  "${document.mediaName}.${document.mediaType}",
                                  style: boldFontsStyle(fontSize: bigFonts),
                                ),
                              ),
                            ],
                          ),
                          document.isLoad != null &&
                                  document.isLoad! &&
                                  index == cIndex
                              ? Container(
                                  margin: EdgeInsets.only(top: 5),
                                  child: Lottie.asset(
                                    login_black_loading,
                                    height: 40,
                                    width: 120,
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                    ),
                  );
                },
              )
            : Container(
                alignment: Alignment.center,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Lottie.asset(
                      no_document_lottie,
                      height: 250,
                      width: 250,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20, right: 20, top: 10),
                      child: Text(
                        S().theres_no_document_yet,
                        textAlign: TextAlign.center,
                        style: regularFontsStyle(fontSize: mediumFont),
                      ),
                    ),
                    SizedBox(height: 30)
                  ],
                ),
              );
  }
}
