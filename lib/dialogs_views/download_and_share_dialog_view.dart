import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/bloc/download_bloc/download_bloc.dart';
import 'package:pmaclient/bloc/download_bloc/download_events.dart';
import 'package:pmaclient/bloc/download_bloc/download_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_fonts.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/widgets/rounded_button.dart';
import 'package:share_plus/share_plus.dart';

class DownloadAndShareDialogView extends StatelessWidget {
  final String? title;
  final String? successMessage;
  final bool isAlbum;
  final bool isDownload;
  final bool isVideo;
  final String? url;
  final String? fileType;
  final List? mediaList;

  const DownloadAndShareDialogView({
    Key? key,
    this.title,
    this.successMessage,
    this.isAlbum = false,
    this.url,
    this.fileType = "image",
    this.isDownload = true,
    this.isVideo = false,
    this.mediaList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isCompleted = false;
    bool isLoading = false;
    DownloadBloc downloadBloc = DownloadBloc();
    return Material(
      child: BlocBuilder<DownloadBloc, DownloadStates>(
        bloc: downloadBloc,
        builder: (context, states) {
          if (states is DownloadCompleteStates) {
            isCompleted = states.isCompleted;
            isLoading = states.isLoading;
          }
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                margin: EdgeInsets.only(top: 10),
                alignment: Alignment.center,
                child: Container(
                  width: 40,
                  height: 3,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    color: lightGrey,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                alignment: Alignment.center,
                child: Text(
                  isCompleted ? "" : title ?? S().download,
                  style: boldFontsStyle(
                    fontFamily: platForm,
                    fontSize: bigLargeFont,
                  ),
                ),
              ),
              isLoading
                  ? Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.all(20),
                      child: Lottie.asset(
                        login_black_loading,
                        height: 45,
                        width: 120,
                      ),
                    )
                  : isCompleted
                      ? Container(
                          margin: EdgeInsets.only(bottom: 35),
                          alignment: Alignment.center,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Image.asset(
                                ic_success,
                                height: 50,
                                width: 50,
                              ),
                              SizedBox(height: 10),
                              Text(
                                successMessage ?? S().downloaded,
                                style: boldFontsStyle(fontSize: mediumFont),
                              )
                            ],
                          ),
                        )
                      : Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                  top: 25, left: 20, right: 20, bottom: 20),
                              child: RoundedButton(
                                borderColor: black,
                                buttonColor: white,
                                onPressed: () async {
                                  downloadBloc.add(
                                    DownloadProgressEvent(
                                      isLoad: true,
                                      isComplete: false,
                                    ),
                                  );
                                  final filePath =
                                      await downloadImage(url ?? "");

                                  try {
                                    if (isDownload) {
                                      isVideo
                                          ? await GallerySaver.saveVideo(
                                              filePath)
                                          : await GallerySaver.saveImage(
                                              filePath);
                                    }
                                    if (!isDownload) {
                                      Share.shareFiles([filePath]);
                                    }
                                    downloadBloc.add(DownloadProgressEvent(
                                        isLoad: false, isComplete: true));
                                    Timer(Duration(seconds: 1), () {
                                      Navigator.pop(context);
                                    });
                                  } catch (e) {
                                    log("$e");
                                    downloadBloc.add(
                                      DownloadProgressEvent(
                                        isLoad: false,
                                        isComplete: false,
                                      ),
                                    );
                                    showToast("$e");
                                  }
                                },
                                buttonText:
                                    isVideo ? S().this_video : S().this_photo,
                              ),
                            ),
                            isAlbum
                                ? Container(
                                    margin: EdgeInsets.only(
                                        left: 20, right: 20, bottom: 20),
                                    child: RoundedButton(
                                      borderColor: black,
                                      buttonColor: white,
                                      onPressed: () async {
                                        downloadBloc.add(DownloadProgressEvent(
                                            isLoad: true, isComplete: false));
                                        List<String> filePathList = [];
                                        for (var gallery in mediaList!) {
                                          String filePath = await downloadImage(
                                            gallery.mediaWithWatermark ?? "",
                                          );
                                          if (isDownload) {
                                            try {
                                              // String path = '/storage/emulated/0/Pictures/';
                                              gallery.fileType == "video"
                                                  ? await GallerySaver
                                                      .saveVideo(
                                                      filePath,
                                                      albumName: "pmaClient",
                                                    )
                                                  : await GallerySaver
                                                      .saveImage(
                                                      filePath,
                                                      albumName: "pmaClient",
                                                    );
                                            } catch (e) {
                                              showToast("$e");
                                              downloadBloc.add(
                                                DownloadProgressEvent(
                                                  isLoad: false,
                                                  isComplete: false,
                                                ),
                                              );
                                            }
                                          } else {
                                            filePathList.add(filePath);
                                          }
                                        }
                                        downloadBloc.add(
                                          DownloadProgressEvent(
                                            isLoad: false,
                                            isComplete: true,
                                          ),
                                        );
                                        Timer(Duration(seconds: 1), () {
                                          if (!isDownload) {
                                            Share.shareFiles(filePathList);
                                          }
                                          Navigator.pop(context);
                                        });
                                      },
                                      buttonText: S().the_whole_album,
                                    ),
                                  )
                                : Container(),
                          ],
                        ),
            ],
          );
        },
      ),
    );
  }
}
