import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';

class InfoTabScreen extends StatefulWidget {
  const InfoTabScreen({Key? key}) : super(key: key);

  @override
  _InfoTabScreenState createState() => _InfoTabScreenState();
}

class _InfoTabScreenState extends State<InfoTabScreen> {
  late ApiBloc apiBloc;
  ProjectDetailsData? model;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    apiBloc = BlocProvider.of<ApiBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ApiBloc, ApiStates>(
      bloc: apiBloc,
      builder: (context, state) {
        if (state is LogoutApiDataLoadState) {
          SpManager.logoutDeleteData();
          goAndClearAllPageNamed(context, loginOtpScreen);
        } else if (state is ProjectDetailsApiDataLoadState) {
          model = state.model?.response;
        }
        List<Info> infoList = model?.infoList ?? [];

        return Container(
          margin: EdgeInsets.only(bottom: 25, left: 25, right: 25, top: 25),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Wrap(
                  spacing: 16.0,
                  runSpacing: 24.0,
                  children: [
                    for (Info info in infoList)
                      if (info.infoDisplayValue != null && info.infoDisplayValue != '')
                        LayoutBuilder(
                          builder: (BuildContext context, BoxConstraints constraints) {
                            return Container(
                              width: (constraints.maxWidth / 2) - 8.0,
                              child: Container(
                                width: SizeUtils.width(context, 50),
                                child: columnTextView(
                                  title: info.infoLabel,
                                  value: info.infoDisplayValue,
                                ),
                              )
                            );
                          }
                        ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Widget columnTextView({title, value}) => Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title ?? "",
            style: regularFontsStyle(
              color: grey,
              fontSize: mediumFont,
            ),
          ),
          SizedBox(height: 10),
          Text(
            value ?? "",
            style: boldFontsStyle(fontSize: bigFonts, height: 1.3),
          )
        ],
      );
}
