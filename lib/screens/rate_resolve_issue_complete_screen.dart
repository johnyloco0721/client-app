import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/feedback_model.dart';
import 'package:pmaclient/models/projects_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/views/header_logo_view.dart';

class RateResolveIssueCompleteScreen extends StatefulWidget {
  const RateResolveIssueCompleteScreen({Key? key}) : super(key: key);

  @override
  _RateResolveIssueCompleteScreenState createState() =>
      _RateResolveIssueCompleteScreenState();
}

class _RateResolveIssueCompleteScreenState
    extends State<RateResolveIssueCompleteScreen> {
  late HomeProject homeProjectModel;
  late FeedBackResponseData feedBackResponseData;
  int status = 0;
  ApiBloc? apiBloc;

  @override
  void initState() {
    // TODO: implement initState
    homeProjectModel = AppDataModel.getHomeProjectModel();
    feedBackResponseData = AppDataModel.getFeedBackModel();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    status = ModalRoute.of(context)!.settings.arguments as int;
    apiBloc = BlocProvider.of<ApiBloc>(context);

    print(status);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => backClick(),
      child: Scaffold(
        backgroundColor: black,
        appBar: AppBar(
          toolbarHeight: 0,
          backgroundColor: black,
          systemOverlayStyle: SystemUiOverlayStyle.light,
        ),
        body: SafeArea(
          bottom: false,
          child: mainLayout(),
        ),
      ),
    );
  }

  Widget mainLayout() => Container(
        color: white,
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            headerLayoutWithCross(
              context,
              S().rate_us,
              backClick: () => backClick(),
            ),
            Container(
              padding: EdgeInsets.only(left: 20, bottom: 20),
              color: black,
              width: SizeUtils.width(context, 100),
              child: Text(
                "${homeProjectModel.propertyName} (${homeProjectModel.projectUnit})",
                style: boldFontsStyle(fontSize: mediumFont, color: white),
              ),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      status == 2
                          ? Container()
                          : Container(
                              margin:
                                  EdgeInsets.only(top: 30, right: 50, left: 50),
                              child: Lottie.asset(
                                nps_thank_you_lottie,
                                height: 200,
                                width: 200,
                              ),
                            ),
                      Container(
                        margin: EdgeInsets.only(top: 10, right: 40, left: 40),
                        child: Text(
                          status == 1
                              ? S().thank_you_for_your_time
                              : S().thank_you,
                          textAlign: TextAlign.center,
                          style: boldFontsStyle(fontSize: bigFonts),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10, right: 40, left: 40),
                        child: Text(
                          status == 1
                              ? S().thank_you_our_team_will_be
                              : S().your_feedback_is_highly_appreciated_and_we,
                          textAlign: TextAlign.center,
                          style: regularFontsStyle(fontSize: mediumFont),
                        ),
                      ),
                      status == 2
                          ? Container(
                              margin:
                                  EdgeInsets.only(top: 20, right: 40, left: 40),
                              child: RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: feedBackResponseData.phoneNumber ??
                                          "",
                                      style: boldFontsStyle(
                                        fontSize: mediumFont,
                                      ),
                                    ),
                                  ],
                                  text: S()
                                      .if_you_required_any_further_help_kindly_contact,
                                  style: regularFontsStyle(
                                    fontSize: mediumFont,
                                  ),
                                ),
                                textAlign: TextAlign.center,
                              ),
                            )
                          : Container(),
                      feedBackResponseData.successMessage != null
                          ? Container(
                              margin: EdgeInsets.only(
                                top: 50,
                                right: 40,
                                left: 40,
                                bottom: 50,
                              ),
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                color: whiteGrey,
                                borderRadius: BorderRadius.circular(12),
                                border: Border.all(color: lightGrey),
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Lottie.asset(
                                    nps_gift_lottie,
                                    height: 100,
                                    width: 100,
                                  ),
                                  SizedBox(width: 10),
                                  Expanded(
                                    child: HtmlWidget(
                                      feedBackResponseData.successMessage ?? "",
                                      textStyle: regularFontsStyle(
                                        fontSize: mediumFont,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          : Container(
                              margin: EdgeInsets.only(
                                top: 50,
                                right: 40,
                                left: 40,
                                bottom: 50,
                              ),
                            ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );

  backClick() {
    apiBloc?.add(ProjectsApiEvent(pageNo: 1));
    Navigator.popUntil(context, ModalRoute.withName(homeScreen));
    return true;
  }
}
