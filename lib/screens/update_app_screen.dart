import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_constants.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_fonts.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/widgets/rounded_button.dart';
import 'package:url_launcher/url_launcher.dart';

class UpdateAppScreen extends StatelessWidget {
  const UpdateAppScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.dark);
    return Scaffold(
      backgroundColor: white,
      body: SafeArea(
        bottom: false,
        child: mainLayout(context),
      ),
    );
  }

  Widget mainLayout(BuildContext context) => Container(
        height: SizeUtils.height(context, 100),
        width: SizeUtils.width(context, 100),
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        margin: EdgeInsets.all(25),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(top: 15),
              width: SizeUtils.width(context, 40),
              child: AspectRatio(
                aspectRatio: 245.58 / 69.05,
                child: Image.asset(
                  ic_makeover_logo,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Lottie.asset(update_app_lottie, height: 250, width: 250),
                    SizedBox(height: 20),
                    Text(
                      S().hi_please_update_your_app,
                      textAlign: TextAlign.center,
                      style: boldFontsStyle(
                          fontSize: bigLargeFont, fontFamily: platForm),
                    ),
                    SizedBox(height: 15),
                    Text(
                      S().update_your_app_so_that,
                      textAlign: TextAlign.center,
                      style: regularFontsStyle(fontSize: bigFonts),
                    ),
                    SizedBox(height: 25),
                    RoundedButton(
                      buttonText: S().update_now,
                      buttonColor: yellow,
                      onPressed: () async => launchURL(
                        await canLaunch(AppDataModel.getUpdateAppLink()) ? await launch(AppDataModel.getUpdateAppLink()) : throw 'Could not launch ${AppDataModel.getUpdateAppLink()}'
                      ),
                      width: 220,
                    ),
                    SizedBox(height: 25),
                  ],
                ),
              ),
            )
          ],
        ),
      );
}
