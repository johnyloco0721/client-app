import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/maintenance_model.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/route_utils.dart';
import '../route_management/app_routes_names.dart';


class MaintenanceScreen extends StatefulWidget {
  @override
  _MaintenanceViewState createState() => _MaintenanceViewState();
}

class _MaintenanceViewState extends State<MaintenanceScreen> {
  ApiBloc apiBloc = ApiBloc();
  MaintenanceModel? maintenanceModel;

  int _topLeftTapCount = 0;
  int _bottomRightTapCount = 0;
  String _bypassCode = '';

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: apiBloc,
      listener: (BuildContext context, state) {
        if (state is MaintenanceByPassApiDataLoadState) {
          maintenanceModel = state.model;
          AppDataModel.getByPassParameter(maintenanceModel!.parameter.toString());
          AppDataModel.getByPassHash(maintenanceModel!.hash.toString());
          AppDataModel.setByPassCode(true);
          goAndClearAllPageNamed(context, loginOtpScreen);
        } else if (state is MaintenanceByPassApiErrorState) {
          _showErrorMessageDialog();
        }
      },
    child: Scaffold(
      backgroundColor: white,
      body: Stack(
        children: [
           GestureDetector(
            onDoubleTap: _handleContainer1DoubleTap,
          child: Align(
            alignment: Alignment.topLeft,
            child: Container(
              margin: EdgeInsets.only(top: 50, left: 10),
              width: 150,
              height: 150,
              color: Colors.white,
              child: Center(
                child: Text(
                  '',
                  style: Theme.of(context).textTheme.headline6!.copyWith(
                        color: Colors.white,
                      ),
                ),
              ),
            ),
            ),
          ),
          GestureDetector(
            onDoubleTap: _handleContainer2DoubleTap,
            child: Align(
              alignment: Alignment.bottomRight,
              child: Container(
                margin: EdgeInsets.only(bottom: 50, right: 10),
                width: 150,
                height: 150,
                color: Colors.white,
                child: Center(
                  child: Text(
                    '',
                    style: Theme.of(context).textTheme.headline6!.copyWith(
                          color: Colors.white,
                        ),
                  ),
                ),
              ),
            ),
          ),
          Positioned.fill(
            bottom: MediaQuery.of(context).size.height * 0.2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  ic_makeover_logo,
                  width: MediaQuery.of(context).size.width * 0.39,
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * 0.4),
                ),
                Lottie.asset(maintenance_lottie, height: 250, width: 250),
                Container(
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Column(
                    children: [
                      Text(
                        S().sorry_the_app_is_under_maintenance,
                        textAlign: TextAlign.center,
                        style: regularFontsStyle(fontSize: bigFonts),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    ),
    );
  }
  void _handleContainer1DoubleTap() {
    setState(() {
      _topLeftTapCount=2;
      if (_topLeftTapCount == 2 && _bottomRightTapCount == 2) {
        _bottomRightTapCount=0;
      }
    });
  }

  void _handleContainer2DoubleTap() {
    setState(() {
      _bottomRightTapCount=2;
      if(_topLeftTapCount == 0 && _bottomRightTapCount == 2){
        _bottomRightTapCount=0;
        _topLeftTapCount=0;
      }else if(_topLeftTapCount == 2 && _bottomRightTapCount == 2){
        _bottomRightTapCount=0;
        _topLeftTapCount=0;
        // Execute your desired action after double tap on both containers
        _showBypassCodeDialog();
      }
    });
  }

  void _showErrorMessageDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.fromLTRB(40, 45, 40, 75),
          title: Container(
            width: double.maxFinite,
            height: 70,
            child: Text('Invalid Bypass Code'),
          ),
          content: Text('Please enter a valid bypass code.'),
          actions: [
            TextButton(
              child: Text(
                'OK',
                style: TextStyle(color: darkYellow),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

// ByPassCode Alert Box
  void _showBypassCodeDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
           contentPadding: EdgeInsets.fromLTRB(40, 40, 40, 70),
          title: Container(
             width: double.maxFinite, // Expand the width to maximum
             height: 50,
             child: Text('Enter Bypass Code'),
          ),
          content: TextFormField(
            decoration: InputDecoration(
              hintText: 'Bypass Code',
            ),
            onChanged: (value) {
              setState(() {
                _bypassCode = value; 
              });
            },
          ),
          actions: [
            TextButton(
              child: Text('Cancel', style: TextStyle(color: black),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Padding(
                padding: EdgeInsets.only(right: 20),
                child: Text('Submit', style: TextStyle(color: darkYellow),),
              ),
              onPressed: () async {
                apiBloc.add(MaintenanceByPassApiEvent(
                  password: _bypassCode,
                ));
              },
            ),
          ],
        );
      },
    );
  }
}