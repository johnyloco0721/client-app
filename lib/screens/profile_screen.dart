import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_bloc.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_events.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_fonts.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/user_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/screens/profile_preview_screen.dart';
import 'package:pmaclient/shimmer_items/profile_shimmer_item.dart';
import 'package:pmaclient/utils/dialog_utils.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/views/network_blur_image_view.dart';
import 'package:pmaclient/widgets/rounded_button.dart';
import 'package:pmaclient/widgets/shimmer_widget.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _picker = ImagePicker();
  String? profileImage;
  String? pickImage;
  late ProfileBloc profileBloc;
  UserModel? userData;
  late ApiBloc apiBloc;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    userData = SpManager.getUserdata();
    getProfileApi();
  }

  @override
  Widget build(BuildContext context) {
    profileBloc = BlocProvider.of<ProfileBloc>(context);
    return Scaffold(
      backgroundColor: black,
      appBar: AppBar(
        toolbarHeight: 0,
        backgroundColor: black,
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      body: SafeArea(
        bottom: false,
        child: RefreshIndicator(
          color: darkYellow,
          onRefresh: () async {
            getProfileApi();
          },
          child: AbsorbPointer(
            absorbing: isLoading,
            child: mainLayout(),
          ),
        ),
      ),
    );
  }

  Widget mainLayout() => Container(
        color: white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            headerLayoutWithCross(context, S.of(context).profile),
            Expanded(
              child: BlocConsumer<ApiBloc, ApiStates>(
                bloc: apiBloc,
                listener: (context, state) {
                  if (state is ApiErrorState) {
                    showToast(
                      state.errorMessage ?? S().server_error_please_try_again,
                    );
                  } else if (state is LogoutApiDataLoadState) {
                    SpManager.logoutDeleteData();
                    goAndClearAllPageNamed(context, loginOtpScreen);
                  } else if (state is ProfileApiDataLoadState) {
                    userData = state.model;
                    profileImage = userData?.avatar;
                    SpManager.saveUser(userData!.toJson());
                    profileBloc.add(
                      UserDataChangeEvent(SpManager.getUserdata()),
                    );
                  } else if (state is LogoutApiLoadingState) {
                    isLoading = state.isEnabled;
                  } else if (state is ProfileApiErrorState) {
                    showToast(
                      state.errorMessage ?? S().server_error_please_try_again,
                    );
                  }
                },
                builder: (context, state) {
                  return state is ApiLoadingState
                      ? ShimmerWidget(
                          widget: ProfileShimmerItem(),
                          isWidget: true,
                        )
                      : SingleChildScrollView(
                          physics: ClampingScrollPhysics(),
                          child: Container(
                            margin: EdgeInsets.only(left: 20, right: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                userImageAndNameView(),
                                Container(
                                  margin: EdgeInsets.only(top: 25),
                                  child: titleAndValueWidget(
                                    title: S.of(context).name,
                                    value: userData?.firstName ?? "",
                                    viewWidth: SizeUtils.width(context, 100),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 25),
                                  child: titleAndValueWidget(
                                    title: S.of(context).email_address,
                                    value: userData?.email ?? "",
                                    viewWidth: SizeUtils.width(context, 100),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 25),
                                  child: titleAndValueWidget(
                                    title: S.of(context).mobile_number,
                                    value: userData?.contactNo ?? "",
                                    viewWidth: SizeUtils.width(context, 100),
                                  ),
                                ),
                                SizedBox(height: 25),
                                InkWell(
                                  onTap: () => goToNextNamedRoute(
                                    context,
                                    termsScreen,
                                  ),
                                  child: textIconButtonView(
                                    S.of(context).terms_of_service,
                                  ),
                                ),
                                InkWell(
                                  onTap: () => goToNextNamedRoute(
                                      context, appSupportScreen),
                                  child: textIconButtonView(
                                    S.of(context).app_support,
                                    isBorderTop: false,
                                  ),
                                ),
                                logoutButton(),
                                // resetPasswordTextButton(),
                                FutureBuilder(
                                  future: PackageInfo.fromPlatform(),
                                  builder: (context, AsyncSnapshot<PackageInfo> snapdata){
                                    String? version = snapdata.data?.version;
                                    String? buildNumber = snapdata.data?.buildNumber;
                                    String? dateTime = formatDate(date: DateTime.now().toString(), format: 'yyyyMMdd');
                                    return buildNumberWidget('Build: $version.$dateTime.$buildNumber');
                                  }
                                ),
                                SizedBox(height: 15),
                              ],
                            ),
                          ),
                        );
                },
              ),
            ),
          ],
        ),
      );

  Widget userImageAndNameView() => Container(
        margin: EdgeInsets.only(bottom: 25),
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            userImageView(),
            userNameTextView(),
          ],
        ),
      );

  Widget userImageView() => BlocProvider(
        create: (_) => profileBloc,
        child: BlocBuilder<ProfileBloc, ProfileStates>(
          bloc: profileBloc,
          builder: (context, state) {
            if (state is ProfileImageSetState) {
              profileImage = state.image;
            }
            return Container(
              margin: EdgeInsets.only(top: 25, bottom: 15),
              height: 120,
              clipBehavior: Clip.hardEdge,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: lightGrey, width: 1),
              ),
              child: InkWell(
                onTap: () => imageSelectionDialog(
                  context,
                  photoClick: () => getImage(ImageSource.gallery),
                  cameraClick: () => getImage(ImageSource.camera),
                ),
                child: Card(
                  margin: EdgeInsets.zero,
                  elevation: 0,
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  shape: CircleBorder(),
                  child: AspectRatio(
                    aspectRatio: 512 / 512,
                    child: profileImage?.isEmpty ?? true
                        ? Image.asset(
                            ic_profile_dummy,
                            fit: BoxFit.fill,
                          )
                        : profileImage!.contains("http")
                            ? NetworkBlurHashImageView(
                                imageUrl: profileImage ?? "",
                              )
                            : Image.file(
                                File(state.image ?? ""),
                                fit: BoxFit.fill,
                              ),
                  ),
                ),
              ),
            );
          },
        ),
      );

  Widget userNameTextView() => Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            "${userData?.firstName} ${userData?.lastName}",
            style: boldFontsStyle(fontFamily: platForm, fontSize: bigLargeFont),
          ),
          SizedBox(width: 10),
          InkWell(
            onTap: () => imageSelectionDialog(
              context,
              photoClick: () => getImage(ImageSource.gallery),
              cameraClick: () => getImage(ImageSource.camera),
            ),
            child: Image.asset(ic_camera, height: 24, width: 24),
          )
        ],
      );

  Widget titleAndValueWidget({title, value, viewWidth}) => Container(
        width: viewWidth ?? SizeUtils.width(context, 50),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: regularFontsStyle(color: grey, fontSize: mediumFont),
            ),
            SizedBox(height: 10),
            Text(
              value,
              style: boldFontsStyle(fontSize: bigFonts),
            ),
          ],
        ),
      );

  Widget textIconButtonView(title, {isBorderTop = true}) => Container(
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: 25, bottom: 25),
        decoration: BoxDecoration(
          border: Border(
            top: isBorderTop
                ? BorderSide(color: lightGrey, width: 0.5)
                : BorderSide.none,
            bottom: BorderSide(color: lightGrey, width: 0.5),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: boldFontsStyle(fontSize: bigFonts),
            ),
            SizedBox(width: 10),
            Image.asset(ic_next, height: 10, width: 20),
          ],
        ),
      );

  Widget logoutButton() => Container(
        margin: EdgeInsets.only(top: 50),
        alignment: Alignment.center,
        child: RoundedButton(
          buttonText: S.of(context).logout,
          width: 200,
          height: 50,
          borderColor: black,
          borderWidth: borderWidth,
          enabled: !isLoading,
          isLoading: isLoading,
          buttonColor: white,
          fontSize: bigFonts,
          onPressed: () => apiBloc.add(LogoutApiEvent()),
        ),
      );

  Widget resetPasswordTextButton() => Container(
        alignment: Alignment.center,
        child: TextButton(
          onPressed: () => goToNextNamedRoute(context, changePasswordScreen),
          child: Text(
            S().change_password,
            style: boldFontsStyle(fontSize: bigFonts),
          ),
        ),
      );

  Widget buildNumberWidget(String buildText) => Container(
        alignment: Alignment.center,
        margin: EdgeInsets.symmetric(vertical: 15.0),
        child: Text(
          buildText,
          style: regularFontsStyle(fontSize: smallFont, color: lightGrey),
        ),
      );

  Future getImage(ImageSource sources) async {
    Navigator.pop(context);
    try {
      final galImage = await _picker.pickImage(
        source: sources,
        imageQuality: 90,
      );
      pickImage = galImage?.path;
      if (pickImage!.isNotEmpty) {
        goToNormalPage(context, ProfilePreviewScreen(image: pickImage));
      }
    } catch (e) {
      print("getImage $sources : $e");
    }
  }

  void getProfileApi() {
    apiBloc = BlocProvider.of<ApiBloc>(context);
    apiBloc.add(ProfileApiEvent());
  }
}
