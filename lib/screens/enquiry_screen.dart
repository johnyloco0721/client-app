import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/list_items/inquiry_list_items.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';

class EnquiryScreen extends StatefulWidget {
  const EnquiryScreen({Key? key}) : super(key: key);

  @override
  _EnquiryScreenState createState() => _EnquiryScreenState();
}

class _EnquiryScreenState extends State<EnquiryScreen> {
  late ApiBloc apiBloc;
  List<Support>? inquiryList = [];

  @override
  void initState() {
    super.initState();
    apiBloc = BlocProvider.of<ApiBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.light);
    return Scaffold(
      backgroundColor: black,
      body: SafeArea(
        bottom: false,
        child: mainLayout(),
      ),
    );
  }

  Widget mainLayout() => Container(
        color: white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            headerLayoutWithBackArrow(context, S().property_enquiry),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: white,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(50),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      inquiryQuestionTextView(),
                      supportListView(),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      );

  Widget inquiryQuestionTextView() => Container(
        margin: EdgeInsets.only(top: 25, left: 20, right: 20),
        child: Text(
          S().having_any_questions_about,
          textAlign: TextAlign.center,
          style: regularFontsStyle(fontSize: bigFonts),
        ),
      );

  Widget supportListView() => BlocBuilder<ApiBloc, ApiStates>(
        bloc: apiBloc,
        builder: (context, state) {
          if (state is LogoutApiDataLoadState) {
            SpManager.logoutDeleteData();
            goAndClearAllPageNamed(context, loginOtpScreen);
          } else if (state is ProjectDetailsApiDataLoadState) {
            inquiryList = state.model?.response?.supports;
          }
          return Container(
            margin: EdgeInsets.only(top: 25, left: 20, right: 20),
            child: inquiryList!.length > 0
                ? ListView.builder(
                    itemCount: inquiryList?.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      Support model = inquiryList![index];
                      return InquiryListItem(
                        companyName: model.companyName,
                        designationName: model.designationName,
                        teamMemberName: model.teamMemberName,
                        teamMemberAvatar: model.teamMemberAvatar,
                        whatsappLink: model.whatsappLink,
                      );
                    },
                  )
                : Container(),
          );
        },
      );
}
