import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/rounded_button.dart';

class ResetSuccessScreen extends StatelessWidget {
  const ResetSuccessScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.dark);
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: mainLayout(context),
      ),
    );
  }

  Widget mainLayout(BuildContext context) => Container(
        margin: EdgeInsets.only(
            top: SizeUtils.height(context, 5), bottom: 20, left: 20, right: 20),
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              welcomeToMakeoverGuysImage(context),
              emailSentTextView(context),
              checkEmailTextView(context),
              backToLoginButton(context),
            ],
          ),
        ),
      );

  Widget emailSentTextView(BuildContext context) => Container(
        margin: EdgeInsets.only(top: 15),
        child: Text(
          S.of(context).reset_password_successful,
          style: boldFontsStyle(fontSize: bigFonts, height: 1.3),
        ),
      );

  Widget checkEmailTextView(BuildContext context) => Container(
        margin: EdgeInsets.only(top: 5),
        child: Text(
          S.of(context).please_login_with_your_new_password,
          style: regularFontsStyle(fontSize: bigFonts, height: 1.3),
        ),
      );

  Widget backToLoginButton(BuildContext context) => Container(
        margin: EdgeInsets.only(top: 50),
        child: RoundedButton(
          onPressed: () {
            SpManager.setLogin(false);
            goAndClearAllPageNamed(context, loginOtpScreen);
          },
          buttonText: S.of(context).login_now,
          width: SizeUtils.width(context, 100),
          fontSize: bigFonts,
        ),
      );
}
