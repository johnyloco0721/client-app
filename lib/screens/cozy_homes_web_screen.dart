import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_constants.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CozyHomesWebScreen extends StatefulWidget {
  const CozyHomesWebScreen({Key? key}) : super(key: key);

  @override
  _CozyHomesWebScreenState createState() => _CozyHomesWebScreenState();
}

class _CozyHomesWebScreenState extends State<CozyHomesWebScreen> {
  bool isProgress = true;
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  WebViewController? webViewController;
  var gestureRecognizers = [
    Factory(() => EagerGestureRecognizer()),
  ].toSet();

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.light);
    return Scaffold(
      backgroundColor: black,
      body: SafeArea(
        bottom: false,
        child: mainLayout(),
      ),
    );
  }

  Widget mainLayout() => Container(
        color: white,
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            headerLayoutWithCross(context, S().cozyHomes),
            Expanded(
              child: Container(
                color: white,
                child: Stack(
                  children: [
                    webView(),
                  ],
                ),
              ),
            ),
          ],
        ),
      );

  Widget webView() => Container(
        margin: EdgeInsets.only(top: 10),
        child: WebView(
          debuggingEnabled: true,
          initialUrl: cozyHomesUrl,
          allowsInlineMediaPlayback: true,
          javascriptMode: JavascriptMode.unrestricted,
          gestureRecognizers: gestureRecognizers,
          onWebViewCreated: (WebViewController controller) {
            _controller.complete(controller);
            webViewController = controller;
          },
          onPageFinished: (url) async {
            await webViewController?.canGoBack();
          },
          gestureNavigationEnabled: true,
        ),
      );
}
