import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/forgot_password_model.dart';
import 'package:pmaclient/screens/login_otp_screen.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/rounded_button.dart';

class EmailSendScreen extends StatefulWidget {
  final String? email;

  const EmailSendScreen({Key? key, this.email}) : super(key: key);

  @override
  _EmailSendScreenState createState() => _EmailSendScreenState();
}

class _EmailSendScreenState extends State<EmailSendScreen> {
  late ApiBloc apiBloc;
  ForgotPasswordModel? model;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    apiBloc = BlocProvider.of<ApiBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor();
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: BlocBuilder<ApiBloc, ApiStates>(
          bloc: apiBloc,
          builder: (context, state) {
            if (state is ForgotPasswordApiDataLoadState) {
              model = state.model;
            }
            return mainLayout();
          },
        ),
      ),
    );
  }

  Widget mainLayout() => Container(
        margin: EdgeInsets.only(
            top: SizeUtils.height(context, 5), bottom: 20, left: 20, right: 20),
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              welcomeToMakeoverGuysImage(context),
              emailSentTextView(),
              checkEmailTextView(),
              backToLoginButton(),
            ],
          ),
        ),
      );

  Widget emailSentTextView() => Container(
        margin: EdgeInsets.only(top: 15),
        child: Row(
          children: [
            Text(
              S.of(context).we_sent_a_link_to,
              style: regularFontsStyle(fontSize: bigFonts, height: 1.3),
            ),
            SizedBox(width: 2),
            Text(
              model?.email ?? "",
              style: boldFontsStyle(fontSize: bigFonts, height: 1.3),
            ),
          ],
        ),
      );

  Widget checkEmailTextView() => Container(
        margin: EdgeInsets.only(top: 18),
        child: Text(
          S.of(context).please_check_your_email_and_click,
          style: regularFontsStyle(fontSize: bigFonts, height: 1.3),
        ),
      );

  Widget backToLoginButton() => Container(
        margin: EdgeInsets.only(top: 50),
        child: RoundedButton(
          onPressed: () => goAndClearCurrentScreen(context, LoginOtpScreen()),
          buttonText: S.of(context).back_to_login_screen,
          width: SizeUtils.width(context, 100),
          fontSize: bigFonts,
        ),
      );
}
