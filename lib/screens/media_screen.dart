import 'dart:io';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:photo_view/photo_view.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/dialogs_views/download_and_share_dialog_view.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:pmaclient/route_management/media_screen_args.dart';
import 'package:pmaclient/utils/dialog_utils.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/round_icon_button.dart';
import 'package:pmaclient/widgets/video_player_widget.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

class MediaScreen extends StatefulWidget {
  final List<MediaList>? mediaList;
  final int index;
  final MediaScreenArgs? mediaScreenArgs;

  const MediaScreen({
    Key? key,
    this.mediaList,
    this.index = 0,
    this.mediaScreenArgs,
  }) : super(key: key);

  @override
  _MediaScreenState createState() => _MediaScreenState();
}

class _MediaScreenState extends State<MediaScreen> {
  // MediaList? args;
  File? file;
  bool isLoading = false;
  bool isComplete = false;
  Uint8List? unit8Lit;
  List mediaList = [];
  int pageIndex = 0;
  AutoScrollController autoScrollController = AutoScrollController();
  PageController? pageController;

  MediaList? args;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pageIndex = widget.mediaScreenArgs?.index ?? 0;
    mediaList = widget.mediaScreenArgs?.mediaList ?? [];
    pageController = PageController(initialPage: pageIndex);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    args = ModalRoute.of(context)!.settings.arguments as MediaList;
    return Scaffold(
      backgroundColor: black,
      appBar: AppBar(
        toolbarHeight: 0,
        backgroundColor: black,
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      body: SafeArea(
        bottom: false,
        child: mainLayout(),
      ),
    );
  }

  Widget mainLayout() => Container(
        color: lightBlack,
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            headerLayoutWithBackArrow(context, S().media),
            Container(
              height: 100,
              alignment: Alignment.centerRight,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // TODO: Temporary hide download button
                  roundIconButton(
                    icon: ic_download,
                    iconColor: black,
                    onPressed: () async {
                      openDownloadOrShareBottomSheet(
                        context,
                        DownloadAndShareDialogView(
                          title: S().download,
                          isAlbum: false,
                          url: args?.mediaWithWatermark ?? args?.url,
                          isVideo: args?.fileType == "video" ? true : false,
                        ),
                      );
                    },
                  ),
                  SizedBox(width: 10),
                  // TODO: Temporary hide share button
                  roundIconButton(
                    icon: ic_share,
                    iconColor: black,
                    onPressed: () async {
                      openDownloadOrShareBottomSheet(
                        context,
                        DownloadAndShareDialogView(
                          title: S().share,
                          isAlbum: false,
                          isDownload: false,
                          url: args?.mediaWithWatermark ?? args?.url,
                          successMessage: S().shared,
                          isVideo: args?.fileType == "video" ? true : false,
                        ),
                      );
                    },
                  ),
                  SizedBox(width: 10),
                ],
              ),
            ),
            SizedBox(height: 5),
            Expanded(
              child: Stack(
                children: [
                  args != null && args?.fileType == "video"
                      ? VideoPlayerWidget(
                          url: args?.url ?? "",
                        )
                      : unit8Lit != null
                          ? Image.memory(unit8Lit!)
                          : PhotoView(
                              backgroundDecoration: BoxDecoration(
                                color: darkGrey,
                              ),
                              enableRotation: false,
                              imageProvider: CachedNetworkImageProvider(
                                args?.url ?? "",
                              ),
                            ),
                ],
              ),
            ),
            Container(height: 105),
          ],
        ),
      );
// Widget mainLayout() => Container(
//       color: darkGrey,
//       padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           headerLayoutWithBackArrow(
//             context,
//             S().media,
//           ),
//           Container(
//             height: 100,
//             alignment: Alignment.centerRight,
//             child: Row(
//               mainAxisSize: MainAxisSize.min,
//               crossAxisAlignment: CrossAxisAlignment.center,
//               children: [
//                 // TODO: Temporary hide download button
//                 roundIconButton(
//                   icon: ic_download,
//                   iconColor: black,
//                   onPressed: () => openDownloadOrShareBottomSheet(
//                     context,
//                     DownloadAndShareDialogView(
//                       isAlbum: true,
//                       url: mediaList[pageIndex].mediaWithWatermark ?? "",
//                       isVideo: mediaList[pageIndex].fileType == "video"
//                           ? true
//                           : false,
//                       mediaList: mediaList,
//                     ),
//                   ),
//                 ),
//                 SizedBox(width: 5),
//                 // TODO: Temporary hide share button
//                 roundIconButton(
//                   icon: ic_share,
//                   iconColor: black,
//                   onPressed: () => openDownloadOrShareBottomSheet(
//                     context,
//                     DownloadAndShareDialogView(
//                       isAlbum: true,
//                       title: S().share,
//                       successMessage: S().shared,
//                       isDownload: false,
//                       isVideo: mediaList[pageIndex].fileType == "video"
//                           ? true
//                           : false,
//                       mediaList: mediaList,
//                       url: mediaList[pageIndex].mediaWithWatermark ?? "",
//                     ),
//                   ),
//                 ),
//                 SizedBox(width: 10),
//               ],
//             ),
//           ),
//           Expanded(
//             child: PageView.builder(
//               itemCount: mediaList.length,
//               physics: ClampingScrollPhysics(),
//               controller: pageController,
//               onPageChanged: (index) async {
//                 setState(() {
//                   pageIndex = index;
//                 });
//                 await autoScrollController.scrollToIndex(
//                   pageIndex,
//                   duration: Duration(milliseconds: 500),
//                   preferPosition: AutoScrollPosition.middle,
//                 );
//               },
//               itemBuilder: (BuildContext context, int index) {
//                 MediaList model = mediaList[index];
//                 return GalleryPageViewItem(
//                   imageUrl: model.url,
//                   fileType: model.fileType,
//                 );
//               },
//             ),
//           ),
//           Container(
//             height: 120,
//             alignment: Alignment.centerLeft,
//             child: Container(
//               height: 60,
//               child: ListView.builder(
//                   itemCount: mediaList.length,
//                   scrollDirection: Axis.horizontal,
//                   controller: autoScrollController,
//                   shrinkWrap: true,
//                   physics: ClampingScrollPhysics(),
//                   itemBuilder: (context, index) {
//                     MediaList model = mediaList[index];
//                     return AutoScrollTag(
//                       controller: autoScrollController,
//                       index: index,
//                       key: ValueKey(index),
//                       child: GalleryImageIndicatorListItem(
//                         imageUrl: model.thumbnail,
//                         index: index,
//                         pageController: pageController,
//                         isCurrent: pageIndex == index ? true : false,
//                       ),
//                     );
//                   }),
//             ),
//           ),
//         ],
//       ),
//     );
}
