import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_constants.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/config_model.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TermsAndServicesScreen extends StatefulWidget {
  const TermsAndServicesScreen({Key? key}) : super(key: key);

  @override
  _TermsAndServicesScreenState createState() => _TermsAndServicesScreenState();
}

class _TermsAndServicesScreenState extends State<TermsAndServicesScreen> {
  bool isProgress = false;
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  WebViewController? webViewController;
  var gestureRecognizers = [
    Factory(() => EagerGestureRecognizer()),
  ].toSet();
  String? termsUrl;
  ConfigModel? configModel;

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    configModel = SpManager.getConfigModel();
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.light);
    return Scaffold(
      backgroundColor: black,
      appBar: AppBar(
        toolbarHeight: 0,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: black,
      ),
      body: SafeArea(
        bottom: false,
        child: Container(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
          child: mainLayout(),
        ),
      ),
    );
  }

  Widget mainLayout() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          headerLayoutWithCross(context, S().terms_of_service),
          Expanded(
            child: Container(
              color: white,
              child: webView(),
            ),
          ),
        ],
      );

  Widget webView() => Container(
        child: WebView(
          debuggingEnabled: true,
          initialUrl: configModel?.response?.term ?? termsOfServicesUrl,
          allowsInlineMediaPlayback: true,
          javascriptMode: JavascriptMode.unrestricted,
          gestureRecognizers: gestureRecognizers,
          onWebViewCreated: (WebViewController controller) {
            _controller.complete(controller);
            webViewController = controller;
          },
          onPageFinished: (url) async {
            await webViewController?.canGoBack();
          },
          onProgress: (int progress) {
            setState(
              () {
                if (progress == 100) {
                  isProgress = false;
                } else {
                  isProgress = true;
                }
              },
            );
          },
          gestureNavigationEnabled: true,
        ),
      );
}
