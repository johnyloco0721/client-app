import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_bloc.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_events.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/utils/dialog_utils.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/rounded_button.dart';

class ProfilePreviewScreen extends StatefulWidget {
  final String? image;

  const ProfilePreviewScreen({Key? key, this.image}) : super(key: key);

  @override
  _ProfilePreviewScreenState createState() => _ProfilePreviewScreenState();
}

class _ProfilePreviewScreenState extends State<ProfilePreviewScreen> {
  late ProfileBloc profileBloc;
  final _picker = ImagePicker();
  late String profileImage, croppedImage;
  bool isLoading = false;
  late ApiBloc apiBloc;
  late Future<ImageInfo> imageInfo;

  double imageHeight = 1.0;
  double imageWidth = 1.0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    profileImage = widget.image ?? "";
    croppedImage = widget.image ?? "";
    getImageAspectRatio();
    apiBloc = BlocProvider.of<ApiBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    profileBloc = BlocProvider.of<ProfileBloc>(context);
    setStatusBarIconColor(iconColor: Brightness.light);
    return Scaffold(
      backgroundColor: black,
      appBar: AppBar(
        toolbarHeight: 0,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: black,
      ),
      body: SafeArea(
        bottom: false,
        child: BlocConsumer<ApiBloc, ApiStates>(
          bloc: apiBloc,
          listener: (BuildContext context, state) {
            if (state is LogoutApiLoadingState) {
              isLoading = state.isEnabled;
            } else if (state is ProfilePhotoApiDataLoadState) {
              profileBloc.add(
                ChangeProfileImageEvent(state.photoUrl ?? profileImage),
              );
              Navigator.pop(context);
            } else if (state is ApiErrorState) {
              showToast(
                state.errorMessage ?? S().server_error_please_try_again,
              );
            }
          },
          builder: (context, state) {
            return mainLayout();
          },
        ),
      ),
    );
  }

  Widget mainLayout() => Container(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            headerLayoutWithCross(context, S().profile_picture),
            Expanded(
              child: Container(
                color: white,
                alignment: Alignment.center,
                child: profileImageView(),
              ),
            ),
            bottomLayout(),
          ],
        ),
      );

  Widget profileImageView() => Container(
        clipBehavior: Clip.antiAlias,
        width: SizeUtils.width(context, 80),
        margin: EdgeInsets.only(left: 55, right: 55, bottom: 10),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: lightGrey),
        ),
        child: Card(
          margin: EdgeInsets.zero,
          elevation: 0,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: CircleBorder(),
          child: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return InkWell(
                onTap: () => _cropImage(),
                child: Container(
                  width: constraints.maxWidth,
                  height: constraints.maxWidth,
                  child: AspectRatio(
                    aspectRatio: imageWidth / imageHeight,
                    child: Image.file(
                      File(
                        profileImage,
                      ),
                      fit: BoxFit.cover,
                      width: imageWidth,
                      height: imageHeight,
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      );

  Widget bottomLayout() => Container(
        color: black,
        alignment: Alignment.center,
        padding: EdgeInsets.all(20),
        child: Row(
          children: [
            reUploadButton(),
            SizedBox(width: 30),
            Expanded(child: saveButton()),
          ],
        ),
      );

  Widget reUploadButton() => RoundedButton(
        buttonText: S.of(context).re_upload,
        borderColor: white,
        buttonColor: black,
        borderWidth: 1.5,
        height: 60.0,
        enabled: !isLoading,
        fontSize: bigFonts,
        padding: EdgeInsets.only(left: 30, right: 30),
        fontColor: white,
        onPressed: () => imageSelectionDialog(
          context,
          photoClick: () => getImage(ImageSource.gallery),
          cameraClick: () => getImage(ImageSource.camera),
        ),
      );

  Widget saveButton() => RoundedButton(
        buttonText: S.of(context).save,
        buttonColor: yellow,
        fontSize: bigFonts,
        height: 60.0,
        isLoading: isLoading,
        enabled: !isLoading,
        onPressed: () => apiBloc.add(
          ProfilePhotoApiEvent(
            image: File(profileImage),
          ),
        ),
      );

  Future getImage(ImageSource sources) async {
    Navigator.pop(context);
    try {
      final galImage = await _picker.pickImage(
        source: sources,
        imageQuality: 90,
      );
      setState(() {
        profileImage = galImage!.path;
        croppedImage = galImage.path;
      });
    } catch (e) {
      print("getImage $sources : $e");
    }
  }

  _cropImage() async {
    CroppedFile? croppedFile = await ImageCropper().cropImage(
        sourcePath: croppedImage,
        // cropStyle: CropStyle.circle,
        aspectRatioPresets: Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio16x9
              ]
            : [
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio5x3,
                CropAspectRatioPreset.ratio5x4,
                CropAspectRatioPreset.ratio7x5,
                CropAspectRatioPreset.ratio16x9
              ],
        uiSettings: [
          AndroidUiSettings(
              toolbarTitle: 'Crop Image',
              toolbarColor: darkYellow,
              toolbarWidgetColor: white,
              initAspectRatio: CropAspectRatioPreset.original,
              lockAspectRatio: false),
          IOSUiSettings(
            title: 'Crop Image',
          ),
          WebUiSettings(
            context: context,
          ),
        ]);
    if (croppedFile != null) {
      setState(() {
        profileImage = croppedFile.path;
      });
      getImageAspectRatio();
    }
  }

  getImageAspectRatio() async {
    var decodedImage =
        await decodeImageFromList(File(profileImage).readAsBytesSync());
    setState(() {
      imageHeight = decodedImage.height.toDouble();
      imageWidth = decodedImage.width.toDouble();
      print("imageHeight --- $imageHeight");
      print("imageWidth --- $imageWidth");
    });
  }

//   imageInfo = await getImageInfo(Image.file(
//     File(profileImage),
//     fit: BoxFit.fill,
//   ));
//   imageInfo.then((value) {
//     setState(() {
//       imageHeight = value.image.height.toDouble();
//       imageWidth = value.image.width.toDouble();
//     });
//   });
// }
}
