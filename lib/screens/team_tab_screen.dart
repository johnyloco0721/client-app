import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intrinsic_grid_view/intrinsic_grid_view.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_fonts.dart';
import 'package:pmaclient/list_items/teams_list_items.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:pmaclient/utils/text_utils.dart';

class TeamTabScreen extends StatefulWidget {
  const TeamTabScreen({Key? key}) : super(key: key);

  @override
  _TeamTabScreenState createState() => _TeamTabScreenState();
}

class _TeamTabScreenState extends State<TeamTabScreen> {
  List<Teams>? teamList = [];
  late ApiBloc apiBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    apiBloc = BlocProvider.of<ApiBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ApiBloc, ApiStates>(
        bloc: apiBloc,
        builder: (context, state) {
          print(state);
          if (state is ProjectDetailsApiDataLoadState) {
            teamList = state.model?.response?.teams;
          }
          return teamList != null && teamList!.length > 0
              ? IntrinsicGridView.vertical(
                  columnCount: 2,
                  horizontalSpace: 15,
                  verticalSpace: 25,
                  children: List.generate(
                    teamList!.length,
                    (index) {
                      Teams model = teamList![index];
                      return TeamsListItem(
                        designationName: model.designationName,
                        memberInfo: model.memberDetails,
                        memberName: model.teamMemberName,
                        teamAvatar: model.teamMemberAvatar,
                        biography: model.biography,
                      );
                    },
                  ),
                )
              : Container(
                  alignment: Alignment.center,
                  child: Text(
                    "No Teams Found",
                    style: boldFontsStyle(
                      fontSize: mediumFont,
                      fontFamily: platForm,
                    ),
                  ),
                );
        });
  }
}
