import 'dart:async';

import 'package:animated_size_and_fade/animated_size_and_fade.dart';
import 'package:expandable_page_view/expandable_page_view.dart';
import 'package:fading_edge_scrollview/fading_edge_scrollview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_fonts.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/dialogs_views/documents_dialogs_view.dart';
import 'package:pmaclient/dialogs_views/property_details_dialog_view.dart';
import 'package:pmaclient/dialogs_views/updates_dialogs_view.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/list_items/dot_list_items.dart';
import 'package:pmaclient/list_items/horizontal_timeline_list_item.dart';
import 'package:pmaclient/list_items/updates_list_items.dart';
import 'package:pmaclient/list_items/vertical_timeline_list_item.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/route_management/property_details_arguments.dart';
import 'package:pmaclient/utils/dialog_utils.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/horizontal_line.dart';
import 'package:pmaclient/views/network_blur_image_view.dart';
import 'package:pmaclient/widgets/expandable_html_widget.dart';
import 'package:pmaclient/widgets/round_icon_button.dart';
import 'package:pmaclient/widgets/shimmer_widget.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

class PropertyDetailsScreen extends StatefulWidget {
  final PropertyDetailsArguments? propertyArguments;

  const PropertyDetailsScreen({
    Key? key,
    this.propertyArguments,
  }) : super(key: key);

  @override
  _PropertyDetailsScreenState createState() => _PropertyDetailsScreenState();
}

class _PropertyDetailsScreenState extends State<PropertyDetailsScreen> {
  final dataKey = new GlobalKey();
  bool isDropDown = false;
  int? currentIndex = 0;
  ScrollController scrollController = ScrollController();
  bool isLoad = true;
  ApiBloc? apiBloc;
  ProjectDetailsData? projectDetailsData;
  AutoScrollController autoScrollController = AutoScrollController();
  int selectedIndex = 0;
  bool isBack = true;
  bool moreDocs = true;
  bool initialState = true;

  @override
  void initState() {
    scrollController.addListener(() => scrollListener());
    super.initState();
    getProjectDetailsApi();
  }

  @override
  void dispose() {
    scrollController.removeListener(() => scrollListener());
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.light);
    return WillPopScope(
      onWillPop: () => backClick(),
      child: Scaffold(
        backgroundColor: isLoad ? shimmerGrey : whiteGrey,
        body: SafeArea(
          top: false,
          bottom: false,
          child: RefreshIndicator(
            color: darkYellow,
            onRefresh: () async {
              getProjectDetailsApi();
            },
            child: LayoutBuilder(
              builder: (context, viewportConstraints) {
                return BlocConsumer<ApiBloc, ApiStates>(
                  bloc: apiBloc,
                  builder: (context, state) {
                    return Stack(
                      children: [
                        SingleChildScrollView(
                          controller: scrollController,
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                              minHeight: viewportConstraints.maxHeight,
                            ),
                            child: mainLayout(),
                          ),
                        ),
                        isLoad
                            ? Container()
                            : SafeArea(
                                child: Container(
                                  margin: EdgeInsets.only(
                                      left: 25, top: 15, right: 25),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      AnimatedContainer(
                                        duration: Duration(milliseconds: 450),
                                        height: isBack ? 40 : 0,
                                        width: isBack ? 40 : 0,
                                        child: roundIconButton(
                                          onPressed: () => backClick(),
                                          iconColor: black,
                                        ),
                                      ),
                                      projectDetailsData?.supports != null &&
                                              projectDetailsData!
                                                  .supports!.isNotEmpty
                                          ? roundIconButton(
                                              color: black,
                                              icon: ic_support,
                                              width: 50.0,
                                              height: 50.0,
                                              iconWidth: 22.0,
                                              iconHeight: 22.0,
                                              iconColor: yellow,
                                              onPressed: () =>
                                                  goToNextNamedRoute(
                                                context,
                                                enquiryScreen,
                                              ),
                                            )
                                          : Container(height: 50, width: 50),
                                    ],
                                  ),
                                ),
                              ),
                      ],
                    );
                  },
                  listener: (BuildContext context, state) {
                    if (state is LogoutApiDataLoadState) {
                      SpManager.logoutDeleteData();
                      goAndClearAllPageNamed(context, loginOtpScreen);
                    } else if (state is ProjectDetailsApiDataLoadState) {
                      projectDetailsData = state.model?.response;
                      isLoad = false;
                      getCurrentProgressStatusText();
                      whenNotificationRedirect();
                    } else if (state is ApiLoadingState) {
                      isLoad = true;
                    } else if (state is ApiErrorState) {
                      isLoad = false;
                      showToast(
                        state.errorMessage ?? S().server_error_please_try_again,
                      );
                    }
                  },
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget mainLayout() => isLoad
      ? SafeArea(
          child: ShimmerWidget(
            imagePath: ic_shimmer_detail_running,
            aspectRatio: 1640 / 4636,
          ),
        )
      : Stack(
          children: [
            Container(),
            Container(
              width: SizeUtils.width(context, 100),
              height: SizeUtils.height(context, 35),
              child: AspectRatio(
                aspectRatio: 1,
                child: NetworkBlurHashImageView(
                  imageUrl: projectDetailsData?.coverImage ?? "",
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              clipBehavior: Clip.antiAlias,
              width: SizeUtils.width(context, 100),
              constraints: BoxConstraints(
                minHeight: SizeUtils.height(context, 70),
              ),
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).padding.bottom),
              margin: EdgeInsets.only(top: SizeUtils.height(context, 30)),
              decoration: BoxDecoration(
                color: whiteGrey,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: shadowColor.withOpacity(0.1),
                    spreadRadius: 1,
                    blurRadius: 16,
                    offset: Offset(0, 0.19),
                  ),
                ],
              ),
              child: Card(
                elevation: 0,
                color: whiteGrey,
                margin: EdgeInsets.zero,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                child: contentLayout(),
              ),
            ),
          ],
        );

  Widget contentLayout() => Container(
        margin: EdgeInsets.all(25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.center,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: Text(
                      projectDetailsData?.propertyName ?? "",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: boldFontsStyle(
                        fontFamily: platForm,
                        fontSize: bigLargeFont,
                      ),
                    ),
                  ),
                  CupertinoButton(
                    onPressed: () => openCupertinoBottomSheet(
                      context,
                      widget: PropertyDetailsDialogView(),
                      title: S().property_details,
                    ),
                    padding: EdgeInsets.zero,
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Image.asset(ic_info, height: 25, width: 25),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              alignment: Alignment.center,
              child: Wrap(
                runSpacing: 8.0,
                alignment: WrapAlignment.center,
                children: [
                  if (projectDetailsData?.projectUnit != null && projectDetailsData?.projectUnit != '')
                    Padding(
                      padding: EdgeInsets.only(right: 15.0),
                      child: Text(
                        projectDetailsData?.projectUnit ?? "",
                        style: regularFontsStyle(
                          fontSize: mediumFont,
                          color: grey,
                        ),
                      ),
                    ),
                  if (projectDetailsData?.projectSize != null && projectDetailsData?.projectSize != "0")
                    Container(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            ic_sq_ft,
                            height: 17,
                            width: 17,
                            color: grey,
                          ),
                          SizedBox(width: 5),
                          Text(
                            "${projectDetailsData?.projectSize ?? ""} sqft",
                            style: regularFontsStyle(
                              fontSize: mediumFont,
                              color: grey,
                            ),
                          ),
                          SizedBox(width: 15),
                        ],
                      ),
                    ),
                  if (projectDetailsData?.propertyType != null && projectDetailsData?.propertyType != '')
                    Container(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(
                            ic_room_type,
                            height: 17,
                            width: 17,
                            color: grey,
                          ),
                          SizedBox(width: 5),
                          Flexible(
                            child: Text(
                              projectDetailsData?.propertyType ?? "",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: regularFontsStyle(
                                fontSize: mediumFont,
                                color: grey,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                ],
              ),
            ),
            documentsViewLayout(),
            progressDetailsView(),
          ],
        ),
      );

  Widget documentsViewLayout() => CupertinoButton(
        onPressed: () {
          AppDataModel.setDocumentList(projectDetailsData?.documents);
          AppDataModel.setMediaTabList(projectDetailsData?.allMediaListArr);
          openCupertinoBottomSheet(
            context,
            widget: DocumentsDialogView(),
            title: S().all_medias_and_Docs,
          );
        },
        padding: EdgeInsets.zero,
        child: Container(
          margin: EdgeInsets.only(top: 25),
          width: SizeUtils.width(context, 100),
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.circular(16),
            boxShadow: [
              BoxShadow(
                color: shadowColor.withOpacity(0.1),
                spreadRadius: 1,
                blurRadius: 16,
                offset: Offset(0, 0.19),
              ),
            ],
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(ic_document, height: 20, width: 20),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 15, top: 3, right: 15),
                  child: Text(
                    S().all_medias_and_Docs,
                    style: boldFontsStyle(fontSize: bigFonts),
                  ),
                ),
              ),
              Image.asset(ic_next, height: 20, width: 20)
            ],
          ),
        ),
      );

  Widget progressDetailsView() {
    return Container(
      margin: EdgeInsets.only(top: 25),
      key: dataKey,
      width: SizeUtils.width(context, 100),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: shadowColor.withOpacity(0.1),
            spreadRadius: 1,
            blurRadius: 16,
            offset: Offset(0, 0.19),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          projectDetailsData?.projectStatus == 'planning' && !(projectDetailsData?.isScheduleTaskCompleted ?? false) ? Container(
            padding: EdgeInsets.symmetric(vertical: 24.0),
            child: Column(
              children: [
                Image.asset(ic_planning_illustration, width: 200.0, height: 200.0),
                SizedBox(height: 24.0),
                Text(
                  S().makeoverInPlanning,
                  style: boldFontsStyle(
                    fontSize: 19.0,
                    height: 1.32,
                  ),
                ),
              ],
            ),
          ) : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 25, left: 20),
                child: Text(
                  S().makeover_progress,
                  style: boldFontsStyle(
                    fontFamily: platForm,
                    fontSize: bigLargeFont,
                  ),
                ),
              ),
              projectDetailsData?.reminder != null &&
                    projectDetailsData!.reminder!.length > 0 &&
                    projectDetailsData!.reminder![0].show!
                  ? Container(
                      margin: EdgeInsets.only(top: 25, left: 20, right: 20),
                      width: SizeUtils.width(context, 100),
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      decoration: BoxDecoration(
                        color: white,
                        borderRadius: BorderRadius.circular(16),
                        boxShadow: [
                          BoxShadow(
                            color: shadowColor.withOpacity(0.1),
                            spreadRadius: 1,
                            blurRadius: 16,
                            offset: Offset(0, 0.19),
                          ),
                        ],
                      ),
                      child: IntrinsicHeight(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              child: Lottie.asset(
                                payment_reminder,
                                height: 100,
                                width: SizeUtils.width(context, 20),
                              ),
                              padding: EdgeInsets.all(10),
                              color: yellow,
                            ),
                            Expanded(
                              child: Container(
                                color: darkGrey,
                                padding: EdgeInsets.all(16),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Builder(
                                      builder: (context) {
                                        return RichText(
                                          text: ExpandableHtmlWidget().htmlToTextSpan(context, projectDetailsData?.reminder![0].title ?? ""),
                                          maxLines: 4,
                                          overflow: TextOverflow.ellipsis
                                        );
                                      }
                                    ),
                                    SizedBox(height: 10),
                                    Builder(
                                      builder: (context) {
                                        return RichText(
                                          text: ExpandableHtmlWidget().htmlToTextSpan(context, projectDetailsData?.reminder![0].description ?? ""),
                                          maxLines: 10,
                                          overflow: TextOverflow.ellipsis
                                        );
                                      }
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  : Container(),
              projectDetailsData?.horizontalProgress != null &&
                      projectDetailsData!.horizontalProgress!.length > 0
                  ? Container(
                      height: 42,
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(top: 15, left: 20, right: 20),
                      child: FadingEdgeScrollView.fromScrollView(
                        gradientFractionOnEnd: 0.5,
                        gradientFractionOnStart: 0.5,
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: projectDetailsData!.horizontalProgress!.length,
                          scrollDirection: Axis.horizontal,
                          controller: autoScrollController,
                          itemBuilder: (context, index) {
                            Progress model = projectDetailsData!.horizontalProgress![index];
                            
                            return AutoScrollTag(
                              key: ValueKey(index),
                              controller: autoScrollController,
                              index: index,
                              child: HorizontalTimeLineListItem(
                                status: model.status,
                                nextStatus: projectDetailsData!.horizontalProgress![index == projectDetailsData!.horizontalProgress!.length - 1 ? index : index + 1].status,
                                index: index,
                                lastIndex: projectDetailsData!.horizontalProgress!.length - 1,
                              ),
                            );
                          },
                        ),
                      ),
                    )
                  : Container(),
              Container(
                margin: EdgeInsets.only(top: 20, left: 20),
                child: ExpandableHtmlWidget(
                  htmlContent: (projectDetailsData?.horizontalProgressLabel ?? ""),
                )
              ),
              HorizontalLine(margin: EdgeInsets.only(top: 25)),
              Container(
                margin: EdgeInsets.only(left: 20, right: 5),
                height: 50,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Container(
                        child: Text(
                          S().details,
                          style: boldFontsStyle(fontSize: bigFonts),
                        ),
                      ),
                    ),
                    SizedBox(width: 15),
                    InkWell(
                      onTap: () {
                        setState(() {
                          isDropDown = !isDropDown;
                        });
                      },
                      child: Container(
                        height: 50,
                        width: 50,
                        alignment: Alignment.center,
                        child: Image.asset(
                          isDropDown ? ic_down_dropdown : ic_up_dropdown,
                          height: 10,
                          width: 16,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              progressListView(),
              SizedBox(height: 1),
            ],
          ),
          HorizontalLine(),
          if (projectDetailsData?.updates != null &&
              projectDetailsData!.updates!.length > 0)
            latestUpdatesTextView(),
          if (projectDetailsData?.updates != null &&
              projectDetailsData!.updates!.length > 0)
            updatesListView(),
          if (projectDetailsData?.updates != null &&
              projectDetailsData!.updates!.length > 0)
            updatesListIndicatorView(),
        ],
      ),
    );
  }

  Widget progressListView() => projectDetailsData?.progress != null &&
          projectDetailsData!.progress!.length > 0
      ? Container(
          child: AnimatedSizeAndFade(
            sizeDuration: Duration(milliseconds: 500),
            sizeCurve: Curves.easeIn,
            child: isDropDown
                ? Column(
                  children: [
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 16.0),
                      padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                      decoration: BoxDecoration(
                        border: Border.all(width: 1.0, color: lightGrey),
                        borderRadius: BorderRadius.all(Radius.circular(6.0))
                      ),
                      child: Wrap(
                        alignment: WrapAlignment.center,
                        children: [
                          Container(
                            child: Text(
                              projectDetailsData?.makeoverDurationLabel ?? '',
                              style: boldFontsStyle(height: 1.29),
                            ),
                          ),
                          Container(
                            child: Text(
                              projectDetailsData?.makeoverDurationValue ?? '',
                              style: boldFontsStyle(height: 1.29, color: darkYellow),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(bottom: 20),
                        child: ListView.builder(
                          itemCount: projectDetailsData?.progress?.length,
                          shrinkWrap: true,
                          padding: EdgeInsets.zero,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            Progress model = projectDetailsData!.progress![index];
                            String? startDate = formatDate(date: model.startDateTime ?? "", format: "dd LLL (EEE)");
                            String? endDate = formatDate(date: model.endDateTime ?? "", format: "dd LLL (EEE)");
                            
                            return VerticalTimeLineListItem(
                              status: model.status,
                              nextStatus: projectDetailsData!.progress![index == projectDetailsData!.progress!.length - 1 ? index : index + 1].status,
                              name: model.name,
                              index: index,
                              startDate: startDate,
                              endDate: endDate,weekLabel: model.weekLabel,
                              lastIndex: projectDetailsData!.progress!.length - 1,
                            );
                          },
                        ),
                      ),
                  ],
                ) : Container(
                    margin: EdgeInsets.only(bottom: 0),
                  ),
          ),
        )
      : Container(margin: EdgeInsets.only(bottom: 0));

  Widget latestUpdatesTextView() => Container(
        margin: EdgeInsets.only(top: 20, left: 20, right: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Text(
                S().latest_updates,
                style: boldFontsStyle(fontSize: bigFonts),
              ),
            ),
            SizedBox(width: 10),
            InkWell(
              onTap: () => openUpdatesDialog(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    S().view_all,
                    style: regularFontsStyle(fontSize: mediumFont),
                  ),
                  SizedBox(width: 10),
                  Container(
                    height: 13,
                    child: AspectRatio(
                      aspectRatio: 48 / 88,
                      child: Image.asset(
                        ic_next,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );

  Widget updatesListView() => Container(
        margin: EdgeInsets.only(top: 20),
        child: projectDetailsData?.updates != null &&
                projectDetailsData!.updates!.length > 0
            ? ExpandablePageView.builder(
                itemCount: projectDetailsData!.updates!.length > 5
                    ? 5
                    : projectDetailsData!.updates!.length,
                onPageChanged: (int index) {
                  setState(() {
                    currentIndex = index;
                  });
                },
                itemBuilder: (context, index) {
                  Update model = projectDetailsData!.updates![index];
                  String date = formatDate(date: model.createdDateTime);
                  String time = formatDate(date: model.createdDateTime, format: 'hh:mm a');
                  String? imageUrl;
                  String? fileType;
                  if (model.mediaList!.length > 0) {
                    imageUrl = model.mediaList![0].fileType == "video"
                        ? model.mediaList![0].thumbnail
                        : model.mediaList![0].url;
                    fileType = model.mediaList![0].fileType;
                  }
                  return UpdatesListItems(
                    imagePath: imageUrl ?? "",
                    dropboxLink: model.dropboxLink ?? "",
                    date: date,
                    title: model.description,
                    time: time,
                    hasUpdate: model.hasUpdate ?? 0,
                    documentList: model.documentList ?? [],
                    mediaList: model.mediaList ?? [],
                    fileType: fileType ?? "",
                    moreDocs: moreDocs,
                    moreClick: () => setState(() => moreDocs = false),
                    isEdited: model.createdDateTime != model.updatedDateTime,
                  );
                },
              )
            : Container(),
      );

  Widget updatesListIndicatorView() => Container(
        margin: EdgeInsets.only(top: 20, bottom: 20),
        height: 10,
        alignment: Alignment.center,
        child: projectDetailsData?.updates != null &&
                projectDetailsData!.updates!.length > 0
            ? ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: projectDetailsData!.updates!.length > 5
                    ? 5
                    : projectDetailsData!.updates!.length,
                itemBuilder: (context, index) {
                  int totalItem = projectDetailsData!.updates!.length > 5
                      ? 5
                      : projectDetailsData!.updates!.length;
                  return DotListIndicator(
                    isCurrent: index == currentIndex ? true : false,
                    icLastIndex: index == totalItem - 1 ? true : false,
                  );
                },
              )
            : Container(),
      );

  getProjectDetailsApi() {
    apiBloc = BlocProvider.of<ApiBloc>(context);
    apiBloc?.add(ProjectDetailsApiEvent(
        projectId: widget.propertyArguments?.projectID ?? 0));
  }

  scrollListener() {
    if (scrollController.offset >=
        (scrollController.position.minScrollExtent +
            SizeUtils.height(context, 20))) {
      setState(() {
        isBack = false;
      });
    } else {
      setState(() {
        isBack = true;
      });
    }
  }

  openUpdatesDialog({int? updatesID}) {
    AppDataModel.setUpdatesList(projectDetailsData?.updates);
    openCupertinoBottomSheet(
      context,
      widget: UpdatesDialogView(autoScrollTo: updatesID),
      title: S().makeoverUpdates,
    );
  }

  whenNotificationRedirect() {
    if (widget.propertyArguments?.type == "progress") {
      isDropDown = true;
      Timer(Duration(milliseconds: 50), () {
        try {
          RenderBox box =
              dataKey.currentContext?.findRenderObject() as RenderBox;
          Offset position =
              box.localToGlobal(Offset.zero); //this is global position
          double y = position.dy;
          scrollController.animateTo(y + (selectedIndex * 60),
              duration: Duration(milliseconds: 550), curve: Curves.easeIn);
        } catch (e) {
          print(e);
        }
      });
    } else if ((widget.propertyArguments?.type == "updates" || widget.propertyArguments?.type == "project-new-update") &&
        initialState == true) {
      initialState = false;
      openUpdatesDialog(updatesID: widget.propertyArguments?.affectedModuleID);
    }
  }

  getCurrentProgressStatusText() {
    projectDetailsData?.progress?.forEach((element) {
      int? index = projectDetailsData?.progress?.indexOf(element);
      if (element.status == "running") {
        selectedIndex = index ?? 0;
        Timer(
          Duration(milliseconds: 50),
          () {
            autoScrollController.scrollToIndex(
              selectedIndex,
              duration: Duration(milliseconds: 500),
              preferPosition: AutoScrollPosition.middle,
            );
          },
        );
      }
    });
  }

  backClick() {
    Route route = ModalRoute.of(context) as Route;

    String? pageName = route.settings.name;
    if (pageName != null &&
        pageName == propertyDetailsScreen &&
        widget.propertyArguments?.type != null) {
      Navigator.popUntil(context, ModalRoute.withName(homeScreen));
    } else {
      Navigator.pop(context);
    }
    return true;
  }
}
