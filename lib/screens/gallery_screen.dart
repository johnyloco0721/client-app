import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/dialogs_views/download_and_share_dialog_view.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/list_items/gallery_image_indicator_list_item.dart';
import 'package:pmaclient/list_items/gallery_page_view_item.dart';
import 'package:pmaclient/route_management/media_screen_args.dart';
import 'package:pmaclient/utils/dialog_utils.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/round_icon_button.dart';
import 'package:pmaclient/widgets/video_player_widget.dart';
import 'package:scroll_to_index/scroll_to_index.dart';
import 'package:video_player/video_player.dart';

class GalleryScreen extends StatefulWidget {
  final MediaScreenArgs? mediaScreenArgs;

  const GalleryScreen({Key? key, this.mediaScreenArgs}) : super(key: key);

  @override
  _GalleryScreenState createState() => _GalleryScreenState();
}

class _GalleryScreenState extends State<GalleryScreen> {
  List galleryList = [];
  ApiBloc? apiBloc;
  int pageIndex = 0;
  AutoScrollController autoScrollController = AutoScrollController();
  PageController pageController = PageController();
  MediaScreenArgs? mediaScreenArgs;
  VideoPlayerController? videoPlayerController;
  PhotoViewScaleStateController imageScaleContaoller = PhotoViewScaleStateController();

  var isVideoPaused = ValueNotifier(false);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    mediaScreenArgs = widget.mediaScreenArgs ??
        MediaScreenArgs(
          isGallery: true,
        );
    apiBloc = BlocProvider.of<ApiBloc>(context);
    pageIndex = widget.mediaScreenArgs?.index ?? 0;
    galleryList = widget.mediaScreenArgs?.mediaList ?? [];
    galleryList.map((e) {
      e.playing = false;
      return e;
    });
    pageController = PageController(initialPage: pageIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: darkGrey,
      appBar: AppBar(
        toolbarHeight: 0,
        backgroundColor: black,
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      body: SafeArea(
        bottom: false,
        child: BlocBuilder<ApiBloc, ApiStates>(
            bloc: apiBloc,
            builder: (context, state) {
              // if (state is ProjectDetailsApiDataLoadState) {
              //   galleryList = state.model?.response?.gallery;
              // }
              return mainLayout();
            }),
      ),
    );
  }

  Widget mainLayout() => Container(
        color: darkGrey,
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            headerLayoutWithBackArrow(
              context,
              S().gallery,
            ),
            Expanded(
              child: Stack(
                children: [
                  PhotoViewGallery.builder(
                    scrollPhysics: BouncingScrollPhysics(),
                    pageController: pageController,
                    itemCount: galleryList.length,
                    loadingBuilder: (context, event) => Center(
                      child: Container(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(
                          value: event == null ? 0 : event.cumulativeBytesLoaded / event.expectedTotalBytes!,
                        ),
                      ),
                    ),
                    backgroundDecoration: BoxDecoration(color: Color(0xff383632)),
                    onPageChanged: (int index) async {
                      setState(() {
                        pageIndex = index;
                      });
                      await autoScrollController.scrollToIndex(pageIndex);
                      imageScaleContaoller.reset();
                    },
                    builder: (BuildContext context, int index) {
                      var model = galleryList[index];
                      if (model.fileType == 'video') {
                        var videoUrl = mediaScreenArgs!.isGallery ? model.media : model.url;
                        return PhotoViewGalleryPageOptions.customChild(
                          child: VideoPlayerWidget(
                            url: videoUrl,
                            autoplay: false,
                            looping: false,
                          ),
                          initialScale: PhotoViewComputedScale.contained * 1.0,
                          minScale: PhotoViewComputedScale.contained * 1.0,
                          heroAttributes: PhotoViewHeroAttributes(tag: model.mediaID),
                        );
                      } 
                      else {
                        return PhotoViewGalleryPageOptions(
                          scaleStateController: imageScaleContaoller,
                          imageProvider: CachedNetworkImageProvider(mediaScreenArgs!.isGallery ? model.media : model.url),
                          initialScale: PhotoViewComputedScale.contained * 1.0,
                          minScale: PhotoViewComputedScale.contained * 1.0,
                          heroAttributes: PhotoViewHeroAttributes(tag: model.mediaID),
                        );
                      }
                    },
                  ),

                  Positioned(
                    top: 0,
                    right: 0,
                    child: Container(
                      height: 100,
                      alignment: Alignment.centerRight,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          // TODO: Temporary hide download button
                          roundIconButton(
                            icon: ic_download,
                            iconColor: black,
                            onPressed: () => openDownloadOrShareBottomSheet(
                              context,
                              DownloadAndShareDialogView(
                                isAlbum: true,
                                url: galleryList[pageIndex].mediaWithWatermark ?? "",
                                isVideo: galleryList[pageIndex].fileType == "video"
                                    ? true
                                    : false,
                                mediaList: galleryList,
                              ),
                            ),
                          ),
                          SizedBox(width: 5),
                          // TODO: Temporary hide share button
                          roundIconButton(
                            icon: ic_share,
                            iconColor: black,
                            onPressed: () => openDownloadOrShareBottomSheet(
                              context,
                              DownloadAndShareDialogView(
                                isAlbum: true,
                                title: S().share,
                                successMessage: S().shared,
                                isDownload: false,
                                isVideo: galleryList[pageIndex].fileType == "video"
                                    ? true
                                    : false,
                                mediaList: galleryList,
                                url: galleryList[pageIndex].mediaWithWatermark ?? "",
                              ),
                            ),
                          ),
                          SizedBox(width: 10),
                        ],
                      ),
                    ),
                  ),

                  Positioned(
                    bottom: 0,
                    left: 0,
                    child: Container(
                      height: 120,
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.centerLeft,
                      child: Container(
                        height: 65,
                        alignment: Alignment.centerLeft,
                        child: ListView.builder(
                          itemCount: galleryList.length,
                          scrollDirection: Axis.horizontal,
                          controller: autoScrollController,
                          shrinkWrap: true,
                          physics: ClampingScrollPhysics(),
                          itemBuilder: (context, index) {
                            var model = galleryList[index];
                            return AutoScrollTag(
                              controller: autoScrollController,
                              index: index,
                              key: ValueKey(index),
                              child: GalleryImageIndicatorListItem(
                                imageUrl: model.thumbnail,
                                index: index,
                                isVideo: model.fileType == "video" ? true : false,
                                pageController: pageController,
                                isCurrent: pageIndex == index ? true : false,
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  ),

                ]
              ),
            ),
          ],
        ),
      );
}
