import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/projects_model.dart';
import 'package:pmaclient/models/rating_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/rounded_button.dart';

class RateResolveIssueScreen extends StatefulWidget {
  const RateResolveIssueScreen({Key? key}) : super(key: key);

  @override
  _RateResolveIssueScreenState createState() => _RateResolveIssueScreenState();
}

class _RateResolveIssueScreenState extends State<RateResolveIssueScreen> {
  late HomeProject homeProjectModel;
  late ApiBloc apiBloc;
  bool isLoading = false;
  int navStatus = 0;

  @override
  void initState() {
    // TODO: implement initState
    homeProjectModel = AppDataModel.getHomeProjectModel();
    super.initState();
    apiBloc = BlocProvider.of<ApiBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: black,
      appBar: AppBar(
        toolbarHeight: 0,
        backgroundColor: black,
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      body: SafeArea(
        bottom: false,
        child: BlocConsumer<ApiBloc, ApiStates>(
          bloc: apiBloc,
          listener: (context, state) {
            if (state is ReviewApiLoadingState) {
              isLoading = true;
            } else if (state is SendReviewApiLoadState) {
              isLoading = false;
              AppDataModel.setFeedBackModel(state.model);
              goAndClearCurrentPageNamed(
                context,
                rateResolveIssueCompleteScreen,
                args: navStatus,
              );
            } else if (state is ReviewApiErrorState) {
              isLoading = false;
              showToast(
                state.errorMessage ?? S().server_error_please_try_again,
              );
            }
          },
          builder: (context, state) {
            return mainLayout();
          },
        ),
      ),
    );
  }

  Widget mainLayout() => AbsorbPointer(
        absorbing: isLoading,
        child: Container(
          color: white,
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              headerLayoutWithCross(context, S().rate_us),
              Container(
                padding: EdgeInsets.only(left: 20, bottom: 20),
                color: black,
                width: SizeUtils.width(context, 100),
                child: Text(
                  "${homeProjectModel.propertyName} (${homeProjectModel.projectUnit})",
                  style: boldFontsStyle(fontSize: mediumFont, color: white),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.all(35),
                        child: Lottie.asset(rate_resolve_issue_lottie,
                            height: 200, width: 200),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 20, right: 20, top: 15),
                        child: Text(
                          S().may_we_reach_out_to_you,
                          style: boldFontsStyle(fontSize: bigFonts),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(left: 20, top: 10, right: 20),
                        child: Text(
                          S().let_us_know_if_we_can_discuss_ways_to_serve,
                          textAlign: TextAlign.center,
                          style: regularFontsStyle(
                            fontSize: mediumFont,
                            height: 1.3,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20, top: 25),
                child: RoundedButton(
                  width: 200,
                  buttonText: S().sure,
                  isLoading: navStatus == 1 && isLoading ? isLoading : false,
                  onPressed: () => sendReviewDataApi(1),
                ),
              ),
              navStatus == 2 && isLoading
                  ? Lottie.asset(login_black_loading, height: 50, width: 120)
                  : GestureDetector(
                      onTap: () => sendReviewDataApi(2),
                      child: Container(
                        margin: EdgeInsets.only(
                            left: 20, bottom: 50, right: 20, top: 25),
                        child: Text(
                          S().no_thanks,
                          textAlign: TextAlign.center,
                          style: boldFontsStyle(fontSize: bigFonts),
                        ),
                      ),
                    ),
            ],
          ),
        ),
      );

  sendReviewDataApi(status) {
    navStatus = status;
    List<ReviewData> reviewList = AppDataModel.getReviewList();
    int index = reviewList.indexWhere((element) => element.questionID == 3);
    reviewList[index].resolve = status == 1 ? true : false;
    RatingModel ratingModel = RatingModel(
      projectID: homeProjectModel.projectID,
      reviewData: reviewList,
    );
    String body = jsonEncode(ratingModel.toJson());
    apiBloc.add(SendReviewEvent(body: body));

    // Clear review list after submit
    AppDataModel.setReviewList([]);
  }
}
