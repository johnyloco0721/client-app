import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/bloc/reset_password_bloc/reset_password_bloc.dart';
import 'package:pmaclient/bloc/reset_password_bloc/reset_password_events.dart';
import 'package:pmaclient/bloc/reset_password_bloc/reset_password_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/screens/login_screen.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/bordered_textfield.dart';
import 'package:pmaclient/widgets/rounded_button.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  TextEditingController? userNameController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  ResetPasswordBloc resetPasswordBloc = ResetPasswordBloc();
  bool enabled = false;
  bool isLoading = false;
  ApiBloc apiBloc = ApiBloc();
  bool isTextFieldError=false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.dark);
    return Scaffold(
      backgroundColor: white,
      body: SafeArea(
        bottom: false,
        child: BlocListener<ApiBloc, ApiStates>(
          bloc: apiBloc,
          listener: (BuildContext context, state) {
            if (state is ApiLoadingState) {
              isTextFieldError = false;
              resetPasswordBloc.add(ResetButtonClickEvents(true));
            } else if (state is ForgotPasswordApiDataLoadState) {
              resetPasswordBloc.add(ResetButtonClickEvents(false));
              print(state.model);
              isTextFieldError = false;
              goToNextNamedRoute(context, emailSendScreen);
            } else if (state is ApiErrorState) {
              isTextFieldError = true;
              resetPasswordBloc.add(ResetButtonClickEvents(false));
            } else {
              resetPasswordBloc.add(ResetButtonClickEvents(false));
            }
          },
          child: BlocBuilder<ResetPasswordBloc, ResetPasswordStates>(
            bloc: resetPasswordBloc,
            builder: (context, state) {
              if (state is ResetPasswordEnabledState) {
                enabled = state.enabled;
              } else if (state is ResetButtonClickState) {
                isLoading = state.isLoading;
              }
              return mainLayout();
            },
          ),
        ),
      ),
    );
  }

  Widget mainLayout() => Container(
        margin: EdgeInsets.only(
            top: SizeUtils.height(context, 5), bottom: 20, left: 20, right: 20),
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                welcomeToMakeoverGuysImage(context),
                loginDescriptionTextView(),
                userNameTextFormField(),
                resetPasswordButton(),
                backToLoginButton()
              ],
            ),
          ),
        ),
      );

  Widget loginDescriptionTextView() => Container(
        margin: EdgeInsets.only(top: 15),
        child: Text(
          S().please_enter_your_username_and_password_to_reset,
          style: regularFontsStyle(fontSize: bigFonts, height: 1.3),
        ),
      );

/*
  Widget userNameTextFormField() => Container(
        margin: EdgeInsets.only(top: 15),
        child: BorderedTextField(
          controller: userNameController,
          hintText: S.of(context).user_name,
          enabled: !isLoading,
          fontSize: bigFonts,
          hintFontSize: bigFonts,
          textInputAction: TextInputAction.next,
          onChanged: (value) => resetPasswordBloc.add(
            UserValueChangeEvent(value),
          ),
          suffixIcon: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {},
            icon: Image.asset(
              enabled ? ic_tick : ic_user,
              height: 20,
              width: 20,
            ),
          ),
        ),
      );
*/

  Widget userNameTextFormField() => Container(
    margin: EdgeInsets.only(top: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BorderedTextField(
          controller: userNameController,
          hintText: S.of(context).user_name,
          fontColor: isTextFieldError ? red : black,
          enabled: !isLoading,
          focusBorderColor: isTextFieldError ? red : yellow,
          enableBorderColor: isTextFieldError ? red : black,
          fontSize: bigFonts,
          hintFontSize: bigFonts,
        onChanged: (value) => resetPasswordBloc.add(
          UserValueChangeEvent(value),
        ),
          suffixIcon: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {},
            icon: Image.asset(
              ic_password,
              height: 20,
              width: 20,
              color: isTextFieldError ? red : black,
            ),
          ),
        ),
        isTextFieldError
            ? Container(
          margin: EdgeInsets.only(top: 10, left: 15),
          child: Text(
            S().invalid_username,
            style:
            regularFontsStyle(color: red, fontSize: mediumFont),
          ),
        )
            : Container()
      ],
    ),
  );


  Widget resetPasswordButton() => Container(
        margin: EdgeInsets.only(top: 50),
        child: RoundedButton(
          onPressed: () => validate(),
          buttonText: S.of(context).reset_password,
          width: SizeUtils.width(context, 100),
          enabled: isLoading ? false : enabled,
          isLoading: isLoading,
          fontSize: bigFonts,
        ),
      );

  Widget backToLoginButton() => Container(
    margin: EdgeInsets.only(top: 20),
    child: RoundedButton(
      onPressed: () => Navigator.popUntil(context, ModalRoute.withName(loginScreen)),
      buttonText: S.of(context).back_to_login_screen,
      width: SizeUtils.width(context, 100),
      borderWidth: borderWidth,
      buttonColor: white,
      fontSize: bigFonts,
      borderColor: black,

    ),
  );


  validate() {
    if (formKey.currentState!.validate()) {
      apiBloc.add(
        ForgotPasswordApiEvent(userName: userNameController?.text ?? ""),
      );
    }
  }
}
