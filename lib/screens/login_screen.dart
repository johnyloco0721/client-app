import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/bloc/login_bloc/login_bloc.dart';
import 'package:pmaclient/bloc/login_bloc/login_events.dart';
import 'package:pmaclient/bloc/login_bloc/login_states.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_bloc.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_events.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/shared_preference_utils/pref_keys.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/utils/validation_utils.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/bordered_textfield.dart';
import 'package:pmaclient/widgets/rounded_button.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController? userNameController = TextEditingController();
  TextEditingController? passwordController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  LoginBloc loginBloc = LoginBloc();
  bool enabled = false;
  bool isLoading = false;
  BuildContext? mContext;
  bool isUpdate = false;
  ApiBloc apiBloc = ApiBloc();
  late ProfileBloc profileBloc;
  bool isTextFieldError = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    profileBloc = BlocProvider.of<ProfileBloc>(context);
    goToUpdateScreen(context);
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.dark);
    return BlocListener(
      bloc: apiBloc,
      listener: (BuildContext context, states) {
        if (states is ApiLoadingState) {
          loginBloc.add(LoginButtonClickEvents(true, false));
        } else if (states is LoginApiDataLoadState) {
          loginBloc.add(LoginButtonClickEvents(false, false));
          SpManager.setLogin(true);
          SpManager.putString(apiToken, states.model?.loginHash ?? "");
          SpManager.saveUser(states.model!.toJson());
          showToast(S().login_success);
          profileBloc.add(UserDataChangeEvent(SpManager.getUserdata()));
          goAndClearAllPageNamed(context, homeScreen);
        } else if (states is ApiErrorState) {
          loginBloc.add(LoginButtonClickEvents(false, true));
        } else {
          loginBloc.add(LoginButtonClickEvents(false, false));
        }
      },
      child: Scaffold(
        backgroundColor: white,
        body: SafeArea(
          bottom: false,
          child: BlocListener<LoginBloc, LoginStates>(
            bloc: loginBloc,
            listener: (BuildContext context, states) {
              if (states is LoginEnabled) {
                enabled = states.enabled;
              } else if (states is LoginButtonClickState) {
                isLoading = states.isLoading;
                isTextFieldError = states.isTextFieldError;
              }
            },
            child: BlocBuilder<LoginBloc, LoginStates>(
              bloc: loginBloc,
              builder: (context, states) {
                return mainLayout();
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget mainLayout() => Container(
        margin: EdgeInsets.only(
            top: SizeUtils.height(context, 5), left: 20, right: 20),
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                welcomeToMakeoverGuysImage(context),
                loginDescriptionTextView(),
                userNameTextFormField(),
                passwordTextFormField(),
                loginButton(),
                forgotPasswordButton()
              ],
            ),
          ),
        ),
      );

  Widget loginDescriptionTextView() => Container(
        margin: EdgeInsets.only(top: 15),
        child: Text(
          S().please_enter_your_username_and_password_to_login,
          style: regularFontsStyle(fontSize: bigFonts, height: 1.3),
        ),
      );

  Widget userNameTextFormField() => Container(
        margin: EdgeInsets.only(top: 15),
        child: BorderedTextField(
          controller: userNameController,
          hintText: S.of(context).user_name,
          enabled: !isLoading,
          textInputAction: TextInputAction.next,
          focusBorderColor: isTextFieldError ? red : yellow,
          fontColor: isTextFieldError ? red : black,
          enableBorderColor: isTextFieldError ? red : black,
          fontSize: bigFonts,
          hintFontSize: bigFonts,
          onChanged: (value) {
            if (isTextFieldError) {
              loginBloc.add(LoginButtonClickEvents(false, false));
            }
            loginBloc.add(
              TextFieldChange(
                enabled,
                passwordController?.text ?? "",
                value,
              ),
            );
          },
          suffixIcon: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {},
            icon: Image.asset(
              ic_user,
              height: 20,
              width: 20,
              color: isTextFieldError ? red : black,
            ),
          ),
        ),
      );

  Widget passwordTextFormField() => Container(
        margin: EdgeInsets.only(top: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BorderedTextField(
              controller: passwordController,
              hintText: S.of(context).password,
              validator: (value) => passwordValidation(value ?? ""),
              obscureText: true,
              fontColor: isTextFieldError ? red : black,
              enabled: !isLoading,
              focusBorderColor: isTextFieldError ? red : yellow,
              enableBorderColor: isTextFieldError ? red : black,
              fontSize: bigFonts,
              hintFontSize: bigFonts,
              onChanged: (value) {
                if (isTextFieldError) {
                  loginBloc.add(LoginButtonClickEvents(false, false));
                }
                loginBloc.add(
                  TextFieldChange(
                    enabled,
                    value,
                    userNameController?.text ?? "",
                  ),
                );
              },
              suffixIcon: IconButton(
                padding: EdgeInsets.zero,
                onPressed: () {},
                icon: Image.asset(
                  ic_password,
                  height: 20,
                  width: 20,
                  color: isTextFieldError ? red : black,
                ),
              ),
            ),
            isTextFieldError
                ? Container(
                    margin: EdgeInsets.only(top: 10, left: 15),
                    child: Text(
                      S().invalid_username_or_incorrect_password,
                      style:
                          regularFontsStyle(color: red, fontSize: mediumFont),
                    ),
                  )
                : Container()
          ],
        ),
      );

  Widget loginButton() => Container(
        margin: EdgeInsets.only(top: 50),
        child: RoundedButton(
          buttonText: S.of(context).login,
          width: SizeUtils.width(context, 100),
          fontColor: black,
          isLoading: isLoading,
          enabled: isLoading ? false : enabled,
          onPressed: () => validate(),
          fontSize: bigFonts,
        ),
      );

  Widget forgotPasswordButton() => Container(
        margin: EdgeInsets.only(top: 15, bottom: 20),
        child: RoundedButton(
          buttonText: S.of(context).forgot_password,
          width: SizeUtils.width(context, 100),
          borderColor: black,
          borderWidth: borderWidth,
          buttonColor: white,
          onPressed: () => goToNextNamedRoute(context, forgotPasswordScreen),
          fontSize: bigFonts,
        ),
      );

  validate() async {
    if (formKey.currentState!.validate()) {
      if (AppDataModel.getDeviceToken().isEmpty ?? true) {
        String? token = await FirebaseMessaging.instance.getToken();
        AppDataModel.setDeviceToken(token);
      }
      apiBloc.add(LoginApiEvent(
        password: replaceWhiteSpace(passwordController?.text ?? ""),
        token: replaceWhiteSpace(AppDataModel.getDeviceToken()),
        userName: replaceWhiteSpace(userNameController?.text ?? ""),
      ));
    }
  }
}
