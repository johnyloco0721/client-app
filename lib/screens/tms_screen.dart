import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';

class TmsScreen extends StatefulWidget {
  const TmsScreen({Key? key}) : super(key: key);

  @override
  _TmsScreenState createState() => _TmsScreenState();
}

class _TmsScreenState extends State<TmsScreen> {
  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.light);
    return Scaffold(
      backgroundColor: black,
      appBar: AppBar(
        toolbarHeight: 0,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: black,
      ),
      body: SafeArea(
        bottom: false,
        child: mainLayout(),
      ),
    );
  }

  Widget mainLayout() => Container(
        color: white,
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            headerLayoutWithBackArrow(context, S().tmsByCozyHomes,
                isCenter: false),
            Expanded(
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: yellow,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(50),
                        ),
                      ),
                      child: Lottie.asset(
                        tms_screen_lottie,
                        width: SizeUtils.width(context, 70),
                        height: SizeUtils.height(context, 40),
                      ),
                    ),
                  ),
                  Container(
                    color: yellow,
                    child: Container(
                      height: SizeUtils.height(context, 42),
                      padding: EdgeInsets.all(25),
                      decoration: BoxDecoration(
                        color: white,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(50),
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            alignment: Alignment.center,
                            child: Text(
                              S().partneringWithCozyHomes,
                              style: boldFontsStyle(fontSize: bigFonts),
                            ),
                          ),
                          SizedBox(height: 15),
                          Expanded(
                            child: Text(
                              S().ourTenantManagementServicesTmshelps,
                              textAlign: TextAlign.center,
                              style: regularFontsStyle(
                                fontSize: mediumFont,
                                color: grey,
                                height: 1.3,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 15),
                            alignment: Alignment.center,
                            child: CupertinoButton(
                              onPressed: () => goToNextNamedRoute(
                                context,
                                codyHomesWebScreen,
                              ),
                              padding: EdgeInsets.zero,
                              child: Container(
                                height: 60,
                                width: 60,
                                margin: EdgeInsets.only(bottom: 30),
                                decoration: BoxDecoration(
                                    color: black,
                                    shape: BoxShape.circle,
                                    boxShadow: [
                                      BoxShadow(
                                        color: grey.withOpacity(0.1),
                                        blurRadius: 5,
                                        spreadRadius: 5,
                                        offset: Offset(0, 3),
                                      ),
                                    ]),
                                alignment: Alignment.center,
                                child: Image.asset(
                                  ic_enter_arrow_icon,
                                  height: 22,
                                  width: 22,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );
}
