import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/projects_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/rounded_button.dart';

class RateUsScreen extends StatefulWidget {
  const RateUsScreen({Key? key}) : super(key: key);

  @override
  _RateUsScreenState createState() => _RateUsScreenState();
}

class _RateUsScreenState extends State<RateUsScreen> {
  late HomeProject homeProjectModel;

  @override
  void initState() {
    // TODO: implement initState
    homeProjectModel = AppDataModel.getHomeProjectModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: black,
      appBar: AppBar(
        toolbarHeight: 0,
        backgroundColor: black,
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      body: SafeArea(bottom: false, child: mainLayout()),
    );
  }

  Widget mainLayout() => Container(
        color: white,
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            headerLayoutWithCross(context, S().rate_us),
            Container(
              padding: EdgeInsets.only(left: 20, bottom: 20),
              color: black,
              width: SizeUtils.width(context, 100),
              child: Text(
                "${homeProjectModel.propertyName} (${homeProjectModel.projectUnit})",
                style: boldFontsStyle(fontSize: mediumFont, color: white),
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.all(35),
                    child:
                        Lottie.asset(rate_us_lottie, height: 200, width: 200),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 20, top: 15),
                    child: Text(
                      S().how_did_we_do,
                      style: boldFontsStyle(fontSize: bigFonts),
                    ),
                  ),
                  Container(
                    width: 300,
                    padding: EdgeInsets.only(left: 20, top: 10, right: 20),
                    child: Text(
                      S().rate_our_service_now_to_get,
                      textAlign: TextAlign.center,
                      style: regularFontsStyle(
                        fontSize: mediumFont,
                        height: 1.5,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 20, right: 20, top: 25),
              child: RoundedButton(
                width: 200,
                buttonText: S().start,
                onPressed: () => goAndClearCurrentPageNamed(
                  context,
                  ratingQuestionsScreen,
                ),
              ),
            ),
            GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Container(
                margin:
                    EdgeInsets.only(left: 20, bottom: 50, right: 20, top: 25),
                child: Text(
                  S().may_be_later,
                  textAlign: TextAlign.center,
                  style: boldFontsStyle(fontSize: bigFonts),
                ),
              ),
            ),
          ],
        ),
      );
}
