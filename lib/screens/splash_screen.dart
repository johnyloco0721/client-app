import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;
  Timer? timer;
  bool isMaintenance = false;
  late ApiBloc apiBloc;

  @override
  void didUpdateWidget(SplashScreen oldWidget) {
    _controller.reset();
    _controller.forward();
    super.didUpdateWidget(oldWidget);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = AnimationController(
        duration: const Duration(milliseconds: 2500), vsync: this, value: 0.5);
    _animation =
        CurvedAnimation(parent: _controller, curve: Curves.bounceInOut);
    _controller.forward();
    apiBloc = BlocProvider.of<ApiBloc>(context);
    timer = Timer(Duration(seconds: 3), () {
      apiBloc.add(ConfigApiEvent());
    });
  }

  @override
  dispose() {
    _controller.dispose();
    super.dispose();
    timer?.cancel();
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.light);
    return BlocConsumer<ApiBloc, ApiStates>(
      bloc: apiBloc,
      listener: (BuildContext context, states) {
        if (states is ConfigApiDataLoadState) {
          SpManager.saveConfigModel(states.configModel?.toJson() ?? {});
          goToNext(states.configModel?.response?.underMaintenance ?? false);
        } else if (states is ApiErrorState) {
          goToNext(false);
        }
      },
      builder: (context, state) {
        return Scaffold(
          backgroundColor: yellow,
          body: SafeArea(
            bottom: false,
            child: Center(
              child: ScaleTransition(
                scale: _animation,
                alignment: Alignment.center,
                child: Container(
                  width: SizeUtils.width(context, 60),
                  child: AspectRatio(
                    aspectRatio: 245.58 / 69.05,
                    child: Image.asset(
                      ic_makeover_logo,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  goToWelcomeScreen() {
    timer = Timer(Duration(seconds: 3), () {
      ApiStates states = apiBloc.state;
      if (states is ConfigApiDataLoadState) {
        goToNext(states.configModel?.response?.underMaintenance ?? false);
      } else if (states is ApiErrorState) {
        goToNext(false);
      }
    });
  }

  void goToNext(bool isMaintenance) {
    if (isMaintenance) {
      goAndClearAllPageNamed(context, maintenanceScreen);
    } else {
      if (SpManager.isLogin()) {
        goAndClearAllPageNamed(context, homeScreen);
      } else {
        if (SpManager.getIsFirstTime()) {
          goAndClearAllPageNamed(context, loginOtpScreen);
        } else {
          goAndClearAllPageNamed(context, welcomeScreen);
        }
      }
    }
  }
}
