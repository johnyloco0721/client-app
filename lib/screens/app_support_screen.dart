import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/config_model.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/rounded_button.dart';
import 'package:url_launcher/url_launcher.dart';

class AppSupportScreen extends StatelessWidget {
  const AppSupportScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.light);
    ApiBloc apiBloc = BlocProvider.of<ApiBloc>(context);
    ConfigModel? model = SpManager.getConfigModel();
    return Scaffold(
      backgroundColor: black,
      body: SafeArea(
        bottom: false,
        child: BlocBuilder<ApiBloc, ApiStates>(
            bloc: apiBloc,
            builder: (context, state) {
              if (state is ConfigApiDataLoadState) {
                model = state.configModel;
              }
              return mainLayout(context, model);
            }),
      ),
    );
  }

  Widget mainLayout(context, ConfigModel? model) => Container(
        color: white,
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            headerLayoutWithCross(context, S().app_support),
            Expanded(
              child: Container(
                color: white,
                padding: EdgeInsets.all(15),
                alignment: Alignment.center,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      width: SizeUtils.width(context, 70),
                      child: AspectRatio(
                        aspectRatio: 400 / 400,
                        child: LottieBuilder.asset(
                          app_support, fit: BoxFit.fill,
                          // height: 250,
                          // width: 250,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 25),
                      child: Text(
                        S().hello_having_issue_with_the_app,
                        textAlign: TextAlign.center,
                        style:
                            regularFontsStyle(fontSize: bigFonts, height: 1.3),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 35, bottom: 40),
                      child: RoundedButton(
                          buttonText: S().contact_us,
                          buttonColor: yellow,
                          fontSize: bigFonts,
                          width: 150,
                          onPressed: () async {
                            if (await canLaunch(emailLaunchUri(
                              model?.response?.appSupportEmail,
                              subject: model?.response?.supportEmailSubject,
                            ).toString())) {
                              launch(
                                emailLaunchUri(
                                  model?.response?.appSupportEmail,
                                  subject: model?.response?.supportEmailSubject,
                                ).toString(),
                              );
                            } else {
                              print("No email client found");
                            }
                          }),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
}
