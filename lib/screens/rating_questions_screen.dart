import 'dart:convert';

import 'package:expandable_page_view/expandable_page_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_array_list.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/list_items/rating_list_items.dart';
import 'package:pmaclient/models/projects_model.dart';
import 'package:pmaclient/models/rating_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/rounded_button.dart';

class RatingQuestionsScreen extends StatefulWidget {
  const RatingQuestionsScreen({Key? key}) : super(key: key);

  @override
  _RatingQuestionsScreenState createState() => _RatingQuestionsScreenState();
}

class _RatingQuestionsScreenState extends State<RatingQuestionsScreen> {
  late HomeProject homeProjectModel;
  late PageController _pageController;
  TextEditingController question1Controller = TextEditingController();
  TextEditingController question2Controller = TextEditingController();
  TextEditingController question3Controller = TextEditingController();
  bool isQuestion = true;
  bool isLoading = false;
  int pageIndex = 0;
  int questionIndex = 1;
  double sliderValue = 9.0;
  int width = 1;
  bool isTextField = false;
  bool enabled = true;
  bool keyBoardVisible = false;
  late ApiBloc apiBloc;

  @override
  void initState() {
    // TODO: implement initState
    homeProjectModel = AppDataModel.getHomeProjectModel();
    super.initState();
    _pageController = PageController(initialPage: 0, keepPage: true);
    apiBloc = BlocProvider.of<ApiBloc>(context);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    keyBoardVisible = MediaQuery.of(context).viewInsets.bottom != 0;
    return Scaffold(
      backgroundColor: black,
      appBar: AppBar(
        toolbarHeight: 0,
        backgroundColor: black,
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      body: SafeArea(
        bottom: false,
        child: BlocConsumer<ApiBloc, ApiStates>(
          bloc: apiBloc,
          listener: (context, state) {
            if (state is ReviewApiLoadingState) {
              isLoading = true;
            } else if (state is SendReviewApiLoadState) {
              isLoading = false;
              AppDataModel.setFeedBackModel(state.model);
              goAndClearCurrentPageNamed(
                context,
                rateResolveIssueCompleteScreen,
                args: 0,
              );
            } else if (state is ReviewApiErrorState) {
              isLoading = false;
              showToast(
                state.errorMessage ?? S().server_error_please_try_again,
              );
            }
          },
          builder: (context, state) {
            return mainLayout();
          },
        ),
      ),
    );
  }

  Widget mainLayout() => AbsorbPointer(
        absorbing: isLoading,
        child: Container(
          color: white,
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              headerLayoutWithCross(context, S().rate_us),
              Container(
                padding: EdgeInsets.only(left: 20, bottom: 20),
                color: black,
                width: SizeUtils.width(context, 100),
                child: Text(
                  "${homeProjectModel.propertyName} (${homeProjectModel.projectUnit})",
                  style: boldFontsStyle(fontSize: mediumFont, color: white),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20, top: 25, right: 20),
                width: SizeUtils.width(context, 100),
                decoration: BoxDecoration(
                  color: whiteGrey,
                  borderRadius: BorderRadius.circular(12),
                  boxShadow: [
                    BoxShadow(
                      color: shadowColor.withOpacity(0.1),
                      spreadRadius: 2,
                      blurRadius: 1,
                      offset: Offset(0, 0.19),
                    ),
                  ],
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: Container(
                    padding: EdgeInsets.all(15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: SizeUtils.width(context, 100),
                          height: 8,
                          child: LayoutBuilder(builder: (context, constraints) {
                            return Stack(
                              children: [
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    height: 2,
                                    width: SizeUtils.width(context, 100),
                                    decoration: BoxDecoration(
                                      color: lightGrey,
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 8,
                                  width:
                                      constraints.maxWidth / 3 * questionIndex,
                                  decoration: BoxDecoration(
                                    color: yellow,
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                ),
                              ],
                            );
                          }),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: HtmlWidget(
                            "<b>${questionIndex.toString()}/3</b> ${S().question}",
                            textStyle: regularFontsStyle(fontSize: mediumFont),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                child: ExpandablePageView.builder(
                  controller: _pageController,
                  itemCount: questionList.length,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return RatingListItems(
                      index: index,
                      sliderValue: sliderValue,
                      isQuestion: isQuestion,
                      isSuggestion: isTextField,
                      question: isQuestion
                          ? questionList[index].question
                          : questionList[index].feedback,
                      onTextFieldChange: (value) => textFieldChange(value),
                      controller: index == 0
                          ? question1Controller
                          : index == 1
                              ? question2Controller
                              : question3Controller,
                      // onPressed: () => doneButtonClick(),
                      onChanged: (value) {
                        setState(() {
                          sliderValue = value;
                        });
                      },
                    );
                  },
                  onPageChanged: (value) {
                    setState(() {
                      pageIndex = value;
                      isQuestion = true;
                      enabled = true;
                      sliderValue = 9.0;
                      questionIndex = value + 1;
                    });
                  },
                ),
              ),
              keyBoardVisible ? Container() : bottomLayout(),
            ],
          ),
        ),
      );

  Widget bottomLayout() => Container(
        margin: EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            pageIndex == 0 && isQuestion
                ? Container()
                : roundIcon(
                    right: 5.0,
                    onPressed: () => previousButtonClick(),
                  ),
            pageIndex == 2 && sliderValue.toInt() >= 7
                ? RoundedButton(
                    buttonText: S().submit,
                    width: 150,
                    isLoading: isLoading,
                    onPressed: () => submitButtonClick(),
                  )
                : roundIcon(
                    icon: ic_right_arrow,
                    left: 5.0,
                    enabled: enabled,
                    onPressed: enabled ? () => nextButtonClick() : null),
          ],
        ),
      );

  Widget roundIcon({icon, left, right, enabled = true, onPressed}) =>
      CupertinoButton(
        padding: EdgeInsets.zero,
        onPressed: onPressed,
        child: Card(
          color: enabled ? black : black.withOpacity(0.2),
          margin: EdgeInsets.zero,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: CircleBorder(),
          child: Container(
            height: 45,
            width: 45,
            alignment: Alignment.center,
            child: Padding(
              padding: EdgeInsets.only(left: left ?? 0, right: right ?? 0),
              child: Image.asset(
                icon ?? ic_left_arrow,
                height: 20,
                width: 20,
                color: yellow,
              ),
            ),
          ),
        ),
      );

  previousButtonClick() => setState(() {
        if (!isQuestion) {
          isQuestion = true;
          enabled = true;
        } else {
          _pageController.animateToPage(
            pageIndex - 1,
            duration: Duration(milliseconds: 500),
            curve: Curves.easeIn,
          );
        }
      });

  nextButtonClick() => setState(() {
        if (pageIndex != 2) {
          if (sliderValue.toInt() < 7 && isQuestion) {
            isQuestion = false;
            enabled = false;
          } else {
            addDataToReviewList(true);
            _pageController.animateToPage(
              pageIndex + 1,
              duration: Duration(milliseconds: 500),
              curve: Curves.easeIn,
            );
          }
        } else {
          addDataToReviewList(true);
          goAndClearCurrentPageNamed(context, rateResolveIssueScreen);
        }
      });

  addDataToReviewList(resolve) {
    String feedBackText = questionIndex == 1
        ? question1Controller.text
        : questionIndex == 2
            ? question2Controller.text
            : question3Controller.text;
    List<ReviewData> reviewList = AppDataModel.getReviewList();
    bool? isExist =
        reviewList.any((element) => element.questionID == questionIndex);
    ReviewData reviewData = ReviewData(
      questionID: questionIndex,
      feedback: feedBackText,
      rate: (sliderValue + 1.0).toInt(),
      resolve: pageIndex == 2 ? resolve : false,
    );
    if (isExist) {
      if (pageIndex == 0) {
        reviewList.clear();
        reviewList.add(reviewData);
      } else {
        reviewList.removeAt(questionIndex - 1);
        reviewList.insert(pageIndex, reviewData);
      }
    } else {
      reviewList.add(reviewData);
    }
    AppDataModel.setReviewList(reviewList);
  }

  doneButtonClick() => setState(() {
        isTextField = false;
        enabled = true;
        FocusScope.of(context).unfocus();
      });

  submitButtonClick() {
    addDataToReviewList(false);
    RatingModel ratingModel = RatingModel(
        projectID: homeProjectModel.projectID,
        reviewData: AppDataModel.getReviewList());
    String body = jsonEncode(ratingModel.toJson());
    print(body);
    apiBloc.add(SendReviewEvent(body: body));
  }

  textFieldChange(String value) {
    {
      setState(() {
        if (value.isNotEmpty) {
          isTextField = true;
          enabled = true;
        } else {
          isTextField = false;
          enabled = false;
        }
      });
    }
  }
}
