import 'package:alt_sms_autofill/alt_sms_autofill.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:huawei_push/huawei_push.dart' as HuaWei;
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/bloc/otp_login_bloc/otp_login_bloc.dart';
import 'package:pmaclient/bloc/otp_login_bloc/otp_login_events.dart';
import 'package:pmaclient/bloc/otp_login_bloc/otp_login_states.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_bloc.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_events.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/models/otp_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/shared_preference_utils/pref_keys.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/phone_number_input/utils/phone_number.dart';
import 'package:pmaclient/widgets/phone_number_input/utils/selector_config.dart';
import 'package:pmaclient/widgets/phone_number_input/widgets/input_widget.dart';
import 'package:pmaclient/widgets/rounded_button.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:timer_count_down/timer_controller.dart';
import 'package:timer_count_down/timer_count_down.dart';

class LoginOtpScreen extends StatefulWidget {
  const LoginOtpScreen({Key? key}) : super(key: key);

  @override
  _LoginOtpScreenState createState() => _LoginOtpScreenState();
}

class _LoginOtpScreenState extends State<LoginOtpScreen> { 
  PageController pageController = PageController();
  TextEditingController? mobileNumberController = TextEditingController();
  // late OtpFieldController otpController = OtpFieldController();
  TextEditingController otpController = TextEditingController();
  FocusNode otpFocusNode = FocusNode();
  TextEditingController otpEmailController = TextEditingController();
  FocusNode otpEmailFocusNode = FocusNode();
  final CountdownController countdownController = new CountdownController(autoStart: true);
  final CountdownController countdownEmailController = new CountdownController(autoStart: true);
  final mobileFormKey = GlobalKey<FormState>();
  late final otpFormKey = GlobalKey<FormState>();
  late final otpEmailFormKey = GlobalKey<FormState>();
  String? mobileNumber;
  int resendCounter = 1;
  int emailresendCounter = 1;
  bool nextButtonEnabled = true;
  bool resendButtonEnabled = true;
  bool resendEmailButtonEnabled = true;
  bool isRequestOtpLoading = false;
  bool isRequestOtpEmailLoading = false;
  bool isResendLoading = false;
  bool isEmailResendLoading = false;
  bool isMobileFieldError = false;
  bool isOtpFieldError = false;
  bool isOtpFieldEnabled = true;
  bool isOtpEmailFieldError = false;
  bool isOtpEmailFieldEnabled = true;
  String? mobileFieldError;
  String otpFieldError = "";
  String otpEmailFieldError = "";
  ApiBloc apiBloc = ApiBloc();
  RequestOtpBloc requestOtpBloc = RequestOtpBloc();
  OtpLoginBloc otpLoginBloc = OtpLoginBloc();
  OtpEmailLoginBloc otpEmailLoginBloc = OtpEmailLoginBloc();
  OtpModel? otpModel;
  EmailOtpModel? emailotpModel;
  late ProfileBloc profileBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    profileBloc = BlocProvider.of<ProfileBloc>(context);
    goToUpdateScreen(context);

    initSmsListener();
  }

  @override
  void dispose() {
    pageController.dispose();
    mobileNumberController?.dispose();
    otpController.dispose();
    otpFocusNode.dispose();
    otpEmailController.dispose();
    otpEmailFocusNode.dispose();
    AltSmsAutofill().unregisterListener();
    
    super.dispose();
  }

  Future<void> initSmsListener() async {
    String? comingSms;
    try {
      comingSms = await AltSmsAutofill().listenForSms;
    } on PlatformException {
      comingSms = 'Failed to get Sms.';
    }
    if (!mounted) return;
    // print("====>Message: $comingSms");
    if (Theme.of(context).platform == TargetPlatform.android) {
      String stringCode = comingSms?.substring(comingSms.length - 4) ?? '';
      // print("====>Code: $stringCode");
      if (comingSms != null && comingSms != 'Failed' && stringCode.length == 4) {
        // Or use regex
        otpController.text = stringCode;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: apiBloc,
      listener: (BuildContext context, state) {
        if (state is RequestOtpApiLoadingState) {
          requestOtpBloc.add(NextButtonClickEvents(true, false, ""));
        } else if (state is RequestOtpEmailApiLoadingState) {
          otpLoginBloc.add(SendEmailButtonClickEvents(true));
        } else if (state is RequestOtpApiDataLoadState) {
          otpModel = state.model;
          requestOtpBloc.add(NextButtonClickEvents(false, false, ""));
          goToOtpFieldPage();
        } else if (state is RequestOtpEmailApiDataLoadState) {
          emailotpModel = state.model;
          otpLoginBloc.add(SendEmailButtonClickEvents(false));
          goToEmailFieldPage();
        } else if (state is RequestOtpApiErrorState) {
          if (state.errorMessage == 'You have already requested OTP earlier. Please try again later.') {
            goToOtpFieldPage();
            requestOtpBloc.add(NextButtonClickEvents(false, false, ""));
          } else {
            requestOtpBloc.add(NextButtonClickEvents(false, true, state.errorMessage!));
          }
        } else if (state is RequestOtpEmailApiErrorState) {
          if (state.errorMessage == 'Email already sent before.') {
            goToEmailFieldPage();
            otpLoginBloc.add(SendEmailButtonClickEvents(false));
          } else {
            otpLoginBloc.add(SendEmailButtonClickEvents(false));
          }
        } else if (state is OtpLoginApiLoadingState) {
        } else if (state is OtpLoginApiDataLoadState) {
          otpLoginBloc.add(OtpFieldChanged(false));
          otpEmailLoginBloc.add(OtpEmailFieldChanged(false));
          SpManager.setLogin(true);
          SpManager.putString(apiToken, state.model?.loginHash ?? "");
          SpManager.saveUser(state.model!.toJson());
          showToast(S().login_success);
          profileBloc.add(UserDataChangeEvent(SpManager.getUserdata()));
          goAndClearAllPageNamed(context, homeScreen);
        } else if (state is OtpLoginApiErrorState) {
          otpLoginBloc.add(OtpFieldChanged(true));
          otpEmailLoginBloc.add(OtpEmailFieldChanged(true));
          otpLoginBloc.add(ResendButtonClickEvents(isResendLoading, true, state.errorMessage!));
          otpEmailLoginBloc.add(EmailResendButtonClickEvents(isEmailResendLoading, true, state.errorMessage!));
        } else {
          requestOtpBloc.add(NextButtonClickEvents(false, false, ""));
          otpLoginBloc.add(SendEmailButtonClickEvents(false));
          otpLoginBloc.add(OtpFieldChanged(true));
          otpEmailLoginBloc.add(OtpEmailFieldChanged(true));
          otpLoginBloc.add(ResendButtonClickEvents(false, false, ""));
          otpEmailLoginBloc.add(EmailResendButtonClickEvents(false, false, ""));
        }
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: white,
        body: SafeArea(
          bottom: false,
          child: mainLayout(),
        ),
      ),
    );
  }

  Widget mainLayout() => Container(
    height: MediaQuery.of(context).size.height * 0.85,
    decoration: BoxDecoration(
      // color: Colors.pink,
      image: DecorationImage(
        image: AssetImage(ic_login_illustration),
        alignment: AlignmentDirectional.bottomEnd,
        scale: 1.05
      ),
    ),
    margin: EdgeInsets.only(top: SizeUtils.height(context, 5), left: 20, right: 20),
    padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        welcomeToMakeoverGuysImage(context),
        Expanded(
          child: PageView(
            scrollDirection: Axis.horizontal,
            physics: NeverScrollableScrollPhysics(),
            controller: pageController,
            onPageChanged: (index) {
              if (index == 1 || index == 2) {
                // otpFocusNode.requestFocus();
                // otpEmailFocusNode.requestFocus();
                // countdownController.restart();
              }
            },
            children: [
              CustomKeepAlive(
                widget: BlocListener<RequestOtpBloc, RequestOtpStates>(
                  bloc: requestOtpBloc,
                  listener: (BuildContext context, state) {
                    if (state is NextButtonEnabled) {
                      nextButtonEnabled = state.enabled;
                    } else if (state is NextButtonClickState) {
                      isRequestOtpLoading = state.isLoading;
                      isMobileFieldError = state.isMobileFieldError;
                      mobileFieldError = state.mobileFieldError;
                    }
                  },
                  child: BlocBuilder<RequestOtpBloc, RequestOtpStates>(
                    bloc: requestOtpBloc,
                    builder: (context, state) {
                      return mobileForm();
                    },
                  ),
                ),
              ),
              CustomKeepAlive(
                widget: BlocListener<OtpLoginBloc, OtpLoginStates>(
                  bloc: otpLoginBloc,
                  listener: (BuildContext context, state) {
                    if (state is OtpFieldEnabled) {
                      // TODO: Set enabled
                      // otpController.setEnabled(state.enabled);
                      isOtpFieldEnabled = state.enabled;
                    } else if (state is SendEmailButtonEnabled) {
                      isOtpEmailFieldEnabled = state.enabled;
                    } else if (state is ResendButtonClickState) {
                      isResendLoading = state.isLoading;
                      isOtpFieldError = state.isOtpFieldError;
                      otpFieldError = state.otpFieldError;
                    } else if (state is SendEmailButtonClickState) {
                      isRequestOtpEmailLoading = state.isEmailLoading;
                    }
                  },
                  child: BlocBuilder<OtpLoginBloc, OtpLoginStates>(
                    bloc: otpLoginBloc,
                    builder: (context, state) {
                      return otpForm();
                    },
                  ),
                ),
              ),
              CustomKeepAlive(
                widget: BlocListener<OtpEmailLoginBloc, OtpEmailLoginStates>(
                  bloc: otpEmailLoginBloc,
                  listener: (BuildContext context, state) {
                    if (state is OtpEmailFieldEnabled) {
                      // TODO: Set enabled
                      isOtpEmailFieldEnabled = state.enabled;
                    } else if (state is EmailResendButtonClickState) {
                      isEmailResendLoading = state.isEmailLoading;
                      isOtpEmailFieldError = state.isOtpEmailFieldError;
                      otpEmailFieldError = state.otpEmailFieldError;
                    }
                  },
                  child: BlocBuilder<OtpEmailLoginBloc, OtpEmailLoginStates>(
                    bloc: otpEmailLoginBloc,
                    builder: (context, state) {
                      return otpEmailForm();
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );

  Widget mobileForm() => Form(
    key: mobileFormKey,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        loginDescriptionTextView(),
        mobileTextFormField(),
        nextButton(),
      ],
    ),
  );

  Widget loginDescriptionTextView() => Container(
        margin: EdgeInsets.only(top: 15),
        child: RichText(
          text: TextSpan(
            style: regularFontsStyle(fontSize: bigFonts, height: 1.3),
            children: <TextSpan>[
              TextSpan(text: S.of(context).enterYour),
              TextSpan(text: S.of(context).mobileNumber, style: boldFontsStyle(fontSize: bigFonts, height: 1.3)),
            ]
          ),
        ),
      );

  Widget mobileTextFormField() => Container(
    margin: EdgeInsets.only(top: 15),
    child: InternationalPhoneNumberInput(
      textFieldController: mobileNumberController,
      hintText: S.of(context).mobileNumber,
      initialValue: PhoneNumber(isoCode: 'MY'),
      formatInput: true,
      maxLength: 15,
      selectorConfig: SelectorConfig(
        showFlags: false,
        selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
        setSelectorButtonAsPrefixIcon: true,
        leadingPadding: 8.0,
        trailingSpace: false,
      ),
      selectorTextStyle: italicFontsStyle(fontSize: bigFonts),
      selectorButtonLabelStyle: mediumFontsStyle(fontSize: bigFonts, color: grey),
      spaceBetweenSelectorAndTextField: 0,
      textStyle: boldFontsStyle(fontSize: bigFonts, color: isMobileFieldError ? red : black),
      inputDecoration: InputDecoration(
        hintText: S.of(context).mobileNumber,
        fillColor: shimmerGrey,
        filled: false,
        isDense: true,
        hintStyle: italicFontsStyle(fontSize: bigFonts, color: grey),
        errorStyle: italicFontsStyle(color: red, height: 1.3),
        errorMaxLines: 5,
        alignLabelWithHint: false,
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            width: borderWidth,
            color: black,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            width: 2.0,
            color: yellow,
          ),
        ),
        disabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(width: 1, color: grey),
        ),
        contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
        errorText: mobileFieldError != "" ? mobileFieldError?.replaceAll('<br>', '\n') : null,
      ),
      searchBoxDecoration: InputDecoration(
        hintText: S.of(context).searchCountry,
        fillColor: shimmerGrey,
        filled: false,
        isDense: true,
        hintStyle: italicFontsStyle(fontSize: bigFonts, color: grey),
        errorStyle: regularFontsStyle(fontSize: bigFonts, color: red),
        alignLabelWithHint: false,
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            width: borderWidth,
            color: black,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            width: 2.0,
            color: yellow,
          ),
        ),
        disabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(width: 1, color: grey),
        ),
        contentPadding: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 15.0),
      ),
      onInputChanged: (PhoneNumber value) {
        mobileFormKey.currentState!.validate();
        mobileNumber = value.toString();
        if (isMobileFieldError) {
          requestOtpBloc.add(NextButtonClickEvents(false, false, ""));
        }
      },
      validator: (String? value) {
        if (value!.isEmpty || value == '') {
          return S.of(context).pleaseEnterYourMobileNumber;
        }
        // hide to avoid text changed error still exist
        // else if (isMobileFieldError && mobileFieldError != "") {
        //   return mobileFieldError;
        // }
        return null;
      },
    ),
  );

  Widget nextButton() => Container(
    margin: EdgeInsets.only(top: 50),
    child: RoundedButton(
      buttonText: S.of(context).next,
      width: SizeUtils.width(context, 100),
      fontColor: black,
      isLoading: isRequestOtpLoading,
      enabled: isRequestOtpLoading ? false : nextButtonEnabled,
      onPressed: () => requestOtpCode(),
      fontSize: bigFonts,
    ),
  );

  requestOtpCode() async {
    setState(() {
      mobileFieldError = null;
    });

    if (mobileFormKey.currentState!.validate()) {
      requestOtpBloc.add(NextButtonClickEvents(true, false, ""));
      apiBloc.add(RequestOtpApiEvent(
        contactNumber: replaceWhiteSpace(mobileNumber ?? ""),
      ));
    }
  }

  Widget otpForm() => Form(
    key: otpFormKey,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        otpDescriptionTextView(),
        otpFormField(),
        clearFormButton(),
        resendButton(),
        otpEmail(),
      ],
    ),
  );

  Widget otpDescriptionTextView() => Container(
    margin: EdgeInsets.only(top: 15),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          child: Icon(Icons.arrow_back_ios),
          onTap: () => goToMobileFieldPage(),
        ),
        Expanded(
          child: RichText(
            text: TextSpan(
              style: regularFontsStyle(fontSize: bigFonts, height: 1.3),
              children: <TextSpan>[
                TextSpan(text: S.of(context).enterThe),
                TextSpan(text: S.of(context).fourDigitCode, style: boldFontsStyle(fontSize: bigFonts, height: 1.3)),
                TextSpan(text: S.of(context).sentToYourMobilePhone),
              ]
            ),
          ),
        ),
      ],
    )
  );

  Widget otpFormField() => Container(
    margin: EdgeInsets.only(top: 30),
    child: PinCodeTextField(
      appContext: context,
      controller: otpController,
      focusNode: otpFocusNode,
      length: 4,
      obscureText: false,
      enabled: true,
      enablePinAutofill: true,
      autoFocus: true,
      keyboardType: TextInputType.number,
      autoDismissKeyboard: true,
      autoDisposeControllers: false,
      animationType: AnimationType.fade,
      animationDuration: Duration(milliseconds: 100),
      errorTextSpace: 0,
      enableActiveFill: true,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderRadius: BorderRadius.circular(4),
        fieldWidth: MediaQuery.of(context).size.width * 0.2,
        fieldHeight: 60,
        activeFillColor: white,
        inactiveFillColor: white,
        selectedFillColor: white,
        activeColor: isOtpFieldError ? red : darkYellow,
        inactiveColor: lightGrey,
        disabledColor: whiteGrey,
        selectedColor: darkYellow,
        borderWidth: 1.0,
        errorBorderColor: red,
      ),
      cursorColor: Colors.black,
      onChanged: (pin) {
        otpFormKey.currentState!.validate();
      },
      onCompleted: (pin) {
        login(pin);
      },
    ),
  );

  Widget clearFormButton() => Container(
    child: Row(
      children: [
        Expanded(
          child: isOtpFieldError ? Text(
            S.of(context).wrongCodeEntered,
            style: italicFontsStyle(color: red),
          ) : SizedBox(),
        ),
        TextButton(
          style: TextButton.styleFrom(primary: yellow),
          onPressed: () => clearCode(),
          child: Text(
            S.of(context).clearCode, 
            style: boldFontsStyle(fontSize: mediumFont, color: darkYellow),
          ),
        ),
      ],
    ),
  );

  Widget resendButton() => Container(
    margin: EdgeInsets.only(top: 50),
    child: isResendLoading ?  
      Countdown(
        key: GlobalObjectKey('countdown'),
        controller: countdownController,
        seconds: (resendCounter % 10 == 0) ? 600 : 120,
        build: (_, double time) {
          var duration = Duration(seconds: time.toInt());
          return RoundedButton(
            buttonText: S.of(context).resendIn + '${duration.inMinutes.remainder(60)}:${duration.inSeconds.remainder(60).toString().padLeft(2, '0')}',
            width: SizeUtils.width(context, 100),
            buttonColor: white,
            borderColor: black,
            borderWidth: 1.0,
            fontColor: black,
            isLoading: false,
            enabled: false,
            fontSize: bigFonts,
          );
        },
        interval: Duration(seconds: 1),
        onFinished: () {
          setState(() {
            isResendLoading = false;
          });
        }
      ):
      RoundedButton(
        buttonText: S.of(context).resendCode,
        width: SizeUtils.width(context, 100),
        buttonColor: white,
        borderColor: black,
        borderWidth: 1.0,
        fontColor: black,
        isLoading: false,
        enabled: isResendLoading ? false : resendButtonEnabled,
        onPressed: () {
          resendCode(); // Your existing code for resending the code
          setState(() {
            resendCounter++; // Increment the resendCounter variable
          });
        },
        fontSize: bigFonts,
      ),
  );

  Widget otpEmail() => Container(
    margin: EdgeInsets.only(top: 30),
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Row(
        children: [
          Expanded(
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: regularFontsStyle(fontSize: bigFonts, height: 1.3),
                children: <TextSpan>[
                  TextSpan(text: S.of(context).havingTrouble, style: boldFontsStyle(fontWeight: FontWeight.w700, fontSize: mediumFont, height: 1.3)),
                ],
              ),
            ),
          ),
        ],
      ),
      Row(
         mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            child: RichText(
              text: TextSpan(
                style: regularFontsStyle(fontSize: bigFonts, height: 1.3),
                children: <TextSpan>[
                  TextSpan(text: S.of(context).verifyEmail, style: TextStyle(fontWeight: FontWeight.w700, color: darkYellow, fontSize: mediumFont, decoration: TextDecoration.underline, height: 1.3)),
                ],
              ),
            ),
            onTap: () {
              requestOtpEmailCode();
            }
          ),
        ],
      ),
    ],
    ),
  );

  requestOtpEmailCode() async {
    if (otpFormKey.currentState!.validate()) {
      otpLoginBloc.add(SendEmailButtonClickEvents(true));
      apiBloc.add(RequestOtpEmailApiEvent(
        ownerID: otpModel!.ownerID.toString(), 
      ));
    }
  }

  Widget otpEmailForm() => Form(
    key: otpEmailFormKey,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        otpEmailDescriptionTextView(),
        otpEmailFormField(),
        clearFormButton(),
        resendEmailButton(),
        otpSMS(),
      ],
    ),
  );

  Widget otpEmailDescriptionTextView() => Container(
    margin: EdgeInsets.only(top: 15),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          child: Icon(Icons.arrow_back_ios),
          onTap: () => goToOtpFieldPage(),
        ),
        Expanded(
          child: RichText(
            text: TextSpan(
              style: regularFontsStyle(fontSize: bigFonts, height: 1.3),
              children: <TextSpan>[
                TextSpan(text: S.of(context).enterThe),
                TextSpan(text: S.of(context).fourDigitCode, style: boldFontsStyle(fontSize: bigFonts, height: 1.3)),
                TextSpan(text: S.of(context).sentToYourEmail),
                TextSpan(text: otpModel?.ownerEmailAddress, style: boldFontsStyle(fontSize: bigFonts, height: 1.3)),
              ]
            ),
          ),
        ),
      ],
    )
  );

  Widget otpEmailFormField() => Container(
    margin: EdgeInsets.only(top: 30),
    child: PinCodeTextField(
      appContext: context,
      controller: otpEmailController,
      focusNode: otpEmailFocusNode,
      length: 4,
      obscureText: false,
      enabled: true,
      enablePinAutofill: true,
      autoFocus: true,
      keyboardType: TextInputType.number,
      autoDismissKeyboard: true,
      autoDisposeControllers: false,
      animationType: AnimationType.fade,
      animationDuration: Duration(milliseconds: 100),
      errorTextSpace: 0,
      enableActiveFill: true,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderRadius: BorderRadius.circular(4),
        fieldWidth: MediaQuery.of(context).size.width * 0.2,
        fieldHeight: 60,
        activeFillColor: white,
        inactiveFillColor: white,
        selectedFillColor: white,
        activeColor: isOtpEmailFieldError ? red : darkYellow,
        inactiveColor: lightGrey,
        disabledColor: whiteGrey,
        selectedColor: darkYellow,
        borderWidth: 1.0,
        errorBorderColor: red,
      ),
      cursorColor: Colors.black,
      onChanged: (pin) {
        otpEmailFormKey.currentState!.validate();
      },
      onCompleted: (pin) {
        login(pin);
      },
    ),
  );

  Widget clearEmailFormButton() => Container(
    child: Row(
      children: [
        Expanded(
          child: isOtpEmailFieldError ? Text(
            S.of(context).wrongCodeEntered,
            style: italicFontsStyle(color: red),
          ) : SizedBox(),
        ),
        TextButton(
          style: TextButton.styleFrom(primary: yellow),
          onPressed: () => clearCode(),
          child: Text(
            S.of(context).clearCode, 
            style: boldFontsStyle(fontSize: mediumFont, color: darkYellow),
          ),
        ),
      ],
    ),
  );

  Widget resendEmailButton() => Container(
    margin: EdgeInsets.only(top: 50),
    child: isEmailResendLoading ?  
      Countdown(
        key: GlobalObjectKey('emailcountdown'),
        controller: countdownEmailController,
        seconds: (emailresendCounter % 10 == 0) ? 900 : 300,
        build: (_, double time) {
          var duration = Duration(seconds: time.toInt());
          return RoundedButton(
            buttonText: S.of(context).resendIn + '${duration.inMinutes.remainder(60)}:${duration.inSeconds.remainder(60).toString().padLeft(2, '0')}',
            width: SizeUtils.width(context, 100),
            buttonColor: white,
            borderColor: black,
            borderWidth: 1.0,
            fontColor: black,
            isLoading: false,
            enabled: false,
            fontSize: bigFonts,
          );
        },
        interval: Duration(seconds: 1),
        onFinished: () {
          setState(() {
            isEmailResendLoading = false;
          });
        }
      ):
      RoundedButton(
        buttonText: S.of(context).resendCode,
        width: SizeUtils.width(context, 100),
        buttonColor: white,
        borderColor: black,
        borderWidth: 1.0,
        fontColor: black,
        isLoading: false,
        enabled: isEmailResendLoading ? false : resendEmailButtonEnabled,
        onPressed: () {
          resendEmailCode(); 
          setState(() {
            emailresendCounter++; 
          });
        },
        fontSize: bigFonts,
      ),
  );

  Widget otpSMS() => Container(
    margin: EdgeInsets.only(top: 30),
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Row(
        children: [
          Expanded(
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: regularFontsStyle(fontSize: bigFonts, height: 1.3),
                children: <TextSpan>[
                  TextSpan(text: S.of(context).havingTrouble, style: boldFontsStyle(fontWeight: FontWeight.w700, fontSize: mediumFont, height: 1.3)),
                ],
              ),
            ),
          ),
        ],
      ),
      Row(
         mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            child: RichText(
              text: TextSpan(
                style: regularFontsStyle(fontSize: bigFonts, height: 1.3),
                children: <TextSpan>[
                  TextSpan(text: S.of(context).verifySMS, style: TextStyle(fontWeight: FontWeight.w700, color: darkYellow, fontSize: mediumFont, decoration: TextDecoration.underline, height: 1.3)),
                ],
              ),
            ),
            onTap: () => goToOtpFieldPage(),
          ),
        ],
      ),
    ],
    ),
  );

  resendCode() async {
    otpLoginBloc.add(OtpFieldChanged(true));
    apiBloc.add(RequestOtpApiEvent(
      contactNumber: replaceWhiteSpace(mobileNumber ?? ""),
    ));
    otpLoginBloc.add(ResendButtonClickEvents(true, false, ''));
  }

  resendEmailCode() async {
    otpEmailLoginBloc.add(OtpEmailFieldChanged(true));
    apiBloc.add(RequestOtpEmailApiEvent(
        ownerID: otpModel!.ownerID.toString(), 
    ));
    otpEmailLoginBloc.add(EmailResendButtonClickEvents(true, false, ''));
  }

  login(otpCode) async {
    if (otpFormKey.currentState!.validate()) {
      if (AppDataModel.getDeviceToken().isEmpty ?? true) {
        if (AppDataModel.getTokenGenerateByList() == 'Huawei') {
          HuaWei.Push.getToken('');
        } else {
          String? token = await FirebaseMessaging.instance.getToken();
          AppDataModel.setDeviceToken(token.toString());
        }
      }
      otpLoginBloc.add(OtpFieldChanged(false));
      apiBloc.add(OtpLoginApiEvent(
        ownerID: otpModel!.ownerID.toString(), 
        otpCode: otpCode, 
        token: replaceWhiteSpace(AppDataModel.getDeviceToken()),
        tokenGenerateBy: AppDataModel.getTokenGenerateByList()
      ));
    }
  }

  void clearCode() {
    setState(() {
      isOtpFieldError = false;
      isOtpEmailFieldError = false;
    });
    otpController.clear();
    otpEmailController.clear();
  }

  void goToMobileFieldPage() {
    FocusScope.of(context).unfocus();
    otpController.clear();
    setState(() {
      isOtpFieldError = false;
    });
    pageController.animateToPage(0, 
      duration: Duration(milliseconds: 300), 
      curve: Curves.easeInOut
    );
  }

  void goToOtpFieldPage() {
    FocusScope.of(context).unfocus();
    setState(() {
      isOtpFieldError = false;
    });
    isResendLoading = true;
    pageController.animateToPage(1, 
      duration: Duration(milliseconds: 300), 
      curve: Curves.easeInOut
    );
  }

  void goToEmailFieldPage() {
    FocusScope.of(context).unfocus();
    otpController.clear();
    otpEmailController.clear();
    setState(() {
      isOtpFieldError = false;
      isOtpEmailFieldError = false;
    });
    isEmailResendLoading = true;
    pageController.animateToPage(2, 
      duration: Duration(milliseconds: 300), 
      curve: Curves.easeInOut
    );
  }
}

class CustomKeepAlive extends StatefulWidget {
  const CustomKeepAlive({Key? key, required this.widget}) : super(key: key);

  final Widget widget;

  @override
  State<CustomKeepAlive> createState() => _CustomKeepAliveState();
}

class _CustomKeepAliveState extends State<CustomKeepAlive> with AutomaticKeepAliveClientMixin{
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return widget.widget;
  }
}
