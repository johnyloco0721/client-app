import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/list_items/notifications_list_items.dart';
import 'package:pmaclient/models/notification_api_model.dart';
import 'package:pmaclient/models/notification_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/shimmer_items/notification_shimmer_items.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/shimmer_widget.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  List<NotificationModel> notificationsList = [];
  bool isLoad = true;
  late ApiBloc apiBloc;
  int pageNo = 1;
  bool isLoading = false;
  NotificationApiModel? notificationApiModel;
  ApiStates? apiStates;
  int totalCountInList = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getNotificationsApi();
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.light);
    return Scaffold(
      backgroundColor: black,
      body: SafeArea(
        bottom: false,
        child: RefreshIndicator(
          color: darkYellow,
          onRefresh: () async {
            getNotificationsApi();
          },
          child: BlocConsumer<ApiBloc, ApiStates>(
            listener: (context, state) {
              apiStates = state;
              if (state is LogoutApiDataLoadState) {
                SpManager.logoutDeleteData();
                goAndClearAllPageNamed(context, loginOtpScreen);
              } else if (state is NotificationsApiDataLoadState) {
                isLoad = false;
                notificationApiModel = state.model;
                if (state.isLoadMore) {
                  List<NotificationModel> loadMoreList = [];
                  loadMoreList.add(NotificationModel(
                      notificationList: notificationApiModel?.response ?? []));
                  notificationsList.addAll(loadMoreList);
                  totalCountInList = totalCountInList +
                      (notificationApiModel?.response?.length ?? 0);
                } else {
                  totalCountInList = notificationApiModel?.response?.length ?? 0;
                  getNotificationList(notificationApiModel?.response ?? []);
                }
              } else if (state is ApiErrorState) {
                isLoad = false;
                if (!state.isLoadMore) {
                  showToast(state.errorMessage ?? "");
                }
              } else if (state is LoadMoreLoadingState) {
                isLoading = state.isEnabled;
                isLoad = false;
              } else if (state is ApiLoadingState) {
                isLoad = true;
              }
            },
            bloc: apiBloc,
            builder: (context, states) {
              return mainLayout();
            },
          ),
        ),
      ),
    );
  }

  Widget mainLayout() => Container(
        color: white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            headerLayoutWithCross(context, S().notifications),
            Expanded(
              child: apiBloc.state is NotificationLoadingState
                  ? ShimmerWidget(
                      widget: NotificationShimmerItem(),
                      isWidget: true,
                    )
                  : NotificationListener<ScrollNotification>(
                      onNotification: (scrollNotification) {
                        if (scrollNotification.metrics.pixels ==
                                scrollNotification.metrics.maxScrollExtent &&
                            totalCountInList <
                                (notificationApiModel?.totalItems as int)) {
                          if (!isLoading) {
                            pageNo = pageNo + 1;
                            apiBloc.add(
                              NotificationsApiEvent(
                                pageNo: pageNo,
                                isLoadMore: true,
                              ),
                            );
                          }
                        }
                        return isLoading;
                      },
                      child: notificationsList.length > 0
                          ? ListView(
                              children: [
                                ListView.builder(
                                  itemCount: notificationsList.length,
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    NotificationModel model =
                                        notificationsList[index];
                                    return NotificationsListItems(
                                      headerText: model.headerTitle,
                                      notificationDataList:
                                          model.notificationList,
                                      index: index,
                                      apiBloc: apiBloc,
                                      isVisible:
                                          (model.notificationList?.where((e) => e.isRead == 0).length ?? 0) > 0
                                              ? true
                                              : false,
                                      length: notificationsList.length,
                                    );
                                  },
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  child: isLoading
                                      ? Lottie.asset(
                                          login_black_loading,
                                          height: 60,
                                          width: 120,
                                        )
                                      : Container(),
                                ),
                                Visibility(
                                  visible: totalCountInList <=
                                          (notificationApiModel?.totalItems
                                              as int)
                                      ? true
                                      : false,
                                  child: Container(
                                    alignment: Alignment.topCenter,
                                    margin: EdgeInsets.only(
                                        left: 30,
                                        right: 30,
                                        top: 15,
                                        bottom: 25),
                                    child: Text(
                                      S().you_have_caught_up_with_all,
                                      style: regularFontsStyle(),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 15.0),
                              ],
                            )
                          : Container(
                              padding: EdgeInsets.only(
                                  bottom: 20, left: 20, right: 20),
                              alignment: Alignment.center,
                              child: emptyNotificationLayout(),
                            ),
                    ),
            ),
          ],
        ),
      );

  Widget emptyNotificationLayout() => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Lottie.asset(
            no_notification_lottie,
            height: 250,
            width: 250,
            fit: BoxFit.fill,
          ),
          SizedBox(height: 30),
          Text(
            S.of(context).there_is_no_notifications_yet,
            style: regularFontsStyle(fontSize: mediumFont, height: 1.3),
            textAlign: TextAlign.center,
          ),
        ],
      );

  getNotificationList(List<NotificationDataList> responseList) {
    if (responseList.length > 0) {
      notificationsList.clear();
      List<NotificationDataList> todayListData = [];
      List<NotificationDataList> pastListData = [];
      responseList.forEach((element) {
        bool isToday = checkTodayDate(element.createdDateTime);
        if (isToday) {
          todayListData.add(element);
        } else {
          pastListData.add(element);
        }
      });
      if (todayListData.length > 0) {
        notificationsList.add(
          NotificationModel(
            headerTitle: S.of(context).today,
            notificationList: todayListData,
          ),
        );
      }
      if (pastListData.length > 0) {
        notificationsList.add(
          NotificationModel(
            headerTitle: S.of(context).past_notifications,
            notificationList: pastListData,
          ),
        );
      }
    }
  }

  void getNotificationsApi() {
    apiBloc = BlocProvider.of<ApiBloc>(context);
    apiBloc.add(NotificationsApiEvent(pageNo: pageNo, isLoadMore: false));
  }

  notificationClick(NotificationDataList notificationList) {}
}
