import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/bloc/reset_password_bloc/reset_password_bloc.dart';
import 'package:pmaclient/bloc/reset_password_bloc/reset_password_events.dart';
import 'package:pmaclient/bloc/reset_password_bloc/reset_password_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/screens/reset_success_screen.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/utils/validation_utils.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/bordered_textfield.dart';
import 'package:pmaclient/widgets/rounded_button.dart';

class ResetNewPasswordScreen extends StatefulWidget {
  const ResetNewPasswordScreen({Key? key}) : super(key: key);

  @override
  _ResetNewPasswordScreenState createState() => _ResetNewPasswordScreenState();
}

class _ResetNewPasswordScreenState extends State<ResetNewPasswordScreen> {
  final formKey = GlobalKey<FormState>();
  ResetPasswordBloc resetPasswordBloc = ResetPasswordBloc();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isConfirm = false;
  bool isLoading = false;
  late ApiBloc apiBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    apiBloc = BlocProvider.of<ApiBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.dark);
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: BlocListener<ApiBloc, ApiStates>(
          bloc: apiBloc,
          listener: (BuildContext context, state) {
            if (state is UpdatePasswordAPiLoadData) {
              apiBloc.add(LogoutApiEvent());
            } else if (state is ApiLoadingState) {
              resetPasswordBloc.add(ResetButtonClickEvents(true));
            } else if (state is ApiErrorState) {
              resetPasswordBloc.add(ResetButtonClickEvents(false));
            } else if (state is LogoutApiDataLoadState) {
              resetPasswordBloc.add(ResetButtonClickEvents(false));
              goAndClearCurrentScreen(context, ResetSuccessScreen());
            }
          },
          child: BlocBuilder<ResetPasswordBloc, ResetPasswordStates>(
            bloc: resetPasswordBloc,
            builder: (context, state) {
              if (state is ResetPasswordConfirmState) {
                isConfirm = state.isConfirm;
              } else if (state is ResetButtonClickState) {
                isLoading = state.isLoading;
              }
              return mainLayout();
            },
          ),
        ),
      ),
    );
  }

  Widget mainLayout() => Container(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        margin: EdgeInsets.only(
            top: SizeUtils.height(context, 5), bottom: 20, left: 20, right: 20),
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                welcomeToMakeoverGuysImage(context),
                loginDescriptionTextView(),
                passwordTextFormField(),
                reEnterPasswordTextFormField(),
                resetPasswordButton()
              ],
            ),
          ),
        ),
      );

  Widget loginDescriptionTextView() => Container(
        margin: EdgeInsets.only(top: 15),
        child: Text(
          S.of(context).please_set_your_new_password,
          style: regularFontsStyle(fontSize: mediumFont),
        ),
      );

  Widget passwordTextFormField() => Container(
        margin: EdgeInsets.only(top: 10),
        child: BorderedTextField(
          controller: passwordController,
          hintText: S.of(context).new_password,
          validator: (value) => passwordValidation(value ?? "", isReset: true),
          obscureText: true,
          enabled: !isLoading,
          onChanged: (value) {},
          suffixIcon: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {},
            icon: Image.asset(ic_password, height: 20, width: 20),
          ),
        ),
      );

  Widget reEnterPasswordTextFormField() => Container(
        margin: EdgeInsets.only(top: 10),
        child: BorderedTextField(
          controller: newPasswordController,
          hintText: S.of(context).re_enter_password,
          validator: (value) => confirmPasswordValidation(
              value ?? "", passwordController.text, context),
          obscureText: true,
          enabled: !isLoading,
          onChanged: (value) {
            if (value != passwordController.text) {
              resetPasswordBloc.add(ConfirmPasswordValidateEvent(false));
            } else {
              resetPasswordBloc.add(ConfirmPasswordValidateEvent(true));
            }
          },
          suffixIcon: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {},
            icon: Image.asset(
              isConfirm ? ic_tick : ic_password,
              height: 20,
              width: 20,
            ),
          ),
        ),
      );

  Widget resetPasswordButton() => Container(
        margin: EdgeInsets.only(top: 50),
        child: RoundedButton(
          onPressed: () => validate(),
          buttonText: S.of(context).confirm_password,
          width: SizeUtils.width(context, 100),
          borderColor: black,
          borderWidth: borderWidth,
          buttonColor: white,
          isLoading: isLoading,
          enabled: isConfirm,
        ),
      );

  validate() {
    if (formKey.currentState!.validate()) {
      apiBloc.add(
        UpdatePasswordAPiApiEvent(
          password: passwordController.text,
          confirmPassword: newPasswordController.text,
        ),
      );
    }
  }
}
