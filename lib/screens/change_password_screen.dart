import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/bloc/reset_password_bloc/reset_password_bloc.dart';
import 'package:pmaclient/bloc/reset_password_bloc/reset_password_events.dart';
import 'package:pmaclient/bloc/reset_password_bloc/reset_password_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/screens/reset_success_screen.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/validation_utils.dart';
import 'package:pmaclient/views/header_logo_view.dart';
import 'package:pmaclient/widgets/bordered_textfield.dart';
import 'package:pmaclient/widgets/rounded_button.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({Key? key}) : super(key: key);

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final formKey = GlobalKey<FormState>();
  ResetPasswordBloc resetPasswordBloc = ResetPasswordBloc();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController currentPasswordController = TextEditingController();
  bool isConfirm = false;
  bool isLoading = false;
  late ApiBloc apiBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    apiBloc = BlocProvider.of<ApiBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: black,
      appBar: AppBar(
        toolbarHeight: 0,
        backgroundColor: black,
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      body: SafeArea(
        bottom: false,
        child: BlocListener<ApiBloc, ApiStates>(
          bloc: apiBloc,
          listener: (BuildContext context, state) {
            if (state is UpdatePasswordAPiLoadData) {
              apiBloc.add(LogoutApiEvent());
            } else if (state is ApiLoadingState) {
              resetPasswordBloc.add(ResetButtonClickEvents(true));
            } else if (state is ApiErrorState) {
              resetPasswordBloc.add(ResetButtonClickEvents(false));
            } else if (state is LogoutApiDataLoadState) {
              resetPasswordBloc.add(ResetButtonClickEvents(false));
              goAndClearCurrentScreen(context, ResetSuccessScreen());
            }
          },
          child: BlocBuilder<ResetPasswordBloc, ResetPasswordStates>(
            bloc: resetPasswordBloc,
            builder: (context, state) {
              if (state is ResetPasswordConfirmState) {
                isConfirm = state.isConfirm;
              } else if (state is ResetButtonClickState) {
                isLoading = state.isLoading;
              }
              return mainLayout();
            },
          ),
        ),
      ),
    );
  }

  Widget mainLayout() => Container(
        color: white,
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            headerLayoutWithBackArrow(context, S().change_password),
            Expanded(
              child: Container(
                color: white,
                child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      chooseNewPasswordText(),
                      currentPasswordTextFormField(),
                      passwordTextFormField(),
                      reEnterPasswordTextFormField(),
                      resetPasswordButton(),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      );

  Widget chooseNewPasswordText() => Container(
        margin: EdgeInsets.only(left: 20, top: 25, right: 20),
        child: Text(
          S().choose_a_new_password,
          style: regularFontsStyle(fontSize: bigFonts),
        ),
      );

  Widget currentPasswordTextFormField() => Container(
        margin: EdgeInsets.only(top: 40, left: 20, right: 20),
        child: BorderedTextField(
          controller: currentPasswordController,
          hintText: S().current_password,
          fontSize: bigFonts,
          hintFontSize: bigFonts,
          validator: (value) => emptyValidation(
              value ?? "", S().current_password_should_not_empty),
          obscureText: true,
          enabled: !isLoading,
          onChanged: (value) => onPasswordChange(value),
          suffixIcon: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {},
            icon: Image.asset(ic_password, height: 20, width: 20),
          ),
        ),
      );

  Widget passwordTextFormField() => Container(
        margin: EdgeInsets.only(top: 10, left: 20, right: 20),
        child: BorderedTextField(
          controller: passwordController,
          hintText: S.of(context).new_password,
          fontSize: bigFonts,
          hintFontSize: bigFonts,
          validator: (value) => passwordValidation(value ?? "", isReset: false),
          obscureText: true,
          enabled: !isLoading,
          onChanged: (value) => onPasswordChange(value),
          suffixIcon: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {},
            icon: Image.asset(ic_password, height: 20, width: 20),
          ),
        ),
      );

  Widget reEnterPasswordTextFormField() => Container(
        margin: EdgeInsets.only(top: 10, left: 20, right: 20),
        child: BorderedTextField(
          controller: newPasswordController,
          hintText: S.of(context).re_enter_password,
          fontSize: bigFonts,
          hintFontSize: bigFonts,
          validator: (value) => confirmPasswordValidation(
              value ?? "", passwordController.text, context),
          obscureText: true,
          enabled: !isLoading,
          onChanged: (value) => onPasswordChange(value),
          suffixIcon: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {},
            icon: Image.asset(
              ic_password,
              height: 20,
              width: 20,
            ),
          ),
        ),
      );

  Widget resetPasswordButton() => Container(
        margin: EdgeInsets.only(top: 50, left: 20, right: 20),
        child: RoundedButton(
          onPressed: () => validate(),
          buttonText: S.of(context).save,
          width: SizeUtils.width(context, 100),
          fontColor: black,
          fontSize: bigFonts,
          isLoading: isLoading,
          enabled: isConfirm,
        ),
      );

  validate() {
    if (formKey.currentState!.validate()) {
      apiBloc.add(
        UpdatePasswordAPiApiEvent(
          currentPassword: currentPasswordController.text,
          password: passwordController.text,
          confirmPassword: newPasswordController.text,
        ),
      );
    }
  }

  onPasswordChange(String value) {
    if (passwordController.text != newPasswordController.text ||
        currentPasswordController.text.isEmpty ||
        passwordController.text.isEmpty ||
        newPasswordController.text.isEmpty) {
      resetPasswordBloc.add(ConfirmPasswordValidateEvent(false));
    } else {
      resetPasswordBloc.add(ConfirmPasswordValidateEvent(true));
    }
  }
}
