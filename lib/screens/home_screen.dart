import 'package:animated_size_and_fade/animated_size_and_fade.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_bloc.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_events.dart';
import 'package:pmaclient/bloc/profile_bloc/profile_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_fonts.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/list_items/promotion_items.dart';
import 'package:pmaclient/list_items/property_list_items.dart';
import 'package:pmaclient/models/config_model.dart';
import 'package:pmaclient/models/projects_model.dart';
import 'package:pmaclient/models/user_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/route_management/property_details_arguments.dart';
import 'package:pmaclient/shimmer_items/home_shimmer_item.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/widgets/shimmer_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String? userName;
  bool isResetPassword = true;
  bool isLoad = true;
  late ProfileBloc profileBloc;
  UserModel? userData;
  late ApiBloc apiBloc;
  ProjectModel? projectModel;
  List<HomeProject> projectsList = [];
  int pageNo = 1;
  int totalPages = 1;
  bool isLoadMore = false;
  bool isReview = false;
  int totalBadge = 0;
  ConfigModel? configModel;
  bool isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    goToUpdateScreen(context);
    profileBloc = BlocProvider.of<ProfileBloc>(context);
    apiBloc = BlocProvider.of<ApiBloc>(context);
    apiBloc.add(ProjectsApiEvent(pageNo: pageNo));
    apiBloc.add(NotificationsApiEvent(pageNo: 1));
    userData = SpManager.getUserdata();
    configModel = SpManager.getConfigModel();
    userName = "John Doe";
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.dark);
    return Scaffold(
      backgroundColor: white,
      body: SafeArea(
        bottom: false,
        child: RefreshIndicator(
          color: darkYellow,
          onRefresh: () async {
            pageNo = 1;
            getProjectsApi(pageNo);
            getNotificationsApi();
          },
          child: BlocConsumer<ApiBloc, ApiStates>(
            listener: (context, state) => apiBlocListener(context, state),
            bloc: apiBloc,
            builder: (context, state) => apiBlocWidgetBuilder(),
          ),
        ),
      ),
    );
  }

  Widget apiBlocWidgetBuilder() => isLoading
      ? ShimmerWidget(widget: HomeShimmerItem(), isWidget: true)
      : Stack(
          alignment: AlignmentDirectional.bottomStart,
          children: [illustrationLayout(), mainLayout()],
        );

  Widget illustrationLayout() => Transform.translate(
        offset: Offset(MediaQuery.of(context).size.width * 0.15, -30.0),
        child: SafeArea(
          child: Image.asset(
            ic_landing_page_illustration,
            width: MediaQuery.of(context).size.width * 0.8,
            fit: BoxFit.fill,
          ),
        ),
      );

  Widget mainLayout() => Container(
        margin: EdgeInsets.only(top: 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(right: 5),
              child: topViewLayout(),
            ),
            Expanded(
              child: NotificationListener<ScrollNotification>(
                onNotification: (scrollNotification) {
                  int totalItems = projectModel?.totalItems as int;
                  int limitPerPage = projectModel?.limitPerPage as int;
                  totalPages = (totalItems / limitPerPage).ceil();

                  if (scrollNotification.metrics.pixels ==
                          scrollNotification.metrics.maxScrollExtent &&
                      projectsList.length < (projectModel?.totalItems as int) && pageNo < totalPages) {
                    if (!isLoadMore && !isLoading) {
                      isLoading = true;
                      pageNo = pageNo + 1;
                      apiBloc.add(ProjectsApiEvent(
                        pageNo: pageNo,
                        isLoadMore: true,
                      ));
                    }
                  }
                  return isLoadMore;
                },
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      randomDescriptionTextView(),
                      betterHomesTextView(),
                      resetPasswordTextLayout(),
                      reviewProjectLayout(),
                      projectsList.length > 0
                          ? propertiesListGridView()
                          : Container(),
                      Visibility(
                        visible: projectsList.length > 0 
                          ? projectsList.length >= (projectModel?.totalItems ?? 0) && !isLoadMore
                            ? true : false
                            : false,
                        child: Container(
                          alignment: Alignment.topCenter,
                          margin: EdgeInsets.only(
                              left: 30,
                              right: 30,
                              top: 15,
                              bottom: MediaQuery.of(context).padding.bottom),
                          child: Text(
                            S().all_projects_are_loaded,
                            style: regularFontsStyle(),
                          ),
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: isLoadMore
                            ? Lottie.asset(
                                login_black_loading,
                                height: 60,
                                width: 120,
                              )
                            : Container(),
                      ),
                      SizedBox(height: 15.0),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );

  Widget topViewLayout() => Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Flexible(
            child: InkWell(
              onTap: () => goToNextNamedRoute(context, notificationScreen, callback: () {
                pageNo = 1;
                getProjectsApi(pageNo);
                getNotificationsApi();
              }),
              customBorder: CircleBorder(),
              child: Container(
                padding: EdgeInsets.all(15),
                child: Image.asset(
                    totalBadge > 0 ? ic_notification_on : ic_notification_off,
                    height: 20,
                    width: 20),
              ),
            ),
          ),
          InkWell(
            onTap: () => goToNextNamedRoute(context, profileScreen, callback: () {
              pageNo = 1;
              getProjectsApi(pageNo);
            }),
            customBorder: CircleBorder(),
            child: Container(
              padding: EdgeInsets.all(15),
              child: Image.asset(ic_user, height: 20, width: 20),
            ),
          ),
        ],
      );

  Widget userNameTextView() => BlocBuilder<ProfileBloc, ProfileStates>(
      bloc: profileBloc,
      builder: (context, state) {
        if (state is UserDataChangeEvent) {
          userData = state.model;
        }
        return Container(
          margin: EdgeInsets.only(top: 10),
          child: Text(
            userData?.username ?? state.model?.username ?? userName ?? "",
            style: boldFontsStyle(
              fontFamily: platForm,
              fontSize: veryLargeFont,
              color: darkYellow,
            ),
          ),
        );
      });

  Widget descriptionTextView() => Container(
        margin: EdgeInsets.only(top: 16, right: 20),
        child: Text(
          "#BetterHomes",
          style: boldFontsStyle(fontSize: bigFonts, color: darkYellow),
        ),
      );

  Widget randomDescriptionTextView() => Container(
        margin: EdgeInsets.only(top: 25, right: 20, left: 20),
        child: HtmlWidget(
          configModel?.response?.homeTitle ??
              "<font color='#EFAC00'>Home</font> is where love resides and memories are created",
          textStyle: boldFontsStyle(
            fontSize: 35.0,
            fontFamily: platForm,
            height: 1.14,
            letterSpacing: 0.5,
          ),
        ),
      );

  Widget betterHomesTextView() => Container(
        margin: EdgeInsets.only(top: 16, right: 20, left: 20),
        child: HtmlWidget(
          configModel?.response?.homeHashtag ?? S().better_homes,
          textStyle: boldFontsStyle(fontSize: bigFonts, color: yellow),
        ),
      );

  Widget resetPasswordTextLayout() => AnimatedSizeAndFade(
        fadeDuration: Duration(microseconds: 300),
        sizeDuration: Duration(microseconds: 600),
        child: userData?.isFirstTimeLogin ?? isResetPassword
            ? GestureDetector(
                onTap: () => goToNextNamedRoute(context, changePasswordScreen, callback: () {
                  pageNo = 1;
                  getProjectsApi(pageNo);
                }),
                child: Container(
                  margin: EdgeInsets.only(top: 32, right: 20, left: 20),
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: darkGrey,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              S().one_time_password_reset,
                              style: boldFontsStyle(
                                fontSize: mediumFont,
                                color: white,
                              ),
                            ),
                            SizedBox(height: 10),
                            Text(
                              S().hiya_remember_to_choose_a_new,
                              style:
                                  regularFontsStyle(color: white, height: 1.3),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 40,
                        width: 40,
                        margin: EdgeInsets.only(left: 20),
                        decoration: BoxDecoration(
                          color: yellow,
                          shape: BoxShape.circle,
                          boxShadow: [
                            BoxShadow(
                              color: grey.withOpacity(0.1),
                              blurRadius: 5,
                              spreadRadius: 5,
                              offset: Offset(0, 3),
                            ),
                          ],
                        ),
                        alignment: Alignment.center,
                        child: Image.asset(
                          ic_enter_arrow_icon,
                          color: black,
                          height: 22,
                          width: 22,
                        ),
                      ),
                    ],
                  ),
                ))
            : Container(),
      );

  Widget reviewProjectLayout() => AnimatedSizeAndFade(
        fadeDuration: Duration(microseconds: 300),
        sizeDuration: Duration(microseconds: 600),
        child: isReview
            ? GestureDetector(
                onTap: () => goToNextNamedRoute(context, rateUsScreen, callback: () {
                  pageNo = 1;
                  getProjectsApi(pageNo);
                }),
                child: Container(
                  margin: EdgeInsets.only(top: 32, right: 10, left: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    // color: darkGrey,
                  ),
                  child: Card(
                    elevation: 3,
                    margin: EdgeInsets.all(10),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    child: IntrinsicHeight(
                      child: Container(
                        color: darkGrey,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Container(
                              color: yellow,
                              padding: EdgeInsets.all(10),
                              child: Lottie.asset(
                                reminder_lottie,
                                // height: 80,
                                width: 80,
                              ),
                            ),
                            Expanded(
                              child: Container(
                                padding: EdgeInsets.all(15),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      projectModel?.npsRating?.npsTitle ??
                                          "${S().rate_us}!",
                                      style: boldFontsStyle(
                                        color: white,
                                        fontSize: mediumFont,
                                      ),
                                    ),
                                    SizedBox(height: 15),
                                    HtmlWidget(
                                      projectModel?.npsRating?.npsDescription ??
                                          S().receive_a_complimentary,
                                      textStyle: regularFontsStyle(
                                        color: white,
                                        fontSize: mediumFont,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              alignment: Alignment.topCenter,
                              child: Container(
                                height: 40,
                                width: 40,
                                margin: EdgeInsets.only(top: 15, right: 15),
                                decoration: BoxDecoration(
                                  color: yellow,
                                  shape: BoxShape.circle,
                                  boxShadow: [
                                    BoxShadow(
                                      color: grey.withOpacity(0.1),
                                      blurRadius: 5,
                                      spreadRadius: 5,
                                      offset: Offset(0, 3),
                                    ),
                                  ],
                                ),
                                alignment: Alignment.center,
                                child: Image.asset(
                                  ic_enter_arrow_icon,
                                  color: black,
                                  height: 22,
                                  width: 22,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              )
            : Container(),
      );

  Widget propertiesListGridView() => Container(
        margin: EdgeInsets.only(top: 32, right: 20, left: 20),
        child: AnimationLimiter(
          child: MasonryGridView.builder(
            mainAxisSpacing: 15,
            gridDelegate: SliverSimpleGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
            ),
            crossAxisSpacing: 15,
            shrinkWrap: true,
            primary: false,
            itemCount: projectsList.length,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              HomeProject model = projectsList[index];
              String status = model.statusLabel ?? "Unknown Status";
              Color dotColor = hexToColor(model.statusDotColor ?? '#ffffff');
              Color dotBorderColor = hexToColor(model.statusDotBorderColor ?? '#ffffff');
              // if (model.projectStatus == "running") {
              //   dotColor = darkYellow;
              //   status = "Makeover \nIn Progress";
              // } else if (model.projectStatus == "completed") {
              //   dotColor = black;
              //   status = "Makeover \nCompleted";
              // } else if (model.projectStatus == "planning") {
              //   dotColor = white;
              //   status = "Makeover \nIn Planning";
              // } else if (model.projectStatus == "rented") {
              //   dotColor = green;
              //   status = "Rented";
              // } else if (model.projectStatus == "pending") {
              //   dotColor = darkYellow;
              //   status = "On Hold";
              // }
              bool isPromotion = false;
              if (model.isPromotion == 1) {
                isPromotion = true;
              }
              return AnimationConfiguration.staggeredGrid(
                columnCount: 2,
                duration: Duration(milliseconds: 350),
                position: index,
                child: SlideAnimation(
                  verticalOffset: 50.0,
                  child: FadeInAnimation(
                    child: isPromotion
                        ? InkWell(
                            onTap: () => launchURL(model.promotionUrl),
                            child: PromotionItems(imagePath: model.coverImage),
                          )
                        : PropertyListItems(
                            imagePath: model.coverImage,
                            projectUnit: model.projectUnit,
                            propertyName: model.propertyName,
                            propertyType: model.propertyType,
                            status: status,
                            hasUpdate: model.hasUpdate??0,
                            projectSize: model.projectSize,
                            dotColor: dotColor,
                            dotBorderColor: dotBorderColor,
                            onTap: () => goToNextNamedRoute(
                              context,
                              (model.projectStatus == "completed" ||
                                      model.projectStatus == "rented" || 
                                      model.projectStatus ==  "handovered") && (model.isHandoverTaskCompleted == true)
                                  ? propertyDetailsCompleteScreen
                                  : propertyDetailsScreen,
                              args: PropertyDetailsArguments(
                                projectID: model.projectID,
                              ),
                              callback: () {
                                pageNo = 1;
                                getProjectsApi(pageNo);
                              },
                            ),
                          ),
                  ),
                ),
              );
            },
          ),
        ),
      );

  apiBlocListener(BuildContext context, ApiStates state) {
    if (state is LogoutApiDataLoadState) {
      SpManager.logoutDeleteData();
      goAndClearAllPageNamed(context, loginOtpScreen);
    } else if (state is ProjectsApiDataLoadState) {
      isLoading = false;
      projectModel = state.model;
      if (state.isLoadMore) {
        projectsList.addAll(state.model?.projects ?? []);
      } else {
        projectsList = state.model?.projects ?? [];
      }
      if (projectModel?.npsRating != null) {
        isReview = true;
        projectsList.forEach((element) {
          if (element.projectID == projectModel?.npsRating?.projectID) {
            AppDataModel.setHomeProjectModel(element);
          }
        });
      } else {
        isReview = false;
      }
    } else if (state is ApiErrorState) {
      isLoading = false;
      if (!state.isLoadMore) showToast(state.errorMessage ?? "");
    } else if (state is LoadMoreLoadingState) {
      isLoading = false;
      isLoadMore = state.isEnabled;
    } else if (state is ProjectApiLoadingState) {
      isLoading = true;
    } else if (state is NotificationsApiDataLoadState) {
      totalBadge = state.model?.totalBadge ?? 0;
      isLoading = false;
    }
  }

  void getProjectsApi(int page) {
    apiBloc.add(ProjectsApiEvent(pageNo: page));
  }

  void getNotificationsApi() {
    apiBloc = BlocProvider.of<ApiBloc>(context);
    apiBloc.add(NotificationsApiEvent(pageNo: pageNo, isLoadMore: false));
  }
}
