import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_array_list.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/list_items/dot_list_items.dart';
import 'package:pmaclient/list_items/welcome_list_items.dart';
import 'package:pmaclient/models/welcome_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen>
    with TickerProviderStateMixin {
  int currentIndex = 0;
  late PageController _pageController;
  late AnimationController controller;
  late Animation<Offset> offset;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _pageController.dispose();
    controller.dispose();
  }

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 600));
    offset = Tween<Offset>(begin: Offset.zero, end: Offset(0.0, 1.0))
        .animate(controller);
    _pageController = PageController(initialPage: 0, keepPage: true);
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.dark);
    return Scaffold(
      backgroundColor: white,
      body: SafeArea(
        top: false,
        child: Container(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
          child: mainLayout(),
        ),
      ),
    );
  }

  Widget mainLayout() => Column(
        children: [
          Expanded(
            child: Stack(
              children: [
                Column(
                  children: [
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          color: yellow,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(50),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      color: yellow,
                      child: Container(
                        height: SizeUtils.height(context, 30),
                        decoration: BoxDecoration(
                          color: white,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(50),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                pagerViewLayout(),
              ],
            ),
          ),
          bottomViewLayout(),
        ],
      );

  Widget pagerViewLayout() => PageView.builder(
        itemCount: welcomeList.length,
        controller: _pageController,
        physics: currentIndex != welcomeList.length - 1
            ? AlwaysScrollableScrollPhysics()
            : NeverScrollableScrollPhysics(),
        onPageChanged: (index) {
          setState(() {
            currentIndex = index;
            if (currentIndex == welcomeList.length - 1) {
              controller.reverse();
            } else {
              controller.forward();
            }
          });
        },
        itemBuilder: (context, index) {
          WelcomeModel model = welcomeList[index];
          return WelcomeListItems(
            lottiePath: model.lottiePath,
            title: model.title,
            description: model.description,
          );
        },
      );

  Widget bottomViewLayout() => Container(
        height: 80,
        alignment: Alignment.center,
        child: currentIndex != welcomeList.length - 1
            ? Container(
                height: 50,
                alignment: Alignment.bottomCenter,
                margin: EdgeInsets.all(20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    currentIndex != 0 && currentIndex != welcomeList.length - 1
                        ? backImageLayout()
                        : Container(height: 20, width: 20),
                    Expanded(
                      child: Container(
                          alignment: Alignment.center,
                          child: currentIndex != welcomeList.length - 1
                              ? indicatorListView()
                              : Container()),
                    ),
                    currentIndex != welcomeList.length - 1
                        ? nextImageLayout()
                        : Container(
                            height: 20,
                            width: 20,
                          ),
                  ],
                ),
              )
            : SlideTransition(
                position: offset,
                child: enterButtonLayout(),
              ),
      );

  Widget backImageLayout() => InkWell(
        onTap: () => backClick(),
        customBorder: CircleBorder(),
        child: Container(
          padding: EdgeInsets.only(top: 10, bottom: 10, right: 10),
          child: Image.asset(
            ic_left_arrow,
            height: 20,
            width: 20,
          ),
        ),
      );

  Widget indicatorListView() => ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: welcomeList.length - 1,
        itemBuilder: (context, index) {
          return DotListIndicator(
            isCurrent: index == currentIndex ? true : false,
            icLastIndex: index == welcomeList.length - 1 ? true : false,
          );
        },
      );

  Widget nextImageLayout() => InkWell(
        onTap: () => nextClick(),
        customBorder: CircleBorder(),
        child: Container(
          padding: EdgeInsets.only(top: 10, bottom: 10, left: 10),
          child: Image.asset(
            ic_right_arrow,
            height: 20,
            width: 20,
          ),
        ),
      );

  Widget enterButtonLayout() => Align(
        alignment: Alignment.bottomCenter,
        child: CupertinoButton(
          onPressed: () => enterButtonClick(),
          padding: EdgeInsets.zero,
          child: Container(
            height: 60,
            width: 60,
            margin: EdgeInsets.only(bottom: 30),
            decoration:
                BoxDecoration(color: black, shape: BoxShape.circle, boxShadow: [
              BoxShadow(
                color: grey.withOpacity(0.1),
                blurRadius: 5,
                spreadRadius: 5,
                offset: Offset(0, 3),
              ),
            ]),
            alignment: Alignment.center,
            child: Image.asset(
              ic_enter_arrow_icon,
              height: 22,
              width: 22,
            ),
          ),
        ),
      );

  nextClick() {
    _pageController.animateToPage(
      currentIndex + 1,
      duration: Duration(milliseconds: 500),
      curve: Curves.easeIn,
    );
  }

  backClick() {
    _pageController.animateToPage(
      currentIndex - 1,
      duration: Duration(milliseconds: 500),
      curve: Curves.easeInOut,
    );
  }

  enterButtonClick() {
    SpManager.setIsFirstTime(true);
    Navigator.pushReplacementNamed(context, loginOtpScreen);
  }
}
