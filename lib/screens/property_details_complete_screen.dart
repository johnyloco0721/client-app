import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:pmaclient/bloc/api_bloc/api_bloc.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/bloc/api_bloc/api_states.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_fonts.dart';
import 'package:pmaclient/constants/app_images.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/dialogs_views/documents_dialogs_view.dart';
import 'package:pmaclient/dialogs_views/property_details_dialog_view.dart';
import 'package:pmaclient/dialogs_views/updates_dialogs_view.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/route_management/media_screen_args.dart';
import 'package:pmaclient/route_management/property_details_arguments.dart';
import 'package:pmaclient/utils/dialog_utils.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/size_utils.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';
import 'package:pmaclient/views/horizontal_line.dart';
import 'package:pmaclient/views/network_blur_image_view.dart';
import 'package:pmaclient/widgets/expandable_html_widget.dart';
import 'package:pmaclient/widgets/round_icon_button.dart';
import 'package:pmaclient/widgets/rounded_button.dart';
import 'package:pmaclient/widgets/shimmer_widget.dart';

class PropertyDetailsCompleteScreen extends StatefulWidget {
  final PropertyDetailsArguments? propertyArguments;

  const PropertyDetailsCompleteScreen({Key? key, this.propertyArguments})
      : super(key: key);

  @override
  _PropertyDetailsCompleteScreenState createState() =>
      _PropertyDetailsCompleteScreenState();
}

class _PropertyDetailsCompleteScreenState
    extends State<PropertyDetailsCompleteScreen> {
  bool isDropDown = false;
  int? currentIndex = 0;
  ScrollController scrollController = ScrollController();
  bool isLoad = true;
  ApiBloc? apiBloc;
  ProjectDetailsData? projectDetailsData;
  bool isBack = true;

  // PropertyDetailsArguments propertyArguments = PropertyDetailsArguments();

  @override
  void initState() {
    scrollController.addListener(() => scrollListener());
    super.initState();
    getProjectDetailsApi();
  }

  @override
  void dispose() {
    scrollController.removeListener(() => scrollListener());
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    setStatusBarIconColor(iconColor: Brightness.light);
    return WillPopScope(
      onWillPop: () => backClick(),
      child: Scaffold(
        backgroundColor: isLoad ? shimmerGrey : whiteGrey,
        body: SafeArea(
          top: false,
          bottom: false,
          child: RefreshIndicator(
            color: darkYellow,
            onRefresh: () async {
              getProjectDetailsApi();
            },
            child: LayoutBuilder(
              builder: (context, viewportConstraints) {
                return BlocConsumer<ApiBloc, ApiStates>(
                  bloc: apiBloc,
                  builder: (context, state) {
                    return Stack(
                      children: [
                        SingleChildScrollView(
                          controller: scrollController,
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                              minHeight: viewportConstraints.maxHeight,
                            ),
                            child: BlocConsumer<ApiBloc, ApiStates>(
                              bloc: apiBloc,
                              builder: (context, state) {
                                return mainLayout();
                              },
                              listener: (BuildContext context, state) {
                                if (state is LogoutApiDataLoadState) {
                                  SpManager.logoutDeleteData();
                                  goAndClearAllPageNamed(context, loginOtpScreen);
                                } else if (state is ProjectDetailsApiDataLoadState) {
                                  projectDetailsData = state.model?.response;
                                  isLoad = false;
                                  if (widget.propertyArguments?.type == "updates" || widget.propertyArguments?.type == "project-new-update") {
                                    openUpdatesDialog();
                                  }
                                } else if (state is ApiLoadingState) {
                                  isLoad = true;
                                } else if (state is ApiErrorState) {
                                  isLoad = false;
                                  showToast(
                                    state.errorMessage ??
                                        S().server_error_please_try_again,
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                        isLoad
                            ? Container()
                            : SafeArea(
                                child: Container(
                                  margin: EdgeInsets.only(
                                      left: 25, top: 15, right: 25),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      AnimatedContainer(
                                        duration: Duration(milliseconds: 450),
                                        height: isBack ? 40 : 0,
                                        width: isBack ? 40 : 0,
                                        child: roundIconButton(
                                          onPressed: () => backClick(),
                                          iconColor: black,
                                        ),
                                      ),
                                      projectDetailsData?.supports != null &&
                                              projectDetailsData!
                                                  .supports!.isNotEmpty
                                          ? roundIconButton(
                                              color: black,
                                              icon: ic_support,
                                              width: 50.0,
                                              height: 50.0,
                                              iconWidth: 22.0,
                                              iconHeight: 22.0,
                                              iconColor: yellow,
                                              onPressed: () =>
                                                  goToNextNamedRoute(
                                                context,
                                                enquiryScreen,
                                              ),
                                            )
                                          : Container(height: 50, width: 50),
                                    ],
                                  ),
                                ),
                              ),
                      ],
                    );
                  },
                  listener: (BuildContext context, state) {
                    if (state is ProjectDetailsApiDataLoadState) {
                      projectDetailsData = state.model?.response;
                      isLoad = false;
                    } else if (state is ApiLoadingState) {
                      isLoad = true;
                    } else if (state is ApiErrorState) {
                      isLoad = false;
                      showToast(
                        state.errorMessage ?? S().server_error_please_try_again,
                      );
                    }
                  },
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget mainLayout() => isLoad
      ? SafeArea(
          child: ShimmerWidget(
            imagePath: ic_shimmer_detail_complete,
            aspectRatio: 490 / 1796,
          ),
        )
      : Stack(
          children: [
            Container(),
            Container(
              width: SizeUtils.width(context, 100),
              height: SizeUtils.height(context, 35),
              child: AspectRatio(
                aspectRatio: 1,
                child: NetworkBlurHashImageView(
                  imageUrl: projectDetailsData?.coverImage ?? "",
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Container(
              clipBehavior: Clip.antiAlias,
              width: SizeUtils.width(context, 100),
              margin: EdgeInsets.only(top: SizeUtils.height(context, 28)),
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).padding.bottom),
              decoration: BoxDecoration(
                color: whiteGrey,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
              child: Card(
                elevation: 0,
                color: whiteGrey,
                margin: EdgeInsets.zero,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                child: contentLayout(),
              ),
            ),
          ],
        );

  Widget contentLayout() => Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(left: 25, right: 25, top: 25),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Text(
                            projectDetailsData?.propertyName ?? "",
                            style: boldFontsStyle(
                              fontFamily: platForm,
                              fontSize: bigLargeFont,
                            ),
                          ),
                        ),
                        CupertinoButton(
                          onPressed: () => openCupertinoBottomSheet(
                            context,
                            widget: PropertyDetailsDialogView(),
                            title: S().property_details,
                          ),
                          padding: EdgeInsets.zero,
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: Image.asset(ic_info, height: 25, width: 25),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        if (projectDetailsData?.projectUnit != null && projectDetailsData?.projectUnit != '')
                          Padding(
                            padding: const EdgeInsets.only(right: 15.0),
                            child: Text(
                              projectDetailsData?.projectUnit ?? "",
                              style: regularFontsStyle(
                                fontSize: mediumFont,
                                color: grey,
                              ),
                            ),
                          ),
                        if (projectDetailsData?.projectSize != null && projectDetailsData?.projectSize != "0")
                          Flexible(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Image.asset(
                                  ic_sq_ft,
                                  height: 17,
                                  width: 17,
                                  color: grey,
                                ),
                                SizedBox(width: 5),
                                Text(
                                  "${projectDetailsData?.projectSize ?? ""}sqft",
                                  style: regularFontsStyle(
                                    fontSize: mediumFont,
                                    color: grey,
                                  ),
                                ),
                                SizedBox(width: 15),
                              ],
                            ),
                          ),
                        if (projectDetailsData?.propertyType != null && projectDetailsData?.propertyType != '')
                          Flexible(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Image.asset(
                                  ic_room_type,
                                  height: 17,
                                  width: 17,
                                  color: grey,
                                ),
                                SizedBox(width: 5),
                                Flexible(
                                  child: Text(
                                    projectDetailsData?.propertyType ?? "",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: regularFontsStyle(
                                      fontSize: mediumFont,
                                      color: grey,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                      ],
                    ),
                  ),
                  documentsViewLayout(),
                ],
              ),
            ),
            HorizontalLine(margin: EdgeInsets.only(top: 40, bottom: 40)),
            progressDetailsView(),
          ],
        ),
      );

  // Widget documentsViewLayout() => Container(
  //       margin: EdgeInsets.only(top: 25),
  //       width: SizeUtils.width(context, 100),
  //       padding: EdgeInsets.all(20),
  //       decoration: BoxDecoration(
  //         color: white,
  //         borderRadius: BorderRadius.circular(16),
  //         boxShadow: [
  //           BoxShadow(
  //             color: shadowColor.withOpacity(0.1),
  //             spreadRadius: 1,
  //             blurRadius: 16,
  //             offset: Offset(0, 0.19),
  //           ),
  //         ],
  //       ),
  //       child: Row(
  //         crossAxisAlignment: CrossAxisAlignment.center,
  //         mainAxisAlignment: MainAxisAlignment.center,
  //         children: [
  //           CupertinoButton(
  //             onPressed: () {
  //               AppDataModel.setDocumentList(projectDetailsData?.documents);
  //               openCupertinoBottomSheet(
  //                 context,
  //                 widget: DocumentsDialogView(),
  //                 title: S().all_medias_and_Docs,
  //               );
  //             },
  //             padding: EdgeInsets.zero,
  //             child: roundIconWithText(
  //               iconPath: ic_document,
  //               text: S().all_medias_and_Docs,
  //             ),
  //           ),
  //           Opacity(
  //             opacity: 1,
  //             child: roundIconWithText(
  //               iconPath: ic_care,
  //               text: S().care,
  //             ),
  //           ),
  //           Opacity(
  //             opacity: 1,
  //             child: CupertinoButton(
  //               onPressed: () => goToNextNamedRoute(context, tmsScreen),
  //               padding: EdgeInsets.zero,
  //               child: roundIconWithText(
  //                 color: lightBlue,
  //                 iconPath: ic_cozy_homes,
  //                 padding: EdgeInsets.zero,
  //                 iconColor: white,
  //                 text: S().tenancy,
  //               ),
  //             ),
  //           ),
  //         ],
  //       ),
  //     );

  Widget documentsViewLayout() => CupertinoButton(
        onPressed: () {
          AppDataModel.setDocumentList(projectDetailsData?.documents);
          AppDataModel.setMediaTabList(projectDetailsData?.allMediaListArr);
          openCupertinoBottomSheet(
            context,
            widget: DocumentsDialogView(),
            title: S().all_medias_and_Docs,
          );
        },
        padding: EdgeInsets.zero,
        child: Container(
          margin: EdgeInsets.only(top: 25),
          width: SizeUtils.width(context, 100),
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.circular(16),
            boxShadow: [
              BoxShadow(
                color: shadowColor.withOpacity(0.1),
                spreadRadius: 1,
                blurRadius: 16,
                offset: Offset(0, 0.19),
              ),
            ],
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(ic_document, height: 20, width: 20),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 15, top: 3, right: 15),
                  child: Text(
                    S().all_medias_and_Docs,
                    style: boldFontsStyle(fontSize: bigFonts),
                  ),
                ),
              ),
              Image.asset(ic_next, height: 20, width: 20)
            ],
          ),
        ),
      );

  Widget progressDetailsView() => Container(
        width: SizeUtils.width(context, 100),
        margin: EdgeInsets.only(right: 25, bottom: 25, left: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S().makeover,
              style: boldFontsStyle(
                fontFamily: platForm,
                height: 0.80,
                fontSize: largeFont,
              ),
            ),
            projectDetailsData?.reminder != null &&
                  projectDetailsData!.reminder!.length > 0 &&
                  projectDetailsData!.reminder![0].show!
                ? Container(
                    margin: EdgeInsets.only(top: 24),
                    width: SizeUtils.width(context, 100),
                    clipBehavior: Clip.antiAliasWithSaveLayer,
                    decoration: BoxDecoration(
                      color: white,
                      borderRadius: BorderRadius.circular(16),
                      boxShadow: [
                        BoxShadow(
                          color: shadowColor.withOpacity(0.1),
                          spreadRadius: 1,
                          blurRadius: 16,
                          offset: Offset(0, 0.19),
                        ),
                      ],
                    ),
                    child: IntrinsicHeight(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            child: Lottie.asset(
                              payment_reminder,
                              height: 100,
                              width: SizeUtils.width(context, 20),
                            ),
                            padding: EdgeInsets.all(10),
                            color: yellow,
                          ),
                          Expanded(
                            child: Container(
                              color: darkGrey,
                              padding: EdgeInsets.all(16),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Builder(
                                    builder: (context) {
                                      return RichText(
                                        text: ExpandableHtmlWidget().htmlToTextSpan(context, projectDetailsData?.reminder![0].title ?? ""),
                                        maxLines: 4,
                                        overflow: TextOverflow.ellipsis
                                      );
                                    }
                                  ),
                                  SizedBox(height: 10),
                                  Builder(
                                    builder: (context) {
                                      return RichText(
                                        text: ExpandableHtmlWidget().htmlToTextSpan(context, projectDetailsData?.reminder![0].description ?? ""),
                                        maxLines: 10,
                                        overflow: TextOverflow.ellipsis
                                      );
                                    }
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                : Container(),
            SizedBox(height: 25),
            Row(
              children: [
                if (projectDetailsData?.startDateTime != null)
                  Expanded(
                    child: makeoverProcessTextViewColumn(
                      title: S().startDate,
                      value: formatDate(
                        date: projectDetailsData?.startDateTime ?? "",
                      ),
                    ),
                  ),
                SizedBox(width: 10),
                if (projectDetailsData?.completionDate != null)
                  Expanded(
                    child: makeoverProcessTextViewColumn(
                      title: S().completedDate,
                      value: formatDate(
                        date: projectDetailsData?.completionDate ?? "",
                      ),
                    ),
                  ),
              ],
            ),
            if (projectDetailsData?.projectHandoverDate != null || projectDetailsData?.themeName != null) SizedBox(height: 25),
            if (projectDetailsData?.projectHandoverDate != null || projectDetailsData?.themeName != null)
              Row(
                children: [
                  if (projectDetailsData?.projectHandoverDate != null)
                    Expanded(
                      child: makeoverProcessTextViewColumn(
                        title: S().handoverDate,
                        value: formatDate(
                          date: projectDetailsData?.projectHandoverDate ?? "",
                        ),
                      ),
                    ),
                  SizedBox(width: 10),
                  if (projectDetailsData?.themeName != null)
                    Expanded(
                      child: makeoverProcessTextViewColumn(
                        title: S().theme,
                        value: projectDetailsData?.themeName,
                      ),
                    ),
                ],
              ),
            if (projectDetailsData?.warrantyStartDate != null ||
                projectDetailsData?.warrantyEndDate != null ||
                false)
              SizedBox(height: 25),
            if (projectDetailsData?.warrantyStartDate != null ||
                projectDetailsData?.warrantyEndDate != null ||
                false)
              makeoverProcessTextViewColumn(
                title: S().warranty_duration,
                isRaw: true,
                startValue: formatDate(
                      date: projectDetailsData?.warrantyStartDate ?? "",
                    ) ??
                    "",
                endValue: formatDate(
                      date: projectDetailsData?.warrantyEndDate ?? "",
                    ) ??
                    "",
              ),
            if (projectDetailsData?.mocStartDate != null &&
                projectDetailsData?.mocEndDate != null)
              SizedBox(height: 25),
            if (projectDetailsData?.mocStartDate != null &&
                projectDetailsData?.mocEndDate != null)
              makeoverProcessTextViewColumn(
                title: S().insurance_validity,
                isRaw: true,
                startValue: formatDate(
                      date: projectDetailsData?.mocStartDate ?? "",
                    ) ??
                    "",
                endValue: formatDate(
                      date: projectDetailsData?.mocEndDate ?? "",
                    ) ??
                    "",
              ),
            HorizontalLine(margin: EdgeInsets.only(top: 25)),
            if ((projectDetailsData?.gallery != null && projectDetailsData!.gallery!.length > 0) || (projectDetailsData?.dropboxLink != '' && projectDetailsData?.dropboxLink != null))
              galleryTextView(),
            if (projectDetailsData?.gallery != null && projectDetailsData!.gallery!.length > 0)
              galleryListView(),
            SizedBox(height: 25),
            if (projectDetailsData?.dropboxLink != '' && projectDetailsData?.dropboxLink != null)
              GestureDetector(
                onTap: () {
                  launchURL(projectDetailsData?.dropboxLink ?? "");
                },
                child: Container(
                  margin: EdgeInsets.only(bottom: 25.0),
                  child: Text(
                    projectDetailsData?.dropboxLink ?? "", 
                    style: boldFontsStyle(fontSize: bigFonts, color: darkYellow)
                  ),
                ),
              ),
            viewPreviousUpdatesButton(),
          ],
        ),
      );

  Widget galleryTextView() => Container(
        margin: EdgeInsets.only(top: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Text(
                S().gallery,
                style: boldFontsStyle(fontSize: bigFonts),
              ),
            ),
            SizedBox(width: 10),
            // TODO: Temporary hide view all button
            projectDetailsData?.gallery != null &&
                    projectDetailsData!.gallery!.length > 0
                ? GestureDetector(
                    onTap: () => goToGalleryPage(0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          S().view_all,
                          style: regularFontsStyle(fontSize: mediumFont),
                        ),
                        SizedBox(width: 10),
                        Container(
                          height: 13,
                          child: AspectRatio(
                            aspectRatio: 48 / 88,
                            child: Image.asset(
                              ic_next,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                : Container()
          ],
        ),
      );

  Widget galleryListView() => Container(
        margin: EdgeInsets.only(top: 20),
        height: 350,
        child: Column(
          children: [
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    flex: 35,
                    child: galleryImage(index: 0)
                  ),
                  if (projectDetailsData!.gallery!.length > 2)
                    Expanded(
                      flex: 60,
                      child: Container(
                        margin: EdgeInsets.only(left: 5.0),
                        child: galleryImage(index: 2)
                      ),
                    ),
                ],
              ),
            ),
            if (projectDetailsData!.gallery!.length > 1)
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 5),
                child: Row(
                    children: [
                      Expanded(
                        child: galleryImage(
                          index: 1,
                        ),
                      ),
                      if (projectDetailsData!.gallery!.length > 3)
                        Container(
                          margin: EdgeInsets.only(left: 5.0),
                          child: Column(
                            children: [
                              Expanded(
                                child: galleryImage(
                                  index: 3,
                                  width: 116.0,
                                ),
                              ),
                              if (projectDetailsData!.gallery!.length > 4)
                                Expanded(
                                  child: Container(
                                    margin: EdgeInsets.only(top: 6),
                                    child: galleryImage(
                                      index: 4,
                                      width: 116.0,
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        ),
                    ],
                  ),
              ),
            ),
          ],
        ),
      );

  Widget galleryImage({
    index,
    height,
    width,
  }) {
    bool isVideo = projectDetailsData?.gallery![index].fileType == "video";
    String? thumbnailPath = isVideo
                            ? projectDetailsData?.gallery![index].thumbnail 
                            : projectDetailsData?.gallery![index].media;
    
    return CupertinoButton(
      padding: EdgeInsets.zero,
      onPressed: () => goToGalleryPage(index),
      child: Stack(
        children: [
          NetworkBlurHashImageView(
            imageUrl: thumbnailPath,
            height: height,
            width: width,
            fit: BoxFit.cover,
          ),
          Positioned.fill(
            child: Stack(
              children: [
                if (projectDetailsData?.gallery![index].fileType == "video")
                  Center(
                    child: Image.asset(ic_play, height: 25, width: 25)
                  ),
                Container(color: black.withOpacity(0.1))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget viewPreviousUpdatesButton() => RoundedButton(
        borderColor: black,
        buttonColor: white,
        fontColor: black,
        fontSize: bigFonts,
        onPressed: () => openUpdatesDialog(),
        buttonText: S().viewMakeoverUpdates,
      );

  Widget roundIconWithText(
          {color, iconPath, elevation, text, iconColor = black, padding}) =>
      Container(
        width: 100,
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            roundIconButton(
              color: color ?? yellow,
              icon: iconPath ?? ic_document,
              iconColor: iconColor,
              padding: padding,
              elevation: elevation ?? 0.0,
            ),
            SizedBox(height: 8),
            Text(
              text ?? "",
              style: boldFontsStyle(),
            )
          ],
        ),
      );

  Widget makeoverProcessTextViewColumn(
          {title, value, bool isRaw = false, startValue, endValue}) =>
      Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: regularFontsStyle(
              color: grey,
              fontSize: mediumFont,
            ),
          ),
          SizedBox(height: 10),
          isRaw
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      startValue ?? "",
                      style: boldFontsStyle(fontSize: bigFonts),
                    ),
                    startValue?.isNotEmpty
                        ? Container(
                            margin: EdgeInsets.only(left: 10, right: 10),
                            child: Image.asset(
                              ic_arrow_next,
                              height: 20,
                              width: 20,
                              color: black,
                            ),
                          )
                        : Container(),
                    Text(
                      endValue ?? "",
                      style: boldFontsStyle(fontSize: bigFonts),
                    ),
                  ],
                )
              : Text(
                  value ?? "",
                  style: boldFontsStyle(fontSize: bigFonts),
                )
        ],
      );

  getProjectDetailsApi() {
    apiBloc = BlocProvider.of<ApiBloc>(context);
    apiBloc?.add(
      ProjectDetailsApiEvent(
          projectId: widget.propertyArguments?.projectID ?? 0),
    );
  }

  // Temporary switch document layout to this
  // Widget documentsViewLayout() => CupertinoButton(
  //       onPressed: () {
  //         AppDataModel.setDocumentList(projectDetailsData?.documents);
  //         openCupertinoBottomSheet(
  //           context,
  //           widget: DocumentsDialogView(),
  //           title: S().all_medias_and_Docs,
  //         );
  //       },
  //       padding: EdgeInsets.zero,
  //       child: Container(
  //         margin: EdgeInsets.only(top: 25),
  //         width: SizeUtils.width(context, 100),
  //         padding: EdgeInsets.all(20),
  //         decoration: BoxDecoration(
  //           color: white,
  //           borderRadius: BorderRadius.circular(16),
  //           boxShadow: [
  //             BoxShadow(
  //               color: shadowColor.withOpacity(0.1),
  //               spreadRadius: 1,
  //               blurRadius: 16,
  //               offset: Offset(0, 0.19),
  //             ),
  //           ],
  //         ),
  //         child: Row(
  //           crossAxisAlignment: CrossAxisAlignment.center,
  //           children: [
  //             Image.asset(ic_document, height: 20, width: 20),
  //             Expanded(
  //               child: Container(
  //                 margin: EdgeInsets.only(left: 15, top: 3, right: 15),
  //                 child: Text(
  //                   S().all_medias_and_Docs,
  //                   style: boldFontsStyle(fontSize: bigFonts),
  //                 ),
  //               ),
  //             ),
  //             Image.asset(ic_next, height: 20, width: 20)
  //           ],
  //         ),
  //       ),
  //     );

  goToGalleryPage(int index) {
    goToNextNamedRoute(
      context,
      galleryScreen,
      args: MediaScreenArgs(
        index: index,
        mediaList: projectDetailsData!.gallery,
      ),
    );
  }

  scrollListener() {
    if (scrollController.offset >=
        (scrollController.position.minScrollExtent +
            SizeUtils.height(context, 20))) {
      setState(() {
        isBack = false;
      });
    } else {
      setState(() {
        isBack = true;
      });
    }
  }

  openUpdatesDialog() {
    AppDataModel.setUpdatesList(projectDetailsData?.updates);
    openCupertinoBottomSheet(
      context,
      widget: UpdatesDialogView(),
      title: S().makeoverUpdates,
    );
  }

  backClick() {
    Route route = ModalRoute.of(context) as Route;
    String? pageName = route.settings.name;
    if (pageName != null &&
        pageName == propertyDetailsCompleteScreen &&
        widget.propertyArguments?.type != null) {
      Navigator.popUntil(context, ModalRoute.withName(homeScreen));
    } else {
      Navigator.pop(context);
    }
    return true;
  }
}
