import 'dart:io';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:dio/dio.dart';
import 'package:pmaclient/api_services/rest_api_client.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/forgot_password_model.dart';
import 'package:pmaclient/models/maintenance_model.dart';
import 'package:pmaclient/models/notification_api_model.dart';
import 'package:pmaclient/models/otp_model.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:pmaclient/models/projects_model.dart';
import 'package:pmaclient/models/user_model.dart';
import 'package:pmaclient/utils/error_utils.dart';
import 'package:pmaclient/utils/util_helper.dart';

class ApiRepository {
  static getConfigApiData() async {
    RestApiClient client = RestApiClient(getNonLoginDio());
    try {
      final response = await client.getConfigs(getCacheOption());
      if (response.status == true) {
        return response;
      } else {
        throw Exception(response.message);
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static postLoginData({userName, password, token}) async {
    RestApiClient client = RestApiClient(getNonLoginDio());
    try {
      if(AppDataModel.getByPassCode()){
        final response = await client.postMaintenanceLogin(userName, password, token);
        if (response["status"] == true) {
          return UserModel.fromJson(response["response"]);
        } else {
          throw response["message"];
        }
      }else{
        final response = await client.postLogin(userName, password, token);
        if (response["status"] == true) {
          return UserModel.fromJson(response["response"]);
        } else {
          throw response["message"];
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static postMaintenanceByPass({password}) async {
    RestApiClient client = RestApiClient(getNonLoginDio());
    try {
      final response = await client.postMaintenanceByPass(password);
        if (response["status"] == true) {
          return MaintenanceModel.fromJson(response['response']);
        } else {
          throw response["message"];
        }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static postRequestOtpData({contactNumber}) async {
    RestApiClient client = RestApiClient(getNonLoginDio());
    try {
      if(AppDataModel.getByPassCode()){
        final response = await client.postMaintenanceGenerateTemCode(contactNumber);
        if (response["status"] == true) {
          return OtpModel.fromJson(response['response']);
        } else if (response["message"] == "You have already requested OTP earlier. Please try again later.") {
          return OtpModel.fromJson(response['response']);
        } else {
          throw response["message"];
        }
      }else{
        final response = await client.postGenerateTemCode(contactNumber);
        if (response["status"] == true) {
          return OtpModel.fromJson(response['response']);
        } else if (response["message"] == "You have already requested OTP earlier. Please try again later.") {
          return OtpModel.fromJson(response['response']);
        } else {
          throw response["message"];
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static postRequestOtpEmailData({ownerID}) async {
    RestApiClient client = RestApiClient(getNonLoginDio());
    try {
      if(AppDataModel.getByPassCode()){
        final response = await client.postMaintenanceGenerateTemCodeEmail(ownerID);
        if (response["status"] == true) {
          return EmailOtpModel.fromJson(response['response']);
        } else if (response["message"] == "Email already sent before.") {
          return EmailOtpModel.fromJson(response['response']);
        } else {
          throw response["message"];
        }
      }else{
        final response = await client.postGenerateTemCodeEmail(ownerID);
        if (response["status"] == true) {
          return EmailOtpModel.fromJson(response['response']);
        } else if (response["message"] == "Email already sent before.") {
          return EmailOtpModel.fromJson(response['response']);
        } else {
          throw response["message"];
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static postOtpLoginData({ownerID, otpCode, token, tokenGenerateBy}) async {
    RestApiClient client = RestApiClient(getNonLoginDio());
    try {
      if(AppDataModel.getByPassCode()){
        final response = await client.postMaintenanceVerifyTemCode(ownerID, otpCode, token, tokenGenerateBy);
        if (response["status"] == true) {
          return UserModel.fromJson(response["response"]);
        } else {
          throw response["message"];
        }
      }else{
        final response = await client.postVerifyTemCode(ownerID, otpCode, token, tokenGenerateBy);
        if (response["status"] == true) {
          return UserModel.fromJson(response["response"]);
        } else {
          throw response["message"];
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static postResetPasswordRequest({userName}) async {
    RestApiClient client = RestApiClient(getNonLoginDio());
    try {
      if(AppDataModel.getByPassCode()){
        final response = await client.postMaintenancePasswordResetRequest(userName);
        if (response["status"] == true) {
          return ForgotPasswordModel.fromJson(response["response"]);
        } else {
          throw response["message"];
        }
      }else{
        final response = await client.postPasswordResetRequest(userName);
        if (response["status"] == true) {
          return ForgotPasswordModel.fromJson(response["response"]);
        } else {
          throw response["message"];
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static sendPasswordUpdate({currentPassword, password, confirmPassword}) async {
    RestApiClient client = RestApiClient(getDio());
    try {
      if(AppDataModel.getByPassCode()){
        final response = await client.sendMaintenancePasswordUpdate(
            currentPassword, password, confirmPassword);
        if (response["status"] == true) {
          return response["message"];
        } else {
          throw response["message"];
        }
      }else{
        final response = await client.sendPasswordUpdate(
            currentPassword, password, confirmPassword);
        if (response["status"] == true) {
          return response["message"];
        } else {
          throw response["message"];
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static getProjects(int currentPage) async {
    RestApiClient client = RestApiClient(getDio());
    try {
      if(AppDataModel.getByPassCode()){
      // final response = await client.getProjects(currentPage, getCacheOption());
        final response = await client.getMaintenanceProjects(currentPage);
        if (response["status"] == true) {
          return ProjectModel.fromJson(response["response"]);
        } else {
          throw response["message"];
        }
      }else{
        final response = await client.getProjects(currentPage);
        if (response["status"] == true) {
          return ProjectModel.fromJson(response["response"]);
        } else {
          throw response["message"];
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static getProjectDetails(int projectId) async {
    RestApiClient client = RestApiClient(getDio());
    try {
      if(AppDataModel.getByPassCode()){
      // ProjectDetailsModel model = await client.getProjectDetails(projectId, getCacheOption());
        ProjectDetailsModel model = await client.getMaintenanceProjectDetails(projectId);
        if (model.status == true) {
          return model;
        } else {
          throw model.message ?? S().server_error_please_try_again;
        }
      }else{
        ProjectDetailsModel model = await client.getProjectDetails(projectId);
        if (model.status == true) {
          return model;
        } else {
          throw model.message ?? S().server_error_please_try_again;
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static getNotifications(int pageNo) async {
    RestApiClient client = RestApiClient(getDio());
    try {
      if(AppDataModel.getByPassCode()){
        NotificationApiModel model =
            // await client.getNotificationsList(pageNo, getCacheOption());
            await client.getMaintenanceNotificationsList(pageNo);
        if (model.status == true) {
          return model;
        } else {
          throw model.message ?? S().server_error_please_try_again;
        }
      }else{
        NotificationApiModel model = await client.getNotificationsList(pageNo);
        if (model.status == true) {
          return model;
        } else {
          throw model.message ?? S().server_error_please_try_again;
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static getProfileData() async {
    RestApiClient client = RestApiClient(getDio());
    try {
      if(AppDataModel.getByPassCode()){
      // final response = await client.getProfileData(getCacheOption());
        final response = await client.getMaintenanceProfileData();
        if (response["status"] == true) {
          return UserModel.fromJson(response["response"]);
        } else {
          throw response["message"];
        }
      }else{
        final response = await client.getProfileData();
        if (response["status"] == true) {
          return UserModel.fromJson(response["response"]);
        } else {
          throw response["message"];
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static updateProfilePhoto(File file) async {
    RestApiClient client = RestApiClient(getDio());
    try {
      if(AppDataModel.getByPassCode()){
        final response = await client.updateMaintenanceProfilePhoto(file);
        if (response["status"] == true) {
          return response["response"]["avatar"];
        } else {
          throw response["message"];
        }
      }else{
        final response = await client.updateProfilePhoto(file);
        if (response["status"] == true) {
          return response["response"]["avatar"];
        } else {
          throw response["message"];
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static logoutUser() async {
    RestApiClient client = RestApiClient(getDio());
    try {
      if(AppDataModel.getByPassCode()){
        final response = await client.logoutMaintenanceUser();
        if (response["status"] == true) {
          return;
        } else {
          throw response["message"];
        }
      }else{
        final response = await client.logoutUser();
        if (response["status"] == true) {
          return;
        } else {
          throw response["message"];
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static getTermsAndPrivacy() async {
    RestApiClient client = RestApiClient(getDio());
    try {
      if(AppDataModel.getByPassCode()){
        final response = await client.getMaintenanceTermsAndPolicy();
        if (response["status"] == true) {
          return response["response"];
        } else {
          throw response["message"];
        }
      }else{
        final response = await client.getTermsAndPolicy();
        if (response["status"] == true) {
          return response["response"];
        } else {
          throw response["message"];
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static updateNotificationStatus(int notificationsId) async {
    RestApiClient client = RestApiClient(getDio());
    try {
      if(AppDataModel.getByPassCode()){
        final response = await client.updateMaintenanceNotifications(notificationsId);
        if (response["status"] == true) {
          return NotificationDataList.fromJson(response["response"][0]);
        } else {
          throw response["message"];
        }
      }else{
        final response = await client.updateNotifications(notificationsId);
        if (response["status"] == true) {
          return NotificationDataList.fromJson(response["response"][0]);
        } else {
          throw response["message"];
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static readAllNotification() async {
    RestApiClient client = RestApiClient(getDio());
    try {
      if(AppDataModel.getByPassCode()){
        final response = await client.notificationsMaintenanceReadAll();
        if (response["status"] == true) {
          return response["message"];
        } else {
          throw response["message"];
        }
      }else{
        final response = await client.notificationsReadAll();
        if (response["status"] == true) {
          return response["message"];
        } else {
          throw response["message"];
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }

  static sendReviewPost(body) async {
    RestApiClient client = RestApiClient(getDio());
    try {
      if(AppDataModel.getByPassCode()){
        final response = await client.sendMaintenanceReviewJson(body);
        if (response.status == true) {
          return response.response;
        } else {
          throw response.message ?? S().server_error_please_try_again;
        }
      }else{
        final response = await client.sendReviewJson(body);
        if (response.status == true) {
          return response.response;
        } else {
          throw response.message ?? S().server_error_please_try_again;
        }
      }
    } on DioError catch (e) {
      throw ErrorUtils.handleError(e);
    }
  }
}
