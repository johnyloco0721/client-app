import 'dart:io';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:dio/dio.dart';
import 'package:pmaclient/constants/app_constants.dart';
import 'package:pmaclient/models/config_model.dart';
import 'package:pmaclient/models/feedback_model.dart';
import 'package:pmaclient/models/notification_api_model.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';

part 'rest_api_client.g.dart';

@RestApi(baseUrl: baseUrl)
abstract class RestApiClient {
  factory RestApiClient(Dio dio, {String baseUrl}) = _RestApiClient;

  @GET("Config")
  Future<ConfigModel> getConfigs(@DioOptions() Options options);

   @POST("Login")
  Future postMaintenanceLogin(
    @Field('username') String userName,
    @Field('password') String password,
    @Field('token') String token,
  );

  @POST("Login")
  Future postLogin(
    @Field('username') String userName,
    @Field('password') String password,
    @Field('token') String token,
  );

  @POST("MaintenanceByPass")
  Future postMaintenanceByPass(
    @Field('password') String password,
  );

  @POST("GenerateTemCode")
  Future postMaintenanceGenerateTemCode(
    @Field('contactNumber') String contactNumber,
  );

  @POST("GenerateTemCode")
  Future postGenerateTemCode(
    @Field('contactNumber') String contactNumber,
  );

  @POST("GenerateTemCodeEmail")
  Future postMaintenanceGenerateTemCodeEmail(
    @Field('ownerID') String ownerID,
  );

  @POST("GenerateTemCodeEmail")
  Future postGenerateTemCodeEmail(
    @Field('ownerID') String ownerID,
  );

  @POST("VerifyTemCode")
  Future postMaintenanceVerifyTemCode(
    @Field('ownerID') String ownerID,
    @Field('otpCode') String otpCode,
    @Field('deviceToken') String deviceToken,
    @Field('tokenGenerateBy') String tokenGenerateBy,
  );

  @POST("VerifyTemCode")
  Future postVerifyTemCode(
    @Field('ownerID') String ownerID,
    @Field('otpCode') String otpCode,
    @Field('deviceToken') String deviceToken,
    @Field('tokenGenerateBy') String tokenGenerateBy,
  );

  @POST("PasswordReset")
  Future postMaintenancePasswordResetRequest(@Field('loginUsername') String userName);

  @POST("PasswordReset")
  Future postPasswordResetRequest(@Field('loginUsername') String userName);

  @POST("PasswordUpdate")
  Future sendMaintenancePasswordUpdate(
      @Field('currentPassword') String currentPassword,
      @Field('password') String password,
      @Field('confirmPassword') String confirmPassword);

  @POST("PasswordUpdate")
  Future sendPasswordUpdate(
      @Field('currentPassword') String currentPassword,
      @Field('password') String password,
      @Field('confirmPassword') String confirmPassword);

  @GET("NotificationList")
  Future<NotificationApiModel> getMaintenanceNotificationsList(
      @Query("currentPageNo") int pageNo);
      // @Query("currentPageNo") int pageNo, @DioOptions() Options options);

  @GET("NotificationList")
  Future<NotificationApiModel> getNotificationsList(
      @Query("currentPageNo") int pageNo);
      // @Query("currentPageNo") int pageNo, @DioOptions() Options options);

  @GET("Landing")
  Future getMaintenanceProjects(
      @Query("currentPageNo") int pageNo);
      // @Query("currentPageNo") int pageNo, @DioOptions() Options options);

  @GET("Landing")
  Future getProjects(
      @Query("currentPageNo") int pageNo);
      // @Query("currentPageNo") int pageNo, @DioOptions() Options options);

  @POST("NotificationUpdate")
  Future updateMaintenanceNotifications(@Field("notificationID") int notificationsId);

  @POST("NotificationUpdate")
  Future updateNotifications(@Field("notificationID") int notificationsId);

  @POST("FeedbackSubmit")
  Future<FeedbackModel> sendMaintenanceReviewJson(@Body() String reviewJson);

  @POST("FeedbackSubmit")
  Future<FeedbackModel> sendReviewJson(@Body() String reviewJson);

  @POST("ProfileImage")
  Future updateMaintenanceProfilePhoto(@Part(value: "image") File file);

  @POST("ProfileImage")
  Future updateProfilePhoto(@Part(value: "image") File file);

  @POST("NotificationReadAll")
  Future notificationsMaintenanceReadAll();

  @POST("NotificationReadAll")
  Future notificationsReadAll();

  @GET("ProjectDetail")
  Future<ProjectDetailsModel> getMaintenanceProjectDetails(
      @Query("projectID") int projectId);
      // @Query("projectID") int projectId, @DioOptions() Options options);

  @GET("ProjectDetail")
  Future<ProjectDetailsModel> getProjectDetails(
      @Query("projectID") int projectId);
      // @Query("projectID") int projectId, @DioOptions() Options options);

  @GET("Profile")
  Future getMaintenanceProfileData();
  // Future getProfileData(@DioOptions() Options options);

  @GET("Profile")
  Future getProfileData();
  // Future getProfileData(@DioOptions() Options options);

  @GET("Logout")
  Future logoutMaintenanceUser();

  @GET("Logout")
  Future logoutUser();

  @GET("Terms")
  Future getMaintenanceTermsAndPolicy();

  @GET("Terms")
  Future getTermsAndPolicy();
}
