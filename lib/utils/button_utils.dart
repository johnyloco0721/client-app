import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';

ButtonStyle defaultButtonStyle(
    {overlayColor = lightGrey,
    backgroundColor = Colors.transparent,
    shadowColor = Colors.transparent,
    padding}) {
  return ButtonStyle(
      overlayColor: MaterialStateProperty.all<Color>(
        (overlayColor ?? black).withOpacity(0.1),
      ),
      backgroundColor:
          MaterialStateProperty.all<Color>(backgroundColor ?? lightBlue),
      shadowColor:
          MaterialStateProperty.all<Color>(shadowColor ?? Colors.transparent),
      padding: MaterialStateProperty.all(padding));
}
