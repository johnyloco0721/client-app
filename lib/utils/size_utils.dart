import 'package:flutter/material.dart';

class SizeUtils {
  static double height(BuildContext context, double v) {
    MediaQueryData _queryData = MediaQuery.of(context);
    return _queryData.size.height / 100.0 * v;
  }

  static double width(BuildContext context, double v) {
    MediaQueryData _queryData = MediaQuery.of(context);
    return _queryData.size.width / 100.0 * v;
  }
}
