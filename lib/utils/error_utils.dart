import 'package:dio/dio.dart';
import 'package:pmaclient/generated/l10n.dart';

class ErrorUtils {
  static handleError(DioError error) {
    String _errorMessage = "";
    switch (error.type) {
      case DioErrorType.cancel:
        _errorMessage = S().request_was_cancelled;
        break;
      case DioErrorType.connectTimeout:
        _errorMessage = S().connection_timeout;
        break;
      case DioErrorType.other:
        _errorMessage = S().connection_failed_due_to_internet;
        break;
      case DioErrorType.receiveTimeout:
        _errorMessage = S().receive_timeout_in_connection;
        break;
      case DioErrorType.response:
        _errorMessage =
            "Received invalid status code: ${error.response?.statusCode}";
        break;
      case DioErrorType.sendTimeout:
        _errorMessage = S().receive_timeout_in_send;
        break;
      default:
        _errorMessage = S().server_error_please_try_again;
        break;
    }
    return _errorMessage;
  }
}
