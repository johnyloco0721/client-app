import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_fonts.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/utils/text_utils.dart';

imageSelectionDialog(context, {photoClick, cameraClick}) =>
    showCupertinoModalPopup(
      context: context,
      barrierColor: black.withOpacity(0.5),
      builder: (BuildContext context) {
        return Padding(
          padding: const EdgeInsets.all(10),
          child: CupertinoActionSheet(
            actions: [
              Container(
                color: white,
                child: CupertinoActionSheetAction(
                  onPressed: photoClick,
                  child: Text(
                    S().photo_gallery,
                    style: regularFontsStyle(color: blue, fontSize: mediumFont),
                  ),
                ),
              ),
              Container(
                color: white,
                child: CupertinoActionSheetAction(
                  onPressed: cameraClick,
                  child: Text(
                    S().camera,
                    style: regularFontsStyle(color: blue, fontSize: mediumFont),
                  ),
                ),
              ),
            ],
            cancelButton: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: white,
              ),
              child: CupertinoActionSheetAction(
                child: Text(
                  S().cancel,
                  style: boldFontsStyle(color: blue, fontSize: mediumFont),
                ),
                isDefaultAction: true,
                onPressed: () => Navigator.pop(context, S.of(context).cancel),
              ),
            ),
          ),
        );
      },
    );

openCupertinoBottomSheet(BuildContext context,
        {title, required Widget widget}) =>
    showCupertinoModalBottomSheet(
      context: context,
      isDismissible: true,
      expand: true,
      topRadius: Radius.circular(16),
      enableDrag: true,
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(16),
        ),
      ),
      useRootNavigator: true,
      backgroundColor: white,
      builder: (context) {
        return SafeArea(
          child: Scaffold(
            backgroundColor: white,
            body: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 10),
                  alignment: Alignment.center,
                  child: Container(
                    width: 40,
                    height: 3,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(2),
                      color: lightGrey,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 25, bottom: 12.5),
                  alignment: Alignment.center,
                  child: Text(
                    title ?? S().all_medias_and_Docs,
                    style: boldFontsStyle(
                      fontFamily: platForm,
                      fontSize: bigLargeFont,
                      letterSpacing: 0.8,
                      height: 1.27
                    ),
                  ),
                ),
                Expanded(child: Container(child: widget)),
                SizedBox(height: 15.0),
              ],
            ),
          ),
        );
      },
    );

openDownloadOrShareBottomSheet(context, widget) =>
    showCupertinoModalBottomSheet(
      context: context,
      isDismissible: true,
      barrierColor: black.withOpacity(0.5),
      elevation: 5,
      topRadius: Radius.circular(16),
      builder: (context) {
        return Container(
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).padding.bottom,
          ),
          child: widget,
        );
      },
    );
