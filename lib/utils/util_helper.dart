import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:open_file/open_file.dart';
// import 'package:open_document/open_document.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pmaclient/bloc/api_bloc/api_events.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_constants.dart';
import 'package:pmaclient/constants/app_data_models.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_fonts.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
// import 'package:pmaclient/custom_module/open_document.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/config_model.dart';
import 'package:pmaclient/models/user_model.dart';
import 'package:pmaclient/route_management/app_routes_names.dart';
import 'package:pmaclient/utils/route_utils.dart';
import 'package:pmaclient/utils/shared_preference_utils/pref_keys.dart';
import 'package:pmaclient/utils/shared_preference_utils/sp_manager.dart';
import 'package:pmaclient/utils/text_utils.dart';
import 'package:url_launcher/url_launcher.dart';

bool isDialogShows = false;

bool checkTodayDate(String? date) {
  DateTime time = DateTime.parse(date ?? "");
  bool isToday = DateTime(time.day, time.month, time.year) ==
          DateTime(
            DateTime.now().day,
            DateTime.now().month,
            DateTime.now().year,
          )
      ? true
      : false;
  return isToday;
}

launchURL(url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

String? encodeQueryParameters(Map<String, String> params) {
  return params.entries
      .map((e) =>
          '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
      .join('&');
}

Uri emailLaunchUri(email, {subject}) => Uri(
      scheme: 'mailto',
      path: email,
      query: encodeQueryParameters(<String, String>{'subject': '$subject'}),
    );

int getExtendedVersionNumber(String version) {
  List versionCells = version.split('.');
  versionCells = versionCells.map((i) => int.parse(i)).toList();
  return versionCells[0] * 100000 + versionCells[1] * 1000 + versionCells[2];
}

goToUpdateScreen(context) async {
  PackageInfo packageInfo = await PackageInfo.fromPlatform();
  String version = packageInfo.version;
  ConfigModel model = SpManager.getConfigModel();
  int appVersion = getExtendedVersionNumber(version);
  int apiVersion = getExtendedVersionNumber(model.response!.mobileVersion ?? '');
  if (appVersion < apiVersion) {
    if (AppDataModel.getTokenGenerateByList() == 'Android') {
      await AppDataModel.setUpdateAppLink(model.response!.AndroidAppUpdateLink); // add new attribute to update new version
    } else if (AppDataModel.getTokenGenerateByList() == 'IOS') {
      await AppDataModel.setUpdateAppLink(model.response!.iOSAppUpdateLink); // add new attribute to update new version
    } else {
      await AppDataModel.setUpdateAppLink(model.response!.HuaweiAppUpdateLink); // add new attribute to update new version
    }
    SchedulerBinding.instance.addPostFrameCallback((_) {
      goToNextNamedRoute(context, updateScreen);
    });
  }
}

showToast(
  String message, {
  Toast toastLength = Toast.LENGTH_LONG,
  ToastGravity gravity = ToastGravity.BOTTOM,
  int timeInSecForIosWeb = 1,
  Color backgroundColor = black,
  Color textColor = white,
  double fontSize = 14,
}) {
  Fluttertoast.showToast(
    toastLength: toastLength,
    msg: message,
    gravity: gravity,
    timeInSecForIosWeb: timeInSecForIosWeb,
    backgroundColor: backgroundColor,
    textColor: textColor,
    fontSize: fontSize,
  );
}

Future<File> getImageFileFromAssets(String path) async {
  var bytes = await rootBundle.load(path);
  String tempPath = (await getTemporaryDirectory()).path;
  File file = File('$tempPath/watermark.html');
  await file.writeAsBytes(
      bytes.buffer.asUint8List(bytes.offsetInBytes, bytes.lengthInBytes));
  return file;
}

// checkFileExistOnDevice(String filePath) async {
//   bool isCheck = await OpenDocument.checkDocument(filePath: filePath);
//   return isCheck;
// }

Future<String> findPathFromCache(String imageUrl) async {
  final file = await DefaultCacheManager().getSingleFile(imageUrl);
  return file.path;
}

Future downloadImage(String url) async {
  await Permission.storage.request();
  if (await Permission.storage.isGranted) {
    String filePath = await findPathFromCache(url);
    if (filePath.isEmpty) {
      var dir;
      if (Platform.isAndroid) {
        dir = await getExternalStorageDirectory();
      } else if (Platform.isIOS) {
        dir = await getApplicationDocumentsDirectory();
      }
      String name = url.substring(url.lastIndexOf("/") + 1);
      String filePath = "${dir.path}/pma_clint/images/$name";
      // final name = await OpenDocument.getNameFile(url: url);
      // final path = await OpenDocument.getPathDocument(folderName: "pma_clint/images");
      // String filePath = "$path/$name";
      if (!await dir.exists()) {
        await dir.create(recursive: true);
      }
      final isCheck = await File(filePath).exists();
      // final isCheck = await checkFileExistOnDevice(filePath);
      if (!isCheck) {
        filePath = await downloadFile(filePath: filePath, url: url);
      }
    }
    return filePath;
  } else { //for > android 13 that not required to granted permission for storage
    String filePath = await findPathFromCache(url);
    if (filePath.isEmpty) {
      var dir;
      if (Platform.isAndroid) {
        dir = await getExternalStorageDirectory();
      } else if (Platform.isIOS) {
        dir = await getApplicationDocumentsDirectory();
      }
      String name = url.substring(url.lastIndexOf("/") + 1);
      String filePath = "${dir.path}/pma_clint/images/$name";
      if (!await dir.exists()) {
        await dir.create(recursive: true);
      }
      final isCheck = await File(filePath).exists();
      if (!isCheck) {
        filePath = await downloadFile(filePath: filePath, url: url);
      }
    }
    return filePath;
  }
}

Options getCacheOption() {
  return buildCacheOptions(Duration(days: 7), forceRefresh: true);
}

Dio getDio() {
  UserModel userModel = SpManager.getUserdata();
  String? accessToken = SpManager.getString(apiToken);
  Dio dio = new Dio();
  dio.interceptors
      .add(DioCacheManager(CacheConfig(baseUrl: baseUrl)).interceptor);
  dio.options.headers['content-type'] = 'application/json; charset=utf-8';
  if ((accessToken != null && accessToken.isNotEmpty) ||
      (userModel.loginHash != null && userModel.loginHash!.isNotEmpty)) {
    dio.options.headers["authorization"] =
        'Bearer ${accessToken?.isEmpty ?? true ? userModel.loginHash : accessToken}';
  }
  dio.options.headers["Accept"] = "application/json";
  dio.options.followRedirects = false;
  dio.options.validateStatus = (status) {
    return status! < 500;
  };
  return dio;
}

replaceWhiteSpace(String value) => value.replaceAll(new RegExp(r"\s+"), "");

Dio getNonLoginDio() {
  Dio dio = new Dio();
  dio.options.headers['content-type'] = 'application/json; charset=utf-8';
  dio.options.headers["Accept"] = "application/json";
  dio.options.followRedirects = false;
  dio.options.validateStatus = (status) {
    return status! < 500;
  };
  return dio;
}

setStatusBarIconColor({iconColor = Brightness.dark}) =>
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: iconColor,
        statusBarBrightness: iconColor,
      ),
    );

formatDate({format = "dd LLL yyyy", date}) {
  try {
    return DateFormat(format).format(DateTime.parse(date ?? ""));
  } catch (e) {
    print(e);
    return "";
  }
}

// Convert hex color code to Color
Color hexToColor(String code) {
  return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xff000000);
}

progressDialog(BuildContext context) {
  showDialog(
      context: context,
      barrierDismissible: false,
      useRootNavigator: true,
      useSafeArea: true,
      builder: (BuildContext context) {
        isDialogShows = true;
        return Container(
          color: Colors.transparent,
          alignment: Alignment.center,
          child: Lottie.asset(
            login_black_loading,
            width: 100,
            height: 60,
            fit: BoxFit.fill,
          ),
        );
      });
}

hideDialog(BuildContext context) {
  if (isDialogShows) {
    isDialogShows = false;
    Navigator.of(context, rootNavigator: true).pop();
  }
}

List<TextSpan> highlightText({
  mainString,
  highLightText,
  textStyle,
  highLightTextStyle,
}) {
  List<TextSpan> children = [];
  List<String> abc = highLightText.toLowerCase().split(" ");
  List<String> myList = mainString.split(" ");
  for (int i = 0; i < myList.length; i++) {
    if (abc.contains(myList[i].toLowerCase())) {
      children.add(
        TextSpan(
          text: "${myList[i]} ",
          style: highLightTextStyle ??
              boldFontsStyle(
                fontSize: veryLargeFont,
                color: darkYellow,
                fontFamily: platForm,
                height: 1.1,
              ),
        ),
      );
    } else {
      children.add(
        TextSpan(
          text: "${myList[i]} ",
          style: textStyle ??
              boldFontsStyle(
                fontSize: veryLargeFont,
                fontFamily: platForm,
                height: 1.1,
              ),
        ),
      );
    }
  }
  return children;
}

getImageInfo(Image img) async {
  final c = new Completer<ImageInfo>();
  img.image
      .resolve(new ImageConfiguration())
      .addListener(new ImageStreamListener((ImageInfo i, bool _) {
    c.complete(i);
  }));
  return c.future;
}

Future<String> downloadFile({String? filePath, String? url}) async {
  Dio dio = new Dio();
  await dio.download(
    url ?? "",
    filePath,
    onReceiveProgress: (count, total) {
      debugPrint('---Download----Rec: $count, Total: $total');
    },
  );
  return filePath ?? "";
}

openDocumentInApp(apiBloc, String documentMedia) async {
  await Permission.storage.request();
  apiBloc.add(DownloadLoaderEvent(isLoad: true));

  var dir;
  if (Platform.isAndroid) {
    dir = await getExternalStorageDirectory();
  } else if (Platform.isIOS) {
    dir = await getApplicationDocumentsDirectory();
  }

  String fileName = documentMedia.substring(documentMedia.lastIndexOf("/") + 1);
  // final name = await OpenDocument.getNameFile(url: documentMedia);
  // final path = await OpenDocument.getPathDocument(folderName: "pma_clint");
  // final isCheck = await OpenDocument.checkDocument(filePath: filePath);
  String filePath = "${dir.path}/pma_clint/$fileName";
  if (!await dir.exists()) {
    await dir.create(recursive: true);
  }
  final isCheck = await File(filePath).exists();
  try {
    if (!isCheck) {
      filePath = await downloadFile(
        filePath: "$filePath",
        url: documentMedia,
      );
    }
    apiBloc.add(DownloadLoaderEvent(isLoad: false));
    // await OpenDocument.openDocument(filePath: filePath);
    await OpenFile.open(filePath);
  } on PlatformException catch (e) {
    debugPrint("ERROR: message_${e.message} ---- detail_${e.details}");
    apiBloc.add(DownloadLoaderEvent(isLoad: false));
  } catch (e) {
    if (documentMedia.isNotEmpty) {
      DioError error = e as DioError;
      debugPrint(e.toString());
      showToast(
        error.response?.statusCode == 404 ? "File Not Found" : error.message,
      );
    } else {
      showToast(S().document_not_found);
    }
    apiBloc.add(DownloadLoaderEvent(isLoad: false));
  }
}
