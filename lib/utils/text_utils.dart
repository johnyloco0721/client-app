import 'package:flutter/material.dart';
import 'package:pmaclient/constants/app_colors.dart';
import 'package:pmaclient/constants/app_dimens.dart';
import 'package:pmaclient/constants/app_fonts.dart';

TextStyle mediumFontsStyle(
    {fontSize = defaultFont, color = white, height = 1.0}) {
  return TextStyle(
    fontWeight: FontWeight.w500,
    fontSize: fontSize,
    color: color,
    height: height,
    fontFamily: dmSans,
  );
}

TextStyle regularFontsStyle({
  fontSize = defaultFont,
  color = black,
  height = 1.0,
  letterSpacing,
}) {
  return TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: fontSize,
    color: color,
    height: height,
    letterSpacing: letterSpacing,
    fontFamily: dmSans,
  );
}

TextStyle italicFontsStyle({
  fontSize = defaultFont,
  color = black,
  height = 1.0,
  letterSpacing,
}) {
  return TextStyle(
    fontSize: fontSize,
    fontStyle: FontStyle.italic,
    color: color,
    height: height,
    letterSpacing: letterSpacing,
    fontFamily: dmSans,
    fontWeight: FontWeight.w400,
  );
}

TextStyle boldFontsStyle({
  fontSize = defaultFont,
  color = black,
  height = 1.0,
  fontFamily = dmSans,
  fontWeight: FontWeight.w700,
  letterSpacing = 0.0,
}) {
  return TextStyle(
    fontWeight: fontWeight,
    fontSize: fontSize,
    color: color,
    height: height,
    fontFamily: fontFamily,
    letterSpacing: letterSpacing,
  );
}
