import 'package:flutter/material.dart';

goAndClearAllPageNamed(BuildContext context, pageName, {args}) {
  Navigator.pushNamedAndRemoveUntil(
      context, pageName, (Route<dynamic> route) => false,
      arguments: args);
}

goAndClearCurrentPageNamed(BuildContext context, pageName, {args}) {
  Navigator.pushReplacementNamed(context, pageName, arguments: args);
}

goToNextNamedRoute(BuildContext context, pageName, {args, VoidCallback? callback}) {
  Navigator.pushNamed(context, pageName, arguments: args).then((value) => (callback != null) ? callback() : null );
}

goAndClearCurrentScreen(BuildContext context, Widget page) {
  Navigator.of(context, rootNavigator: false).pushReplacement(
    MaterialPageRoute(builder: (_) => page),
  );
}

goAndClearAllScreen(BuildContext context, Widget page, {String name = '/'}) {
  Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (BuildContext context) => page),
      ModalRoute.withName(name));
}

goToNormalPage(BuildContext context, Widget page) {
  Navigator.of(context, rootNavigator: false).push(
    MaterialPageRoute<void>(builder: (_) => page),
  );
}

Future<Widget> buildPageAsync(page) async {
  return Future.microtask(() {
    return page;
  });
}
