import 'package:pmaclient/generated/l10n.dart';

emptyValidation(String value, String error) {
  if (value.isEmpty) {
    return error;
  }
}

loginErrorValidation(error) {
  return error;
}

passwordValidation(String value, {isReset = false}) {
  String pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{5,20}$';
  RegExp regex = new RegExp(pattern);
  if (value.isEmpty) {
    return S().password_should_not_empty;
  } else if (!regex.hasMatch(value) && isReset) {
    return S().password_should_be_at_least_5_character;
  }
}

confirmPasswordValidation(String value, String password, context) {
  String pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{5,}$';
  new RegExp(pattern);
  if (value != password) {
    return S.of(context).password_did_not_match;
  }
}
