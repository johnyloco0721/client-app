import 'dart:convert';

import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:pmaclient/constants/app_constants.dart';
import 'package:pmaclient/models/config_model.dart';
import 'package:pmaclient/models/user_model.dart';
import 'package:pmaclient/utils/shared_preference_utils/pref_keys.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SpManager {
  static SpManager? _storageUtil;
  static SharedPreferences? _preferences;

  static Future<SpManager?> getInstance() async {
    _preferences = await SharedPreferences.getInstance();
    if (_storageUtil == null) {
      var secureStorage = SpManager._();
      await secureStorage._init();
      _storageUtil = secureStorage;
    }
    return _storageUtil;
  }

  SpManager._();

  Future _init() async {
    _preferences = await SharedPreferences.getInstance();
  }

  // get string
  static String? getString(String key, {String? defValue}) {
    if (_preferences == null) return defValue;
    return _preferences!.getString(key) ?? defValue;
  }

  // put string
  static Future<bool>? putString(String key, String value) {
    if (_preferences == null) return null;
    return _preferences!.setString(key, value);
  }

  static int getInt(String key, {int defValue = 0}) {
    if (_preferences == null) return defValue;
    return _preferences!.getInt(key) ?? defValue;
  }

  // put string
  static Future<bool>? putInt(String key, int value) {
    if (_preferences == null) return null;
    return _preferences!.setInt(key, value);
  }

  static setLogin(bool value, {bool defValue = false}) {
    if (_preferences == null) return null;
    return _preferences?.setBool(loginKey, value) ?? defValue;
  }

  static isLogin() {
    if (_preferences == null) return null;
    return _preferences?.getBool(loginKey) ?? false;
  }

  static setIsFirstTime(bool value, {bool defValue = false}) {
    if (_preferences == null) return null;
    return _preferences?.setBool(isFirstTime, value) ?? defValue;
  }

  static getIsFirstTime() {
    if (_preferences == null) return null;
    return _preferences?.getBool(isFirstTime) ?? false;
  }

  static saveUser(Map<String, dynamic> user) {
    String mapToStr = json.encode(user);
    putString(userKey, mapToStr);
  }

  static getUserdata() {
    UserModel? user;
    String? stringValue = _preferences!.getString(userKey);
    if (stringValue != null) {
      Map<String, dynamic> userMap = jsonDecode(stringValue);
      user = UserModel.fromJson(userMap);
    }
    return user;
  }

  static saveConfigModel(Map<String, dynamic> map) {
    String mapToStr = json.encode(map);
    putString(configModel, mapToStr);
  }

  static getConfigModel() {
    ConfigModel? model;
    String? stringValue = _preferences!.getString(configModel);
    if (stringValue != null) {
      Map<String, dynamic> map = jsonDecode(stringValue);
      model = ConfigModel.fromJson(map);
    }
    return model;
  }

  static logoutDeleteData() {
    DioCacheManager(CacheConfig(baseUrl: baseUrl)).clearAll();
    removeItem(userKey);
    removeItem(loginKey);
    removeItem(apiToken);
  }

  static Future removeItem(String key) async {
    _preferences!.remove(key);
  }
}
