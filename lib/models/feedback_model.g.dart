// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feedback_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FeedbackModel _$FeedbackModelFromJson(Map<String, dynamic> json) =>
    FeedbackModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] == null
          ? null
          : FeedBackResponseData.fromJson(
              json['response'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$FeedbackModelToJson(FeedbackModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };

FeedBackResponseData _$FeedBackResponseDataFromJson(
        Map<String, dynamic> json) =>
    FeedBackResponseData(
      reviewID: json['reviewID'] as int?,
      phoneNumber: json['phoneNumber'] as String?,
      successMessage: json['success_message'] as String?,
    );

Map<String, dynamic> _$FeedBackResponseDataToJson(
        FeedBackResponseData instance) =>
    <String, dynamic>{
      'reviewID': instance.reviewID,
      'phoneNumber': instance.phoneNumber,
      'success_message': instance.successMessage,
    };
