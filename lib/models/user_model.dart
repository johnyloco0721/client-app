import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class UserModel {
  int? privilegeID;
  String? username;
  String? firstName;
  String? lastName;
  String? contactNo;
  String? email;
  String? avatar;
  String? loginHash;
  int? profileID;
  bool? isFirstTimeLogin;

  UserModel(
      {this.privilegeID,
      this.username,
      this.firstName,
      this.lastName,
      this.contactNo,
      this.email,
      this.avatar,
      this.loginHash,
      this.profileID,
      this.isFirstTimeLogin});

  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
