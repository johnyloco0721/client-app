class QuestionModel {
  String? question;
  String? feedback;

  QuestionModel({this.feedback, this.question});
}
