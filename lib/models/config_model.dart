import 'package:json_annotation/json_annotation.dart';

part 'config_model.g.dart';

@JsonSerializable()
class ConfigModel {
  bool? status;
  String? message;
  ConfigResponse? response;

  ConfigModel({this.status, this.message, this.response});

  factory ConfigModel.fromJson(Map<String, dynamic> json) =>
      _$ConfigModelFromJson(json);

  Map<String, dynamic> toJson() => _$ConfigModelToJson(this);
}

@JsonSerializable()
class ConfigResponse {
  String? mainSiteURL;
  String? pMASiteURL;
  String? mobileSiteURL;
  String? mobileVersion;
  bool? underMaintenance;
  String? appSupportEmail;
  String? supportEmailSubject;
  String? term;
  String? homeTitle;
  String? homeHashtag;
  Support? support;
  String? AndroidAppUpdateLink;
  String? iOSAppUpdateLink;
  String? HuaweiAppUpdateLink;

  ConfigResponse({
    this.mainSiteURL,
    this.pMASiteURL,
    this.mobileSiteURL,
    this.mobileVersion,
    this.underMaintenance,
    this.appSupportEmail,
    this.term,
    this.support,
    this.homeTitle,
    this.homeHashtag,
    this.supportEmailSubject,
    this.AndroidAppUpdateLink,
    this.iOSAppUpdateLink,
    this.HuaweiAppUpdateLink
  });

  factory ConfigResponse.fromJson(Map<String, dynamic> json) =>
      _$ConfigResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ConfigResponseToJson(this);
}

@JsonSerializable()
class Support {
  Topic? topic;

  Support({this.topic});

  factory Support.fromJson(Map<String, dynamic> json) =>
      _$SupportFromJson(json);

  Map<String, dynamic> toJson() => _$SupportToJson(this);
}

@JsonSerializable()
class Topic {
  String? help;
  String? feedback;
  String? others;

  Topic({this.help, this.feedback, this.others});

  factory Topic.fromJson(Map<String, dynamic> json) => _$TopicFromJson(json);

  Map<String, dynamic> toJson() => _$TopicToJson(this);
}
