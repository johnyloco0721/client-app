import 'package:json_annotation/json_annotation.dart';

part 'projects_model.g.dart';

@JsonSerializable()
class ProjectModel {
  int? totalItems;
  int? currentPageNo;
  int? limitPerPage;
  List<HomeProject>? projects;
  NpsRating? npsRating;

  ProjectModel(
      {this.totalItems,
      this.currentPageNo,
      this.limitPerPage,
      this.projects,
      this.npsRating});

  factory ProjectModel.fromJson(Map<String, dynamic> json) =>
      _$ProjectModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectModelToJson(this);
}

@JsonSerializable()
class NpsRating {
  int? projectID;
  String? npsTitle;
  String? npsDescription;

  NpsRating({
    this.projectID,
    this.npsDescription,
    this.npsTitle,
  });

  factory NpsRating.fromJson(Map<String, dynamic> json) =>
      _$NpsRatingFromJson(json);

  Map<String, dynamic> toJson() => _$NpsRatingToJson(this);
}

@JsonSerializable()
class HomeProject {
  int? projectID;
  String? projectArea;
  String? projectUnit;
  int? projectSize;
  String? projectStatus;
  String? propertyName;
  String? propertyState;
  String? propertyType;
  String? coverImage;
  bool? isHandoverTaskCompleted;
  String? statusLabel;
  String? statusDotColor;
  String? statusDotBorderColor;
  int? isPromotion;
  int? promotionID;
  String? promotionUrl;
  int? hasUpdate;

  HomeProject({
    this.projectID,
    this.projectArea,
    this.projectUnit,
    this.projectSize,
    this.projectStatus,
    this.propertyName,
    this.propertyState,
    this.propertyType,
    this.coverImage,
    this.statusLabel,
    this.isHandoverTaskCompleted,
    this.statusDotColor,
    this.statusDotBorderColor,
    this.isPromotion,
    this.promotionID,
    this.promotionUrl,
    this.hasUpdate,
  });

  factory HomeProject.fromJson(Map<String, dynamic> json) =>
      _$HomeProjectFromJson(json);

  Map<String, dynamic> toJson() => _$HomeProjectToJson(this);
}
