class WelcomeModel {
  String? lottiePath;
  String? title;
  String? description;

  WelcomeModel({this.description, this.lottiePath, this.title});
}
