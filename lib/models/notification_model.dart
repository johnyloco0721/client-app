import 'package:pmaclient/models/notification_api_model.dart';

class NotificationModel {
  String? headerTitle;
  List<NotificationDataList>? notificationList;

  NotificationModel({this.headerTitle, this.notificationList});
}
