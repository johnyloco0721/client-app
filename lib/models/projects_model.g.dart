// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'projects_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProjectModel _$ProjectModelFromJson(Map<String, dynamic> json) => ProjectModel(
      totalItems: json['totalItems'] as int?,
      currentPageNo: json['currentPageNo'] as int?,
      limitPerPage: json['limitPerPage'] as int?,
      projects: (json['projects'] as List<dynamic>?)
          ?.map((e) => HomeProject.fromJson(e as Map<String, dynamic>))
          .toList(),
      npsRating: json['npsRating'] == null
          ? null
          : NpsRating.fromJson(json['npsRating'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ProjectModelToJson(ProjectModel instance) =>
    <String, dynamic>{
      'totalItems': instance.totalItems,
      'currentPageNo': instance.currentPageNo,
      'limitPerPage': instance.limitPerPage,
      'projects': instance.projects,
      'npsRating': instance.npsRating,
    };

NpsRating _$NpsRatingFromJson(Map<String, dynamic> json) => NpsRating(
      projectID: json['projectID'] as int?,
      npsDescription: json['npsDescription'] as String?,
      npsTitle: json['npsTitle'] as String?,
    );

Map<String, dynamic> _$NpsRatingToJson(NpsRating instance) => <String, dynamic>{
      'projectID': instance.projectID,
      'npsTitle': instance.npsTitle,
      'npsDescription': instance.npsDescription,
    };

HomeProject _$HomeProjectFromJson(Map<String, dynamic> json) => HomeProject(
      projectID: json['projectID'] as int?,
      projectArea: json['projectArea'] as String?,
      projectUnit: json['projectUnit'] as String?,
      projectSize: json['projectSize'] as int?,
      projectStatus: json['projectStatus'] as String?,
      propertyName: json['propertyName'] as String?,
      propertyState: json['propertyState'] as String?,
      propertyType: json['propertyType'] as String?,
      coverImage: json['coverImage'] as String?,
      statusLabel: json['statusLabel'] as String?,
      isHandoverTaskCompleted: json['isHandoverTaskCompleted'] as bool?,
      statusDotColor: json['statusDotColor'] as String?,
      statusDotBorderColor: json['statusDotBorderColor'] as String?,
      isPromotion: json['isPromotion'] as int?,
      promotionID: json['promotionID'] as int?,
      promotionUrl: json['promotionUrl'] as String?,
      hasUpdate: json['hasUpdate'] as int?,
    );

Map<String, dynamic> _$HomeProjectToJson(HomeProject instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'projectArea': instance.projectArea,
      'projectUnit': instance.projectUnit,
      'projectSize': instance.projectSize,
      'projectStatus': instance.projectStatus,
      'propertyName': instance.propertyName,
      'propertyState': instance.propertyState,
      'propertyType': instance.propertyType,
      'coverImage': instance.coverImage,
      'isHandoverTaskCompleted': instance.isHandoverTaskCompleted,
      'statusLabel': instance.statusLabel,
      'statusDotColor': instance.statusDotColor,
      'statusDotBorderColor': instance.statusDotBorderColor,
      'isPromotion': instance.isPromotion,
      'promotionID': instance.promotionID,
      'promotionUrl': instance.promotionUrl,
      'hasUpdate': instance.hasUpdate,
    };
