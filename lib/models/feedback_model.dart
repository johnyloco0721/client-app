import 'package:json_annotation/json_annotation.dart';

part 'feedback_model.g.dart';

@JsonSerializable(ignoreUnannotated: false)
class FeedbackModel {
  @JsonKey(name: 'status')
  bool? status;
  @JsonKey(name: 'message')
  String? message;
  @JsonKey(name: 'response')
  FeedBackResponseData? response;

  FeedbackModel({this.status, this.message, this.response});

  factory FeedbackModel.fromJson(Map<String, dynamic> json) =>
      _$FeedbackModelFromJson(json);

  Map<String, dynamic> toJson() => _$FeedbackModelToJson(this);
}

@JsonSerializable(ignoreUnannotated: false)
class FeedBackResponseData {
  @JsonKey(name: 'reviewID')
  int? reviewID;
  @JsonKey(name: 'phoneNumber')
  String? phoneNumber;
  @JsonKey(name: 'success_message')
  String? successMessage;

  FeedBackResponseData({this.reviewID, this.phoneNumber, this.successMessage});

  factory FeedBackResponseData.fromJson(Map<String, dynamic> json) =>
      _$FeedBackResponseDataFromJson(json);

  Map<String, dynamic> toJson() => _$FeedBackResponseDataToJson(this);
}
