class ProfileModel {
  bool? _status;
  String? _message;
  Response? _response;

  bool? get status => _status;

  String? get message => _message;

  Response? get response => _response;

  ProfileModel({bool? status, String? message, Response? response}) {
    _status = status;
    _message = message;
    _response = response;
  }

  ProfileModel.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    _response =
        json['response'] != null ? Response.fromJson(json['response']) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_response != null) {
      map['response'] = _response?.toJson();
    }
    return map;
  }
}

class Response {
  ProfileDetails? _profileDetails;

  ProfileDetails? get profileDetails => _profileDetails;

  Response({ProfileDetails? profileDetails}) {
    _profileDetails = profileDetails;
  }

  Response.fromJson(dynamic json) {
    _profileDetails = json['profileDetails'] != null
        ? ProfileDetails.fromJson(json['profileDetails'])
        : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_profileDetails != null) {
      map['profileDetails'] = _profileDetails?.toJson();
    }
    return map;
  }
}

class ProfileDetails {
  int? _privilegeID;
  String? _username;
  String? _firstName;
  String? _lastName;
  String? _contactNo;
  String? _email;
  String? _avatar;
  String? _hash;
  int? _profileID;
  bool? _isFirstTimeLogin;

  int? get privilegeID => _privilegeID;

  String? get username => _username;

  String? get firstName => _firstName;

  String? get lastName => _lastName;

  String? get contactNo => _contactNo;

  String? get email => _email;

  String? get avatar => _avatar;

  String? get hash => _hash;

  int? get profileID => _profileID;

  bool? get isFirstTimeLogin => _isFirstTimeLogin;

  ProfileDetails(
      {int? privilegeID,
      String? username,
      String? firstName,
      String? lastName,
      String? contactNo,
      String? email,
      String? avatar,
      String? hash,
      int? profileID,
      bool? isFirstTimeLogin}) {
    _privilegeID = privilegeID;
    _username = username;
    _firstName = firstName;
    _lastName = lastName;
    _contactNo = contactNo;
    _email = email;
    _avatar = avatar;
    _hash = hash;
    _profileID = profileID;
    _isFirstTimeLogin = isFirstTimeLogin;
  }

  ProfileDetails.fromJson(dynamic json) {
    _privilegeID = json['privilegeID'];
    _username = json['username'];
    _firstName = json['firstName'];
    _lastName = json['lastName'];
    _contactNo = json['contactNo'];
    _email = json['email'];
    _avatar = json['avatar'];
    _hash = json['hash'];
    _profileID = json['profileID'];
    _isFirstTimeLogin = json['isFirstTimeLogin'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['privilegeID'] = _privilegeID;
    map['username'] = _username;
    map['firstName'] = _firstName;
    map['lastName'] = _lastName;
    map['contactNo'] = _contactNo;
    map['email'] = _email;
    map['avatar'] = _avatar;
    map['hash'] = _hash;
    map['profileID'] = _profileID;
    map['isFirstTimeLogin'] = _isFirstTimeLogin;
    return map;
  }
}
