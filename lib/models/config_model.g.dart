// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConfigModel _$ConfigModelFromJson(Map<String, dynamic> json) => ConfigModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] == null
          ? null
          : ConfigResponse.fromJson(json['response'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ConfigModelToJson(ConfigModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };

ConfigResponse _$ConfigResponseFromJson(Map<String, dynamic> json) =>
    ConfigResponse(
      mainSiteURL: json['mainSiteURL'] as String?,
      pMASiteURL: json['pMASiteURL'] as String?,
      mobileSiteURL: json['mobileSiteURL'] as String?,
      mobileVersion: json['mobileVersion'] as String?,
      underMaintenance: json['underMaintenance'] as bool?,
      appSupportEmail: json['appSupportEmail'] as String?,
      term: json['term'] as String?,
      support: json['support'] == null
          ? null
          : Support.fromJson(json['support'] as Map<String, dynamic>),
      homeTitle: json['homeTitle'] as String?,
      homeHashtag: json['homeHashtag'] as String?,
      supportEmailSubject: json['supportEmailSubject'] as String?,
      AndroidAppUpdateLink: json['AndroidAppUpdateLink'] as String?,
      iOSAppUpdateLink: json['iOSAppUpdateLink'] as String?,
      HuaweiAppUpdateLink: json['HuaweiAppUpdateLink'] as String?,
    );

Map<String, dynamic> _$ConfigResponseToJson(ConfigResponse instance) =>
    <String, dynamic>{
      'mainSiteURL': instance.mainSiteURL,
      'pMASiteURL': instance.pMASiteURL,
      'mobileSiteURL': instance.mobileSiteURL,
      'mobileVersion': instance.mobileVersion,
      'underMaintenance': instance.underMaintenance,
      'appSupportEmail': instance.appSupportEmail,
      'supportEmailSubject': instance.supportEmailSubject,
      'term': instance.term,
      'homeTitle': instance.homeTitle,
      'homeHashtag': instance.homeHashtag,
      'support': instance.support,
      'AndroidAppUpdateLink': instance.AndroidAppUpdateLink,
      'iOSAppUpdateLink': instance.iOSAppUpdateLink,
      'HuaweiAppUpdateLink': instance.HuaweiAppUpdateLink,
    };

Support _$SupportFromJson(Map<String, dynamic> json) => Support(
      topic: json['topic'] == null
          ? null
          : Topic.fromJson(json['topic'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SupportToJson(Support instance) => <String, dynamic>{
      'topic': instance.topic,
    };

Topic _$TopicFromJson(Map<String, dynamic> json) => Topic(
      help: json['help'] as String?,
      feedback: json['feedback'] as String?,
      others: json['others'] as String?,
    );

Map<String, dynamic> _$TopicToJson(Topic instance) => <String, dynamic>{
      'help': instance.help,
      'feedback': instance.feedback,
      'others': instance.others,
    };
