import 'package:json_annotation/json_annotation.dart';

part 'rating_model.g.dart';

@JsonSerializable(ignoreUnannotated: false)
class RatingModel {
  @JsonKey(name: 'projectID')
  int? projectID;
  @JsonKey(name: 'reviewData')
  List<ReviewData>? reviewData;

  RatingModel({this.projectID, this.reviewData});

  factory RatingModel.fromJson(Map<String, dynamic> json) =>
      _$RatingModelFromJson(json);

  Map<String, dynamic> toJson() => _$RatingModelToJson(this);
}

@JsonSerializable(ignoreUnannotated: false)
class ReviewData {
  @JsonKey(name: 'questionID')
  int? questionID;
  @JsonKey(name: 'rate')
  int? rate;
  @JsonKey(name: 'feedback')
  String? feedback;
  @JsonKey(name: 'resolve')
  bool? resolve;

  ReviewData({this.questionID, this.rate, this.feedback, this.resolve});

  factory ReviewData.fromJson(Map<String, dynamic> json) =>
      _$ReviewDataFromJson(json);

  Map<String, dynamic> toJson() => _$ReviewDataToJson(this);
}
