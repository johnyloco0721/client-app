import 'package:json_annotation/json_annotation.dart';

part 'maintenance_model.g.dart';

@JsonSerializable()
class MaintenanceModel {
  String? parameter;
  String? hash;

  MaintenanceModel({
    this.parameter,
    this.hash,
  });

  factory MaintenanceModel.fromJson(Map<String, dynamic> json) =>
      _$MaintenanceModelFromJson(json);

  Map<String, dynamic> toJson() => _$MaintenanceModelToJson(this);
}