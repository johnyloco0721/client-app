import 'package:json_annotation/json_annotation.dart';

part 'otp_model.g.dart';

@JsonSerializable()
class OtpModel {
  int? otpID;
  int? ownerID;
  String? ownerEmailAddress;
  String? expiredDate;

  OtpModel({
    this.otpID,
    this.ownerID,
    this.ownerEmailAddress,
    this.expiredDate,
  });

  factory OtpModel.fromJson(Map<String, dynamic> json) =>
      _$OtpModelFromJson(json);

  Map<String, dynamic> toJson() => _$OtpModelToJson(this);
}

class EmailOtpModel {
  int? otpID;
  String? ownerID;
  String? ownerEmailAddress;
  String? expiredDate;

  EmailOtpModel({
    this.otpID,
    this.ownerID,
    this.ownerEmailAddress,
    this.expiredDate,
  });

  factory EmailOtpModel.fromJson(Map<String, dynamic> json) =>
      _$EmailOtpModelFromJson(json);

  Map<String, dynamic> toJson() => _$EmailOtpModelToJson(this);
}