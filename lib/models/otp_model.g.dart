// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'otp_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OtpModel _$OtpModelFromJson(Map<String, dynamic> json) => OtpModel(
      otpID: json['otpID'] as int?,
      ownerID: json['ownerID'] as int?,
      ownerEmailAddress: json['ownerEmailAddress'] as String?,
      expiredDate: json['expiredDate'] as String?,
    );

Map<String, dynamic> _$OtpModelToJson(OtpModel instance) => <String, dynamic>{
      'otpID': instance.otpID,
      'ownerID': instance.ownerID,
      'ownerEmailAddress': instance.ownerEmailAddress,
      'expiredDate': instance.expiredDate,
    };

EmailOtpModel _$EmailOtpModelFromJson(Map<String, dynamic> json) => EmailOtpModel(
      otpID: json['otpID'] as int?,
      ownerID: json['ownerID'] as String?,
      ownerEmailAddress: json['ownerEmailAddress'] as String?,
      expiredDate: json['expiredDate'] as String?,
    );

Map<String, dynamic> _$EmailOtpModelToJson(EmailOtpModel instance) => <String, dynamic>{
      'otpID': instance.otpID,
      'ownerID': instance.ownerID,
      'ownerEmailAddress': instance.ownerEmailAddress,
      'expiredDate': instance.expiredDate,
    };
