// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      privilegeID: json['privilegeID'] as int?,
      username: json['username'] as String?,
      firstName: json['firstName'] as String?,
      lastName: json['lastName'] as String?,
      contactNo: json['contactNo'] as String?,
      email: json['email'] as String?,
      avatar: json['avatar'] as String?,
      loginHash: json['loginHash'] as String?,
      profileID: json['profileID'] as int?,
      isFirstTimeLogin: json['isFirstTimeLogin'] as bool?,
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'privilegeID': instance.privilegeID,
      'username': instance.username,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'contactNo': instance.contactNo,
      'email': instance.email,
      'avatar': instance.avatar,
      'loginHash': instance.loginHash,
      'profileID': instance.profileID,
      'isFirstTimeLogin': instance.isFirstTimeLogin,
    };
