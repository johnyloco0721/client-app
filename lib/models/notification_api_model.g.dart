// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification_api_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationApiModel _$NotificationApiModelFromJson(
        Map<String, dynamic> json) =>
    NotificationApiModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: (json['response'] as List<dynamic>?)
          ?.map((e) => NotificationDataList.fromJson(e as Map<String, dynamic>))
          .toList(),
      totalBadge: json['totalBadge'] as int?,
      totalItems: json['totalItems'] as int?,
      currentPageNo: json['currentPageNo'] as int?,
      limitPerPage: json['limitPerPage'] as int?,
    );

Map<String, dynamic> _$NotificationApiModelToJson(
        NotificationApiModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
      'totalBadge': instance.totalBadge,
      'totalItems': instance.totalItems,
      'currentPageNo': instance.currentPageNo,
      'limitPerPage': instance.limitPerPage,
    };

NotificationDataList _$NotificationDataListFromJson(
        Map<String, dynamic> json) =>
    NotificationDataList(
      notificationID: json['notificationID'] as int?,
      projectID: json['projectID'] as int?,
      moduleName: json['moduleName'] as String?,
      description: json['description'] as String?,
      createdDateTime: json['createdDateTime'] as String?,
      byProfileID: json['ByProfileID'],
      assignedByTeamMemberName: json['assignedByTeamMemberName'] as String?,
      avatar: json['avatar'] as String?,
      isRead: json['isRead'] as int?,
      teamMemberAvatar: json['teamMemberAvatar'] as String?,
      projectStatus: json['projectStatus'] as String?,
      isHandoverTaskCompleted: json['isHandoverTaskCompleted'] as bool?,
    )..affectedModuleID = json['affectedModuleID'] as String?;

Map<String, dynamic> _$NotificationDataListToJson(
        NotificationDataList instance) =>
    <String, dynamic>{
      'notificationID': instance.notificationID,
      'projectID': instance.projectID,
      'moduleName': instance.moduleName,
      'affectedModuleID': instance.affectedModuleID,
      'description': instance.description,
      'createdDateTime': instance.createdDateTime,
      'ByProfileID': instance.byProfileID,
      'assignedByTeamMemberName': instance.assignedByTeamMemberName,
      'avatar': instance.avatar,
      'projectStatus': instance.projectStatus,
      'isHandoverTaskCompleted': instance.isHandoverTaskCompleted,
      'isRead': instance.isRead,
      'teamMemberAvatar': instance.teamMemberAvatar,
    };
