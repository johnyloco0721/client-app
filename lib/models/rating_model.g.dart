// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rating_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RatingModel _$RatingModelFromJson(Map<String, dynamic> json) => RatingModel(
      projectID: json['projectID'] as int?,
      reviewData: (json['reviewData'] as List<dynamic>?)
          ?.map((e) => ReviewData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$RatingModelToJson(RatingModel instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'reviewData': instance.reviewData,
    };

ReviewData _$ReviewDataFromJson(Map<String, dynamic> json) => ReviewData(
      questionID: json['questionID'] as int?,
      rate: json['rate'] as int?,
      feedback: json['feedback'] as String?,
      resolve: json['resolve'] as bool?,
    );

Map<String, dynamic> _$ReviewDataToJson(ReviewData instance) =>
    <String, dynamic>{
      'questionID': instance.questionID,
      'rate': instance.rate,
      'feedback': instance.feedback,
      'resolve': instance.resolve,
    };
