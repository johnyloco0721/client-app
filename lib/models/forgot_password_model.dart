import 'package:json_annotation/json_annotation.dart';

part 'forgot_password_model.g.dart';

@JsonSerializable(ignoreUnannotated: false)
class ForgotPasswordModel {
  @JsonKey(name: 'privilegeID')
  int? privilegeID;
  @JsonKey(name: 'username')
  String? username;
  @JsonKey(name: 'profileID')
  int? profileID;
  @JsonKey(name: 'email')
  String? email;
  @JsonKey(name: 'contactNo')
  String? contactNo;
  @JsonKey(name: 'token')
  String? token;

  ForgotPasswordModel(
      {this.privilegeID,
      this.username,
      this.profileID,
      this.email,
      this.contactNo,
      this.token});

  factory ForgotPasswordModel.fromJson(Map<String, dynamic> json) =>
      _$ForgotPasswordModelFromJson(json);

  Map<String, dynamic> toJson() => _$ForgotPasswordModelToJson(this);
}
