// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'forgot_password_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ForgotPasswordModel _$ForgotPasswordModelFromJson(Map<String, dynamic> json) =>
    ForgotPasswordModel(
      privilegeID: json['privilegeID'] as int?,
      username: json['username'] as String?,
      profileID: json['profileID'] as int?,
      email: json['email'] as String?,
      contactNo: json['contactNo'] as String?,
      token: json['token'] as String?,
    );

Map<String, dynamic> _$ForgotPasswordModelToJson(
        ForgotPasswordModel instance) =>
    <String, dynamic>{
      'privilegeID': instance.privilegeID,
      'username': instance.username,
      'profileID': instance.profileID,
      'email': instance.email,
      'contactNo': instance.contactNo,
      'token': instance.token,
    };
