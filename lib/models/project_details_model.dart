import 'package:json_annotation/json_annotation.dart';

part 'project_details_model.g.dart';

@JsonSerializable()
class ProjectDetailsModel {
  bool? status;
  String? message;
  ProjectDetailsData? response;

  ProjectDetailsModel({this.status, this.message, this.response});

  factory ProjectDetailsModel.fromJson(Map<String, dynamic> json) =>
      _$ProjectDetailsModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectDetailsModelToJson(this);
}

@JsonSerializable()
class ProjectDetailsData {
  int? projectID;
  String? projectArea;
  String? projectUnit;
  String? projectSize;
  int? projectBedroom;
  int? projectBathroom;
  String? marketSegment;
  String? startDateTime;
  String? targetDateTime;
  String? completionDate;
  String? warrantyStartDate;
  String? warrantyEndDate;
  String? mocStartDate;
  String? mocEndDate;
  String? projectStatus;
  String? projectCategory;
  String? themeName;
  String? propertyName;
  String? propertyState;
  String? propertyType;
  String? coverImage;
  String? makeoverDurationLabel;
  String? makeoverDurationValue;
  String? projectHandoverDate;
  List<Document>? documents;
  List<Teams>? teams;
  List<Gallery>? gallery;
  List<Gallery>? allMediaListArr;
  bool? isScheduleTaskCompleted;
  String? horizontalProgressLabel;
  List<Progress>? horizontalProgress;
  List<Progress>? progress;
  List<Reminder>? reminder;
  List<Update>? updates;
  List<Support>? supports;
  List<Info>? infoList;
  String? dropboxLink;

  ProjectDetailsData({
    this.projectID,
    this.projectArea,
    this.projectUnit,
    this.projectSize,
    this.projectBedroom,
    this.projectBathroom,
    this.marketSegment,
    this.startDateTime,
    this.targetDateTime,
    this.completionDate,
    this.warrantyStartDate,
    this.warrantyEndDate,
    this.mocStartDate,
    this.mocEndDate,
    this.projectStatus,
    this.projectCategory,
    this.themeName,
    this.propertyName,
    this.propertyState,
    this.propertyType,
    this.coverImage,
    this.makeoverDurationLabel,
    this.makeoverDurationValue,
    this.projectHandoverDate,
    this.documents,
    this.gallery,
    this.allMediaListArr,
    this.isScheduleTaskCompleted,
    this.horizontalProgressLabel,
    this.horizontalProgress,
    this.progress,
    this.reminder,
    this.updates,
    this.supports,
    this.teams,
    this.dropboxLink,
  });

  factory ProjectDetailsData.fromJson(Map<String, dynamic> json) =>
      _$ProjectDetailsDataFromJson(json);

  Map<String, dynamic> toJson() => _$ProjectDetailsDataToJson(this);
}

@JsonSerializable()
class Document {
  int? mediaID;
  String? media;
  String? mediaType;
  String? fileType;
  String? fileExtension;
  String? url;
  String? mediaName;
  String? createdDateTime;
  bool? isLoad;

  Document({
    this.mediaID,
    this.media,
    this.mediaType,
    this.fileType,
    this.mediaName,
    this.createdDateTime,
    this.isLoad,
    this.url,
    this.fileExtension,
  });

  factory Document.fromJson(Map<String, dynamic> json) =>
      _$DocumentFromJson(json);

  Map<String, dynamic> toJson() => _$DocumentToJson(this);
}

@JsonSerializable()
class Gallery {
  int? mediaID;
  String? media;
  String? mediaWithWatermark;
  String? thumbnail;
  String? mediaName;
  String? mediaType;
  String? fileType;
  String? createdDateTime;

  Gallery({
    this.mediaID,
    this.media,
    this.mediaType,
    this.fileType,
    this.createdDateTime,
    this.mediaWithWatermark,
    this.thumbnail,
    this.mediaName,
  });

  factory Gallery.fromJson(Map<String, dynamic> json) =>
      _$GalleryFromJson(json);

  Map<String, dynamic> toJson() => _$GalleryToJson(this);
}

@JsonSerializable()
class Progress {
  int? milestoneID;
  String? name;
  String? startDateTime;
  String? endDateTime;
  String? status;
  String? weekLabel;

  Progress({
    this.milestoneID,
    this.name,
    this.startDateTime,
    this.endDateTime,
    this.status,
    this.weekLabel,
  });

  factory Progress.fromJson(Map<String, dynamic> json) =>
      _$ProgressFromJson(json);

  Map<String, dynamic> toJson() => _$ProgressToJson(this);
}

@JsonSerializable()
class Reminder {
  bool? show;
  String? title;
  String? description;

  Reminder({this.show, this.title, this.description});

  factory Reminder.fromJson(Map<String, dynamic> json) =>
      _$ReminderFromJson(json);

  Map<String, dynamic> toJson() => _$ReminderToJson(this);
}

@JsonSerializable()
class Update {
  int? milestoneUpdateID;
  int? hasUpdate;
  String? description;
  String? createdDateTime;
  String? updatedDateTime;
  String? dropboxLink;
  List<MediaList>? mediaList;
  List<Document>? documentList;

  Update({
    this.milestoneUpdateID,
    this.description,
    this.createdDateTime,
    this.updatedDateTime,
    this.mediaList,
    this.hasUpdate,
    this.documentList,
  });

  factory Update.fromJson(Map<String, dynamic> json) => _$UpdateFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateToJson(this);
}

@JsonSerializable()
class Support {
  int? teamMemberID;
  String? teamMemberName;
  String? companyName;
  String? designationName;
  String? whatsappLink;
  String? teamMemberAvatar;

  Support(
      {this.teamMemberID,
      this.teamMemberName,
      this.companyName,
      this.designationName,
      this.whatsappLink,
      this.teamMemberAvatar});

  factory Support.fromJson(Map<String, dynamic> json) =>
      _$SupportFromJson(json);

  Map<String, dynamic> toJson() => _$SupportToJson(this);
}

@JsonSerializable()
class Info {
  String? infoLabel;
  String? infoDisplayValue;

  Info({this.infoLabel, this.infoDisplayValue});

  factory Info.fromJson(Map<String, dynamic> json) => _$InfoFromJson(json);

  Map<String, dynamic> toJson() => _$InfoToJson(this);
}

@JsonSerializable()
class MediaList {
  int? mediaID;
  String? url;
  String? fileType;
  String? fileExtension;
  String? mediaWithWatermark;
  String? thumbnail;

  MediaList({
    this.fileExtension,
    this.mediaID,
    this.url,
    this.fileType,
    this.mediaWithWatermark,
    this.thumbnail,
  });

  factory MediaList.fromJson(Map<String, dynamic> json) =>
      _$MediaListFromJson(json);

  Map<String, dynamic> toJson() => _$MediaListToJson(this);
}

@JsonSerializable()
class Teams {
  int? teamMemberID;
  String? teamMemberName;
  String? memberDetails;
  String? designationName;
  String? teamMemberAvatar;
  String? biography;

  Teams({
    this.teamMemberID,
    this.teamMemberName,
    this.memberDetails,
    this.designationName,
    this.teamMemberAvatar,
    this.biography,
  });

  factory Teams.fromJson(Map<String, dynamic> json) => _$TeamsFromJson(json);

  Map<String, dynamic> toJson() => _$TeamsToJson(this);
}
