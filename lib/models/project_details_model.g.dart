// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'project_details_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProjectDetailsModel _$ProjectDetailsModelFromJson(Map<String, dynamic> json) =>
    ProjectDetailsModel(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      response: json['response'] == null
          ? null
          : ProjectDetailsData.fromJson(
              json['response'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ProjectDetailsModelToJson(
        ProjectDetailsModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };

ProjectDetailsData _$ProjectDetailsDataFromJson(Map<String, dynamic> json) =>
    ProjectDetailsData(
      projectID: json['projectID'] as int?,
      projectArea: json['projectArea'] as String?,
      projectUnit: json['projectUnit'] as String?,
      projectSize: json['projectSize'] as String?,
      projectBedroom: json['projectBedroom'] as int?,
      projectBathroom: json['projectBathroom'] as int?,
      marketSegment: json['marketSegment'] as String?,
      startDateTime: json['startDateTime'] as String?,
      targetDateTime: json['targetDateTime'] as String?,
      completionDate: json['completionDate'] as String?,
      warrantyStartDate: json['warrantyStartDate'] as String?,
      warrantyEndDate: json['warrantyEndDate'] as String?,
      mocStartDate: json['mocStartDate'] as String?,
      mocEndDate: json['mocEndDate'] as String?,
      projectStatus: json['projectStatus'] as String?,
      projectCategory: json['projectCategory'] as String?,
      themeName: json['themeName'] as String?,
      propertyName: json['propertyName'] as String?,
      propertyState: json['propertyState'] as String?,
      propertyType: json['propertyType'] as String?,
      coverImage: json['coverImage'] as String?,
      makeoverDurationLabel: json['makeoverDurationLabel'] as String?,
      makeoverDurationValue: json['makeoverDurationValue'] as String?,
      projectHandoverDate: json['projectHandoverDate'] as String?,
      documents: (json['documents'] as List<dynamic>?)
          ?.map((e) => Document.fromJson(e as Map<String, dynamic>))
          .toList(),
      gallery: (json['gallery'] as List<dynamic>?)
          ?.map((e) => Gallery.fromJson(e as Map<String, dynamic>))
          .toList(),
      allMediaListArr: (json['allMediaListArr'] as List<dynamic>?)
          ?.map((e) => Gallery.fromJson(e as Map<String, dynamic>))
          .toList(),
      isScheduleTaskCompleted: json['isScheduleTaskCompleted'] as bool?,
      horizontalProgressLabel: json['horizontalProgressLabel'] as String?,
      horizontalProgress: (json['horizontalProgress'] as List<dynamic>?)
          ?.map((e) => Progress.fromJson(e as Map<String, dynamic>))
          .toList(),
      progress: (json['progress'] as List<dynamic>?)
          ?.map((e) => Progress.fromJson(e as Map<String, dynamic>))
          .toList(),
      reminder: (json['reminder'] as List<dynamic>?)
          ?.map((e) => Reminder.fromJson(e as Map<String, dynamic>))
          .toList(),
      updates: (json['updates'] as List<dynamic>?)
          ?.map((e) => Update.fromJson(e as Map<String, dynamic>))
          .toList(),
      supports: (json['supports'] as List<dynamic>?)
          ?.map((e) => Support.fromJson(e as Map<String, dynamic>))
          .toList(),
      teams: (json['teams'] as List<dynamic>?)
          ?.map((e) => Teams.fromJson(e as Map<String, dynamic>))
          .toList(),
      dropboxLink: json['dropboxLink'] as String?,
    )..infoList = (json['infoList'] as List<dynamic>?)
        ?.map((e) => Info.fromJson(e as Map<String, dynamic>))
        .toList();

Map<String, dynamic> _$ProjectDetailsDataToJson(ProjectDetailsData instance) =>
    <String, dynamic>{
      'projectID': instance.projectID,
      'projectArea': instance.projectArea,
      'projectUnit': instance.projectUnit,
      'projectSize': instance.projectSize,
      'projectBedroom': instance.projectBedroom,
      'projectBathroom': instance.projectBathroom,
      'marketSegment': instance.marketSegment,
      'startDateTime': instance.startDateTime,
      'targetDateTime': instance.targetDateTime,
      'completionDate': instance.completionDate,
      'warrantyStartDate': instance.warrantyStartDate,
      'warrantyEndDate': instance.warrantyEndDate,
      'mocStartDate': instance.mocStartDate,
      'mocEndDate': instance.mocEndDate,
      'projectStatus': instance.projectStatus,
      'projectCategory': instance.projectCategory,
      'themeName': instance.themeName,
      'propertyName': instance.propertyName,
      'propertyState': instance.propertyState,
      'propertyType': instance.propertyType,
      'coverImage': instance.coverImage,
      'makeoverDurationLabel': instance.makeoverDurationLabel,
      'makeoverDurationValue': instance.makeoverDurationValue,
      'projectHandoverDate': instance.projectHandoverDate,
      'documents': instance.documents,
      'teams': instance.teams,
      'gallery': instance.gallery,
      'allMediaListArr': instance.allMediaListArr,
      'isScheduleTaskCompleted': instance.isScheduleTaskCompleted,
      'horizontalProgressLabel': instance.horizontalProgressLabel,
      'horizontalProgress': instance.horizontalProgress,
      'progress': instance.progress,
      'reminder': instance.reminder,
      'updates': instance.updates,
      'supports': instance.supports,
      'infoList': instance.infoList,
      'dropboxLink': instance.dropboxLink,
    };

Document _$DocumentFromJson(Map<String, dynamic> json) => Document(
      mediaID: json['mediaID'] as int?,
      media: json['media'] as String?,
      mediaType: json['mediaType'] as String?,
      fileType: json['fileType'] as String?,
      mediaName: json['mediaName'] as String?,
      createdDateTime: json['createdDateTime'] as String?,
      isLoad: json['isLoad'] as bool?,
      url: json['url'] as String?,
      fileExtension: json['fileExtension'] as String?,
    );

Map<String, dynamic> _$DocumentToJson(Document instance) => <String, dynamic>{
      'mediaID': instance.mediaID,
      'media': instance.media,
      'mediaType': instance.mediaType,
      'fileType': instance.fileType,
      'fileExtension': instance.fileExtension,
      'url': instance.url,
      'mediaName': instance.mediaName,
      'createdDateTime': instance.createdDateTime,
      'isLoad': instance.isLoad,
    };

Gallery _$GalleryFromJson(Map<String, dynamic> json) => Gallery(
      mediaID: json['mediaID'] as int?,
      media: json['media'] as String?,
      mediaType: json['mediaType'] as String?,
      fileType: json['fileType'] as String?,
      createdDateTime: json['createdDateTime'] as String?,
      mediaWithWatermark: json['mediaWithWatermark'] as String?,
      thumbnail: json['thumbnail'] as String?,
      mediaName: json['mediaName'] as String?,
    );

Map<String, dynamic> _$GalleryToJson(Gallery instance) => <String, dynamic>{
      'mediaID': instance.mediaID,
      'media': instance.media,
      'mediaWithWatermark': instance.mediaWithWatermark,
      'thumbnail': instance.thumbnail,
      'mediaName': instance.mediaName,
      'mediaType': instance.mediaType,
      'fileType': instance.fileType,
      'createdDateTime': instance.createdDateTime,
    };

Progress _$ProgressFromJson(Map<String, dynamic> json) => Progress(
      milestoneID: json['milestoneID'] as int?,
      name: json['name'] as String?,
      startDateTime: json['startDateTime'] as String?,
      endDateTime: json['endDateTime'] as String?,
      status: json['status'] as String?,
      weekLabel: json['weekLabel'] as String?,
    );

Map<String, dynamic> _$ProgressToJson(Progress instance) => <String, dynamic>{
      'milestoneID': instance.milestoneID,
      'name': instance.name,
      'startDateTime': instance.startDateTime,
      'endDateTime': instance.endDateTime,
      'status': instance.status,
      'weekLabel': instance.weekLabel,
    };

Reminder _$ReminderFromJson(Map<String, dynamic> json) => Reminder(
      show: json['show'] as bool?,
      title: json['title'] as String?,
      description: json['description'] as String?,
    );

Map<String, dynamic> _$ReminderToJson(Reminder instance) => <String, dynamic>{
      'show': instance.show,
      'title': instance.title,
      'description': instance.description,
    };

Update _$UpdateFromJson(Map<String, dynamic> json) => Update(
      milestoneUpdateID: json['milestoneUpdateID'] as int?,
      description: json['description'] as String?,
      createdDateTime: json['createdDateTime'] as String?,
      updatedDateTime: json['updatedDateTime'] as String?,
      mediaList: (json['mediaList'] as List<dynamic>?)
          ?.map((e) => MediaList.fromJson(e as Map<String, dynamic>))
          .toList(),
      hasUpdate: json['hasUpdate'] as int?,
      documentList: (json['documentList'] as List<dynamic>?)
          ?.map((e) => Document.fromJson(e as Map<String, dynamic>))
          .toList(),
    )..dropboxLink = json['dropboxLink'] as String?;

Map<String, dynamic> _$UpdateToJson(Update instance) => <String, dynamic>{
      'milestoneUpdateID': instance.milestoneUpdateID,
      'hasUpdate': instance.hasUpdate,
      'description': instance.description,
      'createdDateTime': instance.createdDateTime,
      'updatedDateTime': instance.updatedDateTime,
      'dropboxLink': instance.dropboxLink,
      'mediaList': instance.mediaList,
      'documentList': instance.documentList,
    };

Support _$SupportFromJson(Map<String, dynamic> json) => Support(
      teamMemberID: json['teamMemberID'] as int?,
      teamMemberName: json['teamMemberName'] as String?,
      companyName: json['companyName'] as String?,
      designationName: json['designationName'] as String?,
      whatsappLink: json['whatsappLink'] as String?,
      teamMemberAvatar: json['teamMemberAvatar'] as String?,
    );

Map<String, dynamic> _$SupportToJson(Support instance) => <String, dynamic>{
      'teamMemberID': instance.teamMemberID,
      'teamMemberName': instance.teamMemberName,
      'companyName': instance.companyName,
      'designationName': instance.designationName,
      'whatsappLink': instance.whatsappLink,
      'teamMemberAvatar': instance.teamMemberAvatar,
    };

Info _$InfoFromJson(Map<String, dynamic> json) => Info(
      infoLabel: json['infoLabel'] as String?,
      infoDisplayValue: json['infoDisplayValue'] as String?,
    );

Map<String, dynamic> _$InfoToJson(Info instance) => <String, dynamic>{
      'infoLabel': instance.infoLabel,
      'infoDisplayValue': instance.infoDisplayValue,
    };

MediaList _$MediaListFromJson(Map<String, dynamic> json) => MediaList(
      fileExtension: json['fileExtension'] as String?,
      mediaID: json['mediaID'] as int?,
      url: json['url'] as String?,
      fileType: json['fileType'] as String?,
      mediaWithWatermark: json['mediaWithWatermark'] as String?,
      thumbnail: json['thumbnail'] as String?,
    );

Map<String, dynamic> _$MediaListToJson(MediaList instance) => <String, dynamic>{
      'mediaID': instance.mediaID,
      'url': instance.url,
      'fileType': instance.fileType,
      'fileExtension': instance.fileExtension,
      'mediaWithWatermark': instance.mediaWithWatermark,
      'thumbnail': instance.thumbnail,
    };

Teams _$TeamsFromJson(Map<String, dynamic> json) => Teams(
      teamMemberID: json['teamMemberID'] as int?,
      teamMemberName: json['teamMemberName'] as String?,
      memberDetails: json['memberDetails'] as String?,
      designationName: json['designationName'] as String?,
      teamMemberAvatar: json['teamMemberAvatar'] as String?,
      biography: json['biography'] as String?,
    );

Map<String, dynamic> _$TeamsToJson(Teams instance) => <String, dynamic>{
      'teamMemberID': instance.teamMemberID,
      'teamMemberName': instance.teamMemberName,
      'memberDetails': instance.memberDetails,
      'designationName': instance.designationName,
      'teamMemberAvatar': instance.teamMemberAvatar,
      'biography': instance.biography,
    };
