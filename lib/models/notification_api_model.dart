import 'package:json_annotation/json_annotation.dart';

part 'notification_api_model.g.dart';

@JsonSerializable(ignoreUnannotated: false)
class NotificationApiModel {
  @JsonKey(name: 'status')
  bool? status;
  @JsonKey(name: 'message')
  String? message;
  @JsonKey(name: 'response')
  List<NotificationDataList>? response;
  @JsonKey(name: 'totalBadge')
  int? totalBadge;
  @JsonKey(name: 'totalItems')
  int? totalItems;
  @JsonKey(name: 'currentPageNo')
  int? currentPageNo;
  @JsonKey(name: 'limitPerPage')
  int? limitPerPage;

  NotificationApiModel(
      {this.status,
      this.message,
      this.response,
      this.totalBadge,
      this.totalItems,
      this.currentPageNo,
      this.limitPerPage});

  factory NotificationApiModel.fromJson(Map<String, dynamic> json) =>
      _$NotificationApiModelFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationApiModelToJson(this);
}

@JsonSerializable(ignoreUnannotated: false)
class NotificationDataList {
  @JsonKey(name: 'notificationID')
  int? notificationID;
  @JsonKey(name: 'projectID')
  int? projectID;
  @JsonKey(name: 'moduleName')
  String? moduleName;
  @JsonKey(name: 'affectedModuleID')
  String? affectedModuleID;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'createdDateTime')
  String? createdDateTime;
  @JsonKey(name: 'ByProfileID')
  var byProfileID;
  @JsonKey(name: 'assignedByTeamMemberName')
  String? assignedByTeamMemberName;
  @JsonKey(name: 'avatar')
  String? avatar;
  @JsonKey(name: 'projectStatus')
  String? projectStatus;
  @JsonKey(name: 'isHandoverTaskCompleted')
  bool? isHandoverTaskCompleted;
  @JsonKey(name: 'isRead')
  int? isRead;
  @JsonKey(name: 'teamMemberAvatar')
  String? teamMemberAvatar;

  NotificationDataList({
    this.notificationID,
    this.projectID,
    this.moduleName,
    this.description,
    this.createdDateTime,
    this.byProfileID,
    this.assignedByTeamMemberName,
    this.avatar,
    this.isRead,
    this.teamMemberAvatar,
    this.projectStatus,
    this.isHandoverTaskCompleted,
  });

  factory NotificationDataList.fromJson(Map<String, dynamic> json) =>
      _$NotificationDataListFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationDataListToJson(this);
}
