import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pmaclient/shimmer_items/shimmer_items.dart';
import 'package:pmaclient/utils/size_utils.dart';

class NotificationShimmerItem extends StatelessWidget {
  const NotificationShimmerItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              shimmerLine(height: 15, width: 60),
              shimmerBorderButton(radius: 16, width: 120),
            ],
          ),
          SizedBox(height: 30),
          Expanded(
            child: ListView.builder(
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(bottom: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      shimmerLine(
                        height: 13,
                        width: SizeUtils.width(context, 80),
                      ),
                      SizedBox(height: 16),
                      shimmerLine(
                        height: 13,
                        width: SizeUtils.width(context, 70),
                      ),
                      SizedBox(height: 25),
                      shimmerLine(
                        height: 13,
                        width: SizeUtils.width(context, 35),
                      ),
                      SizedBox(height: 30),
                      shimmerLine(
                        height: 0.5,
                        width: SizeUtils.width(context, 100),
                        radius: 0,
                      ),
                    ],
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
