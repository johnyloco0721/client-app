import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pmaclient/shimmer_items/shimmer_items.dart';
import 'package:pmaclient/utils/size_utils.dart';

class HomeShimmerItem extends StatelessWidget {
  const HomeShimmerItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.centerRight,
            margin: EdgeInsets.only(bottom: 40),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                shimmerCircle(size: 25),
                SizedBox(width: 30),
                shimmerCircle(size: 25),
              ],
            ),
          ),
          shimmerLine(height: 25, width: SizeUtils.width(context, 70)),
          SizedBox(height: 15),
          shimmerLine(height: 25, width: SizeUtils.width(context, 60)),
          SizedBox(height: 15),
          shimmerLine(height: 25, width: SizeUtils.width(context, 45)),
          SizedBox(height: 30),
          shimmerLine(height: 13, width: SizeUtils.width(context, 100)),
          SizedBox(height: 12),
          shimmerLine(height: 13, width: SizeUtils.width(context, 55)),
          SizedBox(height: 35),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 8),
                          child: shimmerLine(
                            height: SizeUtils.height(context, 25),
                            width: SizeUtils.width(context, 50),
                            radius: 12.0,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 8),
                          child: shimmerLine(
                            height: SizeUtils.height(context, 25),
                            width: SizeUtils.width(context, 50),
                            radius: 12.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 8),
                          child: shimmerLine(
                            height: SizeUtils.height(context, 25),
                            width: SizeUtils.width(context, 50),
                            radius: 12.0,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(left: 8),
                          child: shimmerLine(
                            height: SizeUtils.height(context, 25),
                            width: SizeUtils.width(context, 50),
                            radius: 12.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
