import 'package:flutter/material.dart';
import 'package:pmaclient/shimmer_items/shimmer_items.dart';
import 'package:pmaclient/utils/size_utils.dart';

class ProfileShimmerItem extends StatelessWidget {
  const ProfileShimmerItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20),
      child: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        shimmerCircle(size: 120.0),
                        SizedBox(height: 15.0),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            shimmerLine(),
                            SizedBox(width: 20),
                            shimmerCircle(),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 35.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: shimmerLine(width: 80.0, height: 13.0),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: shimmerLine(width: 80.0, height: 13.0),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: shimmerLine(width: 107.0, height: 13.0),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          alignment: Alignment.centerLeft,
                          child: shimmerLine(width: 107.0, height: 13.0),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 25.0),
                  shimmerLine(width: 120.0, height: 13.0),
                  SizedBox(height: 20.0),
                  shimmerLine(width: 200.0, height: 13.0),
                  SizedBox(height: 25.0),
                  shimmerLine(width: 120.0, height: 13.0),
                  SizedBox(height: 20.0),
                  shimmerLine(width: 200.0, height: 13.0),
                  SizedBox(height: 25.0),
                  shimmerLine(
                    width: SizeUtils.width(context, 100),
                    height: 0.5,
                    radius: 0.0,
                  ),
                  SizedBox(height: 25.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Flexible(child: shimmerLine(width: 180.0, height: 13.0)),
                      SizedBox(width: 10),
                      shimmerCircle(),
                    ],
                  ),
                  SizedBox(height: 25.0),
                  shimmerLine(
                    width: SizeUtils.width(context, 100),
                    height: 0.5,
                    radius: 0.0,
                  ),
                  SizedBox(height: 25.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Flexible(child: shimmerLine(width: 180.0, height: 13.0)),
                      SizedBox(width: 10),
                      shimmerCircle(),
                    ],
                  ),
                  SizedBox(height: 25.0),
                  shimmerLine(
                    width: SizeUtils.width(context, 100),
                    height: 0.5,
                    radius: 0.0,
                  ),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                shimmerBorderButton(),
                SizedBox(height: 20),
                shimmerLine(width: 145.0, height: 13.0),
              ],
            ),
          )
        ],
      ),
    );
  }
}
