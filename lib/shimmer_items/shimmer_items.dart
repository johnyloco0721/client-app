import 'package:fade_shimmer/fade_shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:pmaclient/constants/app_colors.dart';

Widget shimmerCircle({size = 20.0, color = shimmerGrey}) => Container(
      height: double.parse(size.toString()),
      width: double.parse(size.toString()),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
    );

Widget shimmerLine(
        {height = 20.0, width = 90.0, radius = 60.0, color = shimmerGrey}) =>
    FadeShimmer(
      height: double.parse(height.toString()),
      width: double.parse(width.toString()),
      radius: double.parse(radius.toString()),
      highlightColor: color,
      baseColor: color,
    );

Widget shimmerBorderButton({width = 150, height = 50, radius = 60}) =>
    Container(
      height: double.parse(height.toString()),
      width: double.parse(width.toString()),
      decoration: BoxDecoration(
        color: white,
        borderRadius: BorderRadius.circular(double.parse(radius.toString())),
        border: Border.all(color: shimmerGrey, width: 1),
      ),
    );
