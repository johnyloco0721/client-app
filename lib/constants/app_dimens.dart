const double borderWidth = 1.0;
const double smallFont = 12.0;
const double font13 = 13.0;
const double mediumFont = 16.0;
const double defaultFont = 14.0;
const double bigFonts = 18.0;
const double largeFont = 20.0;
const double bigLargeFont = 22.0;
const double extraLargeFont = 25.0;
const double veryLargeFont = 30.0;
const double veryExtraLargeFont = 34.0;
const double font40 = 40.0;
const double buttonHeight = 55.0;
