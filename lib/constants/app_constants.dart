const demoBlurHash = "LEHV6nWB2yk8pyo0adR*.7kCMdnj";
const dummyImageUrl =
    "https://www.themakeover.my/assets/PMAC/images/noImagePlaceholder.jpeg";
const termsOfServicesUrl = "https://www.themakeover.my/termofservice";
const cozyHomesUrl = "https://www.cozyhomes.my/";
const dummyUserPic = "https://www.gravatar.com/avatar/HASH";
// const apiUrl = "https://www.themakeover.my/";
const apiUrl = "https://test3.themakeover.my/";
const apiPath = "api/v1.1.1/PMACMobile/";
const iosAppUrl =
    "https://apps.apple.com/us/app/the-makeover-guys/id1578221172";
const androidAppUrl =
    "https://play.google.com/store/apps/details?id=com.themakeover.pmaclient";
const baseUrl = apiUrl + apiPath;
