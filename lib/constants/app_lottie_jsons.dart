const String splash_welcome_lottie =
    "assets/lottie_files/splash_welcome_lottie.json";
const String splash_first_screen =
    "assets/lottie_files/splash_first_screen.json";
const String splash_second_screen =
    "assets/lottie_files/splash_third_screen.json";
const String splash_third_screen =
    "assets/lottie_files/splash_second_screen.json";
const String splash_fourth_screen =
    "assets/lottie_files/splash_fourth_screen.json";
const String login_white_loading =
    "assets/lottie_files/login_white_loading.json";
const String login_black_loading =
    "assets/lottie_files/login_black_loading.json";
const String app_support = "assets/lottie_files/app_support.json";
const String payment_reminder = "assets/lottie_files/payment_reminder.json";
const String tms_screen_lottie = "assets/lottie_files/tms_screen_lottie.json";
const String no_document_lottie = "assets/lottie_files/no_document_lottie.json";
const String maintenance_lottie = "assets/lottie_files/maintenance_lottie.json";
const String update_app_lottie = "assets/lottie_files/update_app_lottie.json";
const String no_notification_lottie =
    "assets/lottie_files/no_notification_lottie.json";
const String reminder_lottie = "assets/lottie_files/reminder_lottie.json";
const String rate_us_lottie = "assets/lottie_files/rate_us_lottie.json";
const String rate_1_lottie = "assets/lottie_files/rate_1_lottie.json";
const String rate_2_lottie = "assets/lottie_files/rate_2_lottie.json";
const String rate_3_lottie = "assets/lottie_files/rate_3_lottie.json";
const String rate_4_lottie = "assets/lottie_files/rate_4_lottie.json";
const String rate_5_lottie = "assets/lottie_files/rate_5_lottie.json";
const String rate_6_lottie = "assets/lottie_files/rate_6_lottie.json";
const String rate_7_lottie = "assets/lottie_files/rate_7_lottie.json";
const String rate_8_lottie = "assets/lottie_files/rate_8_lottie.json";
const String rate_9_lottie = "assets/lottie_files/rate_9_lottie.json";
const String rate_10_lottie = "assets/lottie_files/rate_10_lottie.json";
const String rate_resolve_issue_lottie =
    "assets/lottie_files/rate_resolve_issue_lottie.json";
const String nps_thank_you_lottie =
    "assets/lottie_files/nps_thank_you_lottie.json";
const String nps_gift_lottie = "assets/lottie_files/nps_gift_lottie.json";
