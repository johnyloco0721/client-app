import 'package:flutter/cupertino.dart';
import 'package:pmaclient/constants/app_lottie_jsons.dart';
import 'package:pmaclient/generated/l10n.dart';
import 'package:pmaclient/models/question_model.dart';
import 'package:pmaclient/models/welcome_model.dart';
import 'package:pmaclient/screens/info_tab_screen.dart';
import 'package:pmaclient/screens/team_tab_screen.dart';

List<QuestionModel> questionList = [
  QuestionModel(
    question: S().are_you_happy_with_the_design,
    feedback: S().in_what_w_could_we_improve,
  ),
  QuestionModel(
    question: S().are_you_satisfied_with_our_team,
    feedback: S().what_can_we_do_to_improve_your,
  ),
  QuestionModel(
    question: S().how_likely_are_you_to_recommend,
    feedback: "",
  ),
];

List<WelcomeModel> welcomeList = [
  WelcomeModel(
    lottiePath: splash_welcome_lottie,
    title: "Welcome to The Makeover Guys",
    description: "We're happy to have you on board with us! ",
  ),
  WelcomeModel(
    lottiePath: splash_first_screen,
    title: "Keep track of your makeover",
    description:
        "Now you can get live progress updates on your makeover, all in one place!",
  ),
  WelcomeModel(
    lottiePath: splash_second_screen,
    title: "Hear it here first",
    description:
        "Get first-hand updates on our services and latest promotions, and sign up immediately via the app!",
  ),
  WelcomeModel(
    lottiePath: splash_third_screen,
    title: "Show off your property!",
    description:
        "Whether you are attracting tenants or showcasing your new home, get instant access to professional photographs of your unit.",
  ),
  WelcomeModel(
    lottiePath: splash_fourth_screen,
    title: "Ready to roll?",
    description: "Let’s get started with your makeover experience!",
  ),
];
List<Widget> tabList = [InfoTabScreen(), TeamTabScreen()];

List<String> ratingLottieList = [
  rate_1_lottie,
  rate_2_lottie,
  rate_3_lottie,
  rate_4_lottie,
  rate_5_lottie,
  rate_6_lottie,
  rate_7_lottie,
  rate_8_lottie,
  rate_9_lottie,
  rate_10_lottie,
];
