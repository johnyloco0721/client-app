const String ic_makeover_logo = "assets/images/ic_makeover_logo.png";
const String ic_left_arrow = "assets/images/ic_left_arrow.png";
const String ic_right_arrow = "assets/images/ic_right_arrow.png";
const String ic_enter_arrow_icon = "assets/images/ic_enter_arrow_icon.png";
const String ic_password = "assets/images/ic_password.png";
const String ic_user = "assets/images/ic_user.png";
const String ic_welcome_to_makeover =
    "assets/images/ic_welcome_to_makeover.png";
const String ic_tick = "assets/images/ic_tick.png";
const String ic_notification_on = "assets/images/ic_notification_on.png";
const String ic_notification_off = "assets/images/ic_notification_off.png";
const String ic_sq_ft = "assets/images/ic_sq_ft.png";
const String ic_room_type = "assets/images/ic_room_type.png";
const String ic_close = "assets/images/ic_close.png";
const String ic_profile_dummy = "assets/images/ic_profile_dummy.png";
const String ic_camera = "assets/images/ic_camera.png";
const String ic_next = "assets/images/ic_next.png";
const String ic_down = "assets/images/ic_down.png";
const String ic_support = "assets/images/ic_support.png";
const String ic_info = "assets/images/ic_info.png";
const String ic_document = "assets/images/ic_document.png";
const String ic_round_tick = "assets/images/ic_round_tick.png";
const String ic_star = "assets/images/ic_star.png";
const String ic_tmog_head = "assets/images/ic_tmog_head.png";
const String ic_down_dropdown = "assets/images/ic_down_dropdown.png";
const String ic_up_dropdown = "assets/images/ic_up_dropdown.png";
const String ic_arrow_next = "assets/images/ic_arrow_next.png";
const String ic_cozy_homes = "assets/images/ic_cozy_homes.png";
const String ic_care = "assets/images/ic_care.png";
const String ic_play = "assets/images/ic_play.png";
const String ic_career = "assets/images/ic_career.png";
const String ic_whatsapp = "assets/images/ic_whatsapp.png";
const String ic_share = "assets/images/ic_share.png";
const String ic_download = "assets/images/ic_download.png";
const String ic_shimmer_detail_running =
    "assets/images/ic_shimmer_detail_running.png";
const String ic_shimmer_detail_complete =
    "assets/images/ic_shimmer_detail_complete.png";
const String no_image_placeholder = "assets/images/no_image_placeholder.png";
const String ic_success = "assets/images/ic_success.png";
const String ic_landing_page_illustration =
    "assets/images/ic_landing_page_illustration.png";
const String ic_login_illustration =
    "assets/images/ic_login_illustration.png";
const String ic_planning_illustration =
    "assets/images/ic_planning_illustration.png";
