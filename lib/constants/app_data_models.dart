import 'package:pmaclient/models/feedback_model.dart';
import 'package:pmaclient/models/project_details_model.dart';
import 'package:pmaclient/models/projects_model.dart';
import 'package:pmaclient/models/rating_model.dart';

class AppDataModel {
  static String? tokenGenerateBy;
  static HomeProject? homeProject;
  static FeedBackResponseData? feedBackModel;
  static List<ReviewData>? reviewList;
  static List<Document>? documentList;
  static List<Gallery>? mediaTabList;
  static List<Update>? updatesList;
  static String? updateAppLink;
  static String? fcmToken;
  static bool? byPassCode;
  static String? byPassParameter;
  static String? byPassHash;

  static setByPassCode(bool? code){
    byPassCode = code;
  }
 
  static getByPassCode(){
    return byPassCode ?? false;
  }

  static setDeviceToken(String? data) {
    fcmToken = data;
  }

  static String getDeviceToken() {
    return fcmToken ?? '';
  }

  static setUpdateAppLink(String? data) { // change hard code update link that can control by backend
    updateAppLink = data;
  }

  static String getUpdateAppLink() { // change hard code update link that can control by backend
    return updateAppLink ?? "invalid link";
  }

  static setTokenGenerateByList(String? data) {
    tokenGenerateBy = data;
  }

  static String getTokenGenerateByList() {
    return tokenGenerateBy ?? '';
  }

  static setReviewList(List<ReviewData>? list) {
    reviewList = list;
  }

  static List<ReviewData> getReviewList() {
    return reviewList ?? [];
  }

  static setDocumentList(List<Document>? list) {
    documentList = list;
  }

  static List<Document> getDocumentList() {
    return documentList ?? [];
  }

  static setMediaTabList(List<Gallery>? list) {
    mediaTabList = list;
  }

  static List<Gallery> getMediaTabList() {
    return mediaTabList ?? [];
  }

  static setUpdatesList(List<Update>? list) {
    updatesList = list;
  }

  static List<Update> getUpdatesList() {
    return updatesList ?? [];
  }

  static setHomeProjectModel(HomeProject? model) {
    homeProject = model;
  }

  static HomeProject getHomeProjectModel() {
    return homeProject ?? HomeProject();
  }

  static setFeedBackModel(FeedBackResponseData? model) {
    feedBackModel = model;
  }

  static FeedBackResponseData getFeedBackModel() {
    return feedBackModel ?? FeedBackResponseData();
  }

  static getByPassParameter(String? pass) {
    byPassParameter = pass;
  }

  static String setByPassParameter() {
    return byPassParameter ?? 'invalid parameter';
  }

  static getByPassHash(String? pass) {
    byPassHash = pass;
  }

  static String setByPassHash() {
    return byPassHash ?? 'invalid hash';
  }
}
