import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const Color yellow = Color(0xffFFD200);
const Color darkYellow = Color(0xffEFAC00);
const Color white = Color(0xffffffff);
const Color black = Color(0xff000000);
const Color lightBlack = Color(0xff383633);
const Color grey = Color(0xff75716B);
const Color shadowColor = Color(0xff4B360004);
const Color darkGrey = Color(0xff383633);
const Color lightGrey = Color(0xffC0BFBB);
const Color shimmerGrey = Color(0xffE5E5E1);
const Color whiteGrey = Color(0xffF5F5F5);
const Color red = Color(0xffEE4056);
const Color green = Color(0xff00B485);
const Color blue = Color(0xff007AFF);
const Color lightBlue = Color(0xff27B1FA);
