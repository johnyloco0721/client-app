// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Welcome to The `
  String get welcome_to_the {
    return Intl.message(
      'Welcome to The ',
      name: 'welcome_to_the',
      desc: '',
      args: [],
    );
  }

  /// `Makeover Guys,`
  String get makeover_guys {
    return Intl.message(
      'Makeover Guys,',
      name: 'makeover_guys',
      desc: '',
      args: [],
    );
  }

  /// `Makeover`
  String get makeover {
    return Intl.message(
      'Makeover',
      name: 'makeover',
      desc: '',
      args: [],
    );
  }

  /// `Please enter your username and\npassword to login`
  String get please_enter_your_username_and_password_to_login {
    return Intl.message(
      'Please enter your username and\npassword to login',
      name: 'please_enter_your_username_and_password_to_login',
      desc: '',
      args: [],
    );
  }

  /// `Username`
  String get user_name {
    return Intl.message(
      'Username',
      name: 'user_name',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get password {
    return Intl.message(
      'Password',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `New Password`
  String get new_password {
    return Intl.message(
      'New Password',
      name: 'new_password',
      desc: '',
      args: [],
    );
  }

  /// `Confirm Password`
  String get confirm_password {
    return Intl.message(
      'Confirm Password',
      name: 'confirm_password',
      desc: '',
      args: [],
    );
  }

  /// `Re-enter Password`
  String get re_enter_password {
    return Intl.message(
      'Re-enter Password',
      name: 're_enter_password',
      desc: '',
      args: [],
    );
  }

  /// `Username shouldn't empty`
  String get username_should_not_empty {
    return Intl.message(
      'Username shouldn\'t empty',
      name: 'username_should_not_empty',
      desc: '',
      args: [],
    );
  }

  /// `Password shouldn't empty`
  String get password_should_not_empty {
    return Intl.message(
      'Password shouldn\'t empty',
      name: 'password_should_not_empty',
      desc: '',
      args: [],
    );
  }

  /// `Password didn't match`
  String get password_did_not_match {
    return Intl.message(
      'Password didn\'t match',
      name: 'password_did_not_match',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Login Now`
  String get login_now {
    return Intl.message(
      'Login Now',
      name: 'login_now',
      desc: '',
      args: [],
    );
  }

  /// `Forgot Password`
  String get forgot_password {
    return Intl.message(
      'Forgot Password',
      name: 'forgot_password',
      desc: '',
      args: [],
    );
  }

  /// `Please enter your username to reset your password`
  String get please_enter_your_username_and_password_to_reset {
    return Intl.message(
      'Please enter your username to reset your password',
      name: 'please_enter_your_username_and_password_to_reset',
      desc: '',
      args: [],
    );
  }

  /// `Reset Password`
  String get reset_password {
    return Intl.message(
      'Reset Password',
      name: 'reset_password',
      desc: '',
      args: [],
    );
  }

  /// `Change Password`
  String get change_password {
    return Intl.message(
      'Change Password',
      name: 'change_password',
      desc: '',
      args: [],
    );
  }

  /// `Reset Your Password`
  String get reset_your_password {
    return Intl.message(
      'Reset Your Password',
      name: 'reset_your_password',
      desc: '',
      args: [],
    );
  }

  /// `We've sent a link to `
  String get we_sent_a_link_to {
    return Intl.message(
      'We\'ve sent a link to ',
      name: 'we_sent_a_link_to',
      desc: '',
      args: [],
    );
  }

  /// `Kindly check your inbox for the reset password link `
  String get please_check_your_email_and_click {
    return Intl.message(
      'Kindly check your inbox for the reset password link ',
      name: 'please_check_your_email_and_click',
      desc: '',
      args: [],
    );
  }

  /// `Back To Login Screen`
  String get back_to_login_screen {
    return Intl.message(
      'Back To Login Screen',
      name: 'back_to_login_screen',
      desc: '',
      args: [],
    );
  }

  /// `Please set your new password.`
  String get please_set_your_new_password {
    return Intl.message(
      'Please set your new password.',
      name: 'please_set_your_new_password',
      desc: '',
      args: [],
    );
  }

  /// `Reset password successful!`
  String get reset_password_successful {
    return Intl.message(
      'Reset password successful!',
      name: 'reset_password_successful',
      desc: '',
      args: [],
    );
  }

  /// `Please login with your new password.`
  String get please_login_with_your_new_password {
    return Intl.message(
      'Please login with your new password.',
      name: 'please_login_with_your_new_password',
      desc: '',
      args: [],
    );
  }

  /// `Notifications`
  String get notifications {
    return Intl.message(
      'Notifications',
      name: 'notifications',
      desc: '',
      args: [],
    );
  }

  /// `Oh hello! I'm still waiting for my\nfirst notification`
  String get there_is_no_notifications_yet {
    return Intl.message(
      'Oh hello! I\'m still waiting for my\nfirst notification',
      name: 'there_is_no_notifications_yet',
      desc: '',
      args: [],
    );
  }

  /// `Profile`
  String get profile {
    return Intl.message(
      'Profile',
      name: 'profile',
      desc: '',
      args: [],
    );
  }

  /// `Profile Picture`
  String get profile_picture {
    return Intl.message(
      'Profile Picture',
      name: 'profile_picture',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get name {
    return Intl.message(
      'Name',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `Email Address`
  String get email_address {
    return Intl.message(
      'Email Address',
      name: 'email_address',
      desc: '',
      args: [],
    );
  }

  /// `Mobile Number`
  String get mobile_number {
    return Intl.message(
      'Mobile Number',
      name: 'mobile_number',
      desc: '',
      args: [],
    );
  }

  /// `Terms Of Service`
  String get terms_of_service {
    return Intl.message(
      'Terms Of Service',
      name: 'terms_of_service',
      desc: '',
      args: [],
    );
  }

  /// `App Support`
  String get app_support {
    return Intl.message(
      'App Support',
      name: 'app_support',
      desc: '',
      args: [],
    );
  }

  /// `Last Name`
  String get last_name {
    return Intl.message(
      'Last Name',
      name: 'last_name',
      desc: '',
      args: [],
    );
  }

  /// `Logout`
  String get logout {
    return Intl.message(
      'Logout',
      name: 'logout',
      desc: '',
      args: [],
    );
  }

  /// `Past Notifications`
  String get past_notifications {
    return Intl.message(
      'Past Notifications',
      name: 'past_notifications',
      desc: '',
      args: [],
    );
  }

  /// `Read All`
  String get read_all {
    return Intl.message(
      'Read All',
      name: 'read_all',
      desc: '',
      args: [],
    );
  }

  /// `Today`
  String get today {
    return Intl.message(
      'Today',
      name: 'today',
      desc: '',
      args: [],
    );
  }

  /// `You are up-to-date!`
  String get you_have_caught_up_with_all {
    return Intl.message(
      'You are up-to-date!',
      name: 'you_have_caught_up_with_all',
      desc: '',
      args: [],
    );
  }

  /// `Photo Gallery`
  String get photo_gallery {
    return Intl.message(
      'Photo Gallery',
      name: 'photo_gallery',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Camera`
  String get camera {
    return Intl.message(
      'Camera',
      name: 'camera',
      desc: '',
      args: [],
    );
  }

  /// `Reupload`
  String get re_upload {
    return Intl.message(
      'Reupload',
      name: 're_upload',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get save {
    return Intl.message(
      'Save',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `Having headache with the app? Don't\nworry, just email us and we'll help you!`
  String get hello_having_issue_with_the_app {
    return Intl.message(
      'Having headache with the app? Don\'t\nworry, just email us and we\'ll help you!',
      name: 'hello_having_issue_with_the_app',
      desc: '',
      args: [],
    );
  }

  /// `Contact Us`
  String get contact_us {
    return Intl.message(
      'Contact Us',
      name: 'contact_us',
      desc: '',
      args: [],
    );
  }

  /// `Documents`
  String get all_medias_and_Docs {
    return Intl.message(
      'All Medias & Docs',
      name: 'all_medias_and_Docs',
      desc: '',
      args: [],
    );
  }

  /// `Makeover Progress`
  String get makeover_progress {
    return Intl.message(
      'Makeover Progress',
      name: 'makeover_progress',
      desc: '',
      args: [],
    );
  }

  /// `Details`
  String get details {
    return Intl.message(
      'Details',
      name: 'details',
      desc: '',
      args: [],
    );
  }

  /// `Latest Updates`
  String get latest_updates {
    return Intl.message(
      'Latest Updates',
      name: 'latest_updates',
      desc: '',
      args: [],
    );
  }

  /// `View All`
  String get view_all {
    return Intl.message(
      'View All',
      name: 'view_all',
      desc: '',
      args: [],
    );
  }

  /// `Password should be at least 5 to 20 characters in length and should include at least one upper character and one number `
  String get password_should_be_at_least_5_character {
    return Intl.message(
      'Password should be at least 5 to 20 characters in length and should include at least one upper character and one number ',
      name: 'password_should_be_at_least_5_character',
      desc: '',
      args: [],
    );
  }

  /// `Kindly Reminder:`
  String get kindly_reminder {
    return Intl.message(
      'Kindly Reminder:',
      name: 'kindly_reminder',
      desc: '',
      args: [],
    );
  }

  /// `Care`
  String get care {
    return Intl.message(
      'Care',
      name: 'care',
      desc: '',
      args: [],
    );
  }

  /// `TMS`
  String get tms {
    return Intl.message(
      'TMS',
      name: 'tms',
      desc: '',
      args: [],
    );
  }

  /// `Start Date`
  String get startDate {
    return Intl.message(
      'Start Date',
      name: 'startDate',
      desc: '',
      args: [],
    );
  }

  /// `Completed Date`
  String get completedDate {
    return Intl.message(
      'Completed Date',
      name: 'completedDate',
      desc: '',
      args: [],
    );
  }

  /// `Handover Date`
  String get handoverDate {
    return Intl.message(
      'Handover Date',
      name: 'handoverDate',
      desc: '',
      args: [],
    );
  }

  /// `Theme`
  String get theme {
    return Intl.message(
      'Theme',
      name: 'theme',
      desc: '',
      args: [],
    );
  }

  /// `Warranty Duration`
  String get warranty_duration {
    return Intl.message(
      'Warranty Duration',
      name: 'warranty_duration',
      desc: '',
      args: [],
    );
  }

  /// `Insurance Validity`
  String get insurance_validity {
    return Intl.message(
      'Insurance Validity',
      name: 'insurance_validity',
      desc: '',
      args: [],
    );
  }

  /// `Gallery`
  String get gallery {
    return Intl.message(
      'Gallery',
      name: 'gallery',
      desc: '',
      args: [],
    );
  }

  /// `View Makeover Updates`
  String get viewMakeoverUpdates {
    return Intl.message(
      'View Makeover Updates',
      name: 'viewMakeoverUpdates',
      desc: '',
      args: [],
    );
  }

  /// `Makeover Updates`
  String get makeoverUpdates {
    return Intl.message(
      'Makeover Updates',
      name: 'makeoverUpdates',
      desc: '',
      args: [],
    );
  }

  /// `TMS by CozyHomes`
  String get tmsByCozyHomes {
    return Intl.message(
      'TMS by CozyHomes',
      name: 'tmsByCozyHomes',
      desc: '',
      args: [],
    );
  }

  /// `Partnering with CozyHomes`
  String get partneringWithCozyHomes {
    return Intl.message(
      'Partnering with CozyHomes',
      name: 'partneringWithCozyHomes',
      desc: '',
      args: [],
    );
  }

  /// `Our Tenant Management Service (TMS) helps you to find and manage tenants. You can also have an overview of tenant payment status, as well as requesting services or maintenance for your property.`
  String get ourTenantManagementServicesTmshelps {
    return Intl.message(
      'Our Tenant Management Service (TMS) helps you to find and manage tenants. You can also have an overview of tenant payment status, as well as requesting services or maintenance for your property.',
      name: 'ourTenantManagementServicesTmshelps',
      desc: '',
      args: [],
    );
  }

  /// `CozyHomes`
  String get cozyHomes {
    return Intl.message(
      'CozyHomes',
      name: 'cozyHomes',
      desc: '',
      args: [],
    );
  }

  /// `Contact Us`
  String get property_enquiry {
    return Intl.message(
      'Contact Us',
      name: 'property_enquiry',
      desc: '',
      args: [],
    );
  }

  /// `Questions? We’re here to help:`
  String get having_any_questions_about {
    return Intl.message(
      'Questions? We’re here to help:',
      name: 'having_any_questions_about',
      desc: '',
      args: [],
    );
  }

  /// `Whatsapp not installed`
  String get whatsapp_not_installed {
    return Intl.message(
      'Whatsapp not installed',
      name: 'whatsapp_not_installed',
      desc: '',
      args: [],
    );
  }

  /// `It’s empty here,be the first one to post!`
  String get its_empty_here_be_the_first {
    return Intl.message(
      'It’s empty here,be the first one to post!',
      name: 'its_empty_here_be_the_first',
      desc: '',
      args: [],
    );
  }

  /// `There’s no document yet`
  String get theres_no_document_yet {
    return Intl.message(
      'There’s no document yet',
      name: 'theres_no_document_yet',
      desc: '',
      args: [],
    );
  }

  /// `There’s no document yet`
  String get theres_no_media_yet {
    return Intl.message(
      'There’s no media yet',
      name: 'theres_no_media_yet',
      desc: '',
      args: [],
    );
  }

  /// `General Info`
  String get property_details {
    return Intl.message(
      'General Info',
      name: 'property_details',
      desc: '',
      args: [],
    );
  }

  /// `Info`
  String get info {
    return Intl.message(
      'Info',
      name: 'info',
      desc: '',
      args: [],
    );
  }

  /// `Team`
  String get team {
    return Intl.message(
      'Team',
      name: 'team',
      desc: '',
      args: [],
    );
  }

  /// `Project`
  String get project {
    return Intl.message(
      'Project',
      name: 'project',
      desc: '',
      args: [],
    );
  }

  /// `Project Name`
  String get project_name {
    return Intl.message(
      'Project Name',
      name: 'project_name',
      desc: '',
      args: [],
    );
  }

  /// `Project Area`
  String get project_area {
    return Intl.message(
      'Project Area',
      name: 'project_area',
      desc: '',
      args: [],
    );
  }

  /// `Unit Size`
  String get unit_size {
    return Intl.message(
      'Unit Size',
      name: 'unit_size',
      desc: '',
      args: [],
    );
  }

  /// `Unit Number`
  String get unit_number {
    return Intl.message(
      'Unit Number',
      name: 'unit_number',
      desc: '',
      args: [],
    );
  }

  /// `Bedroom No.`
  String get bedroom_no {
    return Intl.message(
      'Bedroom No.',
      name: 'bedroom_no',
      desc: '',
      args: [],
    );
  }

  /// `Bathroom No.`
  String get bathroom_no {
    return Intl.message(
      'Bathroom No.',
      name: 'bathroom_no',
      desc: '',
      args: [],
    );
  }

  /// `Sorry, the app is under maintenance now! Please try again later.`
  String get sorry_the_app_is_under_maintenance {
    return Intl.message(
      'Sorry, the app is under maintenance now! Please try again later.',
      name: 'sorry_the_app_is_under_maintenance',
      desc: '',
      args: [],
    );
  }

  /// `Hi, please update your app`
  String get hi_please_update_your_app {
    return Intl.message(
      'Hi, please update your app',
      name: 'hi_please_update_your_app',
      desc: '',
      args: [],
    );
  }

  /// `Update your app so that we can bring you a better experience!`
  String get update_your_app_so_that {
    return Intl.message(
      'Update your app so that we can bring you a better experience!',
      name: 'update_your_app_so_that',
      desc: '',
      args: [],
    );
  }

  /// `Update Now`
  String get update_now {
    return Intl.message(
      'Update Now',
      name: 'update_now',
      desc: '',
      args: [],
    );
  }

  /// `Media`
  String get media {
    return Intl.message(
      'Media',
      name: 'media',
      desc: '',
      args: [],
    );
  }

  /// `Login Success`
  String get login_success {
    return Intl.message(
      'Login Success',
      name: 'login_success',
      desc: '',
      args: [],
    );
  }

  /// `Connection timeout`
  String get connection_timeout {
    return Intl.message(
      'Connection timeout',
      name: 'connection_timeout',
      desc: '',
      args: [],
    );
  }

  /// `Request was cancelled`
  String get request_was_cancelled {
    return Intl.message(
      'Request was cancelled',
      name: 'request_was_cancelled',
      desc: '',
      args: [],
    );
  }

  /// `Connection failed due to internet connection`
  String get connection_failed_due_to_internet {
    return Intl.message(
      'Connection failed due to internet connection',
      name: 'connection_failed_due_to_internet',
      desc: '',
      args: [],
    );
  }

  /// `Receive timeout in connection`
  String get receive_timeout_in_connection {
    return Intl.message(
      'Receive timeout in connection',
      name: 'receive_timeout_in_connection',
      desc: '',
      args: [],
    );
  }

  /// `Receive timeout in send request`
  String get receive_timeout_in_send {
    return Intl.message(
      'Receive timeout in send request',
      name: 'receive_timeout_in_send',
      desc: '',
      args: [],
    );
  }

  /// `Server Error , Please try again later`
  String get server_error_please_try_again {
    return Intl.message(
      'Server Error , Please try again later',
      name: 'server_error_please_try_again',
      desc: '',
      args: [],
    );
  }

  /// `One-time Password Reset`
  String get one_time_password_reset {
    return Intl.message(
      'One-time Password Reset',
      name: 'one_time_password_reset',
      desc: '',
      args: [],
    );
  }

  /// `Hiya, remember to choose a new password for your account. It's quick & easy!`
  String get hiya_remember_to_choose_a_new {
    return Intl.message(
      'Hiya, remember to choose a new password for your account. It\'s quick & easy!',
      name: 'hiya_remember_to_choose_a_new',
      desc: '',
      args: [],
    );
  }

  /// `*Invalid username or incorrect password`
  String get invalid_username_or_incorrect_password {
    return Intl.message(
      '*Invalid username or incorrect password',
      name: 'invalid_username_or_incorrect_password',
      desc: '',
      args: [],
    );
  }

  /// `All projects are loaded.`
  String get all_projects_are_loaded {
    return Intl.message(
      'All projects are loaded.',
      name: 'all_projects_are_loaded',
      desc: '',
      args: [],
    );
  }

  /// `Choose a new password`
  String get choose_a_new_password {
    return Intl.message(
      'Choose a new password',
      name: 'choose_a_new_password',
      desc: '',
      args: [],
    );
  }

  /// `Current Password`
  String get current_password {
    return Intl.message(
      'Current Password',
      name: 'current_password',
      desc: '',
      args: [],
    );
  }

  /// `Current Password Shouldn't empty`
  String get current_password_should_not_empty {
    return Intl.message(
      'Current Password Shouldn\'t empty',
      name: 'current_password_should_not_empty',
      desc: '',
      args: [],
    );
  }

  /// `#BetterHomes`
  String get better_homes {
    return Intl.message(
      '#BetterHomes',
      name: 'better_homes',
      desc: '',
      args: [],
    );
  }

  /// `(edited)`
  String get edited {
    return Intl.message(
      '(edited)',
      name: 'edited',
      desc: '',
      args: [],
    );
  }

  /// `Rate Us`
  String get rate_us {
    return Intl.message(
      'Rate Us',
      name: 'rate_us',
      desc: '',
      args: [],
    );
  }

  /// `Receive a complimentary <b>Grab voucher worth RM10!</b>`
  String get receive_a_complimentary {
    return Intl.message(
      'Receive a complimentary <b>Grab voucher worth RM10!</b>',
      name: 'receive_a_complimentary',
      desc: '',
      args: [],
    );
  }

  /// `How did we do?`
  String get how_did_we_do {
    return Intl.message(
      'How did we do?',
      name: 'how_did_we_do',
      desc: '',
      args: [],
    );
  }

  /// `Rate our service now to get a Grab voucher worth RM10!`
  String get rate_our_service_now_to_get {
    return Intl.message(
      'Rate our service now to get a Grab voucher worth RM10!',
      name: 'rate_our_service_now_to_get',
      desc: '',
      args: [],
    );
  }

  /// `Maybe Later`
  String get may_be_later {
    return Intl.message(
      'Maybe Later',
      name: 'may_be_later',
      desc: '',
      args: [],
    );
  }

  /// `Start`
  String get start {
    return Intl.message(
      'Start',
      name: 'start',
      desc: '',
      args: [],
    );
  }

  /// `Question`
  String get question {
    return Intl.message(
      'Question',
      name: 'question',
      desc: '',
      args: [],
    );
  }

  /// `Very unhappy`
  String get very_unhappy {
    return Intl.message(
      'Very unhappy',
      name: 'very_unhappy',
      desc: '',
      args: [],
    );
  }

  /// `I love it!`
  String get i_love_it {
    return Intl.message(
      'I love it!',
      name: 'i_love_it',
      desc: '',
      args: [],
    );
  }

  /// `Not at all`
  String get not_at_all {
    return Intl.message(
      'Not at all',
      name: 'not_at_all',
      desc: '',
      args: [],
    );
  }

  /// `Highly satisfied`
  String get highly_satisfied {
    return Intl.message(
      'Highly satisfied',
      name: 'highly_satisfied',
      desc: '',
      args: [],
    );
  }

  /// `Never!`
  String get never {
    return Intl.message(
      'Never!',
      name: 'never',
      desc: '',
      args: [],
    );
  }

  /// `Very likely`
  String get very_likely {
    return Intl.message(
      'Very likely',
      name: 'very_likely',
      desc: '',
      args: [],
    );
  }

  /// `State your suggestion here...`
  String get state_your_suggestion_here {
    return Intl.message(
      'State your suggestion here...',
      name: 'state_your_suggestion_here',
      desc: '',
      args: [],
    );
  }

  /// `Done`
  String get done {
    return Intl.message(
      'Done',
      name: 'done',
      desc: '',
      args: [],
    );
  }

  /// `Are you happy with the design outcome of your makeover?`
  String get are_you_happy_with_the_design {
    return Intl.message(
      'Are you happy with the design outcome of your makeover?',
      name: 'are_you_happy_with_the_design',
      desc: '',
      args: [],
    );
  }

  /// `Are you satisfied with our team's service?`
  String get are_you_satisfied_with_our_team {
    return Intl.message(
      'Are you satisfied with our team\'s service?',
      name: 'are_you_satisfied_with_our_team',
      desc: '',
      args: [],
    );
  }

  /// `How likely are you to recommend The Makeover Guys to your family & friends?`
  String get how_likely_are_you_to_recommend {
    return Intl.message(
      'How likely are you to recommend The Makeover Guys to your family & friends?',
      name: 'how_likely_are_you_to_recommend',
      desc: '',
      args: [],
    );
  }

  /// `In what way could we improve on our makeover design?`
  String get in_what_w_could_we_improve {
    return Intl.message(
      'In what way could we improve on our makeover design?',
      name: 'in_what_w_could_we_improve',
      desc: '',
      args: [],
    );
  }

  /// `What can we do to improve your makeover experience?`
  String get what_can_we_do_to_improve_your {
    return Intl.message(
      'What can we do to improve your makeover experience?',
      name: 'what_can_we_do_to_improve_your',
      desc: '',
      args: [],
    );
  }

  /// `required`
  String get required {
    return Intl.message(
      'required',
      name: 'required',
      desc: '',
      args: [],
    );
  }

  /// `submit`
  String get submit {
    return Intl.message(
      'submit',
      name: 'submit',
      desc: '',
      args: [],
    );
  }

  /// `No Thanks`
  String get no_thanks {
    return Intl.message(
      'No Thanks',
      name: 'no_thanks',
      desc: '',
      args: [],
    );
  }

  /// `Sure`
  String get sure {
    return Intl.message(
      'Sure',
      name: 'sure',
      desc: '',
      args: [],
    );
  }

  /// `May we reach out to you?`
  String get may_we_reach_out_to_you {
    return Intl.message(
      'May we reach out to you?',
      name: 'may_we_reach_out_to_you',
      desc: '',
      args: [],
    );
  }

  /// `Let us know if we can discuss ways to serve you better. It will really help us improve your makeover experience!`
  String get let_us_know_if_we_can_discuss_ways_to_serve {
    return Intl.message(
      'Let us know if we can discuss ways to serve you better. It will really help us improve your makeover experience!',
      name: 'let_us_know_if_we_can_discuss_ways_to_serve',
      desc: '',
      args: [],
    );
  }

  /// `Thank you for your time!`
  String get thank_you_for_your_time {
    return Intl.message(
      'Thank you for your time!',
      name: 'thank_you_for_your_time',
      desc: '',
      args: [],
    );
  }

  /// `Thank you, our team will be in touch soon.`
  String get thank_you_our_team_will_be {
    return Intl.message(
      'Thank you, our team will be in touch soon.',
      name: 'thank_you_our_team_will_be',
      desc: '',
      args: [],
    );
  }

  /// `In the meantime, check your email for your <b>RM 10 Grab voucher</b> treat!`
  String get in_the_meantime_check_your_email_for {
    return Intl.message(
      'In the meantime, check your email for your <b>RM 10 Grab voucher</b> treat!',
      name: 'in_the_meantime_check_your_email_for',
      desc: '',
      args: [],
    );
  }

  /// `Thank you!`
  String get thank_you {
    return Intl.message(
      'Thank you!',
      name: 'thank_you',
      desc: '',
      args: [],
    );
  }

  /// `Your feedback is highly appreciated, and we promise to keep serving you better.`
  String get your_feedback_is_highly_appreciated_and_we {
    return Intl.message(
      'Your feedback is highly appreciated, and we promise to keep serving you better.',
      name: 'your_feedback_is_highly_appreciated_and_we',
      desc: '',
      args: [],
    );
  }

  /// ` If you require any further help, kindly contact our Customer Happiness Officer at`
  String get if_you_required_any_further_help_kindly_contact {
    return Intl.message(
      ' If you require any further help, kindly contact our Customer Happiness Officer at',
      name: 'if_you_required_any_further_help_kindly_contact',
      desc: '',
      args: [],
    );
  }

  /// `This Photo`
  String get this_photo {
    return Intl.message(
      'This Photo',
      name: 'this_photo',
      desc: '',
      args: [],
    );
  }

  /// `This Video`
  String get this_video {
    return Intl.message(
      'This Video',
      name: 'this_video',
      desc: '',
      args: [],
    );
  }

  /// `Download`
  String get download {
    return Intl.message(
      'Download',
      name: 'download',
      desc: '',
      args: [],
    );
  }

  /// `Downloaded!`
  String get downloaded {
    return Intl.message(
      'Downloaded!',
      name: 'downloaded',
      desc: '',
      args: [],
    );
  }

  /// `Share`
  String get share {
    return Intl.message(
      'Share',
      name: 'share',
      desc: '',
      args: [],
    );
  }

  /// `Shared!`
  String get shared {
    return Intl.message(
      'Shared!',
      name: 'shared',
      desc: '',
      args: [],
    );
  }

  /// `The Whole Album`
  String get the_whole_album {
    return Intl.message(
      'The Whole Album',
      name: 'the_whole_album',
      desc: '',
      args: [],
    );
  }

  /// `Tenancy`
  String get tenancy {
    return Intl.message(
      'Tenancy',
      name: 'tenancy',
      desc: '',
      args: [],
    );
  }

  /// `Document not found`
  String get document_not_found {
    return Intl.message(
      'Document not found',
      name: 'document_not_found',
      desc: '',
      args: [],
    );
  }

  /// `Invalid User Name`
  String get invalid_username {
    return Intl.message(
      'Invalid User Name',
      name: 'invalid_username',
      desc: '',
      args: [],
    );
  }

  /// `Mobile Number`
  String get mobileNumber {
    return Intl.message(
      'Mobile Number',
      name: 'mobileNumber',
      desc: '',
      args: [],
    );
  }

  /// `Search country`
  String get searchCountry {
    return Intl.message(
      'Search country / region',
      name: 'searchCountry',
      desc: '',
      args: [],
    );
  }

  /// `Please enter your mobile number`
  String get pleaseEnterYourMobileNumber {
    return Intl.message(
      'Please enter your mobile number',
      name: 'pleaseEnterYourMobileNumber',
      desc: '',
      args: [],
    );
  }

  /// `Next`
  String get next {
    return Intl.message(
      'Next',
      name: 'next',
      desc: '',
      args: [],
    );
  }

  /// `Resend Code`
  String get resendCode {
    return Intl.message(
      'Resend Code',
      name: 'resendCode',
      desc: '',
      args: [],
    );
  }

  /// `Wrong code entered`
  String get wrongCodeEntered {
    return Intl.message(
      'Wrong code entered',
      name: 'wrongCodeEntered',
      desc: '',
      args: [],
    );
  }

  /// `Clear code`
  String get clearCode {
    return Intl.message(
      'Clear code',
      name: 'clearCode',
      desc: '',
      args: [],
    );
  }

  /// `Enter your `
  String get enterYour {
    return Intl.message(
      'Enter your ',
      name: 'enterYour',
      desc: '',
      args: [],
    );
  }

  /// `Enter the `
  String get enterThe {
    return Intl.message(
      'Enter the ',
      name: 'enterThe',
      desc: '',
      args: [],
    );
  }

  /// `4-digit code `
  String get fourDigitCode {
    return Intl.message(
      '4-digit code ',
      name: 'fourDigitCode',
      desc: '',
      args: [],
    );
  }

  /// `sent to your mobile phone`
  String get sentToYourMobilePhone {
    return Intl.message(
      'sent to your mobile phone',
      name: 'sentToYourMobilePhone',
      desc: '',
      args: [],
    );
  }

  /// `sent to your email`
  String get sentToYourEmail {
    return Intl.message(
      'sent to ',
      name: 'sentToYourEmail',
      desc: '',
      args: [],
    );
  }

  /// `Resend In `
  String get resendIn {
    return Intl.message(
      'Resend In ',
      name: 'resendIn',
      desc: '',
      args: [],
    );
  }

  /// `makeover_in_planning`
  String get makeoverInPlanning {
    return Intl.message(
      'Makeover In Planning...',
      name: 'makeoverInPlanning',
      desc: '',
      args: [],
    );
  }

  /// 'Having Trouble'
  String get havingTrouble{
    return Intl.message(
      'Having Trouble Receiving Code?',
      name: 'havingTrouble',
      desc: '',
      args: [],
    );
  }

  /// 'OTP Email'
  String get verifyEmail{
    return Intl.message(
      'Verify Your Account Via Email.',
      name: 'verifyEmail',
      desc: '',
      args: [],
    );
  }

  /// 'OTP SMS'
  String get verifySMS{
    return Intl.message(
      'Verify Your Account Via SMS.',
      name: 'verifySMS',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
