// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en_US locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en_US';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "all_projects_are_loaded":
            MessageLookupByLibrary.simpleMessage("All projects are loaded."),
        "app_support": MessageLookupByLibrary.simpleMessage("App Support"),
        "are_you_happy_with_the_design": MessageLookupByLibrary.simpleMessage(
            "Are you happy with the design outcome of your makeover?"),
        "are_you_satisfied_with_our_team": MessageLookupByLibrary.simpleMessage(
            "Are you satisfied with our team\'s service?"),
        "back_to_login_screen":
            MessageLookupByLibrary.simpleMessage("Back To Login Screen"),
        "bathroom_no": MessageLookupByLibrary.simpleMessage("Bathroom No."),
        "bedroom_no": MessageLookupByLibrary.simpleMessage("Bedroom No."),
        "better_homes": MessageLookupByLibrary.simpleMessage("#BetterHomes"),
        "camera": MessageLookupByLibrary.simpleMessage("Camera"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "care": MessageLookupByLibrary.simpleMessage("Care"),
        "change_password":
            MessageLookupByLibrary.simpleMessage("Change Password"),
        "choose_a_new_password":
            MessageLookupByLibrary.simpleMessage("Choose a new password"),
        "clearCode": MessageLookupByLibrary.simpleMessage("Clear code"),
        "completedDate": MessageLookupByLibrary.simpleMessage("Completed Date"),
        "confirm_password":
            MessageLookupByLibrary.simpleMessage("Confirm Password"),
        "connection_failed_due_to_internet":
            MessageLookupByLibrary.simpleMessage(
                "Connection failed due to internet connection"),
        "connection_timeout":
            MessageLookupByLibrary.simpleMessage("Connection timeout"),
        "contact_us": MessageLookupByLibrary.simpleMessage("Contact Us"),
        "cozyHomes": MessageLookupByLibrary.simpleMessage("CozyHomes"),
        "current_password":
            MessageLookupByLibrary.simpleMessage("Current Password"),
        "current_password_should_not_empty":
            MessageLookupByLibrary.simpleMessage(
                "Current Password Shouldn\'t empty"),
        "details": MessageLookupByLibrary.simpleMessage("Details"),
        "document_not_found":
            MessageLookupByLibrary.simpleMessage("Document not found"),
        "documents": MessageLookupByLibrary.simpleMessage("Documents"),
        "done": MessageLookupByLibrary.simpleMessage("Done"),
        "download": MessageLookupByLibrary.simpleMessage("Download"),
        "downloaded": MessageLookupByLibrary.simpleMessage("Downloaded!"),
        "edited": MessageLookupByLibrary.simpleMessage("(edited)"),
        "email_address": MessageLookupByLibrary.simpleMessage("Email Address"),
        "enterThe": MessageLookupByLibrary.simpleMessage("Enter the "),
        "enterYour": MessageLookupByLibrary.simpleMessage("Enter your "),
        "name": MessageLookupByLibrary.simpleMessage("Name"),
        "forgot_password":
            MessageLookupByLibrary.simpleMessage("Forgot Password"),
        "fourDigitCode": MessageLookupByLibrary.simpleMessage("4-digit code "),
        "gallery": MessageLookupByLibrary.simpleMessage("Gallery"),
        "handoverDate": MessageLookupByLibrary.simpleMessage("Handover Date"),
        "having_any_questions_about": MessageLookupByLibrary.simpleMessage(
            "Questions? We’re here to help:"),
        "hello_having_issue_with_the_app": MessageLookupByLibrary.simpleMessage(
            "Having headache with the app? Don\'t\nworry, just email us and we\'ll help you!"),
        "hi_please_update_your_app":
            MessageLookupByLibrary.simpleMessage("Hi, please update your app"),
        "highly_satisfied":
            MessageLookupByLibrary.simpleMessage("Highly satisfied"),
        "hiya_remember_to_choose_a_new": MessageLookupByLibrary.simpleMessage(
            "Hiya, remember to choose a new password for your account. It\'s quick & easy!"),
        "how_did_we_do": MessageLookupByLibrary.simpleMessage("How did we do?"),
        "how_likely_are_you_to_recommend": MessageLookupByLibrary.simpleMessage(
            "How likely are you to recommend The Makeover Guys to your family & friends?"),
        "i_love_it": MessageLookupByLibrary.simpleMessage("I love it!"),
        "if_you_required_any_further_help_kindly_contact":
            MessageLookupByLibrary.simpleMessage(
                " If you require any further help, kindly contact our Customer Happiness Officer at"),
        "in_the_meantime_check_your_email_for":
            MessageLookupByLibrary.simpleMessage(
                "In the meantime, check your email for your <b>RM 10 Grab voucher</b> treat!"),
        "in_what_w_could_we_improve": MessageLookupByLibrary.simpleMessage(
            "In what way could we improve on our makeover design?"),
        "info": MessageLookupByLibrary.simpleMessage("Info"),
        "insurance_validity":
            MessageLookupByLibrary.simpleMessage("Insurance Validity"),
        "invalid_username":
            MessageLookupByLibrary.simpleMessage("Invalid User Name"),
        "invalid_username_or_incorrect_password":
            MessageLookupByLibrary.simpleMessage(
                "*Invalid username or incorrect password"),
        "its_empty_here_be_the_first": MessageLookupByLibrary.simpleMessage(
            "It’s empty here,be the first one to post!"),
        "kindly_reminder":
            MessageLookupByLibrary.simpleMessage("Kindly Reminder:"),
        "last_name": MessageLookupByLibrary.simpleMessage("Last Name"),
        "latest_updates":
            MessageLookupByLibrary.simpleMessage("Latest Updates"),
        "let_us_know_if_we_can_discuss_ways_to_serve":
            MessageLookupByLibrary.simpleMessage(
                "Let us know if we can discuss ways to serve you better. It will really help us improve your makeover experience!"),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "login_now": MessageLookupByLibrary.simpleMessage("Login Now"),
        "login_success": MessageLookupByLibrary.simpleMessage("Login Success"),
        "logout": MessageLookupByLibrary.simpleMessage("Logout"),
        "makeover": MessageLookupByLibrary.simpleMessage("Makeover"),
        "makeoverInPlanning":
            MessageLookupByLibrary.simpleMessage("Makeover In Planning..."),
        "makeoverUpdates":
            MessageLookupByLibrary.simpleMessage("Makeover Updates"),
        "makeover_guys": MessageLookupByLibrary.simpleMessage("Makeover Guys,"),
        "makeover_progress":
            MessageLookupByLibrary.simpleMessage("Makeover Progress"),
        "may_be_later": MessageLookupByLibrary.simpleMessage("Maybe Later"),
        "may_we_reach_out_to_you":
            MessageLookupByLibrary.simpleMessage("May we reach out to you?"),
        "media": MessageLookupByLibrary.simpleMessage("Media"),
        "mobileNumber": MessageLookupByLibrary.simpleMessage("Mobile Number"),
        "mobile_number": MessageLookupByLibrary.simpleMessage("Mobile Number"),
        "never": MessageLookupByLibrary.simpleMessage("Never!"),
        "new_password": MessageLookupByLibrary.simpleMessage("New Password"),
        "next": MessageLookupByLibrary.simpleMessage("Next"),
        "no_thanks": MessageLookupByLibrary.simpleMessage("No Thanks"),
        "not_at_all": MessageLookupByLibrary.simpleMessage("Not at all"),
        "notifications": MessageLookupByLibrary.simpleMessage("Notifications"),
        "one_time_password_reset":
            MessageLookupByLibrary.simpleMessage("One-time Password Reset"),
        "ourTenantManagementServicesTmshelps": MessageLookupByLibrary.simpleMessage(
            "Our Tenant Management Service (TMS) helps you to find and manage tenants. You can also have an overview of tenant payment status, as well as requesting services or maintenance for your property."),
        "partneringWithCozyHomes":
            MessageLookupByLibrary.simpleMessage("Partnering with CozyHomes"),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "password_did_not_match":
            MessageLookupByLibrary.simpleMessage("Password didn\'t match"),
        "password_should_be_at_least_5_character":
            MessageLookupByLibrary.simpleMessage(
                "Password should be at least 5 to 20 characters in length and should include at least one upper character and one number "),
        "password_should_not_empty":
            MessageLookupByLibrary.simpleMessage("Password shouldn\'t empty"),
        "past_notifications":
            MessageLookupByLibrary.simpleMessage("Past Notifications"),
        "photo_gallery": MessageLookupByLibrary.simpleMessage("Photo Gallery"),
        "pleaseEnterYourMobileNumber": MessageLookupByLibrary.simpleMessage(
            "Please enter your mobile number"),
        "please_check_your_email_and_click":
            MessageLookupByLibrary.simpleMessage(
                "Kindly check your inbox for the reset password link "),
        "please_enter_your_username_and_password_to_login":
            MessageLookupByLibrary.simpleMessage(
                "Please enter your username and\npassword to login"),
        "please_enter_your_username_and_password_to_reset":
            MessageLookupByLibrary.simpleMessage(
                "Please enter your username to reset your password"),
        "please_login_with_your_new_password":
            MessageLookupByLibrary.simpleMessage(
                "Please login with your new password."),
        "please_set_your_new_password": MessageLookupByLibrary.simpleMessage(
            "Please set your new password."),
        "profile": MessageLookupByLibrary.simpleMessage("Profile"),
        "profile_picture":
            MessageLookupByLibrary.simpleMessage("Profile Picture"),
        "project": MessageLookupByLibrary.simpleMessage("Project"),
        "project_area": MessageLookupByLibrary.simpleMessage("Project Area"),
        "project_name": MessageLookupByLibrary.simpleMessage("Project Name"),
        "property_details":
            MessageLookupByLibrary.simpleMessage("General Info"),
        "property_enquiry": MessageLookupByLibrary.simpleMessage("Contact Us"),
        "question": MessageLookupByLibrary.simpleMessage("Question"),
        "rate_our_service_now_to_get": MessageLookupByLibrary.simpleMessage(
            "Rate our service now to get a Grab voucher worth RM10!"),
        "rate_us": MessageLookupByLibrary.simpleMessage("Rate Us"),
        "re_enter_password":
            MessageLookupByLibrary.simpleMessage("Re-enter Password"),
        "re_upload": MessageLookupByLibrary.simpleMessage("Reupload"),
        "read_all": MessageLookupByLibrary.simpleMessage("Read All"),
        "receive_a_complimentary": MessageLookupByLibrary.simpleMessage(
            "Receive a complimentary <b>Grab voucher worth RM10!</b>"),
        "receive_timeout_in_connection": MessageLookupByLibrary.simpleMessage(
            "Receive timeout in connection"),
        "receive_timeout_in_send": MessageLookupByLibrary.simpleMessage(
            "Receive timeout in send request"),
        "request_was_cancelled":
            MessageLookupByLibrary.simpleMessage("Request was cancelled"),
        "required": MessageLookupByLibrary.simpleMessage("required"),
        "resendCode": MessageLookupByLibrary.simpleMessage("Resend Code"),
        "resendIn": MessageLookupByLibrary.simpleMessage("Resend In "),
        "reset_password":
            MessageLookupByLibrary.simpleMessage("Reset Password"),
        "reset_password_successful":
            MessageLookupByLibrary.simpleMessage("Reset password successful!"),
        "reset_your_password":
            MessageLookupByLibrary.simpleMessage("Reset Your Password"),
        "save": MessageLookupByLibrary.simpleMessage("Save"),
        "searchCountry": MessageLookupByLibrary.simpleMessage("Search country / region"),
        "sentToYourMobilePhone":
            MessageLookupByLibrary.simpleMessage("sent to your mobile phone"),
        "server_error_please_try_again": MessageLookupByLibrary.simpleMessage(
            "Server Error , Please try again later"),
        "share": MessageLookupByLibrary.simpleMessage("Share"),
        "shared": MessageLookupByLibrary.simpleMessage("Shared!"),
        "sorry_the_app_is_under_maintenance": MessageLookupByLibrary.simpleMessage(
            "Sorry, the app is under maintenance now! Please try again later."),
        "start": MessageLookupByLibrary.simpleMessage("Start"),
        "startDate": MessageLookupByLibrary.simpleMessage("Start Date"),
        "state_your_suggestion_here": MessageLookupByLibrary.simpleMessage(
            "State your suggestion here..."),
        "submit": MessageLookupByLibrary.simpleMessage("submit"),
        "sure": MessageLookupByLibrary.simpleMessage("Sure"),
        "team": MessageLookupByLibrary.simpleMessage("Team"),
        "tenancy": MessageLookupByLibrary.simpleMessage("Tenancy"),
        "terms_of_service":
            MessageLookupByLibrary.simpleMessage("Terms Of Service"),
        "thank_you": MessageLookupByLibrary.simpleMessage("Thank you!"),
        "thank_you_for_your_time":
            MessageLookupByLibrary.simpleMessage("Thank you for your time!"),
        "thank_you_our_team_will_be": MessageLookupByLibrary.simpleMessage(
            "Thank you, our team will be in touch soon."),
        "the_whole_album":
            MessageLookupByLibrary.simpleMessage("The Whole Album"),
        "theme": MessageLookupByLibrary.simpleMessage("Theme"),
        "there_is_no_notifications_yet": MessageLookupByLibrary.simpleMessage(
            "Oh hello! I\'m still waiting for my\nfirst notification"),
        "theres_no_document_yet":
            MessageLookupByLibrary.simpleMessage("There’s no document yet"),
        "this_photo": MessageLookupByLibrary.simpleMessage("This Photo"),
        "this_video": MessageLookupByLibrary.simpleMessage("This Video"),
        "tms": MessageLookupByLibrary.simpleMessage("TMS"),
        "tmsByCozyHomes":
            MessageLookupByLibrary.simpleMessage("TMS by CozyHomes"),
        "today": MessageLookupByLibrary.simpleMessage("Today"),
        "unit_number": MessageLookupByLibrary.simpleMessage("Unit Number"),
        "unit_size": MessageLookupByLibrary.simpleMessage("Unit Size"),
        "update_now": MessageLookupByLibrary.simpleMessage("Update Now"),
        "update_your_app_so_that": MessageLookupByLibrary.simpleMessage(
            "Update your app so that we can bring you a better experience!"),
        "user_name": MessageLookupByLibrary.simpleMessage("Username"),
        "username_should_not_empty":
            MessageLookupByLibrary.simpleMessage("Username shouldn\'t empty"),
        "very_likely": MessageLookupByLibrary.simpleMessage("Very likely"),
        "very_unhappy": MessageLookupByLibrary.simpleMessage("Very unhappy"),
        "viewMakeoverUpdates":
            MessageLookupByLibrary.simpleMessage("View Makeover Updates"),
        "view_all": MessageLookupByLibrary.simpleMessage("View All"),
        "warranty_duration":
            MessageLookupByLibrary.simpleMessage("Warranty Duration"),
        "we_sent_a_link_to":
            MessageLookupByLibrary.simpleMessage("We\'ve sent a link to "),
        "welcome_to_the":
            MessageLookupByLibrary.simpleMessage("Welcome to The "),
        "what_can_we_do_to_improve_your": MessageLookupByLibrary.simpleMessage(
            "What can we do to improve your makeover experience?"),
        "whatsapp_not_installed":
            MessageLookupByLibrary.simpleMessage("Whatsapp not installed"),
        "wrongCodeEntered":
            MessageLookupByLibrary.simpleMessage("Wrong code entered"),
        "you_have_caught_up_with_all":
            MessageLookupByLibrary.simpleMessage("You are up-to-date!"),
        "your_feedback_is_highly_appreciated_and_we":
            MessageLookupByLibrary.simpleMessage(
                "Your feedback is highly appreciated, and we promise to keep serving you better.")
      };
}
